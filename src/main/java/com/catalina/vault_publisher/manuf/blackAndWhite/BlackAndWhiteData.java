package com.catalina.vault_publisher.manuf.blackAndWhite;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;

public class BlackAndWhiteData {
    private String image_nbr;
    private ImageDAM.AssetType asset_type;
    private String template_name;
    private String trailer_message;
    private boolean in_use;

    public BlackAndWhiteData() {
        this.in_use = false;
    }

    public BlackAndWhiteData(String image_nbr, ImageDAM.AssetType asset_type, String template_name, String trailer_message) {
        this.image_nbr = image_nbr;
        this.asset_type = asset_type;
        this.template_name = template_name;
        if (!trailer_message.isEmpty() && !trailer_message.equalsIgnoreCase("<|>")) {
            this.trailer_message = trailer_message;
        }
        this.trailer_message = trailer_message;
        this.in_use = true;
    }

    public String getImage_nbr() {
        return image_nbr;
    }

    public ImageDAM.AssetType getAsset_type() {
        return asset_type;
    }

    public String getTemplate_name() {
        return template_name;
    }

    public String getTrailer_message() {
        return trailer_message;
    }

    public boolean isIn_use() {
        return in_use;
    }
}
