package com.catalina.vault_publisher.manuf.blackAndWhite;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPI;

public class BlackWhiteDataService {

    public BlackWhiteDataService() {
    }

    public BlackAndWhiteData setBwDataManuf(String bw_product_img_nbr,
                                            String bw_trailer_message) {

        BlackAndWhiteData data = new BlackAndWhiteData(bw_product_img_nbr,
                ImageDAM.AssetType.PRODUCT_SHOT,
                TemplateAPI.ManufBwTemplate.DYNAMIC.getName(),
                bw_trailer_message);

        return data;
    }
}
