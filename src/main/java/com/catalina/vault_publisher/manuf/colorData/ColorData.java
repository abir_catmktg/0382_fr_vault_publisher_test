package com.catalina.vault_publisher.manuf.colorData;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;

public class ColorData {
    private String image_nbr;
    private ImageDAM.AssetType asset_type;
    private String template_name;
    private String trailer_message;

    public ColorData(String image_nbr, String template_name, String trailer_message) {
        this.image_nbr = image_nbr;
        this.asset_type = ImageDAM.AssetType.STATIC_IMAGE;
        this.template_name = template_name;
        if (!trailer_message.isEmpty()) {
            this.trailer_message = trailer_message;
        }
    }

    public ColorData(String image_nbr, ImageDAM.AssetType assetType, String template_name, String trailer_message) {
        this.image_nbr = image_nbr;
        this.asset_type = assetType;
        this.template_name = template_name;
        if (!trailer_message.isEmpty()) {
            this.trailer_message = trailer_message;
        }
    }

    public String getImage_nbr() {
        return image_nbr;
    }

    public ImageDAM.AssetType getAsset_type() {
        return asset_type;
    }

    public String getTemplate_name() {
        return template_name;
    }

    public String getTrailer_message() {
        return trailer_message;
    }
}
