package com.catalina.vault_publisher.manuf.colorData;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;

public class ColorDataService {

    public ColorDataService() {
    }

    public ColorData setColorDataManuf(String color_img_nbr, String color_template_name, String color_trailer_message) {
        return new ColorData(color_img_nbr, color_template_name, color_trailer_message);
    }

    public ColorData setColorDataRetail(String color_img_nbr, ImageDAM.AssetType assetType, String color_template_name, String color_trailer_message) {
        return new ColorData(color_img_nbr, assetType, color_template_name, color_trailer_message);
    }
}
