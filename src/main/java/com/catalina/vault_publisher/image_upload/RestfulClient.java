package com.catalina.vault_publisher.image_upload;

import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class RestfulClient {
    RestTemplate restTemplate;
    String getURL;

    public RestfulClient() {
        restTemplate = new RestTemplate();

    }

    public static void main(String[] args) throws Exception {
        RestfulClient restfulClient = new RestfulClient();
        String postUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/upload/custom_creatives";
        String filePath = "C:/Users/fperessi/Desktop/shared_to_vm/0375_CRF_Proxi_Trade_Offers_HighCo/8120190002808262_image_20190509_150137.png";
        File file = new File(filePath);
        String imgName = file.getName();
        JSONObject jsonImage = restfulClient.postEntity(postUrl, imgName, filePath);
        System.out.println(jsonImage.toString());
    }

    public JSONObject postEntity(String postUrl, String name, String imagePath) {
        System.out.println("Begin /POST request!");

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            FileSystemResource data = new FileSystemResource(imagePath);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", data);
            body.add("filename", name);

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);


            ResponseEntity<String> postResponse = restTemplate.postForEntity(postUrl, requestEntity, String.class);
            System.out.println("Response for Post Request: " + postResponse.getBody());
            System.out.println("Response for Post Request: " + postResponse.getStatusCode());

            JSONObject postJSONResponse = new JSONObject(postResponse.getBody());

            getURL = postJSONResponse.getString("url");
            return postJSONResponse;

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }

    /**
     * get entity
     */
    public void getEntity(String imageOutPath) {
        System.out.println("Begin /GET request!");
        //System.out.println("getURL: "+getURL);
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

            HttpEntity<String> entity = new HttpEntity<String>(headers);

            ResponseEntity<byte[]> getResponse = restTemplate.exchange(getURL, HttpMethod.GET, entity, byte[].class, "1");

            if (getResponse.getStatusCode() == HttpStatus.OK) {
                Files.write(Paths.get(imageOutPath), getResponse.getBody());
            }
        } catch (Exception e) {
            System.out.println(e);
        }


    }

}
