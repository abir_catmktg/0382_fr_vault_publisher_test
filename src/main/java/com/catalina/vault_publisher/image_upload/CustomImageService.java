package com.catalina.vault_publisher.image_upload;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import org.json.JSONObject;
import org.springframework.core.io.FileSystemResource;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class CustomImageService {

    String getURL;
    private CRestTemplate cRestTemplate;

    public CustomImageService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    /**
     * get entity
     */


	/*
	public void getEntity(String imageOutPath){
		System.out.println("Begin /GET request!");
		//System.out.println("getURL: "+getURL);
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));

			HttpEntity<String> entity = new HttpEntity<String>(headers);

			ResponseEntity<byte[]> getResponse = restTemplate.exchange(getURL, HttpMethod.GET, entity, byte[].class, "1");

			if (getResponse.getStatusCode() == HttpStatus.OK) {
				Files.write(Paths.get(imageOutPath), getResponse.getBody());
			}
		}catch(Exception e) {
			System.out.println(e);
		}
	}
*/
    public static void main(String[] args) throws Exception {
	/*
		RestfulClient restfulClient = new RestfulClient();
		String postUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/upload/custom_creatives";
		String filePath = "C:/Users/fperessi/Desktop/shared_to_vm/0375_CRF_Proxi_Trade_Offers_HighCo/8120190002808262_image_20190509_150137.png";
		File file = new File(filePath);
		String imgName = file.getName();
		JSONObject jsonImage = restfulClient.postEntity(postUrl, imgName, filePath);
		System.out.println(jsonImage.toString());
	 */

        String base64Image = new String(Files.readAllBytes(Paths.get("C:/Users/fperessi/Desktop/shared_to_vm/bulk_manuf_fr/base64.txt")));

        new CustomImageService(new CRestTemplate()).postImageB64(Config.Mode.PROD, base64Image);
    }

    public JSONObject postEntity(String postUrl, String name, String imagePath) {
        System.out.println("Begin /POST request!");

        try {
//			HttpHeaders headers = new HttpHeaders();
//			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            FileSystemResource data = new FileSystemResource(imagePath);

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("file", data);
            body.add("filename", name); // TODO remove from method, not usefull

//			HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
//			ResponseEntity<String> postResponse = restTemplate.postForEntity(postUrl, requestEntity, String.class);

            String response = cRestTemplate.executePostRequestWithFormDataHeader(postUrl, body, String.class);

            JSONObject postJSONResponse = new JSONObject(response);

            getURL = postJSONResponse.getString("url");
            return postJSONResponse;

        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }

    public Result postImageB64(Config.Mode mode, String imageB64) {
        String url = mode.dmpUrl + "/creatives/images/upload/custom_creatives";

        byte[] fileContent = Base64.getDecoder().decode(imageB64);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileContent);

        try {
            JsonObject response = cRestTemplate.executePostRequestWithFormDataHeader(url, body, JsonObject.class);
            String urlImg = response.get("url").getAsString();
            return new Result<>(urlImg);
        } catch (HttpResponseStatusException e) {
            System.err.println(e.getMessage());
            return new Result(9, "postImageB64 error : " + e.httpStatus + " " + e.getMessage());
        } catch (JsonParseException e) {
            return new Result(10, "postImageB64 : error parsing the response's json " + e.getMessage());
        }


    }
}
