package com.catalina.vault_publisher.data.estimates;

import java.util.List;

public class Estimates {
    private String channel;
    private String response_rate;
    private String delivery_rate;
    private String audience_size;
    private String analytic_model;
    private String estimated_avg_redemption_rate;
    private String estimated_total_redemption_count;
    private String estimated_total_redemption_cost;
    private List<String> comments;
    private String estimated_avg_impression_cost;
    private String estimated_total_impression_count;
    private String estimated_total_impression_cost;

    public Estimates() {
    }

    public Estimates(String channel, String response_rate, String delivery_rate, String audience_size, String analytic_model, String estimated_avg_redemption_rate, String estimated_total_redemption_count, String estimated_total_redemption_cost, List<String> comments, String estimated_avg_impression_cost, String estimated_total_impression_count, String estimated_total_impression_cost) {
        this.channel = channel;
        this.response_rate = response_rate;
        this.delivery_rate = delivery_rate;
        this.audience_size = audience_size;
        this.analytic_model = analytic_model;
        this.estimated_avg_redemption_rate = estimated_avg_redemption_rate;
        this.estimated_total_redemption_count = estimated_total_redemption_count;
        this.estimated_total_redemption_cost = estimated_total_redemption_cost;
        this.comments = comments;
        this.estimated_avg_impression_cost = estimated_avg_impression_cost;
        this.estimated_total_impression_count = estimated_total_impression_count;
        this.estimated_total_impression_cost = estimated_total_impression_cost;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getResponse_rate() {
        return response_rate;
    }

    public void setResponse_rate(String response_rate) {
        this.response_rate = response_rate;
    }

    public String getDelivery_rate() {
        return delivery_rate;
    }

    public void setDelivery_rate(String delivery_rate) {
        this.delivery_rate = delivery_rate;
    }

    public String getAudience_size() {
        return audience_size;
    }

    public void setAudience_size(String audience_size) {
        this.audience_size = audience_size;
    }

    public String getAnalytic_model() {
        return analytic_model;
    }

    public void setAnalytic_model(String analytic_model) {
        this.analytic_model = analytic_model;
    }

    public String getEstimated_avg_redemption_rate() {
        return estimated_avg_redemption_rate;
    }

    public void setEstimated_avg_redemption_rate(String estimated_avg_redemption_rate) {
        this.estimated_avg_redemption_rate = estimated_avg_redemption_rate;
    }

    public String getEstimated_total_redemption_count() {
        return estimated_total_redemption_count;
    }

    public void setEstimated_total_redemption_count(String estimated_total_redemption_count) {
        this.estimated_total_redemption_count = estimated_total_redemption_count;
    }

    public String getEstimated_total_redemption_cost() {
        return estimated_total_redemption_cost;
    }

    public void setEstimated_total_redemption_cost(String estimated_total_redemption_cost) {
        this.estimated_total_redemption_cost = estimated_total_redemption_cost;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public String getEstimated_avg_impression_cost() {
        return estimated_avg_impression_cost;
    }

    public void setEstimated_avg_impression_cost(String estimated_avg_impression_cost) {
        this.estimated_avg_impression_cost = estimated_avg_impression_cost;
    }

    public String getEstimated_total_impression_count() {
        return estimated_total_impression_count;
    }

    public void setEstimated_total_impression_count(String estimated_total_impression_count) {
        this.estimated_total_impression_count = estimated_total_impression_count;
    }

    public String getEstimated_total_impression_cost() {
        return estimated_total_impression_cost;
    }

    public void setEstimated_total_impression_cost(String estimated_total_impression_cost) {
        this.estimated_total_impression_cost = estimated_total_impression_cost;
    }
}
