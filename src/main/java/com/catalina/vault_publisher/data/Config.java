package com.catalina.vault_publisher.data;

import com.catalina.vault_publisher.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

public class Config {

    public static final String URL_SQA = "https://sqa.dashboard.personalization.catalinamarketing.com"; ///campaigns/57be82eb-736b-4112-b6fb-a2d831bb3529";
    public static final String URL_PROD = "https://production.dashboard.personalization.catalinamarketing.com"; ///campaigns/611b580e-df8a-45be-ba70-19804cd8d5c9";
    public static final String URL_API_DMP_SQA = URL_SQA + "/route-eu/dmp/rest";
    public static final String URL_API_DMP_PROD = URL_PROD + "/route-eu/dmp/rest";//promotions/FRA-BLIP-BL_8247_8152
    public static final String URL_API_PROMOTION_QUERIES_SQA = "http://sqa.search.westeurope.api.catalina.com/solr/promotions";
    public static final String URL_API_PROMOTION_QUERIES_PROD = "http://production.search.westeurope.api.catalina.com/solr/promotions";
    public static final String LMC_USER = "\"suricat\"";
    public static final String[] MANUF_STORES_EXCLUSION = {"110404", "110408", "110153", "110511", "110555", "110562", "110950"};
    public static final List<String> MANUF_NETWORKS_INCLUSION = Arrays.asList("FRA-0020", "FRA-0023", "FRA-0004", "FRA-0028", "FRA-0006");
    public static JsonParser parser = new JsonParser();
    public static Gson gson = new GsonBuilder().disableHtmlEscaping().excludeFieldsWithModifiers(Modifier.FINAL).setPrettyPrinting().create();

    public static String toJsString(Object object) {
        try {
            String strObject = gson.toJson(object);
            return strObject;

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    public enum Mode {
        SQA(1, URL_SQA, URL_API_DMP_SQA, URL_API_PROMOTION_QUERIES_SQA, "sqa"),
        PROD(0, URL_PROD, URL_API_DMP_PROD, URL_API_PROMOTION_QUERIES_PROD, "production");

        public final String urlDashboard, dmpUrl, promotionQueriesUrl, modeStr;
        private final int value;

        Mode(int value, String urlDashboard, String dmpUrl, String promotionQueriesUrl, String modeStr) {
            this.value = value;
            this.urlDashboard = urlDashboard;
            this.dmpUrl = dmpUrl;
            this.promotionQueriesUrl = promotionQueriesUrl;
            this.modeStr = modeStr;
        }

        public int getValue() {
            return value;
        }
    }

    public enum Segment {
        MANUF(Constants.MANUF_BUSINESS_SEGMENT, Constants.MANUF_TYPE, "MANUF"),
        RETAIL(Constants.RETAIL_BUSINESS_SEGMENT, Constants.RETAIL_TYPE, "RETAIL");

        public final String businessSegment, type, segmentStr;

        Segment(String businessSegment, String type, String segmentStr) {
            this.businessSegment = businessSegment;
            this.type = type;
            this.segmentStr = segmentStr;
        }
    }

    public static enum AdStatus {
        PUBLISHED(1), PAUSED(0);

        private final int value;

        AdStatus(final int newValue) {
            value = newValue;
        }

        public static AdStatus getStatus(String status) {
            if (status == null) return null;

            switch (status) {
                case "published":
                case "PUBLISHED":
                    return PUBLISHED;
                case "paused":
                case "PAUSED":
                    return PAUSED;
                default:
                    return null;
            }

        }

        public int getValue() {
            return value;
        }
    }


//    public static Config getInstance() {
//        if (instance == null){
//            instance = new Config();
//        }
//        return instance;
//    }
}
