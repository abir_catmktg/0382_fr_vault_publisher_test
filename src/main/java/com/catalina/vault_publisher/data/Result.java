package com.catalina.vault_publisher.data;

public class Result<T> {

    int m_status = -1;
    String m_message = null;
    String m_data = null;
    T m_object = null;

    public Result() {
        m_status = 0;
    }

    public Result(String data) {
        m_status = 0; //OK
        m_data = data;
    }

    public Result(T object) {
        m_status = 0; //OK
        m_object = object;
    }

    public Result(int status, String message) {
        m_status = status;
        m_message = message;
    }

    public Result(int status, String message, T object) {
        m_status = status;
        m_message = message;
        m_object = object;
    }

    public int getStatus() {
        return m_status;
    }

    public String getMessage() {
        return m_message;
    }

    public String getData() {
        return m_data;
    }

    public T getObject() {
        return m_object;
    }

    public String toString() {
        return m_status + " / " + m_message + " / " + m_data;
    }

    public void setM_message(String message) {
        this.m_message = message;
    }

    public void setM_status(int m_status) {
        this.m_status = m_status;
    }

    public void setM_data(String m_data) {
        this.m_data = m_data;
    }


}

