package com.catalina.vault_publisher.data;

import com.catalina.vault_publisher.utils.DateUtils;
import com.google.gson.JsonArray;

public class Meta {
    public String date_modified;
    public String description;
    public JsonArray examples = new JsonArray();
    public String group;
    public String id;
    public String multiline;
    public String name;
    public String regex;
    public String scope;
    public String type;

    public Meta() {
    }

    public Meta(String description,
                String examples,
                String group,
                String id,
                String multiline,
                String name,
                String regex,
                String scope,
                String type) {
        this.date_modified = DateUtils.getCurrentDateMilliSecondPrecision();
        this.description = description;
        this.examples.add(examples);
        this.group = group;
        this.id = id;
        this.multiline = multiline;
        this.name = name;
        this.regex = regex;
        this.scope = scope;
        this.type = type;
    }
}
