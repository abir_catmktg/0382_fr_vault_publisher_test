package com.catalina.vault_publisher.data.list.vault;

import com.catalina.vault_publisher.utils.CRestTemplate;

public class TouchpointVaultListService {

    private CRestTemplate cRestTemplate;

    public TouchpointVaultListService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public TouchpointVaultList createTouchpointVaultList(MinimalVaultList list) {
        return new TouchpointVaultList(list.getCountry_code(),
                list.getId(),
                list.getName(),
                list.getName(),
                new VaultListService(cRestTemplate).createVaultList(list));
    }

}
