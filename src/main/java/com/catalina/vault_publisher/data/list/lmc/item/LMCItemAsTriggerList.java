package com.catalina.vault_publisher.data.list.lmc.item;

import com.catalina.vault_publisher.data.Catalog;
import com.catalina.vault_publisher.data.list.lmc.group.LMCGroup;

import java.util.List;


public class LMCItemAsTriggerList extends LMCItem {
    private String attribute;
    private String download_url;
    private String text;

    public LMCItemAsTriggerList() {
    }

    public LMCItemAsTriggerList(String _,
                                String id,
                                String name,
                                String type,
                                Integer count,
                                String external_id,
                                String status,
                                List<String> rows,
                                String preview_url,
                                String root_url,
                                Catalog catalog,
                                List<LMCGroup> groups,
                                String attribute,
                                String download_url,
                                String text) {
        super(_, id, name, type, count, external_id, status, rows, preview_url, root_url, catalog, groups);
        this.attribute = attribute;
        this.download_url = download_url;
        this.text = text;
    }

    public String getAttribute() {
        return attribute;
    }

    public String getDownload_url() {
        return download_url;
    }

    public String getText() {
        return text;
    }
}
