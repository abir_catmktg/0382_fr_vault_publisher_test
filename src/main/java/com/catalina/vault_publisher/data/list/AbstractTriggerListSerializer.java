package com.catalina.vault_publisher.data.list;


import com.catalina.vault_publisher.data.list.lmc.group.LMCGroupAsTriggerList;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerList;
import com.catalina.vault_publisher.data.list.manual.ManualList;
import com.catalina.vault_publisher.data.list.vault.VaultTriggerList;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;

public class AbstractTriggerListSerializer implements JsonSerializer<AbstractTriggerList>, JsonDeserializer<AbstractTriggerList> {

    private static final Logger logger = LogManager.getLogger(AbstractTriggerListSerializer.class);
    private Gson gson = new Gson();

    @Override
    public JsonElement serialize(AbstractTriggerList list, Type type, JsonSerializationContext context) {
        return gson.toJsonTree(list);
    }

    @Override
    public AbstractTriggerList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String header = json.getAsJsonObject().get("_").getAsString();
        if (header.equals("list")) {
            return gson.fromJson(json, VaultTriggerList.class);
        } else if (header.equals("lmc")) {
            return gson.fromJson(json, LMCItemAsTriggerList.class);
        } else if (header.equals("lmc-group")) {
            return gson.fromJson(json, LMCGroupAsTriggerList.class);
        } else if (header.equals("manual")) {
            return gson.fromJson(json, ManualList.class);
        }
        logger.warn(String.format("Could not deserialize this abstract_trigger_list's type: %s", header));
        return null;
    }
}
