package com.catalina.vault_publisher.data.list.vault;


import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.utils.Constants;

public class TouchpointVaultList extends TouchPoint {
    private String _;
    private VaultTriggerList list;

    public TouchpointVaultList() {
    }

    public TouchpointVaultList(String country_code, String id, String name, String text, VaultTriggerList list) {
        super(country_code, id, name, text, Constants.LIST_VAULT_HEADER);
        this._ = Constants.LIST_VAULT_HEADER;
        this.list = list;
    }

    public String get_() {
        return _;
    }

    public VaultTriggerList getList() {
        return list;
    }
}
