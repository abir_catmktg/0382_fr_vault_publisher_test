package com.catalina.vault_publisher.data.list.lmc.group;

import com.catalina.vault_publisher.data.list.lmc.item.LMCItem;

import java.util.Arrays;

public class LMCGroupAsTriggerListService {

    private static final String DOWNLOAD_URL_PATTERN_FOR_GROUP = "https://listmanager.catalinamarketing.com/export/csv?id=%s:%s&fields=upc_cd";
    private static final String NAME_PATTERN_FOR_GROUP = "%s ▶ Group #%02d: %s (Total Rows: %s)";
    private static final String PREVIEW_URL_PATTERN_FOR_GROUPS = "https://listmanager.catalinamarketing.com/#/list/%s:%s";

    public LMCGroupAsTriggerListService() {
    }

    public LMCGroupAsTriggerList createTriggerListForGroupsFromLmcItem(LMCItem lmcItem, LMCGroup lmcGroup, int ActivationTriggerGroupIndex) {

        return new LMCGroupAsTriggerList(String.format("%s-group", lmcItem.getExternal_id()),
                lmcItem.getId() + ":" + lmcGroup.getId(),
                String.format(NAME_PATTERN_FOR_GROUP, lmcItem.getName(), ActivationTriggerGroupIndex + 1, lmcGroup.getName(), lmcGroup.getCount()),
                lmcItem.getType(),
                lmcGroup.getCount(),
                String.format("%s-group", lmcItem.getExternal_id()),
                lmcItem.getStatus(),
                lmcItem.getRows(),
                String.format(PREVIEW_URL_PATTERN_FOR_GROUPS, lmcItem.getId(), lmcGroup.getId()),
                lmcItem.getRoot_url(),
                lmcItem.getCatalog(),
                Arrays.asList(lmcGroup),
                "upc_cd",
                String.format(DOWNLOAD_URL_PATTERN_FOR_GROUP, lmcItem.getId(), lmcGroup.getId()),
                String.format(NAME_PATTERN_FOR_GROUP, lmcItem.getName(), ActivationTriggerGroupIndex + 1, lmcGroup.getName(), lmcGroup.getCount()),
                lmcItem.getId(),
                lmcItem.getName(),
                lmcGroup.getId(),
                lmcGroup.getName());
    }
}
