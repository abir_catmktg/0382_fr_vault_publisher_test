package com.catalina.vault_publisher.data.list.vault;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;

public class VaultListService {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(VaultListService.class);
    private CRestTemplate cRestTemplate;
    private MinimalVaultListService minimalVaultListService;
    private TouchpointVaultListService touchpointVaultListService;

    public VaultListService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.minimalVaultListService = new MinimalVaultListService(cRestTemplate);
        this.touchpointVaultListService = new TouchpointVaultListService(cRestTemplate);
    }

    public VaultTriggerList createVaultList(MinimalVaultList miniList) {
        if (miniList.getType().equalsIgnoreCase(VaultTriggerList.ListType.PRODUCT.getValue()))
            return new VaultTriggerList(miniList.getId(), miniList.getName(), miniList.getType());

        else
            return new VaultTriggerList(miniList.getId(),
                    miniList.getName(),
                    String.format("%s - %s", miniList.getName(), miniList.getId()),
                    miniList.getType());
    }

    public Result getVaultList(Config.Mode mode, String listName, VaultTriggerList.ListType type) throws HttpResponseStatusException {

        MinimalVaultList list = minimalVaultListService.getMinimalVaultList(mode, listName, type);

        list.setType(type.getValue());
        if (type == VaultTriggerList.ListType.TOUCHPOINT) {
            TouchpointVaultList touchpointList = touchpointVaultListService.createTouchpointVaultList(list);
            return new Result(touchpointList);
        }
        if (type == VaultTriggerList.ListType.PRODUCT) {
            VaultTriggerList vaultList = createVaultList(list);
            return new Result(vaultList);

        }
        throw new RuntimeException(String.format("Unexpected list type: %s", type.getValue()));
    }
}