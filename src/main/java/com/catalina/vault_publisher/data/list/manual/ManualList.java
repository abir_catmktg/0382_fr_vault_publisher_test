package com.catalina.vault_publisher.data.list.manual;

import com.catalina.vault_publisher.data.list.AbstractTriggerList;

import java.util.List;

public class ManualList extends AbstractTriggerList {
    private List<String> rows;

    public ManualList() {
    }

    public ManualList(String _, List<String> rows) {
        super(_);
        this.rows = rows;
    }

    public List<String> getRows() {
        return rows;
    }
}
