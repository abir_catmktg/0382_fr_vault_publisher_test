package com.catalina.vault_publisher.data.list;

import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(AbstractTriggerListSerializer.class)
public abstract class AbstractTriggerList {
    private String _;

    public AbstractTriggerList() {
    }

    public AbstractTriggerList(String _) {
        this._ = _;
    }

    public String get_() {
        return _;
    }

    public void set_(String _) {
        this._ = _;
    }
}
