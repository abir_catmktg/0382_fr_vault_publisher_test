package com.catalina.vault_publisher.data.list.vault;


import com.catalina.vault_publisher.data.list.AbstractTriggerList;

public class VaultTriggerList extends AbstractTriggerList {

    private String id;
    private String name;
    private String text;
    private String type;

    public VaultTriggerList() {
    }

    public VaultTriggerList(String id, String name, String type) {
        super("list");
        this.id = id;
        this.name = this.text = name;
        this.type = type;
    }

    public VaultTriggerList(String id, String name, String text, String type) {
        super("list");
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
    }

    public enum ListType {
        TOUCHPOINT("touchpoint"), PRODUCT("Product");

        private final String value;

        ListType(final String newValue) {
            value = newValue;
        }

        public String getValue() {
            return value;
        }

    }
}
