package com.catalina.vault_publisher.data.list.vault;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;

public class MinimalVaultListService {

    private static final Logger logger = LogManager.getLogger(MinimalVaultListService.class);
    static HashMap<String, MinimalVaultList> VAULT_LIST_MAP = null;
    private CRestTemplate cRestTemplate;

    public MinimalVaultListService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        MinimalVaultList tmpList1;
        MinimalVaultListService service = new MinimalVaultListService(new CRestTemplate());

        /********************************* TOUCHPOINT LIST *********************************/
        tmpList1 = service.getMinimalVaultList(Config.Mode.SQA, "Carrefour Proxi Store List", VaultTriggerList.ListType.TOUCHPOINT);
        /***********************************************************************************/


        /********************************* PRODUCT LIST ************************************/
        tmpList1 = service.getMinimalVaultList(Config.Mode.PROD, "VUPC_BSC_for_coupon_exception-1", VaultTriggerList.ListType.PRODUCT);
        /***********************************************************************************/
    }

    public void initMinimalVaultList() {
        VAULT_LIST_MAP = new HashMap<String, MinimalVaultList>();
    }

    public MinimalVaultList getMinimalVaultList(Config.Mode m_mode, String m_list_name, VaultTriggerList.ListType m_type) throws HttpResponseStatusException {
        if (VAULT_LIST_MAP == null) {
            initMinimalVaultList();
        }

        System.out.println("START getMinimalVaultList");
        if (VAULT_LIST_MAP.containsKey(m_list_name)) {
            System.out.println("END getMinimalVaultList");
            return VAULT_LIST_MAP.get(m_list_name);
        }
        MinimalVaultList minimalVaultList = getMinimalVaultListNoCache(m_mode, m_list_name, m_type);
        return minimalVaultList;

    }

    public MinimalVaultList getMinimalVaultListNoCache(Config.Mode m_mode, String listName, VaultTriggerList.ListType m_type) throws HttpResponseStatusException {
        String url = String.join("", m_mode.urlDashboard, "/lists?wc={wc}&fq={fq}&fl={fl}");
        String wcParam = String.join("", "{\"name\":\"", listName, "*\"}");
        String fqParam = String.join("", "{\"type\":\"", m_type.getValue(), "\"}");
        String flParam = "id,name,country_code";

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n wc: %s \\n fq: %s \\n fl: %s", wcParam, fqParam, flParam));
        List<MinimalVaultList> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, MinimalVaultList.class, wcParam, fqParam, flParam);

        if (docs.size() != 1) {
            throw new RuntimeException(String.format(String.format("%s list(s) found for the type: %s and name: %s",
                    docs.size(),
                    m_type.getValue(),
                    listName)));
        }

        MinimalVaultList minimalVaultList = docs.get(0);
        VAULT_LIST_MAP.put(listName, minimalVaultList);
        return minimalVaultList;

    }
}
