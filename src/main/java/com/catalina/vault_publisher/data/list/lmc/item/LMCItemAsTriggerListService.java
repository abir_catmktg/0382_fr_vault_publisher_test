package com.catalina.vault_publisher.data.list.lmc.item;

public class LMCItemAsTriggerListService {

    private static final String DOWNLOAD_URL_PATTERN_FOR_ITEM = "https://listmanager.catalinamarketing.com/export/csv?id=%s&fields=upc_cd";
    private static final String NAME_PATTERN_FOR_ITEM = "%s - %s (%s rows)";

    public LMCItemAsTriggerListService() {
    }


    public LMCItemAsTriggerList createLMCItemAsTriggerList(LMCItem lmcItem) {
        return new LMCItemAsTriggerList(lmcItem.getExternal_id(),
                lmcItem.getId(),
                String.format(NAME_PATTERN_FOR_ITEM, lmcItem.getName(), lmcItem.getCatalog().getId(), lmcItem.getCount()),
                lmcItem.getType(),
                lmcItem.getCount(),
                lmcItem.getExternal_id(),
                lmcItem.getStatus(),
                lmcItem.getRows(),
                lmcItem.getPreview_url(),
                lmcItem.getRoot_url(),
                lmcItem.getCatalog(),
                lmcItem.getGroups(),
                "upc_cd",
                String.format(DOWNLOAD_URL_PATTERN_FOR_ITEM, lmcItem.getId()),
                String.format(NAME_PATTERN_FOR_ITEM, lmcItem.getName(), lmcItem.getCatalog().getId(), lmcItem.getCount()));
    }


}
