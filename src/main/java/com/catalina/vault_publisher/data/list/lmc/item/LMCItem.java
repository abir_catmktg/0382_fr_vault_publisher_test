package com.catalina.vault_publisher.data.list.lmc.item;


import com.catalina.vault_publisher.data.Catalog;
import com.catalina.vault_publisher.data.list.AbstractTriggerList;
import com.catalina.vault_publisher.data.list.lmc.group.LMCGroup;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class LMCItem extends AbstractTriggerList {

    public static final TypeToken<List<LMCItem>> LMC_ITEM_LIST_TYPE = new TypeToken<List<LMCItem>>() {
    };

    private String id;
    private String name;
    private String type;
    private Integer count;
    private String external_id;
    private String status;
    private List<String> rows;
    private String preview_url;
    private String root_url;
    private Catalog catalog;
    private List<LMCGroup> groups;

    public LMCItem() {
    }


    public LMCItem(String _,
                   String id,
                   String name,
                   String type,
                   Integer count,
                   String external_id,
                   String status,
                   List<String> rows,
                   String preview_url,
                   String root_url,
                   Catalog catalog,
                   List<LMCGroup> groups) {
        super(_);
        this.id = id;
        this.name = name;
        this.type = type;
        this.count = count;
        this.external_id = external_id;
        this.status = status;
        this.rows = rows;
        this.preview_url = preview_url;
        this.root_url = root_url;
        this.catalog = catalog;
        this.groups = groups;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getRows() {
        return rows;
    }

    public void setRows(List<String> rows) {
        this.rows = rows;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public void setPreview_url(String preview_url) {
        this.preview_url = preview_url;
    }

    public String getRoot_url() {
        return root_url;
    }

    public void setRoot_url(String root_url) {
        this.root_url = root_url;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public List<LMCGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<LMCGroup> groups) {
        this.groups = groups;
    }
}
