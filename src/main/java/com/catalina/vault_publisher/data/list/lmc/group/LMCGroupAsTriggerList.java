package com.catalina.vault_publisher.data.list.lmc.group;

import com.catalina.vault_publisher.data.Catalog;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerList;

import java.util.List;

public class LMCGroupAsTriggerList extends LMCItemAsTriggerList {
	private String list_id;
	private String list_name;
	private String group_id;
	private String group_name;

	public LMCGroupAsTriggerList() {
	}

	public LMCGroupAsTriggerList(String _,
	                             String id,
	                             String name,
	                             String type,
	                             Integer count,
	                             String external_id,
	                             String status,
	                             List<String> rows,
	                             String preview_url,
	                             String root_url,
	                             Catalog catalog,
	                             List<LMCGroup> groups,
	                             String attribute,
	                             String download_url,
	                             String text,
	                             String list_id,
	                             String list_name,
	                             String group_id,
	                             String group_name) {
		super(_, id, name, type, count, external_id, status, rows, preview_url, root_url, catalog, groups, attribute, download_url, text);
		this.list_id = list_id;
		this.list_name = list_name;
		this.group_id = group_id;
		this.group_name = group_name;
	}

	public String getList_id() {
		return list_id;
	}

	public String getList_name() {
		return list_name;
	}

	public String getGroup_id() {
		return group_id;
	}

	public String getGroup_name() {
		return group_name;
	}
}
