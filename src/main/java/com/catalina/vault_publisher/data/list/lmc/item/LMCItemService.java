package com.catalina.vault_publisher.data.list.lmc.item;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LMCItemService {

    private static final Logger logger = LogManager.getLogger(LMCItemService.class);
    private static Map<String, List<LMCItem>> LMCLISTS_MAP = new HashMap<>();
    private CRestTemplate cRestTemplate;

    public LMCItemService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void resetCache() {
        LMCLISTS_MAP.clear();
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        new LMCItemService(new CRestTemplate()).getListLMCNoCache(Config.Mode.PROD, "VUPC Colgate Gamme blancheur PREMIUM 61741");
    }

    public List<LMCItem> getListLMC(Config.Mode mode, String lmcListName) throws HttpResponseStatusException {
        if (LMCLISTS_MAP == null) LMCLISTS_MAP = new HashMap<>();

        if (LMCLISTS_MAP.containsKey(lmcListName)) return LMCLISTS_MAP.get(lmcListName);

        return getListLMCNoCache(mode, lmcListName);

    }

    private List<LMCItem> getListLMCNoCache(Config.Mode mMode, String search) throws HttpResponseStatusException {
        String url = mMode.dmpUrl + "/lists/search";

        String searchParsed = search.replaceAll(" ", "*");
        String querysBody = "{\"user\":" + Config.LMC_USER + ",\"list_type\":\"product\",\"filters\":[\"name:" + searchParsed + "*\"],\"rows\":20,\"start\":0}";

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("body:\\n %s", querysBody));
        List<LMCItem> lmcItems = cRestTemplate.executePostRequestWithJsonHeader(url, querysBody, LMCItem.LMC_ITEM_LIST_TYPE);
        if (lmcItems.isEmpty())
            throw new RuntimeException(String.format("0 LMC list shared with the user: %s matching the name: %s",
                    Config.LMC_USER,
                    search));
        logger.debug(String.format("%s LMCItem(s) found for listname: %s", lmcItems.size(), search));
        LMCLISTS_MAP.put(search, lmcItems);
        return lmcItems;
    }

    public LMCItem getMatchingLMCitem(List<LMCItem> lmcItemList, String listname) {
        return lmcItemList.stream()
                .filter(x -> x.getName().equalsIgnoreCase(listname))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(
                        String.format("0 LMC list shared with the user: %s matching the name: %s",
                                Config.LMC_USER,
                                listname)));
    }
}
