package com.catalina.vault_publisher.data;

public class Item {
    private String id;
    private String name;
    private String text;

    public Item() {
    }

    public Item(String id, String name) {
        this.id = id;
        this.name = this.text = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
