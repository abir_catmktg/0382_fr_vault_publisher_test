package com.catalina.vault_publisher.data.clearingHouse;

import com.catalina.vault_publisher.data.clearingHouse.ClearingHouse.ClearingHouseEnum;

public class ClearingHouseService {


    public ClearingHouse createClearingHouseManuf(String clearingHouse) {
        ClearingHouseEnum clearingHouseEnum = ClearingHouseEnum.fromValues(clearingHouse);
        return createClearingHouse(clearingHouseEnum);
    }

    public ClearingHouse createClearingHouse(ClearingHouseEnum clearingHouseEnum) {
        ClearingHouse clearing = new ClearingHouse(clearingHouseEnum.getName(), clearingHouseEnum.getId(), "store");
        return clearing;
    }

}
