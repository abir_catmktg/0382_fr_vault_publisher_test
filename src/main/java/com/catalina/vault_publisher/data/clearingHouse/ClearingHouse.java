package com.catalina.vault_publisher.data.clearingHouse;

import java.util.Arrays;

public class ClearingHouse {

    private static final String CLEARING_HOUSE_ADDRESS_SCANCOUPON = "TRAITEMENT SCANCOUPON";
    private static final String CLEARING_HOUSE_ID_SCANCOUPON = "FRA-3D-2";

    private static final String CLEARING_HOUSE_ADDRESS_SOGEC = "TRAITEMENT SOGEC";
    private static final String CLEARING_HOUSE_ID_SOGEC = "FRA-3D-1";

    private String address;
    private String id;
    private String name;
    private String text;
    private String type;

    public ClearingHouse() {
    }

    public ClearingHouse(String address, String id, String type) {
        this.address = address;
        this.id = id;
        this.name = this.text = " - " + address;
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    public enum ClearingHouseEnum {
        SCANCOUPON(CLEARING_HOUSE_ADDRESS_SCANCOUPON, CLEARING_HOUSE_ID_SCANCOUPON, "SCANCOUPON"),
        SOGEC(CLEARING_HOUSE_ADDRESS_SOGEC, CLEARING_HOUSE_ID_SOGEC, "SOGEC");

        private final String id, name, clearingStr;

        ClearingHouseEnum(String name, String id, String clearingStr) {
            this.id = id;
            this.name = name;
            this.clearingStr = clearingStr;
        }

        public static ClearingHouseEnum fromValues(String clearingHouse) {
            return Arrays.stream(ClearingHouseEnum.values())
                    .filter(e -> e.getClearingStr().equals(clearingHouse))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Clearing House %s.", clearingHouse)));
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getClearingStr() {
            return clearingStr;
        }

        @Override
        public String toString() {
            return clearingStr;
        }
    }
}
