package com.catalina.vault_publisher.data.composite;

public class Composite {
    private String id;
    private String url;
    private String type;
    private String mime_type;
    private String system_id;
    private String ref_id;

    public Composite() {
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getType() {
        return type;
    }

    public String getMime_type() {
        return mime_type;
    }

    public String getSystem_id() {
        return system_id;
    }

    public String getRef_id() {
        return ref_id;
    }
}
