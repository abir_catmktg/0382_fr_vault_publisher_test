package com.catalina.vault_publisher.data.distribution;

public class AggregateLatestDistribution {
    private int media_deliveries;
    private String media_deliveries_last_seen_date;
    private int impression;
    private String impression_last_seen_date;
    private int clips;
    private String clips_last_seen_date;

    public AggregateLatestDistribution() {
    }

    public AggregateLatestDistribution(int media_deliveries, String media_deliveries_last_seen_date, int impression, String impression_last_seen_date, int clips, String clips_last_seen_date) {
        this.media_deliveries = media_deliveries;
        this.media_deliveries_last_seen_date = media_deliveries_last_seen_date;
        this.impression = impression;
        this.impression_last_seen_date = impression_last_seen_date;
        this.clips = clips;
        this.clips_last_seen_date = clips_last_seen_date;
    }

    public int getMedia_deliveries() {
        return media_deliveries;
    }

    public String getMedia_deliveries_last_seen_date() {
        return media_deliveries_last_seen_date;
    }

    public int getImpression() {
        return impression;
    }

    public String getImpression_last_seen_date() {
        return impression_last_seen_date;
    }

    public int getClips() {
        return clips;
    }

    public String getClips_last_seen_date() {
        return clips_last_seen_date;
    }
}
