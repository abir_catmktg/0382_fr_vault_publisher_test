package com.catalina.vault_publisher.data;

import java.util.Objects;

public class AdGroupLimit {
    private String period;
    private String type;
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdGroupLimit adGroupLimit = (AdGroupLimit) o;
        return Objects.equals(period, adGroupLimit.period) &&
                Objects.equals(type, adGroupLimit.type) &&
                Objects.equals(value, adGroupLimit.value);
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
