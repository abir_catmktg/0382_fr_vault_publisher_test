package com.catalina.vault_publisher.data;

import java.util.Arrays;

public class CountryService {

    public CountryService() {
    }

    public Country.CountryEnum getCountryFromString(String countryName) {
        return Arrays.asList(Country.CountryEnum.values())
                .stream()
                .filter(e -> e.toString().equals(countryName))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported country name : %s.", countryName)));

    }
}
