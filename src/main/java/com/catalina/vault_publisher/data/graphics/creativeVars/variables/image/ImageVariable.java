package com.catalina.vault_publisher.data.graphics.creativeVars.variables.image;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;

public class ImageVariable extends AbstractVariable {
    private String external_id;
    private String height;
    private String id;
    private String mime_type;
    private String system_id;
    private String width;

    public ImageVariable() {
    }

    public ImageVariable(String name,
                         boolean required,
                         String scope,
                         String value,
                         String type,
                         String external_id,
                         String height,
                         String id,
                         String mime_type,
                         String system_id,
                         String width) {
        super(name, required, scope, value, type);
        this.external_id = external_id;
        this.height = height;
        this.id = id;
        this.mime_type = mime_type;
        this.system_id = system_id;
        this.width = width;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public String getSystem_id() {
        return system_id;
    }

    public void setSystem_id(String system_id) {
        this.system_id = system_id;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
