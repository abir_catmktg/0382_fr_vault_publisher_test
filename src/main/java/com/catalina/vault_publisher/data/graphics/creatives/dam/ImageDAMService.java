package com.catalina.vault_publisher.data.graphics.creatives.dam;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.catalina.vault_publisher.data.Config.toJsString;

public class ImageDAMService {

    protected static final String DAM_SYSTEM_ID = "DAM";
    private static final Logger logger = LogManager.getLogger(ImageDAMService.class);
    private CRestTemplate cRestTemplate;
    private Map<String, ImageDAM> damImgMap = new HashMap<>();

    public ImageDAMService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) throws Exception {

//		######################## HEINEKEN STATIC_IMAGE COLOR ########################
        ImageDAM tmpDAM2;
        CRestTemplate restTemplate = new CRestTemplate();
        ImageDAMService service = new ImageDAMService(restTemplate);
        service.resetCache();
        tmpDAM2 = service.createDAMImage(Config.Mode.PROD,
                CountryEnum.FRANCE,
                "233492",
                ImageDAM.PrinterLevel.COLOR,
                ImageDAM.AssetType.STATIC_IMAGE);

        System.out.println("asset_type : " + tmpDAM2.asset_type);
        System.out.println("country_code : " + tmpDAM2.country_code);
        System.out.println("creative_type : " + tmpDAM2.getCreative_type());
        System.out.println("height : " + tmpDAM2.getHeight());
        System.out.println("id : " + tmpDAM2.getId());
        System.out.println("metadata : " + tmpDAM2.getMetadata());
        System.out.println("mime_type : " + tmpDAM2.getMime_type());
        System.out.println("name : " + tmpDAM2.getName());
        System.out.println("printer_level : " + tmpDAM2.printer_level);
        System.out.println("products : " + toJsString(tmpDAM2.getProducts()));
        System.out.println("size : " + tmpDAM2.size);
        System.out.println("source_id : " + tmpDAM2.source_id);
        System.out.println("system_id : " + tmpDAM2.system_id);
        System.out.println("tags : " + tmpDAM2.tags);
        System.out.println("targeting: " + toJsString(tmpDAM2.getTargeting()));
        System.out.println("url: " + tmpDAM2.getUrl());
        System.out.println("width: " + tmpDAM2.getWidth());

    }

    public ImageDAM createDAMImage(Config.Mode mode,
                                   CountryEnum country,
                                   String assetId,
                                   ImageDAM.PrinterLevel printerLevel,
                                   ImageDAM.AssetType assetType) throws HttpResponseStatusException {
        if (assetId != null && !assetId.isEmpty()) {
            ImageDAM img = this.getDAMImage(mode, country, assetId, printerLevel, assetType);
            return img;
        }
        logger.warn("AssetId null or empty");
        return null;
    }

    public ImageDAM getDAMImage(Config.Mode mode,
                                CountryEnum country,
                                String imageNbr,
                                ImageDAM.PrinterLevel printerLvl,
                                ImageDAM.AssetType assetType) throws HttpResponseStatusException {
        String key = country.getCode() + "_" + imageNbr;
        if (damImgMap.containsKey(key)) {
            return damImgMap.get(key);
        }
        ImageDAM imageDAM = getDamImageFromValidResponse(mode, country, imageNbr, printerLvl, assetType);
        return imageDAM;
    }

    private ImageDAM getDamImageFromValidResponse(Config.Mode mode,
                                                  CountryEnum country,
                                                  String imageNbr,
                                                  ImageDAM.PrinterLevel printerLevel,
                                                  ImageDAM.AssetType assetType) throws HttpResponseStatusException {
        String url = mode.dmpUrl + "/creatives/images/dam/" + country.getCode() + "/" + assetType.getValue() + "/" + imageNbr;

        String response = cRestTemplate.executeGetRequestWithJsonHeader(url, String.class);
        return responseWrapperForDamImage(response, country, imageNbr, printerLevel, assetType);

    }

    private ImageDAM responseWrapperForDamImage(String response,
                                                CountryEnum country,
                                                String imageNbr,
                                                ImageDAM.PrinterLevel printerLevel,
                                                ImageDAM.AssetType assetType) {

        ImageDAM damAsset = null;
        try {
            JsonObject damResponse = Config.parser.parse(response).getAsJsonObject();
            damAsset = new ImageDAM(assetType.getValue(),
                    null,
                    damResponse.get("id").getAsString(),
                    new JsonObject(),
                    "image/png",
                    DAM_SYSTEM_ID + ":" + imageNbr,
                    new ImageDAM.Products(new JsonArray(), new JsonArray(), new JsonArray(), new JsonArray()),
                    new Targeting(false),
                    damResponse.get("url").getAsString(),
                    null,
                    assetType.getValue(),
                    country.getCode(),
                    printerLevel.getName(),
                    damResponse.get("size").getAsInt(),
                    imageNbr,
                    new ArrayList<>());
        } catch (JsonParseException jse) {
            logger.error(String.format("Error parsing dam image %s: %s", imageNbr, jse));
        }
        if (damAsset != null) damImgMap.put(country.getCode() + "_" + imageNbr, damAsset);
        return damAsset;
    }

    public void resetCache() {
        damImgMap.clear();
    }
}
