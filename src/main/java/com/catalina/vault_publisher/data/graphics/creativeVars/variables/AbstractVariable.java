package com.catalina.vault_publisher.data.graphics.creativeVars.variables;

import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(VariableSerializer.class)
public abstract class AbstractVariable {

    private String name;
    private boolean required;
    private String scope;
    private String value;
    private String type;

    public AbstractVariable() {
    }

    public AbstractVariable(String name, boolean required, String scope, String value, String type) {
        this.name = name;
        this.required = required;
        this.scope = scope;
        this.value = value;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public boolean isRequired() {
        return required;
    }

    public String getScope() {
        return scope;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }
}
