package com.catalina.vault_publisher.data.graphics.creativeVars.variables.barcode;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariable;

public class BarcodeService {

    public BarcodeVariable createBarcodeVariable(AbstractVariable variable, MetaVariable meta) {
        BarcodeVariable barcodeVariable = (BarcodeVariable) variable;
        barcodeVariable.setMeta(meta);
        return barcodeVariable;
    }
}
