package com.catalina.vault_publisher.data.graphics.creatives;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.graphics.creatives.template.Template;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;

public class CreativeSerializer implements JsonSerializer<Creative>, JsonDeserializer<Creative> {

    private static final Logger logger = LogManager.getLogger(CreativeSerializer.class);
    private Gson gson = new Gson();

    @Override
    public JsonElement serialize(Creative creative, Type type, JsonSerializationContext context) {
        return gson.toJsonTree(creative);
    }

    @Override
    public Creative deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String type = json.getAsJsonObject().get("creative_type").getAsString();
        if (type.equalsIgnoreCase("PRODUCT_SHOT") || type.equalsIgnoreCase("STATIC_IMAGE")) {
            return gson.fromJson(json, ImageDAM.class);
        } else if (type.equalsIgnoreCase("TEMPLATE")) {
            return gson.fromJson(json, Template.class);
        }
        logger.warn(String.format("Could not deserialize this creative's type: %s", type));
        return null;
    }
}
