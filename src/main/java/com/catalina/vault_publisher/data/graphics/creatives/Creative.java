package com.catalina.vault_publisher.data.graphics.creatives;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.google.gson.JsonElement;
import com.google.gson.annotations.JsonAdapter;

@JsonAdapter(CreativeSerializer.class)
public abstract class Creative {
    private String creative_type;
    private String height;
    private String id;
    private JsonElement metadata;
    private String mime_type;
    private String name;
    private ImageDAM.Products products;
    private Targeting targeting;
    private String url;
    private String width;

    public Creative() {
    }

    public Creative(String creative_type,
                    String height,
                    String id,
                    JsonElement metadata,
                    String mime_type,
                    String name,
                    ImageDAM.Products products,
                    Targeting targeting,
                    String url,
                    String width) {
        this.creative_type = creative_type;
        this.height = height;
        this.id = id;
        this.metadata = metadata;
        this.mime_type = mime_type;
        this.name = name;
        this.products = products;
        this.targeting = targeting;
        this.url = url;
        this.width = width;
    }

    public String getCreative_type() {
        return creative_type;
    }

    public String getHeight() {
        return height;
    }

    public String getId() {
        return id;
    }

    public JsonElement getMetadata() {
        return metadata;
    }

    public String getMime_type() {
        return mime_type;
    }

    public String getName() {
        return name;
    }

    public ImageDAM.Products getProducts() {
        return products;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public String getUrl() {
        return url;
    }

    public String getWidth() {
        return width;
    }
}
