package com.catalina.vault_publisher.data.graphics.creativeVars.variables.image;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariable;

import java.util.Objects;

public class StaticImageVariable extends AbstractVariable {
    private String group;
    private MetaVariable meta;
    private String template_id;

    public StaticImageVariable() {
    }

    public StaticImageVariable(String name, boolean required, String scope, String value, String type, String group, MetaVariable meta, String template_id) {
        super(name, required, scope, value, type);
        this.group = group;
        this.meta = meta;
        this.template_id = template_id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public MetaVariable getMeta() {
        return meta;
    }

    public void setMeta(MetaVariable meta) {
        this.meta = meta;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;

    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), isRequired(), getScope(), getType(), getGroup());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StaticImageVariable that = (StaticImageVariable) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(isRequired(), that.isRequired()) &&
                Objects.equals(getScope(), that.getScope()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getGroup(), that.getGroup());
    }
}
