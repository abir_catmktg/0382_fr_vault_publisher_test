package com.catalina.vault_publisher.data.graphics.templateAPI;

import com.catalina.vault_publisher.data.graphics.adsize.AdSize;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;

import java.util.Arrays;
import java.util.List;

public class TemplateAPI {
    private AdSize adsize;
    private String id;
    private String name;
    private String preview;
    private String status;
    private List<AbstractVariable> variables;

    public TemplateAPI() {
    }

    public TemplateAPI(AdSize adsize, String id, String name, String preview, String status, List<AbstractVariable> variables) {
        this.adsize = adsize;
        this.id = id;
        this.name = name;
        this.preview = preview;
        this.status = status;
        this.variables = variables;
    }

    public AdSize getAdsize() {
        return adsize;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPreview() {
        return preview;
    }

    public String getStatus() {
        return status;
    }

    public List<AbstractVariable> getVariables() {
        return variables;
    }

    public void setVariables(List<AbstractVariable> variables) {
        this.variables = variables;
    }

    public enum ManufColorTemplate {
        ESSENTIEL("FRA Manuf- Coupon ESSENTIEL from C3 2019", "31c2e410-af56-4b7f-bc8a-929946e3730c"),
        EXPRESSION("FRA Manuf- Coupon EXPRESSION from C3 2019", "c0435452-9b44-434d-9307-b533ee0f1427"),
        PANORAMIQUE("FRA Manuf- Coupon PANORAMIQUE from C3 2019", "b3268529-06a2-4f5f-a8ae-d4b30a8cbe2a"),
        TRAILER("Trailer Message", "6df64371-f185-412e-8ca6-14676fb4488a"),
        AUTH_CODE_COLOR("Auth code couleur", "6294a0c4-d972-4822-8594-8540d13aed92"),
        RETAILER_LOGO("retailer logo couleur", "5835f122-1754-4a35-a3a1-fc2b54ac66ce"),
        REDEEM("MANUF Redeem area couleur", "c62f97f1-0add-4237-9e87-23216f85b827");

        private final String name, id;

        ManufColorTemplate(String name, String id) {
            this.name = name;
            this.id = id;
        }

        public static ManufColorTemplate fromValues(String manufColorTemplateName) {
            return Arrays.stream(ManufColorTemplate.values())
                    .filter(e -> e.getName().equalsIgnoreCase(manufColorTemplateName))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Template Name for Manuf Color: %s", manufColorTemplateName)));
        }

        public String getName() {
            return name;
        }

        public String getId() {
            return id;
        }
    }

    public enum ManufBwTemplate {
        DYNAMIC("Manuf BW Dynamic Template CRF PROXI", "ce931b96-7761-44f4-a6fc-05d848c9c479"),
        STATIC("CFR PROXI BW GRAPHIC MANUF", "ad1266cc-01a2-4e09-97ef-1b6755f86684"),
        TRAILER("TRAILER MESSAGE BW MANUF CRF PROXI", "3cd67241-4a9a-4db1-8255-81f7329d0836"),
        REDEEM("REDEEM BW CRF PROXI MANUF", "2c9ba117-e3b1-4779-872d-664cadaf6e05");

        private final String name, id;

        ManufBwTemplate(String name, String id) {
            this.name = name;
            this.id = id;
        }

        public static ManufBwTemplate fromValues(String manufBwTemplateName) {
            return Arrays.stream(ManufBwTemplate.values())
                    .filter(e -> e.getName().equalsIgnoreCase(manufBwTemplateName))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Template Name for Manuf B&W: %s", manufBwTemplateName)));
        }

        public String getName() {
            return name;
        }

        public String getId() {
            return id;
        }
    }
}
