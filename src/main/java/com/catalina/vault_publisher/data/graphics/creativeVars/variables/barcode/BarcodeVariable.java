package com.catalina.vault_publisher.data.graphics.creativeVars.variables.barcode;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.text.TextVariable;

public class BarcodeVariable extends TextVariable {

    private String barcode_type;

    public BarcodeVariable() {
    }

    public BarcodeVariable(String name, boolean required, String scope, String value, String type, Formatting formatting, String group, MetaVariable meta, String template_id, String barcode_type) {
        super(name, required, scope, value, type, formatting, group, meta, template_id);
        this.barcode_type = barcode_type;
    }

    public String getBarcode_type() {
        return barcode_type;
    }

    public void setBarcode_type(String barcode_type) {
        this.barcode_type = barcode_type;
    }
}
