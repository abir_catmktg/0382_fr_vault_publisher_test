package com.catalina.vault_publisher.data.graphics.creatives.template;

import com.catalina.vault_publisher.data.graphics.creatives.Creative;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class Template extends Creative {
    public String channel_type;
    public List<JsonObject> creative_variables;
    public int dpi;
    public String external_id;
    public String external_system;
    public String rotation;
    public String status;

    public Template() {
    }

    public Template(String creative_type,
                    int dpi,
                    String height,
                    String id,
                    JsonObject metadata,
                    String mime_type,
                    String name,
                    ImageDAM.Products products,
                    Targeting targeting,
                    String url,
                    String width,
                    String channel_type,
                    String external_id,
                    String external_system,
                    String rotation,
                    String status) {
        super(creative_type, height, id, metadata, mime_type, name, products, targeting, url, width);
        this.channel_type = channel_type;
        this.creative_variables = new ArrayList<>();
        this.dpi = dpi;
        this.external_id = external_id;
        this.external_system = external_system;
        this.rotation = rotation;
        this.status = status;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public List<JsonObject> getCreative_variables() {
        return creative_variables;
    }

    public int getDpi() {
        return dpi;
    }

    public String getExternal_id() {
        return external_id;
    }

    public String getExternal_system() {
        return external_system;
    }

    public String getStatus() {
        return status;
    }
}
