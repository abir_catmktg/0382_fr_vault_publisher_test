package com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

public class MetaVariableService {

    private static final Logger logger = LogManager.getLogger(MetaVariableService.class);
    private static HashMap<String, MetaVariable> META_VARIABLES_MAP = null;
    private CRestTemplate cRestTemplate;

    public MetaVariableService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void resetMetaMap() {
        META_VARIABLES_MAP = new HashMap<>();
    }

    public MetaVariable getMetaVariable(Config.Mode mode, String metaName) throws HttpResponseStatusException {
        if (META_VARIABLES_MAP == null) {
            resetMetaMap();
        }

        if (META_VARIABLES_MAP.containsKey(metaName)) {
            return META_VARIABLES_MAP.get(metaName);
        }
        String url = mode.urlDashboard + "/variables/" + metaName;
        logger.info(String.format("GET: %s", url));
        MetaVariable meta = cRestTemplate.executeGetRequestWithJsonHeader(url, MetaVariable.class);
        //logger.debug(String.format("response:\\n %s",Config.toJsString(meta)));
        META_VARIABLES_MAP.put(metaName, meta);
        return meta;
    }
}
