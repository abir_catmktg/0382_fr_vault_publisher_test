package com.catalina.vault_publisher.data.graphics.creativeVars.variables.text;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariable;

import java.util.Objects;

public class TextVariable extends AbstractVariable {
    private Formatting formatting;
    private String group;
    private MetaVariable meta;
    private String template_id;

    public TextVariable() {
    }

    public TextVariable(String name, boolean required, String scope, String value, String type, Formatting formatting, String group, MetaVariable meta, String template_id) {
        super(name, required, scope, value, type);
        this.formatting = formatting;
        this.group = group;
        this.meta = meta;
        this.template_id = template_id;
    }

    public Formatting getFormatting() {
        return formatting;
    }

    public void setFormatting(Formatting formatting) {
        this.formatting = formatting;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public MetaVariable getMeta() {
        return meta;
    }

    public void setMeta(MetaVariable meta) {
        this.meta = meta;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getScope(), getType(), getGroup());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TextVariable that = (TextVariable) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getScope(), that.getScope()) &&
                Objects.equals(getType(), that.getType()) &&
                Objects.equals(getGroup(), that.getGroup());
    }

    public static class Formatting {
        private String date;

        public Formatting() {
        }

        public String getDate() {
            return date;
        }

        @Override
        public int hashCode() {
            return Objects.hash(date);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Formatting that = (Formatting) o;
            return Objects.equals(getDate(), that.getDate());
        }
    }
}
