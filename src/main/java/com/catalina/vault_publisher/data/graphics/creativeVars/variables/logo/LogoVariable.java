package com.catalina.vault_publisher.data.graphics.creativeVars.variables.logo;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;

import java.util.Objects;

public class LogoVariable extends AbstractVariable {
    private String template_id;

    public LogoVariable() {
    }

    public LogoVariable(String name, boolean required, String scope, String value, String type, String template_id) {
        super(name, required, scope, value, type);
        this.template_id = template_id;
    }

    public static void main(String[] args) {
        LogoVariable logoVariable1 = new LogoVariable("logo", true, "Touchpoint", null, "logo", null);
        LogoVariable logoVariable2 = new LogoVariable("logo", true, "Touchpoint", null, "logo", null);

        boolean toto = logoVariable1.equals(logoVariable2);
        System.out.println(toto);
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getScope(), getType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogoVariable that = (LogoVariable) o;
        return Objects.equals(getName(), that.getName()) &&
                Objects.equals(getScope(), that.getScope()) &&
                Objects.equals(getType(), that.getType());
    }
}
