package com.catalina.vault_publisher.data.graphics.adsize;

public class AdSize {
    private Boolean active;
    private String channel_type;
    private int count;
    private String date_modified;
    private Integer dpi;
    private String height;
    private String id;
    private Boolean interactive;
    private String media_type;
    private String name;
    private Placement placement;
    private String rotate;
    private String size_limit;
    private String type;
    private String width;

    public AdSize() {
    }

    public AdSize(Boolean active, String channel_type, int count, String date_modified, Integer dpi, String height, String id, Boolean interactive, String media_type, String name, Placement placement, String rotate, String size_limit, String type, String width) {
        this.active = active;
        this.channel_type = channel_type;
        this.count = count;
        this.date_modified = date_modified;
        this.dpi = dpi;
        this.height = height;
        this.id = id;
        this.interactive = interactive;
        this.media_type = media_type;
        this.name = name;
        this.placement = placement;
        this.rotate = rotate;
        this.size_limit = size_limit;
        this.type = type;
        this.width = type;
    }

    public Boolean getActive() {
        return active;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public int getCount() {
        return count;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public Integer getDpi() {
        return dpi;
    }

    public String getHeight() {
        return height;
    }

    public String getId() {
        return id;
    }

    public Boolean getInteractive() {
        return interactive;
    }

    public String getMedia_type() {
        return media_type;
    }

    public String getName() {
        return name;
    }

    public Placement getPlacement() {
        return placement;
    }

    public String getRotate() {
        return rotate;
    }

    public String getSize_limit() {
        return size_limit;
    }

    public String getType() {
        return type;
    }

    public String getWidth() {
        return width;
    }

    public static class Placement {
        public String id;
        public String name;

        public Placement() {
        }

        public Placement(String id, String name) {
            this.id = id;
            this.name = name;
        }
    }
}
