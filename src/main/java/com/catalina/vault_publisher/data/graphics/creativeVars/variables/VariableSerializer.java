package com.catalina.vault_publisher.data.graphics.creativeVars.variables;

import com.catalina.vault_publisher.data.graphics.creativeVars.variables.barcode.BarcodeVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.image.ImageVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.image.StaticImageVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.logo.LogoVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.text.TextVariable;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;

public class VariableSerializer implements JsonSerializer<AbstractVariable>, JsonDeserializer<AbstractVariable> {

    private static final Logger logger = LogManager.getLogger(VariableSerializer.class);
    private Gson gson = new Gson();

    @Override
    public JsonElement serialize(AbstractVariable AbstractVariable, Type type, JsonSerializationContext context) {
        return gson.toJsonTree(AbstractVariable);
    }

    @Override
    public AbstractVariable deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String type = json.getAsJsonObject().get("type").getAsString();
        boolean hasMimeType = json.getAsJsonObject().has("mime_type");
        if (type.equals("text")) {
            return gson.fromJson(json, TextVariable.class);
        } else if (type.equals("barcode")) {
            return gson.fromJson(json, BarcodeVariable.class);
        } else if (type.equals("image") && hasMimeType) {
            return gson.fromJson(json, ImageVariable.class);
        } else if (type.equals("image") && !hasMimeType) {
            return gson.fromJson(json, StaticImageVariable.class);
        } else if (type.equals("product_shot")) {
            return gson.fromJson(json, StaticImageVariable.class);
        } else if (type.equals("logo")) {
            return gson.fromJson(json, LogoVariable.class);
        }
        logger.warn(String.format("Could not deserialize this AbstractVariable's type: %s", type));
        return null;
    }
}
