package com.catalina.vault_publisher.data.graphics.creativeVars;

import com.catalina.vault_publisher.data.InputVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;

import java.util.List;

public class VariablesResult {
    private List<AbstractVariable> abstractVariables;
    private List<InputVariable> inputVariables;

    public VariablesResult() {
    }

    public VariablesResult(List<AbstractVariable> abstractVariables, List<InputVariable> inputVariables) {
        this.abstractVariables = abstractVariables;
        this.inputVariables = inputVariables;
    }

    public List<AbstractVariable> getAbstractVariables() {
        return abstractVariables;
    }

    public List<InputVariable> getInputVariables() {
        return inputVariables;
    }
}
