package com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta;

import java.util.List;
import java.util.Objects;

public class MetaVariable {
    private String date_modified;
    private String description;
    private List<String> examples;
    private String group;
    private String id;
    private String multiline;
    private String name;
    private String regex;
    private String scope;
    private String type;


    public MetaVariable() {
    }

    public String getDate_modified() {
        return date_modified;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getExamples() {
        return examples;
    }

    public String getGroup() {
        return group;
    }

    public String getId() {
        return id;
    }

    public String getMultiline() {
        return multiline;
    }

    public String getName() {
        return name;
    }

    public String getRegex() {
        return regex;
    }

    public String getScope() {
        return scope;
    }

    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(group, id, name, scope, type);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetaVariable that = (MetaVariable) o;
        return
                Objects.equals(getGroup(), that.getGroup()) &&
                        Objects.equals(getId(), that.getId()) &&
                        Objects.equals(getName(), that.getName()) &&
                        Objects.equals(getScope(), that.getScope()) &&
                        Objects.equals(getType(), that.getType());
    }
}

