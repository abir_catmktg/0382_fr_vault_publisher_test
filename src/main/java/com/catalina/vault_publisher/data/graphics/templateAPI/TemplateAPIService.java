package com.catalina.vault_publisher.data.graphics.templateAPI;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class TemplateAPIService {

    private static final Logger logger = LogManager.getLogger(TemplateAPIService.class);
    private static HashMap<String, TemplateAPI> TEMPLATES_MAP = null;
    private CRestTemplate cRestTemplate;

    public TemplateAPIService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void initTemplate() {
        TEMPLATES_MAP = new HashMap<>();
    }

    public static void main(String[] args) {
        try {
            new TemplateAPIService(new CRestTemplate()).getTemplate(Config.Mode.PROD, "41b340e3-a195-4161-a72c-dfe939ba2963");
        } catch (HttpResponseStatusException e) {
            e.printStackTrace();
        }
    }

    public TemplateAPI.ManufColorTemplate getManufColorTemplate(String templateName) {
        return Arrays.stream(TemplateAPI.ManufColorTemplate.values())
                .filter(x -> x.getName().equalsIgnoreCase(templateName))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Manuf Template Name : %s.", templateName)));
    }

    public TemplateAPI.ManufBwTemplate getManufBwTemplate(String templateName) {
        return Arrays.stream(TemplateAPI.ManufBwTemplate.values())
                .filter(x -> x.getName().equalsIgnoreCase(templateName))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Manuf Template Name : %s.", templateName)));
    }

    public TemplateAPI getTemplate(Config.Mode mode, String templateId) throws HttpResponseStatusException {
        if (TEMPLATES_MAP == null) {
            initTemplate();
        }

        if (TEMPLATES_MAP.containsKey(templateId)) {
            return TEMPLATES_MAP.get(templateId);
        }

        String url = mode.urlDashboard + "/templates?fq={fq}&fl={fl}";
        String fqParam = "{\"id\":\"" + templateId + "\"}";
        String flParam = "adsize,id,name,variables,preview,status";
        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fq: %s \\n fl : %s", fqParam, flParam));
        List<TemplateAPI> templates = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, TemplateAPI.class, fqParam, flParam);
        //logger.debug(String.format("response:\\n %s",templates));
        if (templates.size() == 1) {
            TemplateAPI templateAPI = templates.get(0);
            TEMPLATES_MAP.put(templateId, templateAPI);
            return templateAPI;
        }
        throw new RuntimeException(String.format("%s templates(s) found for the id: %s", templates.size(), templateId));
    }
}
