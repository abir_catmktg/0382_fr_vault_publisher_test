package com.catalina.vault_publisher.data.graphics;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.InputVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.VariablesResult;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creatives.template.Template;
import com.catalina.vault_publisher.data.graphics.creatives.template.TemplateService;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPI;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPIService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TemplatePackageService {

    private static final Logger logger = LogManager.getLogger(TemplatePackageService.class);
    private CRestTemplate cRestTemplate;
    private TemplateService templateService;
    private TemplateAPIService templateAPIService;

    public TemplatePackageService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.templateService = new TemplateService(cRestTemplate);
        this.templateAPIService = new TemplateAPIService(cRestTemplate);
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        TemplatePackageService templatePackageService = new TemplatePackageService(new CRestTemplate());


        InputVariable input1 = new InputVariable("discount_text", "12 €");
        InputVariable input2 = new InputVariable("static_image", null);
        InputVariable input3 = new InputVariable("expire_date", "23/06/2020");
        InputVariable input4 = new InputVariable("toto", "tata");


        List<InputVariable> inputVariableList = Arrays.asList(input1, input2, input3, input4);
        TemplatePackage templatePackage = templatePackageService.createTemplatePackage(Mode.PROD,
                Arrays.asList("31c2e410-af56-4b7f-bc8a-929946e3730c"),
                inputVariableList);
        System.out.println(templatePackage);
    }

    public TemplatePackage createTemplatePackage(Mode mode,
                                                 List<String> templateIdList,
                                                 List<InputVariable> inputVariableList) throws HttpResponseStatusException {
        List<Template> templateFinalList = new ArrayList<>();
        List<InputVariable> excludedVariables = inputVariableList;
        List<AbstractVariable> creativeVariableList = new ArrayList<>();
        for (String currentTemplateId : templateIdList) {
            TemplateAPI templateAPI = templateAPIService.getTemplate(mode, currentTemplateId);

            Template template = templateService.createCreativesTemplate(templateAPI);

            VariablesResult variablesResult =
                    templateService.setValueForCreativeVariables(mode, inputVariableList, templateAPI.getVariables());

            templateFinalList.add(template);
            creativeVariableList.addAll(variablesResult.getAbstractVariables());
            //Remove each InputVariables that matched a Template variable -> The ones with no match will remain
            excludedVariables = templateService.getNonMatchingInputVariables(excludedVariables, variablesResult.getInputVariables());
        }
        logger.info(String.format("%s inputVariable(s) had no matching template's creative_vars", excludedVariables.size()));
        VariablesResult variablesResult = new VariablesResult(creativeVariableList, excludedVariables);

        return new TemplatePackage(templateFinalList, variablesResult);
    }

    public List<TemplatePackage> setTemplateManuf(Mode mode,
                                                  String colorTemplateName,
                                                  String bwTemplateName,
                                                  String saveLine,
                                                  String dealNbr,
                                                  String clearingHouseName,
                                                  String qualifierLine2,
                                                  String qualifierLine3,
                                                  String qualifierLine4) throws HttpResponseStatusException {
        List<TemplatePackage> finalList = new ArrayList<>();
        TemplatePackage colorTemplates =
                getTemplateColorManuf(mode, colorTemplateName, saveLine, dealNbr, clearingHouseName);

        if (bwTemplateName != null) {
            TemplatePackage bwTemplates = this.getTemplateBWmanuf(mode,
                    bwTemplateName,
                    saveLine,
                    dealNbr,
                    clearingHouseName,
                    qualifierLine2,
                    qualifierLine3,
                    qualifierLine4);

            finalList.add(bwTemplates);
        }


        finalList.add(colorTemplates);

        return finalList;

    }

    private TemplatePackage getTemplateColorManuf(Mode mode,
                                                  String templateName,
                                                  String saveLine,
                                                  String dealNbr,
                                                  String clearingHouseName) throws HttpResponseStatusException {

        TemplateAPI.ManufColorTemplate templateEnum = new TemplateAPIService(cRestTemplate).getManufColorTemplate(templateName);
        //ClearingHouse.ClearingHouseEnum clearingHouse = ClearingHouse.ClearingHouseEnum.fromValues(clearingHouseName);

        //InputVariable input1 = new InputVariable("discount_amount_off",saveLine);
        InputVariable input2 = new InputVariable("discount_text", saveLine + " €");
        InputVariable input3 = new InputVariable("deal", dealNbr);
        InputVariable input4 = new InputVariable("redeem_address", "{redeem_address}");

        List<String> templateIdList = Arrays.asList(templateEnum.getId(),
                TemplateAPI.ManufColorTemplate.TRAILER.getId(),
                TemplateAPI.ManufColorTemplate.AUTH_CODE_COLOR.getId(),
                TemplateAPI.ManufColorTemplate.RETAILER_LOGO.getId(),
                TemplateAPI.ManufColorTemplate.REDEEM.getId());

        List<InputVariable> inputVariableList;
        if (dealNbr == null || dealNbr.isEmpty()) inputVariableList = Arrays.asList(input2, input4);
        else inputVariableList = Arrays.asList(input2, input3, input4);

        TemplatePackage templatePackage = createTemplatePackage(mode, templateIdList, inputVariableList);

        return templatePackage;
    }

    public TemplatePackage getTemplateBWmanuf(Mode mode,
                                              String templateName,
                                              String saveLine,
                                              String dealNbr,
                                              String clearingHouseName,
                                              String qLine2,
                                              String qLine3,
                                              String qLine4) throws HttpResponseStatusException {

        TemplateAPI.ManufBwTemplate templateEnum = new TemplateAPIService(cRestTemplate).getManufBwTemplate(templateName);

        InputVariable input1 = new InputVariable("discount_amount_off", saveLine + " €");
        InputVariable input2 = new InputVariable("verbiage1", dealNbr);
        InputVariable input3 = new InputVariable("verbiage2", qLine2);
        InputVariable input4 = new InputVariable("verbiage3", qLine3);
        InputVariable input5 = new InputVariable("verbiage4", qLine4);
        InputVariable input6 = new InputVariable("redeem_address", "{redeem_address}");

        List<String> templateIdList = Arrays.asList(templateEnum.getId(), TemplateAPI.ManufBwTemplate.REDEEM.getId(), TemplateAPI.ManufBwTemplate.TRAILER.getId());

        List<InputVariable> inputVariableList;
        if (dealNbr == null || dealNbr.isEmpty())
            inputVariableList = Arrays.asList(input1, input3, input4, input5, input6);
        else inputVariableList = Arrays.asList(input1, input2, input3, input4, input5, input6);

        TemplatePackage templatePackage = createTemplatePackage(mode, templateIdList, inputVariableList);

        return templatePackage;
    }
}
