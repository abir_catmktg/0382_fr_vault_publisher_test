package com.catalina.vault_publisher.data.graphics;

import com.catalina.vault_publisher.data.graphics.creativeVars.VariablesResult;
import com.catalina.vault_publisher.data.graphics.creatives.template.Template;

import java.util.List;

public class TemplatePackage {
    private List<Template> templates;
    private VariablesResult creativeVars;

    public TemplatePackage(List<Template> templates, VariablesResult creativeVars) {
        this.templates = templates;
        this.creativeVars = creativeVars;
    }

    public List<Template> getTemplates() {
        return templates;
    }

    public VariablesResult getCreativeVars() {
        return creativeVars;
    }

    public void setCreativeVars(VariablesResult creativeVars) {
        this.creativeVars = creativeVars;
    }
}
