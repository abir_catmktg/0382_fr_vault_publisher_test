package com.catalina.vault_publisher.data.graphics.creatives.template;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.InputVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.VariablesResult;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.barcode.BarcodeVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.image.ImageVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.image.StaticImageVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.logo.LogoVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariable;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariableService;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.text.TextVariable;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPI;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class TemplateService {

    private static final Logger logger = LogManager.getLogger(TemplateService.class);
    private CRestTemplate cRestTemplate;
    private MetaVariableService metaVariableService;

    public TemplateService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.metaVariableService = new MetaVariableService(cRestTemplate);
    }

    public static void main(String[] args) {

		/*
		String templateName = "Legal + Trailer Message";
		boolean toto = TemplateManuf.RETAIL_INTERMARCHE_TEMPLATE_LIST_NAME.stream().anyMatch(x -> templateName.equalsIgnoreCase(x));
		System.out.println(toto);
		*/


    }

    public Template createCreativesTemplate(TemplateAPI templateAPI) {
        return new Template("TEMPLATE",
                templateAPI.getAdsize().getDpi(),
                templateAPI.getAdsize().getHeight(),
                templateAPI.getId(),
                null,
                templateAPI.getAdsize().getMedia_type(),
                templateAPI.getName(),
                null,
                new Targeting(false),
                templateAPI.getPreview(),
                templateAPI.getAdsize().getWidth(),
                templateAPI.getAdsize().getChannel_type(),
                templateAPI.getId(),
                Constants.EXTERNAL_SYSTEM,
                templateAPI.getAdsize().getRotate(),
                templateAPI.getStatus());
    }

    private List<AbstractVariable> createCreativeVariables(Config.Mode mode, List<AbstractVariable> abstractVariables) throws HttpResponseStatusException {
        List<AbstractVariable> creativeVarsList = new ArrayList<>();
        for (AbstractVariable currentVariable : abstractVariables) {
            if (currentVariable instanceof TextVariable) {
                MetaVariable meta = metaVariableService.getMetaVariable(mode, currentVariable.getName());
                if (meta.getGroup().equalsIgnoreCase("Tracking")) continue;
                TextVariable textVariable = (TextVariable) currentVariable;
                textVariable.setMeta(meta);
                textVariable.setGroup(meta.getGroup());
                textVariable.setTemplate_id(null);
                creativeVarsList.add(textVariable);
            } else if (currentVariable instanceof BarcodeVariable) {
                MetaVariable meta = metaVariableService.getMetaVariable(mode, currentVariable.getName());
                BarcodeVariable barcodeVariable = (BarcodeVariable) currentVariable;
                barcodeVariable.setMeta(meta);
                barcodeVariable.setGroup(meta.getGroup());
                creativeVarsList.add(barcodeVariable);
            } else if (currentVariable instanceof ImageVariable) {
                ImageVariable imageVariable = (ImageVariable) currentVariable;
                creativeVarsList.add(imageVariable);
            } else if (currentVariable instanceof StaticImageVariable) {
                MetaVariable meta = metaVariableService.getMetaVariable(mode, currentVariable.getName());
                StaticImageVariable staticImageVariable = (StaticImageVariable) currentVariable;
                staticImageVariable.setMeta(meta);
                staticImageVariable.setGroup(meta.getGroup());
                staticImageVariable.setTemplate_id(null);
                creativeVarsList.add(staticImageVariable);
            } else if (currentVariable instanceof LogoVariable) {
                LogoVariable logoVariable = (LogoVariable) currentVariable;
                logoVariable.setTemplate_id(null);
                creativeVarsList.add(logoVariable);
            } else {
                logger.warn(String.format("variable %s will be ignored cause it has an unknown type: %s",
                        currentVariable.getName(),
                        currentVariable.getType()));
                continue;
            }
        }
        return creativeVarsList;
    }

    public List<InputVariable> getNonMatchingInputVariables(List<InputVariable> reference, List<InputVariable> inputWithMatches) {
        Collection<InputVariable> collection = CollectionUtils.subtract(reference, inputWithMatches);
        return new ArrayList<>(collection);
    }

    public VariablesResult setValueForCreativeVariables(Config.Mode mode,
                                                        List<InputVariable> inputVariableList,
                                                        List<AbstractVariable> abstractVariablesList) throws HttpResponseStatusException {

        List<AbstractVariable> updatedAbstractVariablesList = createCreativeVariables(mode, abstractVariablesList);

        //Create MAP <name,value> of AbstractVariable
        Map<String, String> abstractVariableMap = updatedAbstractVariablesList.stream()
                //Getting Name twice instead of value to avoid null pointer [no need to store value -> it's null]
                .collect(Collectors.toMap(AbstractVariable::getName, AbstractVariable::getName));

        //Create MAP <type,value> of InputVariable
        Map<String, String> inputVariableMap = inputVariableList.stream()
                .filter(x -> x.getValue() != null)
                .collect(Collectors.toMap(InputVariable::getKey, InputVariable::getValue));

        //List of InputVariables matching the template's variables
        List<InputVariable> inputWithMatch = inputVariableList.stream()
                .filter(l -> abstractVariableMap.containsKey(l.getKey()))
                .collect(Collectors.toList());


        //Set Value for matching variables
        updatedAbstractVariablesList.stream()
                .filter(l -> inputVariableMap.containsKey(l.getName()))
                .forEach(l -> l.setValue(inputVariableMap.get(l.getName())));

        VariablesResult result = new VariablesResult(updatedAbstractVariablesList, inputWithMatch);

        return result;
    }
}
