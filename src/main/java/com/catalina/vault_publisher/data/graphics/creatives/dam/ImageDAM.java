package com.catalina.vault_publisher.data.graphics.creatives.dam;

import com.catalina.vault_publisher.data.graphics.creatives.Creative;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;

import static com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAMService.DAM_SYSTEM_ID;

public class ImageDAM extends Creative {

    public final String system_id = DAM_SYSTEM_ID;
    public String asset_type;
    public String country_code;
    public String printer_level;
    public int size;
    public String source_id;
    public List<String> tags;

    public ImageDAM() {
    }

    public ImageDAM(String creative_type, String height, String id, JsonObject metadata, String mime_type, String name, Products products, Targeting targeting, String url, String width, String asset_type, String country_code, String printer_level, int size, String source_id, List<String> tags) {
        super(creative_type, height, id, metadata, mime_type, name, products, targeting, url, width);
        this.asset_type = asset_type;
        this.country_code = country_code;
        this.printer_level = printer_level;
        this.size = size;
        this.source_id = source_id;
        this.tags = tags;
    }

    public String getAsset_type() {
        return asset_type;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getPrinter_level() {
        return printer_level;
    }

    public int getSize() {
        return size;
    }

    public String getSource_id() {
        return source_id;
    }

    public String getSystem_id() {
        return system_id;
    }

    public List<String> getTags() {
        return tags;
    }

    public enum PrinterLevel {
        COLOR("COLOR"), BW("BLACK/WHITE");

        private final String name;

        PrinterLevel(String name) {
            this.name = name;
        }

        public static PrinterLevel fromValue(String prnterLevel) {
            return Arrays.stream(PrinterLevel.values())
                    .filter(x -> x.getName().equalsIgnoreCase(prnterLevel))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported printer level: %s", prnterLevel)));
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum AssetType {
        STATIC_IMAGE(1, "STATIC_IMAGE"), PRODUCT_SHOT(0, "PRODUCT_SHOT");

        private final int id;
        private final String value;

        AssetType(int id, String value) {
            this.id = id;
            this.value = value;
        }

        public static AssetType fromValue(String assetType) {
            return Arrays.stream(AssetType.values())
                    .filter(x -> x.getValue().equalsIgnoreCase(assetType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported asset type: %s", assetType)));
        }

        public int getId() {
            return id;
        }

        public String getValue() {
            return value;
        }
    }

    public static class Products {
        JsonArray brands;
        JsonArray categories;
        JsonArray sales_categories;
        JsonArray upcs;

        public Products() {
        }

        public Products(JsonArray brands, JsonArray categories, JsonArray sales_categories, JsonArray upcs) {
            this.brands = brands;
            this.categories = categories;
            this.sales_categories = sales_categories;
            this.upcs = upcs;
        }

        public JsonArray getBrands() {
            return brands;
        }

        public JsonArray getCategories() {
            return categories;
        }

        public JsonArray getSales_categories() {
            return sales_categories;
        }

        public JsonArray getUpcs() {
            return upcs;
        }
    }
}
