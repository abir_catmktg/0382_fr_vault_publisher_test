package com.catalina.vault_publisher.data.forecasting;

public class DigitalForecasting {
    private String channel;
    private String estimated_avg_clip_cost;
    private String event;

    public DigitalForecasting() {
    }

    public DigitalForecasting(String channel, String event) {
        this.channel = channel;
        this.event = event;
    }
}
