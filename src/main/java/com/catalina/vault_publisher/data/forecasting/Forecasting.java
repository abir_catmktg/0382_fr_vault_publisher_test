package com.catalina.vault_publisher.data.forecasting;

public class Forecasting {

    private String channel;               // = "store";
    private String estimated_avg_impression_cost;
    private int estimated_total_impression_count;
    private String event;                 // = "impression";

    public Forecasting() {
    }

    public Forecasting(String _channel, String _event) {
        channel = _channel;
        event = _event;
    }

    public Forecasting(String _channel, String _event, int distributionCapping) {
        channel = _channel;
        event = _event;
        estimated_total_impression_count = distributionCapping;
    }
}
