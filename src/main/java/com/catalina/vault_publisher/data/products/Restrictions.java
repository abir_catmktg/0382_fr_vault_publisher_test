package com.catalina.vault_publisher.data.products;

class Restrictions {
    private boolean age_restricted_use;
    private boolean alcohol_use;
    private boolean liquid_dairy_use;
    private String nbic_count;
    private boolean nbic_use;
    private boolean sensitive_use;
    private boolean smoking_use;

    public Restrictions() {
    }

    public Restrictions(boolean age_restricted_use, boolean alcohol_use, boolean liquid_dairy_use, String nbic_count, boolean nbic_use, boolean sensitive_use, boolean smoking_use) {
        this.age_restricted_use = age_restricted_use;
        this.alcohol_use = alcohol_use;
        this.liquid_dairy_use = liquid_dairy_use;
        this.nbic_count = nbic_count;
        this.nbic_use = nbic_use;
        this.sensitive_use = sensitive_use;
        this.smoking_use = smoking_use;
    }
}
