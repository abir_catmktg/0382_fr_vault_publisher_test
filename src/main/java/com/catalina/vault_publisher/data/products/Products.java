package com.catalina.vault_publisher.data.products;

import com.catalina.vault_publisher.data.productValidation.ProductValidation;
import com.google.gson.JsonArray;

import java.util.List;

public class Products {
    private JsonArray brands;
    private JsonArray categories;
    private List<ProductValidation> inventory;
    private Restrictions legal_restrictions;
    private JsonArray lists;
    private JsonArray upcs;

    public Products() {
    }

    public Products(JsonArray brands, JsonArray categories, List<ProductValidation> inventory, Restrictions legal_restrictions, JsonArray lists, JsonArray upcs) {
        this.brands = brands;
        this.categories = categories;
        this.inventory = inventory;
        this.legal_restrictions = legal_restrictions;
        this.lists = lists;
        this.upcs = upcs;
    }

}
