package com.catalina.vault_publisher.data.products;

import com.catalina.vault_publisher.data.productValidation.ProductValidation;
import com.google.gson.JsonArray;

import java.util.List;

public class ProductsService {

    public ProductsService() {
    }


    public boolean setNbicManuf(String campaignType, String leverType) {
        if ("CP".equalsIgnoreCase(leverType) || !"NON ALCOOL".equalsIgnoreCase(campaignType)) {
            return true;
        }
        return false;
    }

    public Products setupProductsManuf(boolean nbicUse, List<ProductValidation> productValidation) {
        return new Products(new JsonArray(),
                new JsonArray(),
                productValidation,
                new RestrictionsService().setupRestrictionsManuf(nbicUse),
                new JsonArray(),
                new JsonArray());
    }

    public Products setupProductsMinimalPromotion(boolean nbicUse, List<ProductValidation> productValidation) {
        return new Products(new JsonArray(),
                new JsonArray(),
                productValidation,
                new RestrictionsService().setupRestrictionsMinimalPromotion(nbicUse),
                new JsonArray(),
                new JsonArray());
    }
}
