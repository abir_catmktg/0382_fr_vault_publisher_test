package com.catalina.vault_publisher.data.products;

public class RestrictionsService {
    public RestrictionsService() {
    }

    public Restrictions setupRestrictionsManuf(boolean nbicUse) {
        return new Restrictions(false, false, false, "3", nbicUse, false, false);
    }

    public Restrictions setupRestrictionsMinimalPromotion(boolean nbicUse) {
        return new Restrictions(false, false, false, "3", nbicUse, false, false);
    }


}
