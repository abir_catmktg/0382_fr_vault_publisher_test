package com.catalina.vault_publisher.data.touchpoint;

import com.catalina.vault_publisher.data.list.vault.TouchpointVaultList;
import com.catalina.vault_publisher.data.touchpoint.chain.Chain;
import com.catalina.vault_publisher.data.touchpoint.network.Network;
import com.catalina.vault_publisher.data.touchpoint.siteId.SiteId;
import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;

public class TouchPointSerializer implements JsonSerializer<TouchPoint>, JsonDeserializer<TouchPoint> {

    private static final Logger logger = LogManager.getLogger(TouchPointSerializer.class);
    private Gson gson = new Gson();

    @Override
    public JsonElement serialize(TouchPoint touchPoint, Type type, JsonSerializationContext context) {
        return gson.toJsonTree(touchPoint);
    }

    @Override
    public TouchPoint deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String type = json.getAsJsonObject().get("type").getAsString();
        if (type.equals("network")) {
            return gson.fromJson(json, Network.class);
        } else if (type.equals("site_id")) {
            return gson.fromJson(json, SiteId.class);
        } else if (type.equals("chain")) {
            return gson.fromJson(json, Chain.class);
        } else if (type.equals("list")) {
            return gson.fromJson(json, TouchpointVaultList.class);
        }
        logger.warn(String.format("Could not deserialize this TouchPoint's type: %s", type));
        return null;
    }
}
