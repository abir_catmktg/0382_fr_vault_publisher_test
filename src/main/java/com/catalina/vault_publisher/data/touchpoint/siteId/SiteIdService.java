package com.catalina.vault_publisher.data.touchpoint.siteId;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SiteIdService {

	private CRestTemplate cRestTemplate;

	public SiteIdService(CRestTemplate cRestTemplate) {
		this.cRestTemplate = cRestTemplate;
	}

	public static void main(String[] args) throws HttpResponseStatusException {
		new SiteIdService(new CRestTemplate()).getSiteId(Config.Mode.PROD,
				CountryEnum.FRANCE,
				Arrays.asList("99999", "110408"));
	}

	public List<SiteId> getSiteIdFromList(CountryEnum country, List<String> siteIdList) {
		// 1 : Convert Array to List
		// 2 : For each id item in the list => create new SiteId from an id
		// 3 : Convert the stream to List
		return siteIdList.stream().map(x -> new SiteId(country.getCode(), x, x)).collect(Collectors.toList());
	}

	public List<SiteId> getSiteId(Config.Mode m_mode, CountryEnum country, List<String> siteIdList) throws HttpResponseStatusException {
		if (siteIdList == null || siteIdList.isEmpty()) throw new RuntimeException("The site_id list is null or empty");

		List<SiteId> siteIds = new ArrayList<>();
		String url = String.join("", m_mode.dmpUrl, "/touchpoints/taxonomy/", country.getCode(), "/stores?term={term}");

		for (String currentSiteId : siteIdList) {
			String termParam = currentSiteId;

			List<LinkedHashMap> response = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, LinkedHashMap.class, termParam);

			if (response.size() != 1)
				throw new RuntimeException(String.format("%s site_id found with the id: %s", response.size(), currentSiteId));

			SiteId siteId = new SiteId(country.getCode(), response.get(0).get("id").toString(),
					response.get(0).get("name").toString());

			siteIds.add(siteId);
		}

		return siteIds;

	}
}
