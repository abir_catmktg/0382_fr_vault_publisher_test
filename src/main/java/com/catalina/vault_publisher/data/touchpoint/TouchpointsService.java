package com.catalina.vault_publisher.data.touchpoint;


import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.InputVariable;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.data.list.vault.MinimalVaultListService;
import com.catalina.vault_publisher.data.list.vault.TouchpointVaultList;
import com.catalina.vault_publisher.data.list.vault.VaultListService;
import com.catalina.vault_publisher.data.list.vault.VaultTriggerList;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint.TouchPointType;
import com.catalina.vault_publisher.data.touchpoint.chain.Chain;
import com.catalina.vault_publisher.data.touchpoint.chain.ChainService;
import com.catalina.vault_publisher.data.touchpoint.network.Network;
import com.catalina.vault_publisher.data.touchpoint.network.NetworkService;
import com.catalina.vault_publisher.data.touchpoint.siteId.SiteId;
import com.catalina.vault_publisher.data.touchpoint.siteId.SiteIdService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;

import java.util.*;

import static com.catalina.vault_publisher.data.Config.MANUF_NETWORKS_INCLUSION;
import static com.catalina.vault_publisher.data.Config.MANUF_STORES_EXCLUSION;

public class TouchpointsService {

    private CRestTemplate cRestTemplate;
    private SiteIdService siteIdService;
    private ChainService chainService;
    private NetworkService networkService;
    private MinimalVaultListService miniVaultListService;
    private VaultListService vaultListService;
    private Map<String, Touchpoints> campaignTouchpointsMap = new HashMap<>();

    public TouchpointsService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.siteIdService = new SiteIdService(cRestTemplate);
        this.chainService = new ChainService(cRestTemplate);
        this.networkService = new NetworkService(cRestTemplate);
        this.miniVaultListService = new MinimalVaultListService(cRestTemplate);
        this.vaultListService = new VaultListService(cRestTemplate);

    }

    public TouchpointVaultList excludeListVault(Config.Mode mode, String controlGrp) throws HttpResponseStatusException {
        if (controlGrp == null || controlGrp.trim().isEmpty()) {
            return null;
        }

        Result<TouchpointVaultList> resList = vaultListService.getVaultList(mode, controlGrp, VaultTriggerList.ListType.TOUCHPOINT);
        if (resList.getStatus() != 0) throw new RuntimeException(resList.toString());

        TouchpointVaultList exclusionList = resList.getObject();
        return exclusionList;

    }

    public List<SiteId> excludeSiteIdManuf() {
        List<SiteId> siteArray = siteIdService.getSiteIdFromList(Country.CountryEnum.FRANCE, Arrays.asList(MANUF_STORES_EXCLUSION));
        return siteArray;
    }

    public List<Network> includeNetworkManuf(Config.Mode mode) throws HttpResponseStatusException {
        return networkService.getNetworks(mode, Country.CountryEnum.FRANCE, MANUF_NETWORKS_INCLUSION);
    }

    public void resetCache() {
        campaignTouchpointsMap.clear();
    }

    public Touchpoints createTouchpoints(List<TouchPoint> included, List<TouchPoint> excluded) {
        boolean override = included.size() >= 1 || excluded.size() >= 1;
        return new Touchpoints(included, excluded, false, override);
    }

    public List<TouchPoint> formatTouchPointList(Config.Mode mode,
                                                 Country.CountryEnum country,
                                                 List<InputVariable> inputTouchPointList) throws HttpResponseStatusException {

        List<TouchPoint> touchPointList = new ArrayList<>();

        for (InputVariable currentTp : inputTouchPointList) {
            TouchPointType touchPointType = TouchPointType.fromValue(currentTp.getValue());

            if (touchPointType == TouchPointType.STORE) {
                List<SiteId> siteIdList = siteIdService.getSiteId(mode,
                        country,
                        Collections.singletonList(currentTp.getKey()));
                touchPointList.addAll(siteIdList);
            } else if (touchPointType == TouchPointType.CHAIN) {
                List<Chain> chainList = chainService.getChains(mode,
                        country,
                        Collections.singletonList(currentTp.getKey()));
                touchPointList.addAll(chainList);
            } else if (touchPointType == TouchPointType.NETWORK) {
                List<Network> networkList =
                        networkService.getNetworks(mode, country, Collections.singletonList(currentTp.getKey()));
                touchPointList.addAll(networkList);
            } else if (touchPointType == TouchPointType.LIST) {
                Result<TouchpointVaultList> resVaultList =
                        vaultListService.getVaultList(mode,
                                currentTp.getKey(),
                                VaultTriggerList.ListType.TOUCHPOINT);
                if (resVaultList.getStatus() != 0) break;
                TouchpointVaultList vaultLists = resVaultList.getObject();
                touchPointList.add(vaultLists);

            }
        }
        return touchPointList;
    }

}