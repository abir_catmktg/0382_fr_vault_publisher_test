package com.catalina.vault_publisher.data.touchpoint.network;

import com.catalina.vault_publisher.data.Country;

public class NetworkAPI {
    private Country country;
    private String external_id;
    private String id;
    private String name;

    public NetworkAPI() {
    }

    public Country getCountry() {
        return country;
    }

    public String getExternal_id() {
        return external_id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
