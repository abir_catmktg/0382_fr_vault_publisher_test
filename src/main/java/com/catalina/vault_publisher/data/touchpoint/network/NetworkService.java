package com.catalina.vault_publisher.data.touchpoint.network;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class NetworkService {

    private static final Logger logger = LogManager.getLogger(NetworkService.class);
    private CRestTemplate cRestTemplate;

    public NetworkService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        new NetworkService(new CRestTemplate()).getNetworks(Config.Mode.PROD,
                CountryEnum.FRANCE,
                Config.MANUF_NETWORKS_INCLUSION);
    }

    public List<Network> getNetworks(Config.Mode m_mode, CountryEnum country, List<String> networkList) throws HttpResponseStatusException {

        String url = m_mode.urlDashboard + "/networks?fl={fl}&fq={fq}&wc={wc}";

        StringJoiner paramJoiner = new StringJoiner("|");
        for (String networkId : networkList) paramJoiner.add(networkId);

        String flParam = "id|name|external_id|country";
        String fqParam = String.format("{\"country\":{\"country_code\":\"%s\"}}", country.getCode());
        String wcParam = String.format("{\"id\":\"%s\"}", paramJoiner.toString());

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fl: %s \\n fq: %s \\n wc: %s", flParam, fqParam, wcParam));
        List<NetworkAPI> response = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                NetworkAPI.class,
                flParam,
                fqParam,
                wcParam);

        List<Network> networks = mapResponseToNetwork(response);
        return networks;

    }

    private List<Network> mapResponseToNetwork(List<NetworkAPI> networkAPIList) {
        if (networkAPIList == null || networkAPIList.isEmpty())
            throw new RuntimeException("List of network is null or empty");
        try {
            List<Network> networkList = networkAPIList.stream()
                    .map(x -> new Network(x.getCountry().getCountry_code(),
                            x.getId(),
                            x.getName(),
                            Integer.parseInt(x.getExternal_id())))
                    .collect(Collectors.toList());
            return networkList;
        } catch (NumberFormatException e) {
            throw new RuntimeException(String.format("Unable to parse the external_id of this network %s", e.getMessage()));
        }
    }
}
