package com.catalina.vault_publisher.data.touchpoint.chain;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ChainService {

    private static final Logger logger = LogManager.getLogger(ChainService.class);
    private CRestTemplate cRestTemplate;

    public ChainService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public List<Chain> getChains(Config.Mode mode, CountryEnum country, List<String> chainsId) throws HttpResponseStatusException {

        List<LinkedHashMap> responses = new ArrayList<>();
        if (chainsId == null || chainsId.isEmpty()) throw new RuntimeException("List of chain's id is empty or null");

        String url = String.format("%s/touchpoints/taxonomy/%s/chains?term={term}", mode.dmpUrl, country);

        for (String currentChain : chainsId) {
            if (currentChain == null || currentChain.isEmpty()) {
                logger.warn("currentChain is null or is empty");
                continue;
            }

            String termParam = currentChain;
            logger.info(String.format("GET: %s", url));
            List<LinkedHashMap> response = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, LinkedHashMap.class, termParam);
            responses.addAll(response);
        }

        List<Chain> chainList = responses.stream()
                .map(x -> new Chain(country.getCode(),
                        x.get("id").toString(),
                        x.get("name").toString()))
                .collect(Collectors.toList());
        return chainList;


    }
}
