package com.catalina.vault_publisher.data.touchpoint.chain;

import com.catalina.vault_publisher.data.touchpoint.TouchPoint;

import java.util.Collections;
import java.util.List;

public class Chain extends TouchPoint {

    private List<MiniChain> chains;
    private List<String> values;

    public Chain() {
    }

    public Chain(String country_code, String id, String name) {
        super(country_code, id, name, String.format("%s - %s", name, id), TouchPointType.CHAIN.getValue());
        this.chains = Collections.singletonList(new MiniChain(id, name));
        this.values = Collections.singletonList(name);
    }

    public List<MiniChain> getChains() {
        return chains;
    }

    public void setChains(List<MiniChain> chains) {
        this.chains = chains;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
