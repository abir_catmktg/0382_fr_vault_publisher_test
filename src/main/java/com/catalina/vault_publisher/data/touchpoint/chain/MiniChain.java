package com.catalina.vault_publisher.data.touchpoint.chain;

public class MiniChain {
    private String id;
    private String name;

    public MiniChain() {
    }

    public MiniChain(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
