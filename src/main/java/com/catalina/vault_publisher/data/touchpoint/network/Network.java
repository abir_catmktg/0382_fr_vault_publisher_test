package com.catalina.vault_publisher.data.touchpoint.network;

import com.catalina.vault_publisher.data.touchpoint.TouchPoint;

import java.util.Collections;
import java.util.List;


public class Network extends TouchPoint {
    private List<Values> values;

    public Network() {
    }

    public Network(String country_code, String id, String name, int externalId) {
        super(country_code, id, name, String.format("%s - %s", name, id), "network");
        this.values = Collections.singletonList(new Values(externalId, name, country_code));
    }

    /*   public Network(String countryCode, String id, String name, String externalId, String text){
        super(countryCode,id, name, text, "network");
        values = Collections.singletonList(new Values(externalId,name,countryCode));
    }*/


    public static class Values {
        String country_code;
        int id; //external_id
        String name;

        public Values(int id, String name, String countryCode) {
            this.id = id;
            this.name = name;
            this.country_code = countryCode;
        }
    }


}
