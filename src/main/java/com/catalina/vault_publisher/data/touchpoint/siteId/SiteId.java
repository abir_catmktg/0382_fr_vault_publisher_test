package com.catalina.vault_publisher.data.touchpoint.siteId;

import com.catalina.vault_publisher.data.touchpoint.TouchPoint;

import java.util.Collections;
import java.util.List;


public class SiteId extends TouchPoint {

    private final int estimated_reach = 1;
    private List<String> values;

    public SiteId() {
    }

    public SiteId(String country_code, String id, String name) {
        super(country_code, id, name, String.format("%s - %s", id, name), "site_id");
        this.values = Collections.singletonList(id);
    }


}
