package com.catalina.vault_publisher.data.touchpoint;

import com.google.gson.annotations.JsonAdapter;

import java.util.Arrays;
import java.util.Objects;


@JsonAdapter(TouchPointSerializer.class)
public abstract class TouchPoint {
    public String country_code;
    public String download = "/route/dmp/rest/";
    public String id;
    public String name;
    public String text;
    public String type;

    public TouchPoint() {
    }

    public TouchPoint(String country_code, String id, String name, String text, String type) {
        this.country_code = country_code;
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getDownload() {
        return download;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TouchPoint that = (TouchPoint) o;
        return Objects.equals(country_code, that.country_code) &&
                Objects.equals(download, that.download) &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(text, that.text) &&
                Objects.equals(type, that.type);
    }

    public enum TouchPointType {
        STORE("site_id"),
        CHAIN("chain"),
        NETWORK("network"),
        LIST("list");

        private final String value;

        TouchPointType(String value) {
            this.value = value;
        }

        public static TouchPointType fromValue(String touchpointType) {
            return Arrays.stream(TouchPoint.TouchPointType.values())
                    .filter(x -> x.value.equalsIgnoreCase(touchpointType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported TouchPointType %s.", touchpointType)));
        }

        public String getValue() {
            return value;
        }
    }
}
