package com.catalina.vault_publisher.data.touchpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Touchpoints {
    private List<TouchPoint> exclude;

    private List<TouchPoint> include;

    //private String modified_date;
    private boolean operation_or;
    private boolean override;


    public Touchpoints() {
    }

    public Touchpoints(List<TouchPoint> include, List<TouchPoint> exclude) {
        this.exclude = exclude;
        this.include = include;
        //this.modified_date = DateUtils.getCurrentDateMilliSecondPrecision();
        this.operation_or = false;
        this.override = true;
    }

    public Touchpoints(List<TouchPoint> include, List<TouchPoint> exclude, boolean operation_or, boolean override) {
        this.exclude = exclude;
        this.include = include;
        this.operation_or = operation_or;
        this.override = override;
    }

    public Touchpoints(boolean override) {
        this.exclude = new ArrayList<>();
        this.include = new ArrayList<>();
        //this.modified_date = DateUtils.getCurrentDateMilliSecondPrecision();
        this.operation_or = false;
        this.override = override;
    }


    public List<TouchPoint> getInclude() {
        return include;
    }

    public void setInclude(List<TouchPoint> include) {
        this.include = include;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //implement List comparaison using HashSet
        Touchpoints that = (Touchpoints) o;
        return operation_or == that.operation_or &&
                override == that.override &&
                Objects.equals(exclude, that.exclude) &&
                Objects.equals(include, that.include);
    }

}
