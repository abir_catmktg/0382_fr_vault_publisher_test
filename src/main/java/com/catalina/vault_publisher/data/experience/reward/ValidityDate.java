package com.catalina.vault_publisher.data.experience.reward;


import java.util.Arrays;

public class ValidityDate {
    private String date_format;         //  "dd/MM/yyyy"
    private String event;
    private String type;                //  "fixed"
    private String value;               //  "2019-07-29"

    public ValidityDate() {
    }

    public ValidityDate(String date_format, String event, String type, String value) {
        this.date_format = date_format;
        this.event = event;
        this.type = type;
        this.value = value;
    }

    public String getDate_format() {
        return date_format;
    }

    public String getEvent() {
        return event;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }


    public enum DateType {
        ROLLING("rolling"),
        FIXED("fixed");

        private final String type;

        DateType(String type) {
            this.type = type;
        }

        public static DateType fromValue(String dateType) {
            return Arrays.stream(DateType.values())
                    .filter(e -> e.getType().equalsIgnoreCase(dateType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Date Type %s.", dateType)));
        }

        public String getType() {
            return type;
        }
    }

    public enum RollingPeriod {
        MINUTES("MINUTE"),
        HOURS("HOUR"),
        DAYS("DAY"),
        WEEKS("WEEK"),
        MONTH("MONTH"),
        YEAR("YEAR");

        private final String value;

        RollingPeriod(String value) {
            this.value = value;
        }

        public static RollingPeriod fromValue(String rollingUnits) {
            return Arrays.stream(RollingPeriod.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(rollingUnits))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Rolling Units %s.", rollingUnits)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum DateEvent {
        IMPRESSION("print"),
        WALLET("wallet");

        private final String value;

        DateEvent(String value) {
            this.value = value;
        }

        public static DateEvent fromValue(String dateEvent) {
            if (dateEvent == null) return null;
            return Arrays.stream(DateEvent.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(dateEvent))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Date Event %s.", dateEvent)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum DateFormat {
        MM_DD("MM/dd"),
        M_D_YY("M/d/yy"),
        M_D_YYYY("M/d/yyyy"),
        MM_DD_YY("MM/dd/yy"),
        MM_DD_YYYY("MM/dd/yyyy"),
        MM_DD_5DAYS("MM/dd:+5DAYS"),
        MM_DD_6DAYS("MM/dd:+6DAYS"),
        MM_DD_1WEEK("MM/dd:+1WEEK");

        private final String value;

        DateFormat(String value) {
            this.value = value;
        }

        public static DateFormat fromValue(String dateFormat) {
            if (dateFormat == null) return null;
            return Arrays.stream(DateFormat.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(dateFormat))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Date Format %s.", dateFormat)));
        }

        public String getValue() {
            return value;
        }
    }
}
