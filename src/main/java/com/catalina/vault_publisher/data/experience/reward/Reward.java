package com.catalina.vault_publisher.data.experience.reward;

import com.catalina.vault_publisher.data.barcode.Barcode;
import com.catalina.vault_publisher.data.experience.Conditions;
import com.catalina.vault_publisher.data.experience.Funding;
import com.catalina.vault_publisher.data.experience.Loyalty;
import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.trailer.Trailer;

import java.util.List;

public class Reward {
    private Barcode barcode;
    private String code;
    private Conditions conditions;
    private String digital_offer_code;
    private ValidityDate effective_date;
    private ValidityDate expiration_date;
    private String external_id; //VAR_BL_ID
    private Funding funding;
    private String id;          //VAR_BL_EXTERNAL_ID
    private boolean is_digital;
    private List<Trailer> legal;
    private Loyalty loyalty;
    private String offer_code_type;
    private List<OCLU_LEASE_RESPONSE_ENTITY> offer_codes;
    private Schedule schedule;
    private RewardValue value;

    public Reward() {
    }

    public Reward(Barcode barcode,
                  String code,
                  Conditions conditions,
                  String digital_offer_code,
                  ValidityDate effective_date,
                  ValidityDate expiration_date,
                  String external_id,
                  Funding funding,
                  String id,
                  boolean is_digital,
                  List<Trailer> legal,
                  Loyalty loyalty,
                  String offer_code_type,
                  List<OCLU_LEASE_RESPONSE_ENTITY> offer_codes,
                  Schedule schedule,
                  RewardValue value) {
        this.barcode = barcode;
        this.code = code;
        this.conditions = conditions;
        this.digital_offer_code = digital_offer_code;
        this.effective_date = effective_date;
        this.expiration_date = expiration_date;
        this.external_id = external_id;
        this.funding = funding;
        this.id = id;
        this.is_digital = is_digital;
        this.legal = legal;
        this.loyalty = loyalty;
        this.offer_code_type = offer_code_type;
        this.offer_codes = offer_codes;
        this.schedule = schedule;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public String getCode() {
        return code;
    }

    public Conditions getConditions() {
        return conditions;
    }

    public String getDigital_offer_code() {
        return digital_offer_code;
    }

    public ValidityDate getEffective_date() {
        return effective_date;
    }

    public ValidityDate getExpiration_date() {
        return expiration_date;
    }

    public Funding getFunding() {
        return funding;
    }

    public boolean isIs_digital() {
        return is_digital;
    }

    public List<Trailer> getLegal() {
        return legal;
    }

    public Loyalty getLoyalty() {
        return loyalty;
    }

    public String getOffer_code_type() {
        return offer_code_type;
    }

    public void setOffer_code_type(String offer_code_type) {
        this.offer_code_type = offer_code_type;
    }

    public List<OCLU_LEASE_RESPONSE_ENTITY> getOffer_codes() {
        return offer_codes;
    }

    public void setOffer_codes(List<OCLU_LEASE_RESPONSE_ENTITY> offer_codes) {
        this.offer_codes = offer_codes;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public RewardValue getValue() {
        return value;
    }
}
