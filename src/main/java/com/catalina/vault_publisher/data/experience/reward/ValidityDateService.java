package com.catalina.vault_publisher.data.experience.reward;

public class ValidityDateService {

    private ValidityDate createValidityDate(ValidityDate.DateType type,
                                            ValidityDate.DateEvent event,
                                            ValidityDate.DateFormat format,
                                            String value) {
        return new ValidityDate((format == null) ? null : format.getValue(),
                (event == null) ? null : event.getValue(),
                type.getType(),
                value);
    }

    public ValidityDate createRollingValidityDate(int value,
                                                  ValidityDate.RollingPeriod period,
                                                  ValidityDate.DateEvent event,
                                                  ValidityDate.DateFormat format) {
        return createValidityDate(ValidityDate.DateType.ROLLING,
                event,
                format,
                "NOW+" + value + period.getValue());
    }

    public ValidityDate createFixedValidityDate(String date,
                                                ValidityDate.DateFormat format) {
        return createValidityDate(ValidityDate.DateType.FIXED, ValidityDate.DateEvent.IMPRESSION, format, date);
    }

    public ValidityDate setupExpirationDateManuf() {
        return createRollingValidityDate(12,
                ValidityDate.RollingPeriod.WEEKS,
                null,
                null);
    }


}
