package com.catalina.vault_publisher.data.experience.reward;

import com.google.gson.JsonElement;

public class RewardValue {
    public JsonElement amount;
    public JsonElement maximum_amount_per_item;

    public RewardValue() {
    }

    public RewardValue(JsonElement amount, JsonElement maximum_amount_per_item) {
        this.amount = amount;
        this.maximum_amount_per_item = maximum_amount_per_item;
    }

    public JsonElement getAmount() {
        return amount;
    }

    public JsonElement getMaximum_amount_per_item() {
        return maximum_amount_per_item;
    }
}
