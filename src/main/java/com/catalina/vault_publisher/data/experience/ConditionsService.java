package com.catalina.vault_publisher.data.experience;

import com.catalina.vault_publisher.data.targeting.ConfigurationService;
import com.google.gson.JsonArray;

public class ConditionsService {

    public ConditionsService() {
    }

    public Conditions setupConditionsManuf() {
        return new Conditions(new JsonArray(), new JsonArray(), new ConfigurationService().setupConfigurationReward(), false, "class");
    }

    public Conditions setupConditionsMinimalPromotion() {
        return new Conditions(new JsonArray(), new JsonArray(), new ConfigurationService().setupConfigurationReward(), false, "class");
    }
}
