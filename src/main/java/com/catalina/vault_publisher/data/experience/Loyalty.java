package com.catalina.vault_publisher.data.experience;

public class Loyalty {
    private String action;
    private LoyaltyValue value;

    public Loyalty() {
    }

    public Loyalty(String action, LoyaltyValue value) {
        this.action = action;
        this.value = value;
    }

    public static class LoyaltyValue {
        private String amount;
        private String type;

        public LoyaltyValue() {
        }
    }
}
