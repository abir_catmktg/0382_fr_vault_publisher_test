package com.catalina.vault_publisher.data.experience;

import com.catalina.vault_publisher.data.targeting.Configuration;
import com.google.gson.JsonArray;

public class Conditions {
    public JsonArray activation_trigger_groups;
    public JsonArray activation_triggers;
    public Configuration configuration;
    public boolean readonly;
    public String type;

    public Conditions() {
    }

    public Conditions(JsonArray activation_trigger_groups, JsonArray activation_triggers, Configuration configuration, boolean readonly, String type) {
        this.activation_trigger_groups = activation_trigger_groups;
        this.activation_triggers = activation_triggers;
        this.configuration = configuration;
        this.readonly = readonly;
        this.type = type;
    }
}
