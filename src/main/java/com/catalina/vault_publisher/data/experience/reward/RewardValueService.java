package com.catalina.vault_publisher.data.experience.reward;

import com.google.gson.JsonObject;

public class RewardValueService {

    public RewardValueService() {
    }

    public RewardValue setupRewardValueManuf() {
        return new RewardValue(new JsonObject(), new JsonObject());
    }
}
