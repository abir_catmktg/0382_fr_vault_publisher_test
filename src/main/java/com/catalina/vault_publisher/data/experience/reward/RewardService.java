package com.catalina.vault_publisher.data.experience.reward;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.barcode.Barcode;
import com.catalina.vault_publisher.data.barcode.BarcodeService;
import com.catalina.vault_publisher.data.clearingHouse.ClearingHouse;
import com.catalina.vault_publisher.data.experience.*;
import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.trailer.Trailer;
import com.catalina.vault_publisher.utils.CRestTemplate;

import java.util.ArrayList;
import java.util.List;

public class RewardService {

    private BarcodeService barcodeService;

    public RewardService() {
        this.barcodeService = new BarcodeService(new CRestTemplate());
    }

    public Reward setupRewardManuf(String adExternalId,
                                   String promotionId,
                                   Barcode barcode,
                                   ClearingHouse clearingHouse,
                                   List<Trailer> trailerList) {

        return createReward(barcode,
                null,
                new ConditionsService().setupConditionsManuf(),
                null,
                null,
                new ValidityDateService().setupExpirationDateManuf(),
                adExternalId,
                new Funding(clearingHouse, Config.Segment.MANUF),
                promotionId,
                false,
                trailerList,
                new LoyaltyService().setupRewardLoyaltyManuf(),
                null,
                new ArrayList<>(),
                new Schedule(false),
                new RewardValueService().setupRewardValueManuf());

    }

    public Reward setupRewardMinimalPromotion(String adExternalId,
                                              String promotionId,
                                              ValidityDate expirationDate,
                                              Funding funding) {

        return createMinimalReward(barcodeService.setupBarcodeMinimalPromotion(),
                null,
                expirationDate,
                adExternalId,
                funding,
                promotionId,
                null,
                null);
    }

    private Reward createReward(Barcode barcode,
                                String code,
                                Conditions conditions,
                                String digitalOfferCode,
                                ValidityDate effectiveDate,
                                ValidityDate expirationDate,
                                String adExternalId,
                                Funding funding,
                                String promotionId,
                                boolean isDigital,
                                List<Trailer> legal,
                                Loyalty loyalty,
                                String offerCodeType,
                                List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes,
                                Schedule schedule,
                                RewardValue value) {

        return new Reward(barcode,
                code,
                conditions,
                digitalOfferCode,
                effectiveDate,
                expirationDate,
                adExternalId,
                funding,
                promotionId,
                isDigital,
                legal,
                loyalty,
                offerCodeType,
                offerCodes,
                schedule,
                value);
    }

    public Reward createMinimalReward(Barcode barcode,
                                      ValidityDate effectiveDate,
                                      ValidityDate expirationDate,
                                      String adExternalId,
                                      Funding funding,
                                      String promotionId,
                                      List<Trailer> legal,
                                      Schedule schedule) {
        return createReward(barcode,
                null,
                new ConditionsService().setupConditionsManuf(),
                null,
                effectiveDate,
                expirationDate,
                adExternalId,
                funding,
                promotionId,
                false,
                legal,
                null,
                null,
                new ArrayList<>(),
                schedule,
                null);
    }
}
