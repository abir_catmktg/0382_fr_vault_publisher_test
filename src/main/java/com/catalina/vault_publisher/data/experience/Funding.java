package com.catalina.vault_publisher.data.experience;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.clearingHouse.ClearingHouse;

public class Funding {
    public ClearingHouse clearing_house;
    //public String business_segment;
    public String type;

    public Funding() {
    }

    public Funding(ClearingHouse clearing_house, Config.Segment segment) {
        this.clearing_house = clearing_house;
        //this.business_segment = segment.businessSegment;
        this.type = segment.type;
    }
}
