package com.catalina.vault_publisher.data.experience;

import com.catalina.vault_publisher.data.barcode.Barcode;
import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.clearingHouse.ClearingHouse;
import com.catalina.vault_publisher.data.experience.Experience.ExperienceEnum;
import com.catalina.vault_publisher.data.experience.reward.Reward;
import com.catalina.vault_publisher.data.experience.reward.RewardService;
import com.catalina.vault_publisher.data.experience.reward.ValidityDate;
import com.catalina.vault_publisher.data.trailer.Trailer;
import com.catalina.vault_publisher.data.triggers.TriggerService;
import com.catalina.vault_publisher.data.triggers.Triggers;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class ExperienceService {

    private CRestTemplate cRestTemplate;
    private TriggerService triggerService;
    private RewardService rewardService;

    public ExperienceService() {
        this.cRestTemplate = new CRestTemplate();
        this.triggerService = new TriggerService(this.cRestTemplate);
        this.rewardService = new RewardService();
    }

    private Experience createExperience(String capPerSession,
                                        boolean endOfOrder,
                                        List<FrequencyCap> frequencyCaps,
                                        JsonObject frequencyStatics,
                                        ExperienceEnum experienceEnum,
                                        JsonObject loyalty,
                                        JsonObject pins,
                                        Reward reward,
                                        Triggers triggers) {
        return new Experience(capPerSession,
                endOfOrder,
                frequencyCaps,
                frequencyStatics,
                experienceEnum.getId(),
                loyalty,
                experienceEnum.getName(),
                pins,
                reward,
                triggers,
                experienceEnum.getType());
    }

    public Experience createMinimalExperience(List<FrequencyCap> frequencyCaps,
                                              ExperienceEnum experienceEnum,
                                              Reward reward,
                                              Triggers triggers) {
        return createExperience("0",
                false,
                frequencyCaps,
                new JsonObject(),
                experienceEnum,
                new JsonObject(),
                new JsonObject(),
                reward,
                triggers);
    }

    public Experience createExperienceManuf(String adExternalId,
                                            String promotionId,
                                            Barcode barcode,
                                            ClearingHouse clearingHouse,
                                            List<Trailer> trailerList,
                                            Triggers triggers) {
        Experience experience = new Experience("0",
                false,
                new ArrayList<>(),
                new JsonObject(),
                ExperienceEnum.COUPON.getId(),
                new JsonObject(),
                ExperienceEnum.COUPON.getName(),
                new JsonObject(),
                rewardService.setupRewardManuf(adExternalId, promotionId, barcode, clearingHouse, trailerList),
                triggers,
                ExperienceEnum.COUPON.getType());
        return experience;
    }

    public Experience createExperiencePromotion(String adExternalId,
                                                String promotionId,
                                                ValidityDate expirationDate,
                                                Funding funding) {

        Experience experience = new Experience(null,
                false,
                new ArrayList<>(),
                null,
                ExperienceEnum.COUPON.getId(),
                new JsonObject(),
                ExperienceEnum.COUPON.getName(),
                null,
                rewardService.setupRewardMinimalPromotion(adExternalId, promotionId, expirationDate, funding),
                triggerService.createTriggers(new ArrayList<>(), new ArrayList<>()),
                ExperienceEnum.COUPON.getType());
        return experience;
    }
}
