package com.catalina.vault_publisher.data.experience;

import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.experience.reward.Reward;
import com.catalina.vault_publisher.data.triggers.Triggers;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Arrays;
import java.util.List;

public class Experience {
    private String cap_per_session;
    private boolean expected;
    private boolean end_of_order;
    private List<FrequencyCap> frequency_caps;
    private JsonElement frequency_statics;
    private String id;
    private JsonElement loyalty;
    //	private boolean ignore_long_term_controls;
    private String name;
    private JsonElement pins;
    private Reward reward;
    private Triggers triggers;
    private String type;

    public Experience() {
    }

    public Experience(String cap_per_session,
                      boolean end_of_order,
                      List<FrequencyCap> frequency_caps,
                      JsonObject frequency_statics,
                      String id,
                      JsonObject loyalty,
                      String name,
                      JsonObject pins,
                      Reward reward,
                      Triggers triggers,
                      String type) {
        this.cap_per_session = cap_per_session;
        this.end_of_order = end_of_order;
        this.frequency_caps = frequency_caps;
        this.frequency_statics = frequency_statics;
        this.id = id;
        this.loyalty = loyalty;
        this.name = name;
        this.pins = pins;
        this.reward = reward;
        this.triggers = triggers;
        this.type = type;
    }

    public String getCap_per_session() {
        return cap_per_session;
    }


/*
	public Experience(PromotionType promotionType) {
		this.reward = new Reward(promotionType);
	}

	public void updateRewardFromAdExperience(JsonObject experienceObj) {
		this.reward = new Gson().fromJson(experienceObj.get("reward"), Ad.Reward.class);
		this.reward.loyalty = null;
		this.reward.value = null;
	}
	*/

    public boolean isExpected() {
        return expected;
    }

    public boolean isEnd_of_order() {
        return end_of_order;
    }

    public List<FrequencyCap> getFrequency_caps() {
        return frequency_caps;
    }

    public JsonElement getFrequency_statics() {
        return frequency_statics;
    }

    public String getId() {
        return id;
    }

    public JsonElement getLoyalty() {
        return loyalty;
    }

    public String getName() {
        return name;
    }

    public JsonElement getPins() {
        return pins;
    }

    public Reward getReward() {
        return reward;
    }

    public Triggers getTriggers() {
        return triggers;
    }

    public String getType() {
        return type;
    }

    public enum ExperienceEnum {
        COUPON("coupon", "Coupon", "Coupon"),
        ADVERTISEMENT("ad", "Advertisement", "Coupon"),
        LOYALTY_REWARD("loyalty-reward", "Loyalty Reward", "Coupon"),
        MY_FAVORITE_DEALS("mfd", "My Favorite Deals", "Coupon");
        private final String id, name, type;

        ExperienceEnum(String id, String name, String type) {
            this.id = id;
            this.name = name;
            this.type = type;
        }

        public static ExperienceEnum fromValue(String experienceId) {
            return Arrays.stream(ExperienceEnum.values())
                    .filter(e -> e.getId().equalsIgnoreCase(experienceId))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Experience enum %s.", experienceId)));
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }
    }


}
