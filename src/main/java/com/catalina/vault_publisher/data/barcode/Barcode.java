package com.catalina.vault_publisher.data.barcode;

import java.util.Arrays;

public class Barcode {
    Format format;
    String type;

    public Barcode() {
    }

    public Barcode(Format format, String type) {
        this.format = format;
        this.type = type;
    }

    public Format getFormat() {
        return format;
    }

    public String getType() {
        return type;
    }

    public enum BarcodeType {
        UPC_A(""),
        UPC_E(""),
        EAN_8("ean8"),
        EAN_13("ean13"),
        EAN_18("ean18"),
        EAN_128("ean128"),
        GS1_DATABAR_EXPANDED_STACKED(""),
        GS1_DATABAR_NORTH_AMERICAN("");

        private String value;

        BarcodeType(String type) {
            this.value = type;
        }

        public static BarcodeType fromValue(String barcodeType) {
            return Arrays.stream(BarcodeType.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(barcodeType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Barcode Type %s.", barcodeType)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum BarcodeFormatEnum {
        STATIC_BARCODE("static"),
        DYNAMIC_BARCODE("dynamic"),
        MULTIPLE_BARCODE("multipack"),
        DYNAMIC_LINK("link");

        private String format;

        BarcodeFormatEnum(String format) {
            this.format = format;
        }

        public static BarcodeFormatEnum fromValue(String barcodeFormat) {
            return Arrays.stream(BarcodeFormatEnum.values())
                    .filter(e -> e.getFormat().equalsIgnoreCase(barcodeFormat))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Barcode Format %s.", barcodeFormat)));
        }

        public String getFormat() {
            return format;
        }
    }
}
