package com.catalina.vault_publisher.data.barcode.element;

import com.catalina.vault_publisher.data.barcode.element.BarcodeElement.BarcodeElementEnum;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class BarcodeElementService {

    public BarcodeElementService() {
    }

    public BarcodeElement createCurrencyCodeElement(String currencyCode) {
        return new BarcodeElement(BarcodeElementEnum.CURRENCY_CODE.getLabel().replaceAll(" ", ""),
                "9999",
                false,
                UUID.randomUUID().toString(),
                BarcodeElementEnum.CURRENCY_CODE.getLabel(),
                4,
                BarcodeElementEnum.CURRENCY_CODE.getType(),
                currencyCode);
    }

    public BarcodeElement createOfferCodeElement(String offerCode) {
        return new BarcodeElement(null,
                "99999",
                false,
                UUID.randomUUID().toString(),
                BarcodeElementEnum.OFFER_CODE.getLabel(),
                4,
                BarcodeElementEnum.OFFER_CODE.getType(),
                offerCode);
    }

    public BarcodeElement createTriggerClassElement(String triggerClass) {
        return new BarcodeElement(null,
                "9",
                false,
                UUID.randomUUID().toString(),
                BarcodeElementEnum.TRIGGER_CLASS.getLabel(),
                1,
                BarcodeElementEnum.TRIGGER_CLASS.getType(),
                triggerClass);
    }

    public BarcodeElement createValueCodeElement(String valueCode) {
        return new BarcodeElement(BarcodeElementEnum.VALUE_CODE.getLabel().replaceAll(" ", ""),
                "999",
                false, UUID.randomUUID().toString(),
                BarcodeElementEnum.VALUE_CODE.getLabel(),
                3,
                BarcodeElementEnum.VALUE_CODE.getType(),
                valueCode);
    }

    public List<BarcodeElement> createBarcodeElementListForManuf(String currencyCode,
                                                                 String offerCode,
                                                                 String triggerClass,
                                                                 String valueCode) {
        BarcodeElement currencyCodeElement = createCurrencyCodeElement(currencyCode);
        BarcodeElement offerCodeElement = createOfferCodeElement(offerCode);
        BarcodeElement triggerClassElement = createTriggerClassElement(triggerClass);
        BarcodeElement valueCodeElement = createValueCodeElement(valueCode);
        return Arrays.asList(currencyCodeElement, offerCodeElement, triggerClassElement, valueCodeElement);
    }
}
