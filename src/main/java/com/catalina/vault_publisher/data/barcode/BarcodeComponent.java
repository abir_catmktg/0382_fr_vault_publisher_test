package com.catalina.vault_publisher.data.barcode;

import java.util.List;

public class BarcodeComponent {

    public List<ValueCodes> value_codes;

    public BarcodeComponent() {
    }

    public List<ValueCodes> getValue_codes() {
        return value_codes;
    }

    public static class ValueCodes {
        public String value_code;
        public String currency_code;
        public String country_code;
        public List networks_ids;

        public String getValue_code() {
            return value_code;
        }

        public String getCurrency_code() {
            return currency_code;
        }
    }
}