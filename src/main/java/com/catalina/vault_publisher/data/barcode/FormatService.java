package com.catalina.vault_publisher.data.barcode;

import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeFormatEnum;
import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeType;
import com.catalina.vault_publisher.data.barcode.element.BarcodeElement;
import com.catalina.vault_publisher.data.barcode.element.BarcodeElementService;
import com.catalina.vault_publisher.data.barcode.format.BarcodeFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FormatService {

    private static final String PREVIEW_URL_PATTERN = "/barcodes/png?bcid=%s&text=%s&includetext=true&parsefnc=true";

    public FormatService() {
    }

    public Format setupFormatManuf(String valueCode, String currencyCode) {

        BarcodeElementService barcodeElementService = new BarcodeElementService();

        BarcodeElement currencyCodeObj = barcodeElementService.createCurrencyCodeElement(currencyCode);
        BarcodeElement offerCodeObj = barcodeElementService.createOfferCodeElement("9999");
        BarcodeElement triggerClassObj = barcodeElementService.createTriggerClassElement("9");
        BarcodeElement valueCodeObj = barcodeElementService.createValueCodeElement(valueCode);

        List<BarcodeElement> elements = Arrays.asList(currencyCodeObj, offerCodeObj, triggerClassObj, valueCodeObj);

        return createDynamicFormat(elements, BarcodeType.EAN_13);
    }

    public Format setupFormatMinimalPromotion() {
        return createDynamicFormat(new ArrayList<>(), BarcodeType.EAN_13);
    }

    private Format createFormat(List<BarcodeElement> elements,
                                List<BarcodeFormat> formats,
                                String previewURL,
                                String serial_number,
                                BarcodeFormatEnum formatEnum,
                                String value) {
        return new Format(elements, formats, previewURL, serial_number, formatEnum.getFormat(), value);
    }

    public Format createStaticFormat(BarcodeType type, String barcodeStr) {
        String previewUrl = String.format(PREVIEW_URL_PATTERN, type.getValue(), barcodeStr);
        return createFormat(new ArrayList<>(), new ArrayList<>(), previewUrl, null, BarcodeFormatEnum.STATIC_BARCODE, barcodeStr);
    }

    public Format createDynamicFormat(List<BarcodeElement> elements, Barcode.BarcodeType type) {
        String barcodeStr = getBarcodeFromElements(elements);
        String previewUrl = String.format(PREVIEW_URL_PATTERN, type.getValue(), barcodeStr);
        return createFormat(elements, new ArrayList<>(), previewUrl, null, BarcodeFormatEnum.DYNAMIC_BARCODE, null);
    }

    public Format createMultipleFormat(List<BarcodeFormat> formats) {
        return createFormat(new ArrayList<>(), formats, null, null, BarcodeFormatEnum.MULTIPLE_BARCODE, null);
    }

    private String getBarcodeFromElements(List<BarcodeElement> elements) {
        StringBuffer buf = new StringBuffer();
        for (BarcodeElement currentElement : elements) {
            buf.append(currentElement.getValue());
        }
        String barcodeStr = buf.toString();
        return barcodeStr;
    }
}
