package com.catalina.vault_publisher.data.barcode;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeType;
import com.catalina.vault_publisher.data.barcode.element.BarcodeElement;
import com.catalina.vault_publisher.data.barcode.element.BarcodeElementService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;

import java.util.List;

public class BarcodeService {
    private CRestTemplate cRestTemplate;
    private FormatService formatService;
    private BarcodeComponentService barcodeComponentService;
    private BarcodeElementService barcodeElementService;

    public BarcodeService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.formatService = new FormatService();
        this.barcodeComponentService = new BarcodeComponentService(cRestTemplate);
        this.barcodeElementService = new BarcodeElementService();
    }

    public Barcode setupBarcodeMinimalPromotion() {

        Format format = new FormatService().setupFormatMinimalPromotion();
        return new Barcode(format, "ean13");
    }

    private Barcode createBarcode(Format format, BarcodeType type) {
        return new Barcode(format, type.getValue());
    }

    public Barcode createStaticBarcode(BarcodeType type, String barcodeStr) {
        Format format = formatService.createStaticFormat(type, barcodeStr);
        return createBarcode(format, type);
    }

    public Barcode createDynamicBarcode(BarcodeType type, List<BarcodeElement> elements) {
        Format format = formatService.createDynamicFormat(elements, type);
        return createBarcode(format, type);
    }

    public Barcode createBarcodeManuf(Config.Mode mode, String saveLine) throws HttpResponseStatusException {
        BarcodeComponent barcodeComponent = barcodeComponentService.getBarcodeElements(mode, saveLine);

        List<BarcodeElement> barcodeElements = barcodeElementService.createBarcodeElementListForManuf(
                barcodeComponent.value_codes.get(0).getCurrency_code(),
                "9999",
                "9",
                barcodeComponent.value_codes.get(0).getValue_code());

        return createDynamicBarcode(BarcodeType.EAN_13, barcodeElements);

    }

}
