package com.catalina.vault_publisher.data.barcode;

import com.catalina.vault_publisher.data.barcode.element.BarcodeElement;
import com.catalina.vault_publisher.data.barcode.format.BarcodeFormat;

import java.util.List;

public class Format {
    private List<BarcodeElement> elements;
    private List<BarcodeFormat> formats;
    private String preview_url;
    private String serial_number;
    private String type;
    private String value;

    public Format() {
    }

    public Format(List<BarcodeElement> elements, List<BarcodeFormat> formats, String preview_url, String serial_number, String type, String value) {
        this.elements = elements;
        this.formats = formats;
        this.preview_url = preview_url;
        this.serial_number = serial_number;
        this.type = type;
        this.value = value;
    }

    public List<BarcodeElement> getElements() {
        return elements;
    }

    public List<BarcodeFormat> getFormats() {
        return formats;
    }

    public String getPreview_url() {
        return preview_url;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
