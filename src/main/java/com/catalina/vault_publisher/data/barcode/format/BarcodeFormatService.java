package com.catalina.vault_publisher.data.barcode.format;

import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeFormatEnum;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;

import java.util.List;
import java.util.UUID;

public class BarcodeFormatService {

    public BarcodeFormatService() {
    }


    public BarcodeFormat createBarcodeFormat(List<TouchPoint> include, List<TouchPoint> exclude, String barcodeStr) {
        Targeting targeting = new Targeting(include, exclude);
        return new BarcodeFormat(UUID.randomUUID().toString(), targeting, BarcodeFormatEnum.MULTIPLE_BARCODE.getFormat(), barcodeStr);
    }
}
