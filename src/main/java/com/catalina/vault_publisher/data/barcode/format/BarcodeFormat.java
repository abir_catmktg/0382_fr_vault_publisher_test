package com.catalina.vault_publisher.data.barcode.format;

import com.catalina.vault_publisher.data.targeting.Targeting;

public class BarcodeFormat {

    private String id;
    private Targeting targeting;
    private String type;
    private String value;

    public BarcodeFormat() {
    }

    public BarcodeFormat(String id, Targeting targeting, String type, String value) {
        this.id = id;
        this.targeting = targeting;
        this.type = type;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
