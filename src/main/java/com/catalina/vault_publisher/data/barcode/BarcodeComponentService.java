package com.catalina.vault_publisher.data.barcode;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BarcodeComponentService {
    private static final Logger logger = LogManager.getLogger(BarcodeComponentService.class);
    private CRestTemplate cRestTemplate;

    public BarcodeComponentService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) {
        BarcodeComponent barcodeComponent = null;
        try {
            barcodeComponent = new BarcodeComponentService(
                    new CRestTemplate()).getBarcodeElements(Config.Mode.PROD, "0,60");
        } catch (HttpResponseStatusException e) {
            logger.error(e);
        }

        System.out.println(barcodeComponent.value_codes.get(0).value_code);
        System.out.println(barcodeComponent.value_codes.get(0).currency_code);
    }

    public BarcodeComponent getBarcodeElements(Config.Mode mode, String saveLine) throws HttpResponseStatusException {
        String url = mode.dmpUrl + "/barcode/discount/" + saveLine + " €/codes/FRA";
        logger.debug(String.format("Save Line value : %s", saveLine));
        if (saveLine == null || saveLine.isEmpty()) return null;

        BarcodeComponent response = cRestTemplate.executeGetRequestWithJsonHeader(url, BarcodeComponent.class);
        logger.debug(Config.toJsString(response));
        return response;
    }
}
