package com.catalina.vault_publisher.data.barcode.element;

import java.util.Arrays;

public class BarcodeElement {
    private String codeType;
    private String example;
    private Boolean focus;
    private String id;
    private String label;
    private int pad;
    private String type;
    private String value;

    public BarcodeElement() {
    }

    public BarcodeElement(String codeType, String example, Boolean focus, String id, String label, int pad, String type, String value) {
        this.codeType = codeType;
        this.example = example;
        this.focus = focus;
        this.id = id;
        this.label = label;
        this.pad = pad;
        this.type = type;
        this.value = value;
    }

    public String getCodeType() {
        return codeType;
    }

    public Boolean getFocus() {
        return focus;
    }

    public String getExample() {
        return example;
    }

    public boolean isFocus() {
        return focus;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public int getPad() {
        return pad;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public enum BarcodeElementEnum {
        CURRENCY_CODE("Currency Code", "currency_code"),
        OFFER_CODE("Offer Code", "offer_code"),
        TRIGGER_CLASS("Trigger #", "trigger_class"),
        VALUE_CODE("Value Code", "value_code");

        private final String label, type;

        BarcodeElementEnum(String label, String type) {
            this.label = label;
            this.type = type;
        }

        public static BarcodeElementEnum fromValue(String barcodeElementType) {
            return Arrays.stream(BarcodeElementEnum.values())
                    .filter(e -> e.getType().equalsIgnoreCase(barcodeElementType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Barcode Element Type %s.", barcodeElementType)));
        }

        public String getLabel() {
            return label;
        }

        public String getType() {
            return type;
        }
    }
}
