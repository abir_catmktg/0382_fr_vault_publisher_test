package com.catalina.vault_publisher.data.ratio;

import com.catalina.vault_publisher.data.ratio.Ratio.RatioScope;

import java.util.ArrayList;
import java.util.List;

public class RatioService {

    public RatioService() {
    }

    public Ratio createRatio(RatioScope scope, List<Double> value) {

        if (value.size() == 0) return new Ratio(scope.getValue(), new ArrayList<>());

        List<Ratio.Value> valueList = new ArrayList<>();
        for (Double currentDoubleValue : value) valueList.add(new Ratio.Value(String.valueOf(currentDoubleValue)));
        return new Ratio(scope.getValue(), valueList);
    }
}
