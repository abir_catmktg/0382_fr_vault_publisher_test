package com.catalina.vault_publisher.data.ratio;

import java.util.Arrays;
import java.util.List;

public class Ratio {
    private String scope;
    private List<Value> values;

    public Ratio() {
    }

    public Ratio(String scope, List<Value> values) {
        this.scope = scope;
        this.values = values;
    }

    public String getScope() {
        return scope;
    }

    public List<Value> getValues() {
        return values;
    }

    public enum RatioScope {
        TOUCHPOINT("touchpoint"),
        NETWORK("network");

        private final String value;

        RatioScope(String value) {
            this.value = value;
        }

        public static RatioScope fromValues(String scope) {
            return Arrays.asList(RatioScope.values()).stream()
                    .filter(x -> x.getValue().equalsIgnoreCase(scope))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported RatioScope : %s", scope)));
        }

        public String getValue() {
            return value;
        }
    }

    protected static class Value {
        private String value;

        public Value() {
        }

        public Value(String value) {
            this.value = value;
        }
    }
}
