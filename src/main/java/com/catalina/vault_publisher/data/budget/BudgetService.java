package com.catalina.vault_publisher.data.budget;

import com.catalina.vault_publisher.data.capping.CappingService;
import com.catalina.vault_publisher.data.capping.DistributionCap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class BudgetService {

    private static final Logger logger = LogManager.getLogger(BudgetService.class);

    public BudgetService() {
    }

    public Budget setBudgetManuf(String count) {

        if (count == null || count.isEmpty()) {
            logger.debug(String.format("count value is empty or null: %s", count));
            return new Budget(false);
        }

        try {
            Integer.parseInt(count);
            DistributionCap cap = new CappingService().setupDistributionCappingManuf(count);
            return new Budget(Arrays.asList(cap), true);
        } catch (NumberFormatException nfe) {
            logger.error(String.format("%s could not be cast as an Integer : %s", count, nfe));
            return new Budget(false);
        }
    }
}
