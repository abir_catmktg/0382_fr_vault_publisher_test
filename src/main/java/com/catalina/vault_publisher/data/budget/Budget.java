package com.catalina.vault_publisher.data.budget;

import com.catalina.vault_publisher.data.capping.DistributionCap;

import java.util.ArrayList;
import java.util.List;

public class Budget {
    private List<DistributionCap> caps;
    private boolean override;

    public Budget() {
    }

    public Budget(List<DistributionCap> caps, boolean override) {
        this.caps = caps;
        this.override = override;
    }

    public Budget(boolean override) {
        this.caps = new ArrayList<>();
        this.override = override;
    }

    public List<DistributionCap> getCaps() {
        return caps;
    }

    public void setCaps(List<DistributionCap> caps) {
        this.caps = caps;
    }

    public boolean isOverride() {
        return override;
    }

    public void setOverride(boolean override) {
        this.override = override;
    }
}
