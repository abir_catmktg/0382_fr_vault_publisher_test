package com.catalina.vault_publisher.data.classification;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.classification.Classification.ClassificationName;
import com.catalina.vault_publisher.data.classification.variable.ClassificationVariable;
import com.catalina.vault_publisher.data.experience.Experience;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassificationService {
    private static final Logger logger = LogManager.getLogger(ClassificationService.class);
    private CRestTemplate cRestTemplate;


    public ClassificationService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        System.out.println(ClassificationName.GENERIC_COUPON_AD.getValue());
        new ClassificationService(new CRestTemplate()).getClassification(Config.Mode.PROD, ClassificationName.BUY_N_SAVE_X_DOLLARS);
    }

    public Classification getClassification(Config.Mode mode, Classification.ClassificationName classification) throws HttpResponseStatusException {
        String url = String.format("%s/discounts?fq={fq}", mode.urlDashboard);
        String fqParam = String.format("{\"name\":\"%s\"}", classification.getValue());

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fq: %s", fqParam));

        List<Classification> classificationList =
                cRestTemplate.executeSolrGetRequestWithJsonHeader(url, Classification.class, fqParam);

        if (classificationList.size() != 1) throw new RuntimeException(
                String.format("%s classification(s) found with the name: %s",
                        classificationList.size(),
                        classification.getValue()));

        return classificationList.get(0);
    }

    private Classification createClassification(String date_modified,
                                                String id,
                                                Classification.ClassificationName name,
                                                String reward,
                                                String risk,
                                                String scope,
                                                Experience.ExperienceEnum type,
                                                List<ClassificationVariable> variables) {
        Classification classification = new Classification(date_modified,
                id,
                name.getValue(),
                reward,
                risk,
                scope,
                type.getName(),
                variables);
        return classification;
    }

    public Classification createGenericCouponAdClassification() {
        return createClassification("2017-03-29T19:43:18.236Z",
                "99",
                ClassificationName.GENERIC_COUPON_AD,
                "Free",
                "High",
                "Item",
                Experience.ExperienceEnum.COUPON,
                new ArrayList<>());
    }

    public Classification createSaveXPercentClassification() {
        ClassificationVariable variable1 = new ClassificationVariable("discount_amount_off", true);
        ClassificationVariable variable2 = new ClassificationVariable("verbiage1", true);

        return new Classification("2017-03-29T19:43:17.773Z",
                "4",
                ClassificationName.BUY_N_SAVE_X_PERCENT.getValue(),
                "Percent",
                "Low",
                "Item",
                Experience.ExperienceEnum.COUPON.getName(),
                Arrays.asList(variable1, variable2));
    }

    public Classification createSpendXSaveYClassification() {
        ClassificationVariable variable1 = new ClassificationVariable("discount_amount_off", true);
        ClassificationVariable variable2 = new ClassificationVariable("verbiage1", true);

        return createClassification("2017-03-29T19:43:17.915Z",
                "7",
                ClassificationName.SPEND_X_SAVE_Y,
                "Monetary",
                "High",
                "Order",
                Experience.ExperienceEnum.COUPON,
                Arrays.asList(variable1, variable2));
    }


}
