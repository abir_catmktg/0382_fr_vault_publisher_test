package com.catalina.vault_publisher.data.classification.variable;

public class ClassificationVariable {
    private String name;
    private Boolean required;

    public ClassificationVariable() {
    }

    public ClassificationVariable(String name, Boolean required) {
        this.name = name;
        this.required = required;
    }

    public String getName() {
        return name;
    }

    public Boolean getRequired() {
        return required;
    }
}
