package com.catalina.vault_publisher.data.classification;

import com.catalina.vault_publisher.data.classification.variable.ClassificationVariable;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Classification {
    public String date_modified;
    public String id;
    public String name;
    public String reward;
    public String risk;
    public String scope;
    public String type;
    public List<ClassificationVariable> variables;

    public Classification() {
    }

    public Classification(String date_modified, String id, String name, String reward, String risk, String scope, String type, List<ClassificationVariable> variables) {
        this.date_modified = date_modified;
        this.id = id;
        this.name = name;
        this.reward = reward;
        this.risk = risk;
        this.scope = scope;
        this.type = type;
        this.variables = variables;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getReward() {
        return reward;
    }

    public String getRisk() {
        return risk;
    }

    public String getScope() {
        return scope;
    }

    public String getType() {
        return type;
    }

    public List<ClassificationVariable> getVariables() {
        return variables;
    }

    @Override
    public int hashCode() {
        return Integer.parseInt(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Classification that = (Classification) o;
        return Objects.equals(date_modified, that.date_modified) &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(reward, that.reward) &&
                Objects.equals(risk, that.risk) &&
                Objects.equals(scope, that.scope) &&
                Objects.equals(type, that.type);
    }

    public enum ClassificationName {
        BUY_N_SAVE_X_DOLLARS("Buy N/Save $X"),
        BUY_N_GET_N_FREE("Buy N/Get N Free"),
        TAKE_X_OFF_N_ITEM("Take $X off (N) item"),
        SAVE_X_NEXT_ORDER("Save $X/Next Order"),
        BUY_N_SAVE_X_PERCENT("Buy N/Save X%"),
        SPEND_X_SAVE_Y("Spend $X/Save $Y"),
        GENERIC_COUPON_AD("Generic Coupon/Ad"),
        EARN_N_POINTS_GET_N_MORE_POINTS("Earn N Points/Get N More Points"),
        MFD("MFD Discount"),
        MESSAGE("Message"),
        BUY_N_FOR_Y_DOLLARS("Buy N/For $S"),
        SPEND_X_GET_N_POINTS("Spend $X/Get n Points"),
        BUY_N_GET_N_POINTS("Buy N/Get n Points");

        private final String value;

        ClassificationName(String value) {
            this.value = value;
        }

        public static ClassificationName fromValue(String classificationName) {
            return Arrays.stream(ClassificationName.values())
                    .filter(x -> x.getValue().equalsIgnoreCase(classificationName))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Classification Name %s.", classificationName)));
        }

        public String getValue() {
            return value;
        }
    }
}
