package com.catalina.vault_publisher.data.targeting;

public class ConfigurationService {

    public ConfigurationService() {
    }

    public Configuration setupConfigurationTrigger() {
        return new Configuration("Please ensure you at least have a core trigger condition if you are not using trigger groups.",
                "No Trigger Groups Defined",
                "Trigger Group",
                "Trigger Groups");
    }

    public Configuration setupConfigurationReward() {
        return new Configuration("This advertisement has no redemption requirements defined - this will prevent analytic measurement as well as digital clearing.",
                "No Redemption Requirements",
                "redemption requirement",
                "redemption requirements");
    }
}
