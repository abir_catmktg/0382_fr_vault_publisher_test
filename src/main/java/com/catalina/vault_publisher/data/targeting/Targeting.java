package com.catalina.vault_publisher.data.targeting;

import com.catalina.vault_publisher.data.Locations;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.Touchpoints;
import com.catalina.vault_publisher.data.touchpoint.network.Network;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Targeting {

    private Audiences audiences;
    private String description;
    private Locations locations;
    private Networks networks;
    private Schedule schedule;
    private Touchpoints touchpoints;

    public Targeting() {
    }

    public Targeting(List<TouchPoint> includedTouchpoints, List<TouchPoint> excludedTouchpoints) {
        this.audiences = new Audiences(false);
        this.locations = new Locations(false);
        this.schedule = new Schedule(false);
        touchpoints = new Touchpoints(includedTouchpoints, excludedTouchpoints);
    }

    public Targeting(boolean bool) {
        this.audiences = new Audiences(bool);
        this.locations = new Locations(bool);
        this.networks = new Networks(bool);
        this.schedule = new Schedule(bool);
        this.touchpoints = new Touchpoints(bool);
    }

    public Targeting(String target_description, boolean bool) {
        this.audiences = new Audiences(bool);
        this.description = target_description;
        this.locations = new Locations(bool);
        this.networks = new Networks(bool);
        this.schedule = new Schedule(bool);
        this.touchpoints = new Touchpoints(bool);
    }

    public Targeting(Schedule schedule, boolean bool) {
        this.audiences = new Audiences(bool);
        this.locations = new Locations(bool);
        this.networks = new Networks(bool);
        this.schedule = schedule;
        this.touchpoints = new Touchpoints(bool);
    }

    public Audiences getAudiences() {
        return audiences;
    }

    public Locations getLocations() {
        return locations;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public Touchpoints getTouchpoints() {
        return touchpoints;
    }

    public void setTouchpoints(Touchpoints touchpoints) {
        this.touchpoints = touchpoints;
    }

    public static class Audiences {
        private List<LinkedHashMap> exclude;
        private List<LinkedHashMap> include;
        private boolean override;

        public Audiences() {
            this.exclude = new ArrayList<>();
            this.include = new ArrayList<>();
        }

        public Audiences(boolean override) {
            this.exclude = new ArrayList<>();
            this.include = new ArrayList<>();
            this.override = override;
        }
    }

    public static class Networks {
        private List<Network> exclude;
        private List<Network> include;
        private boolean override;

        public Networks() {
            this.exclude = new ArrayList<>();
            this.include = new ArrayList<>();
        }

        public Networks(boolean override) {
            this.exclude = new ArrayList<>();
            this.include = new ArrayList<>();
            this.override = override;
        }
    }


}

