package com.catalina.vault_publisher.data.targeting;

public class Configuration {
    private String empty_text;
    private String empty_title;
    private String entity_name;
    private String entity_name_plural;

    public Configuration() {
    }

    public Configuration(String empty_text, String empty_title, String entity_name, String entity_name_plural) {
        this.empty_text = empty_text;
        this.empty_title = empty_title;
        this.entity_name = entity_name;
        this.entity_name_plural = entity_name_plural;
    }
}
