package com.catalina.vault_publisher.data.targeting.schedule;

import java.time.LocalTime;

public class ScheduleObjectService {

    public ScheduleObjectService() {
    }

    public static void main(String[] args) {
        new ScheduleObjectService().createScheduleObject(ScheduleObject.DayEnum.FRIDAY, "11:12", "12:45");
        new ScheduleObjectService().createScheduleObject(ScheduleObject.DayEnum.FRIDAY, "60:102", "hh:R6");
    }

    public ScheduleObject createScheduleObject(ScheduleObject.DayEnum dayEnum, String startTime, String stopTime) {

        LocalTime t1 = LocalTime.parse(startTime);
        LocalTime t2 = LocalTime.parse(stopTime);

        return new ScheduleObject(dayEnum.getValue(), startTime, stopTime);
    }
}
