package com.catalina.vault_publisher.data.targeting.schedule;

import java.util.List;

public class ScheduleService {

    public ScheduleService() {
    }

    public Schedule createSchedule(List<ScheduleObject> include, List<ScheduleObject> exclude) {
        return new Schedule(include, exclude, false);
    }
}
