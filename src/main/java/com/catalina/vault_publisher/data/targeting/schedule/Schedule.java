package com.catalina.vault_publisher.data.targeting.schedule;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Schedule {
    public List<ScheduleObject> exclude;
    public List<ScheduleObject> include;
    public boolean override;


    public Schedule() {
    }

    public Schedule(List<ScheduleObject> exclude, List<ScheduleObject> include, boolean override) {
        this.exclude = exclude;
        this.include = include;
        this.override = override;
    }

    public Schedule(boolean override) {
        this.exclude = new ArrayList<>();
        this.include = new ArrayList<>();
        this.override = override;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //implement List comparaison using HashSet
        Schedule schedule = (Schedule) o;
        return override == schedule.override &&
                Objects.equals(exclude, schedule.exclude) &&
                Objects.equals(include, schedule.include);
    }
}
