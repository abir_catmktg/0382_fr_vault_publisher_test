package com.catalina.vault_publisher.data.targeting.schedule;

import java.util.Arrays;

public class ScheduleObject {
    private String day;
    private String start;
    private String stop;

    public ScheduleObject() {
    }

    public ScheduleObject(String day, String start, String stop) {
        this.day = day;
        this.start = start;
        this.stop = stop;
    }

    public String getDay() {
        return day;
    }

    public String getStart() {
        return start;
    }

    public String getStop() {
        return stop;
    }

    public enum DayEnum {
        EVERYDAY("Everyday"),
        MONDAY("Monday"),
        TUESDAY("Tuesday"),
        WEDNESDAY("Wednesday"),
        THURSDAY("Thursday"),
        FRIDAY("Friday"),
        SATURDAY("Saturday"),
        SUNDAY("Sunday"),
        WEEKDAYS("Weekdays"),
        WEEKENDS("Weekends");

        private final String value;

        DayEnum(String value) {
            this.value = value;
        }

        public static DayEnum fromValue(String scheduleDay) {
            return Arrays.stream(DayEnum.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(scheduleDay))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported DayEnum: %s", scheduleDay)));
        }

        public String getValue() {
            return value;
        }
    }
}
