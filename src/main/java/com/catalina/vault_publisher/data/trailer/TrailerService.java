package com.catalina.vault_publisher.data.trailer;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;

import java.util.ArrayList;
import java.util.List;

public class TrailerService {

    public TrailerService() {
    }

    public List<Trailer> setTrailerManuf(String colorTrailerMessage, String bwTrailerMessage) {
        List<Trailer> trailerList = new ArrayList<Trailer>();
        if (colorTrailerMessage != null) {
            trailerList.add(new Trailer(ImageDAM.PrinterLevel.COLOR, colorTrailerMessage, false, true));
        }

        if (bwTrailerMessage != null) {
            trailerList.add(new Trailer(ImageDAM.PrinterLevel.BW, bwTrailerMessage, true, false));
        }

        return trailerList;
    }
}
