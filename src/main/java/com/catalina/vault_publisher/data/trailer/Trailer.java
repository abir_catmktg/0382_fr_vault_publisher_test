package com.catalina.vault_publisher.data.trailer;

import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.targeting.Targeting;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Trailer {
    private String id;                                  //"VAR_ID_TRAILER"
    private List<String> printer_levels;                   //["BLACK/WHITE"] || ["COLOR"]
    private Targeting targeting;
    private String text;                                //"VAR_ID_TRAILER"
    private boolean checked_bw;
    private boolean checked_color;

    public Trailer() {
    }

    public Trailer(ImageDAM.PrinterLevel printerLevel, String trailerMessage, boolean checked_bw, boolean checked_color) {
        this.id = UUID.randomUUID().toString();
        this.printer_levels = Collections.singletonList(printerLevel.toString());
        this.targeting = new Targeting(false);
        this.text = trailerMessage;
        this.checked_bw = checked_bw;
        this.checked_color = checked_color;
    }

    public String getId() {
        return id;
    }

    public List<String> getPrinter_levels() {
        return printer_levels;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public String getText() {
        return text;
    }

    public boolean isChecked_bw() {
        return checked_bw;
    }

    public boolean isChecked_color() {
        return checked_color;
    }
}
