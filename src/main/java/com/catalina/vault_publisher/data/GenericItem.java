package com.catalina.vault_publisher.data;

public class GenericItem {
    private String id;
    private String name;

    public GenericItem() {
    }

    public GenericItem(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
