package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.data.History;
import com.catalina.vault_publisher.data.Locations;
import com.catalina.vault_publisher.data.billing.BillingCampaign;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.Touchpoints;
import com.catalina.vault_publisher.utils.Constants;
import com.google.gson.JsonNull;
import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Campaign extends AbstractOffer {

    private final static JsonNull account = JsonNull.INSTANCE;
    @Expose
    private BillingCampaign billing;
    @Expose
    private Client client;
    private String classification;
    private String country_code;
    private Boolean favorite;
    private Boolean ignore_long_term_controls;
    private Locations locations;
    private List<Object> networks;
    private List<Object> publication_errors;
    private List<Object> publication_warnings;
    private Schedule schedule;
    private Touchpoints touchpoints;
    private String type;


    public Campaign() {
    }

    public Campaign(String id, String name) {
        super(id, name);
    }

    public Campaign(List<String> channels,
                    History history,
                    String name,
                    String status,
                    String date_modified,
                    String country_code,
                    boolean favorite,
                    boolean ignore_long_term_controls,
                    Locations locations,
                    List<Object> networks,
                    List<Object> publication_errors,
                    List<Object> publication_warnings,
                    Schedule schedule,
                    Touchpoints touchpoints,
                    String type) {
        super(channels, history, name, status, date_modified);
        this.country_code = country_code;
        this.favorite = favorite;
        this.ignore_long_term_controls = ignore_long_term_controls;
        this.locations = locations;
        this.networks = networks;
        this.publication_errors = publication_errors;
        this.publication_warnings = publication_warnings;
        this.schedule = schedule;
        this.touchpoints = touchpoints;
        this.type = type;
    }

    public Campaign(String name, BillingCampaign billing, Client client, String classification, boolean ignore_long_term_controls, String country_code, List<TouchPoint> includedTouchpoints, List<TouchPoint> excludedTouchpoints, String type, String created_by) {
        super(name, Constants.STATUS_DRAFT, includedTouchpoints, excludedTouchpoints, created_by);
        this.country_code = country_code;
        this.billing = billing;
        this.client = client;
        this.classification = classification;
        this.favorite = false;
        this.ignore_long_term_controls = ignore_long_term_controls;
        this.locations = new Locations(false);
        this.networks = new ArrayList<>();
        this.publication_errors = new ArrayList<>();
        this.publication_warnings = new ArrayList<>();
        this.schedule = new Schedule(false);
        this.touchpoints = new Touchpoints(false);
        this.type = type;
    }

    public JsonNull getAccount() {
        return account;
    }

    public BillingCampaign getBilling() {
        return billing;
    }

    public void setBilling(BillingCampaign billing) {
        this.billing = billing;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isIgnore_long_term_controls() {
        return ignore_long_term_controls;
    }

    public void setIgnore_long_term_controls(boolean ignore_long_term_controls) {
        this.ignore_long_term_controls = ignore_long_term_controls;
    }

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public List<Object> getNetworks() {
        return networks;
    }

    public void setNetworks(List<Object> networks) {
        this.networks = networks;
    }

    public List<Object> getPublication_errors() {
        return publication_errors;
    }

    public void setPublication_errors(List<Object> publication_errors) {
        this.publication_errors = publication_errors;
    }

    public List<Object> getPublication_warnings() {
        return publication_warnings;
    }

    public void setPublication_warnings(List<Object> publication_warnings) {
        this.publication_warnings = publication_warnings;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public Touchpoints getTouchpoints() {
        return touchpoints;
    }

    public void setTouchpoints(Touchpoints touchpoints) {
        this.touchpoints = touchpoints;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //implement List comparaison using HashSet
        Campaign campaign = (Campaign) o;
        return favorite == campaign.favorite &&
                ignore_long_term_controls == campaign.ignore_long_term_controls &&
                Objects.equals(account, campaign.account) &&
                Objects.equals(billing, campaign.billing) &&
                Objects.equals(client, campaign.client) &&
                Objects.equals(classification, campaign.classification) &&
                Objects.equals(country_code, campaign.country_code) &&
                Objects.equals(locations, campaign.locations) &&
                Objects.equals(networks, campaign.networks) &&
                Objects.equals(publication_errors, campaign.publication_errors) &&
                Objects.equals(publication_warnings, campaign.publication_warnings) &&
                Objects.equals(schedule, campaign.schedule) &&
                Objects.equals(touchpoints, campaign.touchpoints) &&
                Objects.equals(type, campaign.type);
    }
}
