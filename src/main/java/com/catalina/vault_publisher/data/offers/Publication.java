package com.catalina.vault_publisher.data.offers;

import java.util.List;

public class Publication {
    private String uuid;
    private Network network;
    private String publisher_id;
    private String publish_date;
    private String response_date;
    private List<String> channels;
    private List<String> comments;
    private String external_id;
    private String external_system;
    private String status;
    private List<String> destinations;
    private Metadata metadata;


    private static class Network {
        private String id;
        private String name;
        private String country_code;

        public Network() {
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getCountry_code() {
            return country_code;
        }
    }

    private static class Metadata {
        private String mclu_id;
        private String award_id;

        public Metadata() {
        }

        public String getMclu_id() {
            return mclu_id;
        }

        public String getAward_id() {
            return award_id;
        }
    }
}
