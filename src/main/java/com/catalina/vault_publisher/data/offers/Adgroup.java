package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.data.AdGroupLimit;
import com.catalina.vault_publisher.data.AdGroupRules;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.experience.AdGroupExperience;
import com.catalina.vault_publisher.data.measurement.AdGroupMeasurement;
import com.catalina.vault_publisher.utils.Constants;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Adgroup extends AbstractOffer {
    private String ad_rotation_method;
    private Budget budget;
    private ParentOffer campaign;
    private String delivery_method;
    private AdGroupExperience experience;
    private List<FrequencyCap> frequency_caps;
    private AdGroupMeasurement measurement;
    private String roadblocking;
    private AdGroupRules rules;

    public Adgroup() {
    }

    public Adgroup(String id, String name) {
        super(id, name);
    }

    public Adgroup(String campaignId,
                   String campaignName,
                   String adGroupName,
                   String adRotationMethod,
                   String deliveryMethod,
                   List<FrequencyCap> capsList,
                   String roadBlockingMethod,
                   String created_by) {
        super(adGroupName, Constants.STATUS_DRAFT, created_by);
        this.ad_rotation_method = adRotationMethod;
        this.budget = new Budget(false);
        this.campaign = new ParentOffer(campaignId, campaignName);
        this.delivery_method = deliveryMethod;
        this.experience = new AdGroupExperience();
        this.frequency_caps = capsList;
        this.measurement = new AdGroupMeasurement();
        this.roadblocking = roadBlockingMethod;
        this.rules = new AdGroupRules(new AdGroupLimit());

    }

    public Adgroup(String name,
                   String status,
                   String createdBy,
                   String ad_rotation_method,
                   Budget budget,
                   ParentOffer campaign,
                   String delivery_method,
                   AdGroupExperience experience,
                   List<FrequencyCap> frequency_caps,
                   AdGroupMeasurement measurement,
                   String roadblocking,
                   AdGroupRules rules) {
        super(name, status, createdBy);
        this.ad_rotation_method = ad_rotation_method;
        this.budget = budget;
        this.campaign = campaign;
        this.delivery_method = delivery_method;
        this.experience = experience;
        this.frequency_caps = frequency_caps;
        this.measurement = measurement;
        this.roadblocking = roadblocking;
        this.rules = rules;
    }

    public String getAd_rotation_method() {
        return ad_rotation_method;
    }

    public void setAd_rotation_method(String ad_rotation_method) {
        this.ad_rotation_method = ad_rotation_method;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public ParentOffer getCampaign() {
        return campaign;
    }

    public void setCampaign(ParentOffer campaign) {
        this.campaign = campaign;
    }

    public String getDelivery_method() {
        return delivery_method;
    }

    public void setDelivery_method(String delivery_method) {
        this.delivery_method = delivery_method;
    }

    public AdGroupExperience getExperience() {
        return experience;
    }

    public void setExperience(AdGroupExperience experience) {
        this.experience = experience;
    }

    public List<FrequencyCap> getFrequency_caps() {
        return frequency_caps;
    }

    public void setFrequency_caps(List<FrequencyCap> frequency_caps) {
        this.frequency_caps = frequency_caps;
    }

    public AdGroupMeasurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(AdGroupMeasurement measurement) {
        this.measurement = measurement;
    }

    public String getRoadblocking() {
        return roadblocking;
    }

    public void setRoadblocking(String roadblocking) {
        this.roadblocking = roadblocking;
    }

    public AdGroupRules getRules() {
        return rules;
    }

    public void setRules(AdGroupRules rules) {
        this.rules = rules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //implement List comparaison using HashSet
        Adgroup adgroup = (Adgroup) o;
        return Objects.equals(ad_rotation_method, adgroup.ad_rotation_method) &&
                Objects.equals(budget, adgroup.budget) &&
                Objects.equals(campaign, adgroup.campaign) &&
                Objects.equals(delivery_method, adgroup.delivery_method) &&
                Objects.equals(experience, adgroup.experience) &&
                Objects.equals(frequency_caps, adgroup.frequency_caps) &&
                Objects.equals(measurement, adgroup.measurement) &&
                Objects.equals(roadblocking, adgroup.roadblocking) &&
                Objects.equals(rules, adgroup.rules);
    }

    public enum DeliveryMethod {
        STANDARD("STANDARD"),
        ACCELERATED("ACCELERATED"),
        FRONTLOADED("FRONTLOADED");

        private final String value;

        DeliveryMethod(String value) {
            this.value = value;
        }

        public static DeliveryMethod fromValue(String deliveryMethod) {
            return Arrays.stream(DeliveryMethod.values())
                    .filter(e -> e.getValue().equals(deliveryMethod))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(
                            String.format("Unsupported Delivery Method %s.", deliveryMethod)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum AdRotation {
        DEFAULT("DEFAULT"),
        MANUAL("MANUAL"),
        PERCENT("PERCENT"),
        SCORE("SCORE");

        private final String value;

        AdRotation(String value) {
            this.value = value;
        }

        public static AdRotation fromValue(String value) {
            for (AdRotation adRotation : AdRotation.values()) {
                if (value.equals(adRotation.value)) return adRotation;
            }
            return null;
        }

        public String getValue() {
            return value;
        }
    }

    public enum RoadBloacking {
        SINGLE("SINGLE"),
        CONNECTED("CONNECTED");

        private final String value;

        RoadBloacking(String value) {
            this.value = value;
        }

        public static RoadBloacking fromValue(String value) {
            for (RoadBloacking roadBloacking : RoadBloacking.values()) {
                if (value.equals(roadBloacking.value)) return roadBloacking;
            }
            return null;
        }

        public String getValue() {
            return value;
        }
    }

}

