package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.data.AuditHistory;
import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.billing.BillingAd;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.classification.Classification;
import com.catalina.vault_publisher.data.classification.ClassificationService;
import com.catalina.vault_publisher.data.experience.Experience;
import com.catalina.vault_publisher.data.forecasting.DigitalForecasting;
import com.catalina.vault_publisher.data.forecasting.DigitalForecastingService;
import com.catalina.vault_publisher.data.forecasting.Forecasting;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creatives.Creative;
import com.catalina.vault_publisher.data.measurement.Measurement;
import com.catalina.vault_publisher.data.products.Products;
import com.catalina.vault_publisher.data.reporting_attributes.ReportingAttribute;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.*;

public class Ad extends AbstractOffer {
    private String ad_creative_set_id = UUID.randomUUID().toString();
    private ParentOffer adgroup;
    private String adgroup_id;
    private List<AuditHistory> audit_history;
    private BillingAd billing;
    private Budget budget;
    private ParentOffer campaign;
    private String campaign_id;
    private Classification classification;
    private Client client;
    private List<JsonObject> composites;
    private Country country;
    private String country_code;
    private String creative_rotation_method;
    private Set<AbstractVariable> creative_variables;
    private List<Creative> creatives;
    private JsonElement digital;
    private DigitalForecasting digital_forecasting;
    private boolean do_not_sync;
    private boolean durable_mode;
    private Experience experience;
    private String external_id;
    private Forecasting forecasting;
    private Measurement measurement;
    private List<ReportingAttribute> metadata;
    private Products products;
    private List<JsonElement> publications;
    private boolean publish;
    private JsonElement sales_objective;
    private String start_time;
    private String stop_time;
    private String system_id;
    private List<String> templates;

    public Ad() {
    }

    public Ad(String campaignId,
              String campaignName,
              String adGroupId,
              String adGroupName,
              String name,
              String external_id,
              Country.CountryEnum country,
              String start_date,
              String stop_date,
              String created_by,
              String targeting_description) {
        super(name, start_date, stop_date, Constants.STATUS_DRAFT, created_by, targeting_description);
        this.adgroup = new ParentOffer(adGroupId, adGroupName);
        this.adgroup_id = adGroupId;
        //this.billing => Will be done Later
        this.budget = new Budget(false);
        this.campaign = new ParentOffer(campaignId, campaignName);
        this.campaign_id = campaignId;
        this.classification = new ClassificationService(new CRestTemplate()).createGenericCouponAdClassification();
        //this.client => Will be done Later
        this.composites = new ArrayList<>();
        this.country = new Country(country);
        this.country_code = country.getCode();
        this.creative_rotation_method = CREATIVE_ROTATION_METHOD_OPTIMIZED;
        this.creative_variables = new HashSet<>();
        this.creatives = new ArrayList<>();
        this.external_id = external_id;
        this.forecasting = new Forecasting("store", "impression");
        this.measurement = new Measurement();
        this.digital = new JsonObject();
        this.digital_forecasting = new DigitalForecastingService().setupDigitalForcastingManuf();
        this.do_not_sync = false;
        this.durable_mode = true;
        this.metadata = new ArrayList<>();
        this.publications = new ArrayList<>();
        this.publish = false;
        this.sales_objective = new JsonObject();
        this.system_id = SYSTEM_ID;
        this.templates = new ArrayList<>();
    }

    public static void main(String[] args) throws Exception {

//		searchAdFromBL(Config.Mode.PROD, "BK_2970_2632");
//		CRestTemplate restTemplate = new CRestTemplate();
//		final boolean exists = new AdService(restTemplate).blExists(Mode.PROD, "BL_4106_6126");
//		System.out.println(exists);
//		System.out.println("{\"channel\": \"store\", \"estimated_avg_impression_cost\": null, \"event\": \"impression\"}");

        System.out.println("{" +
                "\"action\": \"NONE\", " +
                "\"value\": {" +
                "\"amount\": null, " +
                "\"type\": null " +
                "}}");

    }

    public String getAd_creative_set_id() {
        return ad_creative_set_id;
    }

    public void setAd_creative_set_id(String ad_creative_set_id) {
        this.ad_creative_set_id = ad_creative_set_id;
    }

    public ParentOffer getAdgroup() {
        return adgroup;
    }

    public void setAdgroup(ParentOffer adgroup) {
        this.adgroup = adgroup;
    }

    public List<AuditHistory> getAudit_history() {
        return audit_history;
    }

    public void setAudit_history(List<AuditHistory> audit_history) {
        this.audit_history = audit_history;
    }

    public BillingAd getBilling() {
        return billing;
    }

    public void setBilling(BillingAd billing) {
        this.billing = billing;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public ParentOffer getCampaign() {
        return campaign;
    }

    public void setCampaign(ParentOffer campaign) {
        this.campaign = campaign;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<JsonObject> getComposites() {
        return composites;
    }

    public void setComposites(List<JsonObject> composites) {
        this.composites = composites;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCreative_rotation_method() {
        return creative_rotation_method;
    }

    public void setCreative_rotation_method(String creative_rotation_method) {
        this.creative_rotation_method = creative_rotation_method;
    }

    public Set<AbstractVariable> getCreative_variables() {
        return creative_variables;
    }

    public void setCreative_variables(Set<AbstractVariable> creative_variables) {
        this.creative_variables = creative_variables;
    }

    public List<Creative> getCreatives() {
        return creatives;
    }

    public void setCreatives(List<Creative> creatives) {
        this.creatives = creatives;
    }

    public JsonElement getDigital() {
        return digital;
    }

    public void setDigital(JsonElement digital) {
        this.digital = digital;
    }

    public DigitalForecasting getDigital_forecasting() {
        return digital_forecasting;
    }

    public void setDigital_forecasting(DigitalForecasting digital_forecasting) {
        this.digital_forecasting = digital_forecasting;
    }

    public boolean isDo_not_sync() {
        return do_not_sync;
    }

    public void setDo_not_sync(boolean do_not_sync) {
        this.do_not_sync = do_not_sync;
    }

    public boolean isDurable_mode() {
        return durable_mode;
    }

    public void setDurable_mode(boolean durable_mode) {
        this.durable_mode = durable_mode;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public Forecasting getForecasting() {
        return forecasting;
    }

    public void setForecasting(Forecasting forecasting) {
        this.forecasting = forecasting;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public List<ReportingAttribute> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<ReportingAttribute> metadata) {
        this.metadata = metadata;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public List<JsonElement> getPublications() {
        return publications;
    }

    public void setPublications(List<JsonElement> publications) {
        this.publications = publications;
    }

    public boolean isPublish() {
        return publish;
    }

    public void setPublish(boolean publish) {
        this.publish = publish;
    }

    public JsonElement getSales_objective() {
        return sales_objective;
    }

    public void setSales_objective(JsonObject sales_objective) {
        this.sales_objective = sales_objective;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getSystem_id() {
        return system_id;
    }

    //public void setTargeting(JsonObject targeting) {
    //	this.targeting = targeting;
    //}

    public void setSystem_id(String system_id) {
        this.system_id = system_id;
    }

    public List<String> getTemplates() {
        return templates;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    @Override
    public String toString() {
        return Config.toJsString(this);
    }
}
