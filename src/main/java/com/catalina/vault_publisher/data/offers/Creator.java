package com.catalina.vault_publisher.data.offers;


public class Creator {
    //This class is willingly incomplete. It"s only there for Promotion deserialization purposes.
    private String id;
    private String email;
    private String username;
    private String displayName;

    public Creator() {
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getDisplayName() {
        return displayName;
    }
}
