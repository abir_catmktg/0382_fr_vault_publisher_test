package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.CPGPublisher;
import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.History;
import com.catalina.vault_publisher.data.Locations;
import com.catalina.vault_publisher.data.billing.BillingCampaign;
import com.catalina.vault_publisher.data.billing.BillingService;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.billing.ClientService;
import com.catalina.vault_publisher.data.list.vault.TouchpointVaultList;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.Touchpoints;
import com.catalina.vault_publisher.data.touchpoint.TouchpointsService;
import com.catalina.vault_publisher.data.touchpoint.network.Network;
import com.catalina.vault_publisher.data.touchpoint.siteId.SiteId;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CampaignService {

    private static final Logger logger = LogManager.getLogger(CampaignService.class);
    private CRestTemplate cRestTemplate;
    private OfferService offerService;

    public CampaignService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.offerService = new OfferService(cRestTemplate);
    }

    public static void main(String[] args) throws Exception {
//        Campaign m_campaign = new Campaign(true, "[Bulk Manuf] HEINEKEN_FPE", "58963", "mags_off_mut_JD119HEI01_VAULT", Ad.Mode.PROD);
//        m_campaign.getBilling("58963");
//        System.out.println(m_campaign.m_control_grp);
//        m_campaign.getVAULTList("mags_off_mut_JD119HEI01_VAULT");

        new CampaignService(new CRestTemplate())
                .searchCampaignFromId(Config.Mode.PROD, CountryEnum.FRANCE, "00490d35-f5b6-41c0-9a9b-51604626c965");

    }

    public Campaign setupCampaignManuf(Config.Mode mode, String name, String billingNbr, String controlGrp) throws HttpResponseStatusException {

        //BILLING && CLIENT
        BillingCampaign campaignBilling = null;
        Client campaignClient = null;
        try {
            if (billingNbr != null && !billingNbr.isEmpty()) {
                // Getting billing informations
                campaignBilling = new BillingService(cRestTemplate).getBillingCampaign(mode, CountryEnum.FRANCE, billingNbr, "1");
                // Getting client informations
                campaignClient = new ClientService(cRestTemplate).getClient(mode, campaignBilling.client_id);
            } else {
                throw new RuntimeException("createCampaign() error => billing_nbr cannot be empty or null");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        TouchpointsService tpService = new TouchpointsService(cRestTemplate);

        //HANDLES TOUCHPOINT INCLUSIONS
        List<Network> includeNtwk = tpService.includeNetworkManuf(mode);
        List<TouchPoint> includedTouchpoints = new ArrayList<>();
        includedTouchpoints.addAll(includeNtwk);


        //HANDLES TOUCHPOINT EXCLUSIONS
        TouchpointVaultList excludeList = tpService.excludeListVault(mode, controlGrp);
        List<SiteId> excludeSites = tpService.excludeSiteIdManuf();

        List<TouchPoint> excludedTouchpoints = new ArrayList<>();
        excludedTouchpoints.add(excludeList);
        excludedTouchpoints.addAll(excludeSites);


        Campaign campaign = new Campaign(name,
                campaignBilling,
                campaignClient,
                null,
                false,
                Country.FRA_COUNTRY_CODE,
                includedTouchpoints,
                excludedTouchpoints,
                "DISPLAY",
                CPGPublisher.createdBy != null ? CPGPublisher.createdBy : Constants.BULK_MANUF);

        return campaign;
    }

    public BillingService getNewBillingService(CRestTemplate restTemplate) {
        return new BillingService(restTemplate);
    }

    public ClientService getNewClientService(CRestTemplate restTemplate) {
        return new ClientService(restTemplate);
    }

    public TouchpointsService getNewTouchpointsService(CRestTemplate restTemplate) {
        return new TouchpointsService(restTemplate);
    }

    public Campaign setupMinimalCampaign(Country.CountryEnum country, String createdBy, String name) {
        History history = new History(createdBy);

        Campaign campaign = new Campaign(Arrays.asList("store"),
                history,
                name,
                Constants.STATUS_DRAFT,
                history.getCreated_date(),
                country.getCode(),
                false,
                false,
                new Locations(false),
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                new Schedule(false),
                new Touchpoints(false),
                "DISPLAY");

        return campaign;
    }

    public List<Campaign> searchForCampaign(Config.Mode mode, CountryEnum country, String name) throws HttpResponseStatusException {
        String fqParam = String.format("{\"name\":\"%s\"}", name);
        List<Campaign> campaignList = offerService.getOffersFromDashBoard(mode, country, Campaign.class, fqParam);
        return campaignList;
    }

    public List<Campaign> searchCampaignFromId(Config.Mode mode, CountryEnum country, String id) throws HttpResponseStatusException {
        String fqParam = "{\"id\":\"" + id + "\"}";
        List<Campaign> campaignList = offerService.getOffersFromDashBoard(mode, country, Campaign.class, fqParam);
        return campaignList;
    }
}
