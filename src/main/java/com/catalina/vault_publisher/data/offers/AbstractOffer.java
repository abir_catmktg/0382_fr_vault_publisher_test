package com.catalina.vault_publisher.data.offers;


import com.catalina.vault_publisher.data.History;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;

import java.util.Arrays;
import java.util.List;


public abstract class AbstractOffer {

    public static final String AD_ROTATION_DEFAULT = "DEFAULT";
    public static final String ROADBLOCKING_METHOD_SINGLE = "SINGLE";
    public static final String CREATIVE_ROTATION_METHOD_OPTIMIZED = "OPTIMIZED";
    public static final String SYSTEM_ID = "BLIP";

    public static final String CAMPAIGNS_URL = "/campaigns";
    public static final String ADGROUPS_URL = "/adgroups";
    public static final String ADS_URL = "/ads";
    private List<String> channels;
    private History history;
    private String id;
    private String name;
    private String start_date;
    private String stop_date;
    private String status;
    private Targeting targeting;
    private String date_modified;
    public AbstractOffer() {
    }

    public AbstractOffer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public AbstractOffer(List<String> channels, History history, String name, String status, String date_modified) {
        this.channels = channels;
        this.history = history;
        this.name = name;
        this.status = status;
        this.date_modified = date_modified;
    }

    public AbstractOffer(String name, String status, List<TouchPoint> includedTouchpoints, List<TouchPoint> excludedTouchpoints, String createdBy) {
        this.channels = Arrays.asList("store");
        this.history = new History(createdBy);
        this.name = name;
        this.status = status;
        this.date_modified = this.history.getCreated_date();
        this.targeting = new Targeting(includedTouchpoints, excludedTouchpoints);
    }

    public AbstractOffer(String name, String status, String createdBy) {
        this.channels = Arrays.asList("store");
        this.history = new History(createdBy);
        this.name = name;
        this.status = status;
        this.date_modified = this.history.getCreated_date();
        this.targeting = new Targeting(false);

    }

    public AbstractOffer(String name, String start_date, String stop_date, String status, String createdBy, String target_description) {
        this.channels = Arrays.asList("store");
        this.history = new History(createdBy);
        this.name = name;
        this.start_date = start_date;
        this.stop_date = stop_date;
        this.status = status;
        this.date_modified = this.history.getCreated_date();
        this.targeting = new Targeting(target_description, false);
    }

    public List<String> getChannels() {
        return channels;
    }

    public void setChannels(List<String> channels) {
        this.channels = channels;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStop_date() {
        return stop_date;
    }

    public void setStop_date(String stop_date) {
        this.stop_date = stop_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public void setTargeting(Targeting targeting) {
        this.targeting = targeting;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public enum OfferEnum {
        CAMPAIGN("CAMPAIGN", CAMPAIGNS_URL, new Campaign()),
        ADGROUP("ADGROUP", ADGROUPS_URL, new Adgroup()),
        AD("AD", ADS_URL, new Ad());

        public final String name, url;
        private AbstractOffer clazzOfObject;

        OfferEnum(String name, String url, AbstractOffer clazzObject) {
            this.name = name;
            this.url = url;
            this.clazzOfObject = clazzObject;
        }

        public static <T> OfferEnum fromClass(Class<T> clazz) {
            return Arrays.stream(OfferEnum.values())
                    .filter(x -> clazz.isAssignableFrom(x.getClazzOfObject().getClass()))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported class for OfferEnum : %s.", clazz)));
        }

        public String getName() {
            return name;
        }

        public String getUrl() {
            return url;
        }

        public AbstractOffer getClazzOfObject() {
            return clazzOfObject;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public enum Scope {
        CAMPAIGN("Campaign"),
        ADGROUP("Adgroup"),
        AD("Ad");

        private final String value;

        Scope(String value) {
            this.value = value;
        }

        public static Scope fromValue(String scope) {
            return Arrays.stream(Scope.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(scope))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Scope %s.", scope)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum ChannelType {
        IN_STORE("store"),
        ONLINE("web"),
        MOBILE("mobile");

        private final String value;

        ChannelType(String value) {
            this.value = value;
        }

        public static ChannelType fromValue(String channelType) {
            return Arrays.stream(ChannelType.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(channelType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Channel Type %s.", channelType)));
        }

        public String getValue() {
            return value;
        }
    }
}
