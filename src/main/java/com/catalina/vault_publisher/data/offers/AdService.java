package com.catalina.vault_publisher.data.offers;

import com.catalina.common.util.Pair;
import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.History;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.data.barcode.Barcode;
import com.catalina.vault_publisher.data.barcode.BarcodeService;
import com.catalina.vault_publisher.data.billing.BillingAd;
import com.catalina.vault_publisher.data.billing.BillingService;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.billing.ClientService;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethod.TargetingMethodEnum;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.budget.BudgetService;
import com.catalina.vault_publisher.data.clearingHouse.ClearingHouse;
import com.catalina.vault_publisher.data.clearingHouse.ClearingHouseService;
import com.catalina.vault_publisher.data.experience.ExperienceService;
import com.catalina.vault_publisher.data.forecasting.Forecasting;
import com.catalina.vault_publisher.data.graphics.TemplatePackage;
import com.catalina.vault_publisher.data.graphics.TemplatePackageService;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAMService;
import com.catalina.vault_publisher.data.graphics.creatives.template.Template;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPI;
import com.catalina.vault_publisher.data.measurement.Measurement;
import com.catalina.vault_publisher.data.measurement.MeasurementService;
import com.catalina.vault_publisher.data.productValidation.ProductValidation;
import com.catalina.vault_publisher.data.productValidation.ProductValidationService;
import com.catalina.vault_publisher.data.products.ProductsService;
import com.catalina.vault_publisher.data.reporting_attributes.ReportingAttribute;
import com.catalina.vault_publisher.data.reporting_attributes.ReportingAttributeService;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.trailer.Trailer;
import com.catalina.vault_publisher.data.trailer.TrailerService;
import com.catalina.vault_publisher.data.triggers.TriggerPackage;
import com.catalina.vault_publisher.data.triggers.TriggerService;
import com.catalina.vault_publisher.data.triggers.Triggers;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;
import com.catalina.vault_publisher.manuf.blackAndWhite.BlackAndWhiteData;
import com.catalina.vault_publisher.manuf.blackAndWhite.BlackWhiteDataService;
import com.catalina.vault_publisher.manuf.colorData.ColorData;
import com.catalina.vault_publisher.manuf.colorData.ColorDataService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.DateUtils;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.io.UnsupportedEncodingException;
import java.util.*;


public class AdService {

    private static final Logger logger = LogManager.getLogger(AdService.class);
    private CRestTemplate cRestTemplate;
    private BillingService billingService;
    private ClientService clientService;
    private ExperienceService experienceService;
    private TriggerService triggerService;
    private ReportingAttributeService reportingAttributeService;
    private BarcodeService barcodeService;
    private ClearingHouseService clearingHouseService;
    private TrailerService trailerService;
    private BudgetService budgetService;
    private ImageDAMService imageDAMService;
    private ProductsService productsService;
    private ProductValidationService productValidationService;
    private TemplatePackageService templatePackageService;
    private MeasurementService measurementService;
    private CampaignService campaignService;
    private OfferService offerService;

    public AdService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.billingService = new BillingService(cRestTemplate);
        this.clientService = new ClientService(cRestTemplate);
        this.experienceService = new ExperienceService();
        this.triggerService = new TriggerService(cRestTemplate);
        this.reportingAttributeService = new ReportingAttributeService();
        this.barcodeService = new BarcodeService(cRestTemplate);
        this.clearingHouseService = new ClearingHouseService();
        this.trailerService = new TrailerService();
        this.budgetService = new BudgetService();
        this.imageDAMService = new ImageDAMService(cRestTemplate);
        this.productsService = new ProductsService();
        this.productValidationService = new ProductValidationService(cRestTemplate);
        this.templatePackageService = new TemplatePackageService(cRestTemplate);
        this.measurementService = new MeasurementService();
        this.campaignService = new CampaignService(cRestTemplate);
        this.offerService = new OfferService(cRestTemplate);
    }

    public static void main(String[] args) throws HttpResponseStatusException, UnsupportedEncodingException {

        new AdService(new CRestTemplate()).findAd(Config.Mode.PROD,
                CountryEnum.FRANCE,
                "6e07e39d-c859-4745-a654-850f0c573120",
                "51c03a59-7f32-43bd-8b63-7ccfd0fc59e7",
                "[TEST] 005ES OU Ach nat de Rozana 6X1L x2 et +");


    }

    public Result updateAdManuf(Ad ad,
                                Config.Mode mode,
                                String billingNbr,
                                String campaignTyp,
                                String adGroupName,
                                String targetingMethod,
                                String adExternalId,
                                String promotionId,
                                String leverType,
                                String saveLine,
                                String count,
                                String colorImgId,
                                String bwProductShotId,
                                String vupcListName,
                                List<String> adExternalIdListForCT,
                                String clearingHouse,
                                String colorTemplateName,
                                String dealNbr,
                                String qualifierLine2,
                                String qualifierLine3,
                                String qualifierLine4,
                                String colorTrailerMsg,
                                String bwTrailerMsg,
                                String operator,
                                String triggerQty,
                                String triggerListName,
                                String triggerGroupName,
                                String reportingAttribute) throws HttpResponseStatusException {

        //GET COLOR DATA
        ColorData dataColor = new ColorDataService().setColorDataManuf(colorImgId, colorTemplateName, colorTrailerMsg);

        //GET BLACK & WHITE DATA
        BlackAndWhiteData dataBW =
                new BlackWhiteDataService().setBwDataManuf(bwProductShotId, bwTrailerMsg);

        //GET BARCODE
        Barcode barcode = barcodeService.createBarcodeManuf(mode, saveLine);

        //GET CLEARING HOUSE
        ClearingHouse clearingHse = clearingHouseService.createClearingHouseManuf(clearingHouse);

        //GET TRAILER
        List<Trailer> trailerList = trailerService.setTrailerManuf(dataColor.getTrailer_message(), dataBW.getTrailer_message());

        //GET TRIGGERS
        Result<TriggerPackage> resTrigger = triggerService.getTriggersManuf(mode, leverType,
                (operator == null || operator.isEmpty()) ? null : ActivationTrigger.Operator.fromValue(operator),
                (triggerQty == null || triggerQty.equals("")) ? null : Double.valueOf(triggerQty),
                triggerListName,
                triggerGroupName,
                adExternalIdListForCT);
        if (resTrigger.getStatus() != 0) return resTrigger;
        Triggers triggers = triggerService.createTriggersFromPackages(Arrays.asList(resTrigger.getObject()));

        //SET BL && FRA-BLIP-BL && BARCODE && CLEARING HOUSE && SEGMENT && TRAILERS && TRIGGERS
        ad.setExperience(experienceService.createExperienceManuf(adExternalId,
                promotionId,
                barcode,
                clearingHse,
                trailerList,
                triggers));
        ad.setExternal_id(adExternalId);

        //GET TARGETING METHOD
        TargetingMethodEnum targetingMethodEnum = TargetingMethodEnum.fromValue(targetingMethod);

        //SET BILLING
        BillingAd billing = billingService.createBillingAD(mode, CountryEnum.FRANCE, billingNbr, "1", targetingMethodEnum);
        ad.setBilling(billing);

        //SET CLIENT
        Client client = clientService.getClient(mode, billing.getClient_id());
        ad.setClient(client);

        //SET DISTRIBUTION CAPPING
        Budget budget = budgetService.setBudgetManuf(count);
        ad.setBudget(budget);

        //DAM
        ImageDAM imageColor = imageDAMService.createDAMImage(mode,
                CountryEnum.FRANCE,
                dataColor.getImage_nbr(),
                ImageDAM.PrinterLevel.COLOR,
                ImageDAM.AssetType.STATIC_IMAGE);
        ad.getCreatives().add(imageColor);

        if (dataBW.isIn_use()) {
            ImageDAM imageBW = imageDAMService.createDAMImage(mode,
                    CountryEnum.FRANCE,
                    dataBW.getImage_nbr(),
                    ImageDAM.PrinterLevel.BW,
                    dataBW.getAsset_type());
            ad.getCreatives().add(imageBW);
        }

        //SET NBIC USAGE && PRODUCT VALIDATION
        boolean nbicValue = productsService.setNbicManuf(campaignTyp, leverType);
        List<ProductValidation> pvList = productValidationService.setProductValidationManuf(mode,
                vupcListName,
                triggerListName,
                campaignTyp,
                leverType);
        ad.setProducts(productsService.setupProductsManuf(nbicValue, pvList));

        //SET TEMPLATE
        List<TemplatePackage> allTemplates = templatePackageService.setTemplateManuf(mode,
                dataColor.getTemplate_name(),
                dataBW.getTemplate_name(),
                saveLine,
                dealNbr,
                clearingHouse,
                qualifierLine2,
                qualifierLine3,
                qualifierLine4);
        for (TemplatePackage template : allTemplates) {
            List<Template> templateList = template.getTemplates();
            ad.getCreatives().addAll(templateList);
            ad.getCreative_variables().addAll(template.getCreativeVars().getAbstractVariables());
            //for (Template currentTemplate : templateList) ad.getTemplates().add(currentTemplate.getId());
        }

        ad.getTemplates().addAll(Arrays.asList(TemplateAPI.ManufColorTemplate.fromValues(dataColor.getTemplate_name()).getId(),
                TemplateAPI.ManufColorTemplate.TRAILER.getId(),
                TemplateAPI.ManufColorTemplate.AUTH_CODE_COLOR.getId(),
                TemplateAPI.ManufColorTemplate.RETAILER_LOGO.getId(),
                TemplateAPI.ManufColorTemplate.REDEEM.getId(),
                TemplateAPI.ManufBwTemplate.fromValues(dataBW.getTemplate_name()).getId(),
                TemplateAPI.ManufBwTemplate.REDEEM.getId(),
                TemplateAPI.ManufBwTemplate.TRAILER.getId()));
        //Collections.sort(ad.getTemplates(),Collections.reverseOrder());

        //SET MEASUREMENT & FORECASTING
        if (targetingMethodEnum.name.toLowerCase().contains("ecolink")) {
            Pair<Measurement, Forecasting> pairMeasurement = measurementService.setMeasurementManufEcolink(count,
                    Constants.MEASUREMENT_PERCENT_ECOLINK);
            ad.setMeasurement(pairMeasurement.getKey());
            ad.setForecasting(pairMeasurement.getValue());
        }

        //SET REPORTING ATTRIBUTE
        if (reportingAttribute != null && !reportingAttribute.isEmpty()) {
            ReportingAttribute reportAttributeObject = reportingAttributeService.createReportingAttribute(reportingAttribute);
            ad.getMetadata().add(reportAttributeObject);
        }


        return new Result();

    }

    public Result createMinimalAd(Config.Mode mode,
                                  String campaignId,
                                  String campaignName,
                                  String adgroupId,
                                  String adgroupName,
                                  String name,
                                  String dateStart,
                                  String dateStop,
                                  CountryEnum country) {
        String adExtId = null;
        try {
            adExtId = getBL(mode, country);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new Result(999, "createMinimalAd error - " + e.getMessage());
        }
        Ad ad = new Ad(campaignId,
                campaignName,
                adgroupId,
                adgroupName,
                name,
                adExtId,
                country,
                dateStart,
                dateStop,
                Constants.COLIBRI,
                null);

        return new Result(ad);
    }

    public List<Ad> findAd(Config.Mode mode, CountryEnum country, String campaignId, String adGroupId, String name) throws HttpResponseStatusException {

        String fqParam = String.format("{\"campaign\": {\"id\": \"%s\"},\"adgroup\":{\"id\": \"%s\"},\"name\": \"%s\"}",
                campaignId,
                adGroupId,
                name);

        List<Ad> docs = offerService.getOffersFromDashBoard(mode, country, Ad.class, fqParam);
        return docs;
    }

    public List<Ad> searchAdFromBL(Config.Mode mode, CountryEnum country, String externalId, HttpStatus expectedStatus) throws HttpResponseStatusException {
        String fqParam = String.format("{\"external_id\":\"%s\"}", externalId);
        List<Ad> docs = offerService.getOffersFromDashBoard(mode, country, Ad.class, fqParam);
        return docs;
    }

    public boolean blExists(Config.Mode mode, CountryEnum country, String _bl) {
        String external_id = country.getCode() + "-" + Constants.EXTERNAL_SYSTEM + "-" + _bl;
        String urlApi = mode.dmpUrl + "/promotions/" + external_id;

        try {
            cRestTemplate.executeRequestWithJsonHeader(urlApi, null, String.class, HttpMethod.GET, HttpStatus.NOT_FOUND);
            return false;
        } catch (HttpResponseStatusException e) {
            return true;
        }
    }

    public String getBL(Config.Mode mode, CountryEnum country) {
        String external_id = generateBL();

        boolean bBlExists = blExists(mode, country, external_id);
        while (bBlExists) {
            external_id = generateBL();
            bBlExists = blExists(mode, country, external_id);
        }
        return external_id;
    }

    protected String generateBL() {
        String prefix = "BL";//classical ad prefix
        String postfix = String.format("%04d", (int) (Math.floor(Math.random() * 9999) + 1));
        String rearfix = String.format("%04d", (int) (Math.floor(Math.random() * 9999) + 1));
        String external_id = prefix + '_' + rearfix + '_' + postfix;
        return external_id;
    }

    public void editValues(Config.Mode mode, Ad ad) {

        // reset id to null
        ad.setId(null);
        ad.setAd_creative_set_id(UUID.randomUUID().toString());
        ad.setAudit_history(null);
        ad.setComposites(new ArrayList<>());
        ad.getExperience().getReward().setOffer_code_type(null);
        ad.getExperience().getReward().setOffer_codes(new ArrayList<>());

        // Get oldName && Rename ad: "oldName Copy #dd-MM-yyyy HH:mm:ss"
        String newName = ad.getName() + " Copy #" + DateUtils.getCurrentDateSecondPrecision();
        ad.setName(newName);

        CountryEnum countryEnum = CountryEnum.fromValues(ad.getCountry_code());

        // Update history date and name
        ad.setHistory(new History(Constants.VAULT_PUBLISHER, DateUtils.getCurrentDateMilliSecondPrecision()));

        //Update FRA-BLIP-BL && BL
        String adExtId = getBL(mode, countryEnum);

        String promotionId = ad.getCountry_code() + "-" + Constants.EXTERNAL_SYSTEM + "-" + adExtId;

        ad.setExternal_id(adExtId);
        ad.getExperience().getReward().setExternal_id(adExtId);
        ad.getExperience().getReward().setId(promotionId);

        //Update status
        ad.setPublish(false);
        ad.setPublications(new ArrayList<>());
        ad.setStatus(Constants.STATUS_DRAFT);
    }

    public String finalize(String adStr) {
        return adStr.replaceAll("<br/>", "\\\\n");
    }

    public void updateAdVetoManuf(Ad ad,
                                  List<TouchPoint> campaignTouchpoint,
                                  String veto_itm,
                                  String veto_su,
                                  String veto_smatch,
                                  String veto_mpx,
                                  String veto_crf) {
        boolean bIsOneVetoAtLeast = false;

        Iterator<TouchPoint> itr = campaignTouchpoint.iterator();
        while (itr.hasNext()) {
            TouchPoint currentTp = itr.next();
            int index = -1;
            // VETO ITM
            if (currentTp.id.equals("FRA-0006") && !veto_itm.equalsIgnoreCase("NON")) {
                index = campaignTouchpoint.indexOf(currentTp);
                bIsOneVetoAtLeast = true;
            }

            // VETO SU
            if (currentTp.id.equals("FRA-0004") && !veto_su.equalsIgnoreCase("NON")) {
                index = campaignTouchpoint.indexOf(currentTp);
                bIsOneVetoAtLeast = true;
            }

            // VETO SMATCH
            if (currentTp.id.equals("FRA-0020") && !veto_smatch.equalsIgnoreCase("NON")) {
                index = campaignTouchpoint.indexOf(currentTp);
                bIsOneVetoAtLeast = true;
            }

            // VETO Monoprix
            if (currentTp.id.equals("FRA-0028") && !veto_mpx.equalsIgnoreCase("NON")) {
                index = campaignTouchpoint.indexOf(currentTp);
                bIsOneVetoAtLeast = true;
            }

            // VETO CRF PROXI
            if (currentTp.id.equals("FRA-0023") && !veto_crf.equalsIgnoreCase("NON")) {
                index = campaignTouchpoint.indexOf(currentTp);
                bIsOneVetoAtLeast = true;
            }

            if (index > -1) {
                itr.remove();
            }
        }

        if (bIsOneVetoAtLeast) {
            ad.getTargeting().getTouchpoints().setInclude(campaignTouchpoint);
        }
    }

    public List<TouchPoint> getCampaignTouchpoints(Config.Mode mode,
                                                   CountryEnum country,
                                                   Campaign campaign,
                                                   String campaignID) throws HttpResponseStatusException {

        if (campaign != null) return campaign.getTargeting().getTouchpoints().getInclude();

        String queryParam2 = String.format("{\"id\":\"%s\"}", campaignID);
        List<Campaign> campaignList = offerService.getOffersFromDashBoard(mode, country, Campaign.class, queryParam2);
        if (campaignList.size() != 1) {
            throw new RuntimeException(String.format("%s campaigns found with the id: %s", campaignList.size(), campaignID));
        }

        //ONLY ONE CAMPAIGN -> GET ID
        Campaign campaignFromDashBoard = campaignList.get(0);
        return campaignFromDashBoard.getTargeting().getTouchpoints().getInclude();
    }

}
