package com.catalina.vault_publisher.data.offers;

import java.util.List;

public class OfferResponseWrapper {
    private int deleted;
    private int errors;
    private int inserted;
    private int replaced;
    private int skipped;
    private int unchanged;
    private List<String> generated_keys;

    public OfferResponseWrapper() {
    }

    public int getDeleted() {
        return deleted;
    }

    public int getErrors() {
        return errors;
    }

    public int getInserted() {
        return inserted;
    }

    public int getReplaced() {
        return replaced;
    }

    public int getSkipped() {
        return skipped;
    }

    public int getUnchanged() {
        return unchanged;
    }

    public List<String> getGenerated_keys() {
        return generated_keys;
    }
}
