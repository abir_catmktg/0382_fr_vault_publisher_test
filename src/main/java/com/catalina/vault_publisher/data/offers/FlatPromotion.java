package com.catalina.vault_publisher.data.offers;

import java.math.BigInteger;
import java.util.List;

public class FlatPromotion {
    private String id;
    private String name;
    private String status;
    private String external_id;
    private String country_code;
    private String system_id;
    private String valid;
    private String rank;
    private String campaign_id;
    private String campaign_name;
    private String ad_creative_set_id;
    private String ad_group_id;
    private String ad_group_name;
    private String channel_type;
    private String user_id;
    private String user_email;
    private String user_username;
    private String user_display_name;
    private String user_source;
    private String line_item_type;
    private String classification_name;
    private String classification_targeting_scope;
    private String classification_trigger_scope;
    private String classification_type;
    private String client_id;
    private String client_name;
    private String delivery_method;
    private String index_date;
    private String modified_date;
    private List<String> channel_types;
    private String experience_id;
    private String experience_name;
    private String advertised_flag;
    private String unlimited_flag;
    private String expected_flag;
    private String end_of_order_flag;
    private String reward_expiration_date;
    private String reward_funding_business_segment;
    private String reward_offer_code_type;
    private String reward_barcode_format;
    private String reward_barcode_type;
    private Double trigger_count;
    private List<String> upc_list_names;
    private List<String> trigger_upcs;
    private Double trigger_upc_count;
    private List<String> trigger_type_names;
    private List<String> trigger_group_names;
    private List<String> trigger_scope_names;
    private String ignore_long_term_controls_flag;
    private String promotion_exclusivity_flag;
    private String measurement_scope;
    private String billing_id;
    private String billing_name;
    private String durable_mode;
    private String start_date;
    private String stop_date;
    private Double promoted_brands_count;
    private Double promoted_categories_count;
    private Double promoted_sales_categories_count;
    private List<String> promoted_upcs;
    private Double promoted_upc_count;
    private String promoted_product_legal_age_restricted;
    private String promoted_product_legal_alcohol_use;
    private String promoted_product_legal_liquid_dairy_use;
    private String promoted_product_legal_sensitive_use;
    private String promoted_product_legal_smoking_use;
    private String promoted_product_legal_outside_services;
    private String promoted_product_legal_restrictions;
    private String targeted_enabled;
    private String location_wkt_enabled;
    private String location_attr_enabled;
    private List<String> network_ids;
    private List<String> touchpoint_targeting_types;
    private String version;
    private String index_hostname;
    private List<String> variable_names;
    private String publications_enabled;
    private Double publications_count;
    private String scoring_enabled;
    private String variable_value_verbiage_1;
    private String serving_status;
    private Double impression_count;
    private Double media_delivery_count;
    private Double clip_count;
    private String cap_descr_store;
    private String is_capped_store;
    private String entity;
    private String index_server;
    private BigInteger _version_;

    public FlatPromotion() {
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getExternal_id() {
        return external_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public String getSystem_id() {
        return system_id;
    }

    public String getValid() {
        return valid;
    }

    public String getRank() {
        return rank;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public String getAd_creative_set_id() {
        return ad_creative_set_id;
    }

    public String getAd_group_id() {
        return ad_group_id;
    }

    public String getAd_group_name() {
        return ad_group_name;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_email() {
        return user_email;
    }

    public String getUser_username() {
        return user_username;
    }

    public String getUser_display_name() {
        return user_display_name;
    }

    public String getUser_source() {
        return user_source;
    }

    public String getLine_item_type() {
        return line_item_type;
    }

    public String getClassification_name() {
        return classification_name;
    }

    public String getClassification_targeting_scope() {
        return classification_targeting_scope;
    }

    public String getClassification_trigger_scope() {
        return classification_trigger_scope;
    }

    public String getClassification_type() {
        return classification_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public String getDelivery_method() {
        return delivery_method;
    }

    public String getIndex_date() {
        return index_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public List<String> getChannel_types() {
        return channel_types;
    }

    public String getExperience_id() {
        return experience_id;
    }

    public String getExperience_name() {
        return experience_name;
    }

    public String getAdvertised_flag() {
        return advertised_flag;
    }

    public String getUnlimited_flag() {
        return unlimited_flag;
    }

    public String getExpected_flag() {
        return expected_flag;
    }

    public String getEnd_of_order_flag() {
        return end_of_order_flag;
    }

    public String getReward_expiration_date() {
        return reward_expiration_date;
    }

    public String getReward_funding_business_segment() {
        return reward_funding_business_segment;
    }

    public String getReward_offer_code_type() {
        return reward_offer_code_type;
    }

    public String getReward_barcode_format() {
        return reward_barcode_format;
    }

    public String getReward_barcode_type() {
        return reward_barcode_type;
    }

    public Double getTrigger_count() {
        return trigger_count;
    }

    public List<String> getUpc_list_names() {
        return upc_list_names;
    }

    public List<String> getTrigger_upcs() {
        return trigger_upcs;
    }

    public Double getTrigger_upc_count() {
        return trigger_upc_count;
    }

    public List<String> getTrigger_type_names() {
        return trigger_type_names;
    }

    public List<String> getTrigger_group_names() {
        return trigger_group_names;
    }

    public List<String> getTrigger_scope_names() {
        return trigger_scope_names;
    }

    public String getIgnore_long_term_controls_flag() {
        return ignore_long_term_controls_flag;
    }

    public String getPromotion_exclusivity_flag() {
        return promotion_exclusivity_flag;
    }

    public String getMeasurement_scope() {
        return measurement_scope;
    }

    public String getBilling_id() {
        return billing_id;
    }

    public String getBilling_name() {
        return billing_name;
    }

    public String getDurable_mode() {
        return durable_mode;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getStop_date() {
        return stop_date;
    }

    public Double getPromoted_brands_count() {
        return promoted_brands_count;
    }

    public Double getPromoted_categories_count() {
        return promoted_categories_count;
    }

    public Double getPromoted_sales_categories_count() {
        return promoted_sales_categories_count;
    }

    public List<String> getPromoted_upcs() {
        return promoted_upcs;
    }

    public Double getPromoted_upc_count() {
        return promoted_upc_count;
    }

    public String getPromoted_product_legal_age_restricted() {
        return promoted_product_legal_age_restricted;
    }

    public String getPromoted_product_legal_alcohol_use() {
        return promoted_product_legal_alcohol_use;
    }

    public String getPromoted_product_legal_liquid_dairy_use() {
        return promoted_product_legal_liquid_dairy_use;
    }

    public String getPromoted_product_legal_sensitive_use() {
        return promoted_product_legal_sensitive_use;
    }

    public String getPromoted_product_legal_smoking_use() {
        return promoted_product_legal_smoking_use;
    }

    public String getPromoted_product_legal_outside_services() {
        return promoted_product_legal_outside_services;
    }

    public String getPromoted_product_legal_restrictions() {
        return promoted_product_legal_restrictions;
    }

    public String getTargeted_enabled() {
        return targeted_enabled;
    }

    public String getLocation_wkt_enabled() {
        return location_wkt_enabled;
    }

    public String getLocation_attr_enabled() {
        return location_attr_enabled;
    }

    public List<String> getNetwork_ids() {
        return network_ids;
    }

    public List<String> getTouchpoint_targeting_types() {
        return touchpoint_targeting_types;
    }

    public String getVersion() {
        return version;
    }

    public String getIndex_hostname() {
        return index_hostname;
    }

    public List<String> getVariable_names() {
        return variable_names;
    }

    public String getPublications_enabled() {
        return publications_enabled;
    }

    public Double getPublications_count() {
        return publications_count;
    }

    public String getScoring_enabled() {
        return scoring_enabled;
    }

    public String getVariable_value_verbiage_1() {
        return variable_value_verbiage_1;
    }

    public String getServing_status() {
        return serving_status;
    }

    public Double getImpression_count() {
        return impression_count;
    }

    public Double getMedia_delivery_count() {
        return media_delivery_count;
    }

    public Double getClip_count() {
        return clip_count;
    }

    public String getCap_descr_store() {
        return cap_descr_store;
    }

    public String getIs_capped_store() {
        return is_capped_store;
    }

    public String getEntity() {
        return entity;
    }

    public String getIndex_server() {
        return index_server;
    }

    public BigInteger get_version_() {
        return _version_;
    }
}
