package com.catalina.vault_publisher.data.offers;

import com.catalina.common.Constraint;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.data.billing.BillingAd;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.classification.Classification;
import com.catalina.vault_publisher.data.composite.Composite;
import com.catalina.vault_publisher.data.distribution.AggregateLatestDistribution;
import com.catalina.vault_publisher.data.estimates.Estimates;
import com.catalina.vault_publisher.data.experience.Experience;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.AbstractVariable;
import com.catalina.vault_publisher.data.graphics.creatives.Creative;
import com.catalina.vault_publisher.data.measurement.Measurement;
import com.catalina.vault_publisher.data.products.Products;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.google.gson.JsonElement;

import java.util.List;
import java.util.Set;

public class Promotion {

    private List<String> tags;
    private String id;
    private String name;
    private String campaign_id;
    private String campaign_name;
    private ParentOffer campaign;
    private BillingAd billing;
    private String adgroup_id;
    private String adgroup_name;
    private ParentOffer adgroup;
    private String version;
    private String ad_creative_set_id;
    //sales_objective : [Object] not important
    private String delivery_method;
    private String creative_rotation_method;
    private String ratio;
    private Budget budget;
    private String forcasting;
    private List<Estimates> estimates;
    private Experience experience;
    private String exclusivity;
    private Targeting targeting;
    private String type;
    private Client client;
    private String status;
    private String channel_type;
    private List<String> channel_types;
    private String start_date;
    private String start_time;
    private String stop_date;
    private String stop_time;
    private String system_id;
    private String external_id;
    private String country_code;
    private Products products;
    private Classification classification;
    private String enrichment_date;
    private String modified_date;
    private List<String> scores;
    private List<Creative> creatives;
    private List<Composite> composites;
    private String params;
    private String rank;
    private String blind;
    private Set<AbstractVariable> creative_variables;
    private List<Publication> publications;
    @Constraint(notNull = true)
    private boolean publish;
    private Measurement measurement;
    private Creator creator;
    private User user;
    private JsonElement metadata;
    private String timestamp;
    private String serving_status;
    private AggregateLatestDistribution aggregated_latest_distribution;
    private String countryCode;
    private String adgroupId;
    private String campaignId;
    private String externalId;
    private String cacheKey;
    private boolean durable_mode;

    public Promotion() {
    }

    public Promotion(List<String> tags,
                     String id,
                     String name,
                     String campaign_id,
                     String campaign_name,
                     ParentOffer campaign,
                     BillingAd billing,
                     String adgroup_id,
                     String adgroup_name,
                     ParentOffer adgroup,
                     String version,
                     String ad_creative_set_id,
                     String delivery_method,
                     String creative_rotation_method,
                     String ratio,
                     Budget budget,
                     String forcasting,
                     List<Estimates> estimates,
                     Experience experience,
                     String exclusivity,
                     Targeting targeting,
                     String type,
                     Client client,
                     String status,
                     String channel_type,
                     List<String> channel_types,
                     String start_date,
                     String start_time,
                     String stop_date,
                     String stop_time,
                     String system_id,
                     String external_id,
                     String country_code,
                     Products products,
                     Classification classification,
                     String enrichment_date,
                     String modified_date,
                     List<String> scores,
                     List<Creative> creatives,
                     List<Composite> composites,
                     String params,
                     String rank,
                     String blind,
                     Set<AbstractVariable> creative_variables,
                     List<Publication> publications,
                     boolean publish,
                     Measurement measurement,
                     Creator creator,
                     User user,
                     JsonElement metadata,
                     String timestamp,
                     String serving_status,
                     AggregateLatestDistribution aggregated_latest_distribution,
                     String countryCode,
                     String adgroupId,
                     String campaignId,
                     String externalId,
                     String cacheKey,
                     boolean durable_mode) {
        this.tags = tags;
        this.id = id;
        this.name = name;
        this.campaign_id = campaign_id;
        this.campaign_name = campaign_name;
        this.campaign = campaign;
        this.billing = billing;
        this.adgroup_id = adgroup_id;
        this.adgroup_name = adgroup_name;
        this.adgroup = adgroup;
        this.version = version;
        this.ad_creative_set_id = ad_creative_set_id;
        this.delivery_method = delivery_method;
        this.creative_rotation_method = creative_rotation_method;
        this.ratio = ratio;
        this.budget = budget;
        this.forcasting = forcasting;
        this.estimates = estimates;
        this.experience = experience;
        this.exclusivity = exclusivity;
        this.targeting = targeting;
        this.type = type;
        this.client = client;
        this.status = status;
        this.channel_type = channel_type;
        this.channel_types = channel_types;
        this.start_date = start_date;
        this.start_time = start_time;
        this.stop_date = stop_date;
        this.stop_time = stop_time;
        this.system_id = system_id;
        this.external_id = external_id;
        this.country_code = country_code;
        this.products = products;
        this.classification = classification;
        this.enrichment_date = enrichment_date;
        this.modified_date = modified_date;
        this.scores = scores;
        this.creatives = creatives;
        this.composites = composites;
        this.params = params;
        this.rank = rank;
        this.blind = blind;
        this.creative_variables = creative_variables;
        this.publications = publications;
        this.publish = publish;
        this.measurement = measurement;
        this.creator = creator;
        this.user = user;
        this.metadata = metadata;
        this.timestamp = timestamp;
        this.serving_status = serving_status;
        this.aggregated_latest_distribution = aggregated_latest_distribution;
        this.countryCode = countryCode;
        this.adgroupId = adgroupId;
        this.campaignId = campaignId;
        this.externalId = externalId;
        this.cacheKey = cacheKey;
        this.durable_mode = durable_mode;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(String campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public void setCampaign_name(String campaign_name) {
        this.campaign_name = campaign_name;
    }

    public ParentOffer getCampaign() {
        return campaign;
    }

    public void setCampaign(ParentOffer campaign) {
        this.campaign = campaign;
    }

    public BillingAd getBilling() {
        return billing;
    }

    public void setBilling(BillingAd billing) {
        this.billing = billing;
    }

    public String getAdgroup_id() {
        return adgroup_id;
    }

    public void setAdgroup_id(String adgroup_id) {
        this.adgroup_id = adgroup_id;
    }

    public String getAdgroup_name() {
        return adgroup_name;
    }

    public void setAdgroup_name(String adgroup_name) {
        this.adgroup_name = adgroup_name;
    }

    public ParentOffer getAdgroup() {
        return adgroup;
    }

    public void setAdgroup(ParentOffer adgroup) {
        this.adgroup = adgroup;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAd_creative_set_id() {
        return ad_creative_set_id;
    }

    public void setAd_creative_set_id(String ad_creative_set_id) {
        this.ad_creative_set_id = ad_creative_set_id;
    }

    public String getDelivery_method() {
        return delivery_method;
    }

    public void setDelivery_method(String delivery_method) {
        this.delivery_method = delivery_method;
    }

    public String getCreative_rotation_method() {
        return creative_rotation_method;
    }

    public void setCreative_rotation_method(String creative_rotation_method) {
        this.creative_rotation_method = creative_rotation_method;
    }

    public String getRatio() {
        return ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public String getForcasting() {
        return forcasting;
    }

    public void setForcasting(String forcasting) {
        this.forcasting = forcasting;
    }

    public List<Estimates> getEstimates() {
        return estimates;
    }

    public void setEstimates(List<Estimates> estimates) {
        this.estimates = estimates;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public String getExclusivity() {
        return exclusivity;
    }

    public void setExclusivity(String exclusivity) {
        this.exclusivity = exclusivity;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public void setTargeting(Targeting targeting) {
        this.targeting = targeting;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public List<String> getChannel_types() {
        return channel_types;
    }

    public void setChannel_types(List<String> channel_types) {
        this.channel_types = channel_types;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getStop_date() {
        return stop_date;
    }

    public void setStop_date(String stop_date) {
        this.stop_date = stop_date;
    }

    public String getStop_time() {
        return stop_time;
    }

    public void setStop_time(String stop_time) {
        this.stop_time = stop_time;
    }

    public String getSystem_id() {
        return system_id;
    }

    public void setSystem_id(String system_id) {
        this.system_id = system_id;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Classification getClassification() {
        return classification;
    }

    public void setClassification(Classification classification) {
        this.classification = classification;
    }

    public String getEnrichment_date() {
        return enrichment_date;
    }

    public void setEnrichment_date(String enrichment_date) {
        this.enrichment_date = enrichment_date;
    }

    public String getModified_date() {
        return modified_date;
    }

    public void setModified_date(String modified_date) {
        this.modified_date = modified_date;
    }

    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public List<Creative> getCreatives() {
        return creatives;
    }

    public void setCreatives(List<Creative> creatives) {
        this.creatives = creatives;
    }

    public List<Composite> getComposites() {
        return composites;
    }

    public void setComposites(List<Composite> composites) {
        this.composites = composites;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getBlind() {
        return blind;
    }

    public void setBlind(String blind) {
        this.blind = blind;
    }

    public Set<AbstractVariable> getCreative_variables() {
        return creative_variables;
    }

    public void setCreative_variables(Set<AbstractVariable> creative_variables) {
        this.creative_variables = creative_variables;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public boolean isPublish() {
        return publish;
    }

    public void setPublish(boolean publish) {
        this.publish = publish;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JsonElement getMetadata() {
        return metadata;
    }

    public void setMetadata(JsonElement metadata) {
        this.metadata = metadata;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getServing_status() {
        return serving_status;
    }

    public void setServing_status(String serving_status) {
        this.serving_status = serving_status;
    }

    public AggregateLatestDistribution getAggregated_latest_distribution() {
        return aggregated_latest_distribution;
    }

    public void setAggregated_latest_distribution(AggregateLatestDistribution aggregated_latest_distribution) {
        this.aggregated_latest_distribution = aggregated_latest_distribution;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAdgroupId() {
        return adgroupId;
    }

    public void setAdgroupId(String adgroupId) {
        this.adgroupId = adgroupId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public boolean isDurable_mode() {
        return durable_mode;
    }

    public void setDurable_mode(boolean durable_mode) {
        this.durable_mode = durable_mode;
    }
}
