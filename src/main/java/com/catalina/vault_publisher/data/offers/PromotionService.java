package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.SolrParam;
import com.catalina.vault_publisher.data.experience.ExperienceService;
import com.catalina.vault_publisher.data.products.ProductsService;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class PromotionService {

    private static final Logger logger = LogManager.getLogger(PromotionService.class);
    private CRestTemplate cRestTemplate;
    private OfferService offerService;

    public PromotionService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.offerService = new OfferService(cRestTemplate);
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        PromotionService promotionService = new PromotionService(new CRestTemplate());
//		String fqParam = "external_id:BL_3009_3692";
//		SolrParam param1 = new SolrParam("rows","1");
//		SolrParam param2 = new SolrParam("fq","status:PUBLISHED");
//		SolrParam param3 = new SolrParam("fq","country_code:FRA");
//		SolrParam param4 = new SolrParam("fq","modified_date:[NOW-1HOUR TO NOW]");
//		SolrParam param5 = new SolrParam("fq","classification_type:Coupon");
//		SolrParam param6 = new SolrParam("sort","modified_date desc");
//		SolrParam param7 = new SolrParam("q","*:*");

        promotionService.getPromotion(Config.Mode.PROD, "FRA-BLIP-BL_9962_5556");
//		promotionService.getFlatPromotionsFromCustomFilters(Config.Mode.PROD, Arrays.asList(param1,
//				param2,
//				param3,
//				param4,
//				param5,
//				param6,
//				param7));

        promotionService.compareCappingBetweenPromotionNAdManuf(Config.Mode.PROD, Country.CountryEnum.FRANCE, 24 * 24);

    }

    public List<String> getFlatPromotionsIdFromCampaignId(Config.Mode mode, List<String> campaignIdList) throws HttpResponseStatusException {

        String url = mode.promotionQueriesUrl + "/select?rows={rows}&fl={fl}&fq={fq}&q={q}";

        String campaignListToString = String.join(" ", campaignIdList).trim();

        String rowsParam = "150";
        String flParam = "id";
        String fqParam = String.format("campaign_id:(%s)", campaignListToString);
        String qParam = "*.*";
        logger.info(String.format("GET: %s", url));

        List<FlatPromotion> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                false,
                FlatPromotion.class,
                rowsParam,
                flParam,
                fqParam,
                qParam);

        if (docs.size() < 1)
            throw new RuntimeException(String.format("No promotion found for campaigns: %s", campaignListToString));

        logger.info(String.format("%s promotion(s) were found", docs.size()));

        List<String> promotionList = docs.stream().map(FlatPromotion::getId).collect(Collectors.toList());
        return promotionList;
        //WILL RETURN AN EMPTY LIST IF NO ADS WERE FOUND FOR THIS CAMPAIGN ID
    }

    public Promotion getPromotion(Config.Mode mode, String promoId) throws HttpResponseStatusException {
        String url = mode.dmpUrl + "/promotions/" + promoId;
        logger.info(String.format("GET: %s", url));
        Promotion promo = cRestTemplate.executeGetRequestWithJsonHeader(url, Promotion.class);
        return promo;
    }

    public List<FlatPromotion> getFlatPromotionsFromCustomFilters(Config.Mode mode, String... fqParams) throws HttpResponseStatusException {

        String url = mode.promotionQueriesUrl + "/select?rows={rows}&q={q}";

        int i = 0;
        StringBuilder stringBuilder = new StringBuilder(url);
        while (i < fqParams.length) {
            stringBuilder.append("&fq={fq}");
            i++;
        }

        url = stringBuilder.toString();

        String rowsParam = "150";
        String qParam = "*:*";
        logger.info(String.format("GET: %s", url));

        //Server respond with String instead of json, so I have to get the response as a String then convert it to FlatPromotion
        List<String> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                false,
                String.class,
                rowsParam,
                qParam,
                fqParams);

        logger.info(String.format("%s promotion(s) found", docs.size()));
        List<FlatPromotion> flatPromotionList = new ArrayList<>();
        for (String currentPromotStr : docs) {
            FlatPromotion promo = Config.gson.fromJson(currentPromotStr, FlatPromotion.class);
            flatPromotionList.add(promo);
        }

        return flatPromotionList;
        //WILL RETURN AN EMPTY LIST IF NO ADS WERE FOUND FOR THIS CAMPAIGN ID
    }

    public List<FlatPromotion> getFlatPromotionsFromCustomFilters(Config.Mode mode, List<SolrParam> params) throws HttpResponseStatusException {

        String url = mode.promotionQueriesUrl + "/select?";

        StringJoiner paramJoiner = new StringJoiner("&");
        for (SolrParam currentParam : params) {
            paramJoiner.add(String.format("%s={%s}",
                    currentParam.getKey(),
                    currentParam.getKey()));
        }

        url = String.join("", url, paramJoiner.toString());

        String[] paramsValues = params.stream().map(SolrParam::getValue).toArray(String[]::new);


        logger.info(String.format("GET: %s", url));

        //Server respond with plain/text instead of json
        List<JsonObject> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                false,
                JsonObject.class,
                paramsValues);

        logger.info(String.format("%s promotion(s) found", docs.size()));
        List<FlatPromotion> flatPromotionList = new ArrayList<>();
        for (JsonObject currentPromJs : docs) {
            FlatPromotion promo = Config.gson.fromJson(currentPromJs.toString(), FlatPromotion.class);
            flatPromotionList.add(promo);
        }

        return flatPromotionList;
        //WILL RETURN AN EMPTY LIST IF NO ADS WERE FOUND FOR THIS CAMPAIGN ID
    }

    private List<FlatPromotion> getLastPublishedPromotionForManuf(Config.Mode mode, Country.CountryEnum country, int timePeriodInHour) throws HttpResponseStatusException {
        SolrParam param = new SolrParam("rows", "1000");
        SolrParam param1 = new SolrParam("fq", "status:PUBLISHED");
        SolrParam param2 = new SolrParam("fq", String.format("country_code:%s", country.getCode()));
        SolrParam param3 = new SolrParam("fq", String.format("modified_date:[NOW-%sHOUR TO NOW]", timePeriodInHour));
        SolrParam param4 = new SolrParam("fq", "classification_type:Coupon");
        SolrParam param5 = new SolrParam("fq", "reward_funding_business_segment:MANUFACTURING");
        SolrParam param6 = new SolrParam("sort", "modified_date desc");
        SolrParam param7 = new SolrParam("q", "*:*");

        List<FlatPromotion> flatPromotionList = getFlatPromotionsFromCustomFilters(mode, Arrays.asList(param,
                param1,
                param2,
                param3,
                param4,
                param5,
                param6,
                param7));
        return flatPromotionList;
    }

    public Promotion getLastPublishedNManuallyMadePromotion(Config.Mode mode, Country.CountryEnum country, int timePeriodInHour) throws HttpResponseStatusException {
        SolrParam param = new SolrParam("rows", "1");
        SolrParam param1 = new SolrParam("fq", "status:PUBLISHED");
        SolrParam param2 = new SolrParam("fq", String.format("country_code:%s", country.getCode()));
        SolrParam param3 = new SolrParam("fq", String.format("modified_date:[NOW-%sHOUR TO NOW]", timePeriodInHour));
        SolrParam param4 = new SolrParam("fq", "classification_type:Coupon");
        SolrParam param5 = new SolrParam("fq", "user_source:ldap");
        SolrParam param6 = new SolrParam("sort", "modified_date desc");
        SolrParam param7 = new SolrParam("q", "*:*");

        List<FlatPromotion> flatPromotionList = getFlatPromotionsFromCustomFilters(mode, Arrays.asList(param,
                param1,
                param2,
                param3,
                param4,
                param5,
                param6,
                param7));

        if (flatPromotionList.isEmpty())
            throw new RuntimeException(String.format("%s promotion found in the last %s hour(s)",
                    flatPromotionList.size(),
                    timePeriodInHour));

        FlatPromotion flatPromotion = flatPromotionList.get(0);
        Promotion promotion = getPromotion(mode, flatPromotion.getId());

        return promotion;
    }

    private Integer parseCappingInAd(Ad ad) {
        Integer adCapping = null;
        if (ad.getBudget() != null && ad.getBudget().getCaps() != null && !ad.getBudget().getCaps().isEmpty()) {
            adCapping = ad.getBudget().getCaps().get(0).getCount();
        }
        return adCapping;
    }

    private Integer parseCappingInFlatPromotion(FlatPromotion flatPromotion) {
        if (flatPromotion.getCap_descr_store() == null || flatPromotion.getCap_descr_store().isEmpty()) return null;

        String cappingDescription = flatPromotion.getCap_descr_store();
        //"cap_descr_store":"14000: impression for ADGROUP during 1 Lifetime for store channel(s)",
        if (cappingDescription.contains(" AD ")) {
            try {
                String cappingStr = cappingDescription.substring(0, cappingDescription.indexOf(":"));
                int promotionCapping = Integer.parseInt(cappingStr);
                return promotionCapping;
            } catch (NumberFormatException nbe) {
                logger.error(String.format("%s, error parsing flatPromotion capping: %s",
                        flatPromotion.getExternal_id(),
                        nbe.getMessage()));
                return null;
            } catch (Exception e) {
                logger.error(String.format("%s, error parsing flatPromotion capping: %s",
                        flatPromotion.getExternal_id(),
                        e.getMessage()));
                return null;
            }
        }
        return null;
    }

    public List<String> compareCappingBetweenPromotionNAdManuf(Config.Mode mode, Country.CountryEnum country, int timePeriodInHour) throws HttpResponseStatusException {
        List<String> adsWithCappignAnomaly = new ArrayList<>();

        List<FlatPromotion> flatPromotionList = getLastPublishedPromotionForManuf(mode, country, timePeriodInHour);

        for (FlatPromotion currentFlatPromotion : flatPromotionList) {
            String externalId = currentFlatPromotion.getExternal_id();

            /**************** GET THE AD LINKED TO THE PROMOTION ****************/
            SolrParam adParam1 = new SolrParam("fq",
                    String.format("{\"external_id\":\"%s\"}", externalId));
            List<Ad> adList = offerService.getOffersFromDashBoard(mode, Ad.class, Arrays.asList(adParam1));

            if (adList.size() != 1) throw new RuntimeException(String.format("%s ad(s) found with the external_id: %s",
                    adList.size(),
                    externalId));

            Ad ad = adList.get(0);
            /********************************************************************/


            /**************** PARSE THE CAPPING IN BOTH OBJECTS ****************/
            Integer adCapping = parseCappingInAd(ad);
            Integer promotionCapping = parseCappingInFlatPromotion(currentFlatPromotion);
            /********************************************************************/

            if (String.valueOf(adCapping).equals(String.valueOf(promotionCapping))) continue;

            logger.warn(String.format("%s: Promotion capping %s | Ad capping %s", externalId, promotionCapping, adCapping));
            adsWithCappignAnomaly.add(externalId);
        }
        return adsWithCappignAnomaly;
    }

    public Promotion createMinimalPromotion(Ad ad, List<TouchPoint> touchpointList) {
        Promotion promotion = new Promotion(new ArrayList<>(),
                ad.getExperience().getReward().getId(),
                ad.getName(),
                ad.getCampaign().getId(),
                ad.getCampaign().getName(),
                ad.getCampaign(),
                ad.getBilling(),
                ad.getAdgroup().getId(),
                ad.getAdgroup().getName(),
                ad.getAdgroup(),
                null,
                ad.getAd_creative_set_id(),
                "ACCELERATED",
                null,
                null,
                ad.getBudget(),
                null,
                null,
                new ExperienceService().createExperiencePromotion(
                        ad.getExternal_id(),
                        ad.getExperience().getReward().getId(),
                        ad.getExperience().getReward().getExpiration_date(),
                        ad.getExperience().getReward().getFunding()),
                null,
                new Targeting(touchpointList, new ArrayList<>()),
                null,
                ad.getClient(),
                ad.getStatus(),
                null,
                ad.getChannels(),
                ad.getStart_date(),
                null,
                ad.getStop_date(),
                null,
                ad.getSystem_id(),
                ad.getExternal_id(),
                ad.getCountry_code(),
                new ProductsService().setupProductsMinimalPromotion(false, new ArrayList<>()),
                ad.getClassification(),
                null,
                ad.getDate_modified(),
                null,
                ad.getCreatives(),
                new ArrayList<>(),
                null,
                null,
                null,
                ad.getCreative_variables(),
                new ArrayList(),
                false,
                null,
                null,
                null,
                JsonNull.INSTANCE,
                null,
                null,
                null,
                ad.getCountry_code(),
                ad.getAdgroup().getId(),
                ad.getCampaign().getId(),
                ad.getExternal_id(),
                null,
                false);
        return promotion;
    }
}
