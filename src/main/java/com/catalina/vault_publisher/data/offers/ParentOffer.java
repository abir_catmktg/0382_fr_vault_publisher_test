package com.catalina.vault_publisher.data.offers;

public class ParentOffer {
    private String id;
    private String name;

    public ParentOffer() {
    }

    public ParentOffer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
