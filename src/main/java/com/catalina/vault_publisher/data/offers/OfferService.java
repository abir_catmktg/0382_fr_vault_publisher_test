package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.InputVariable;
import com.catalina.vault_publisher.data.SolrParam;
import com.catalina.vault_publisher.data.offers.AbstractOffer.OfferEnum;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.Touchpoints;
import com.catalina.vault_publisher.data.touchpoint.TouchpointsService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.*;

public class OfferService {

    private CRestTemplate cRestTemplate;
    private static final Logger logger = LogManager.getLogger(OfferService.class);

    public OfferService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public Touchpoints setTouchpointsForAnOffer(Config.Mode mode,
                                                Country.CountryEnum country,
                                                List<InputVariable> touchPointToInclude,
                                                List<InputVariable> touchPointToExclude) throws HttpResponseStatusException {
        TouchpointsService touchpointsService = new TouchpointsService(cRestTemplate);

        List<TouchPoint> includeList = touchpointsService.formatTouchPointList(mode, country, touchPointToInclude);
        List<TouchPoint> excludeList = touchpointsService.formatTouchPointList(mode, country, touchPointToExclude);

        Touchpoints touchpoints = touchpointsService.createTouchpoints(includeList, excludeList);

        return touchpoints;

    }

    public <T> String postPromotion(Config.Mode mode, Promotion body) throws HttpResponseStatusException {

        String url = mode.dmpUrl + "/promotions";
        logger.info(String.format("POST: %s", url));

        LinkedHashMap response = cRestTemplate.executePostRequestWithJsonHeader(url,
                Config.toJsString(body),
                LinkedHashMap.class);
        if (response.containsKey("id") && response.containsKey("name")) {
            return response.get("id").toString();
        } else throw new RuntimeException("Error posting Promotion");
    }

    public <T> String postOffer(Config.Mode mode, OfferEnum offerType, T body) throws HttpResponseStatusException {
        String url = mode.urlDashboard + offerType.url;

        String payload;
        if (body instanceof String) payload = body.toString();
        else payload = Config.toJsString(body);

        logger.info(String.format("POST: %s", url));
        OfferResponseWrapper response = cRestTemplate.executePostRequestWithJsonHeader(url, payload, OfferResponseWrapper.class);

        if (response.getErrors() > 0) throw new RuntimeException(String.format("Error posting %s", offerType.name));

        else if (response.getInserted() == 1) {
            String offerUUID = response.getGenerated_keys().get(0);
            logger.info(String.format("%s inserted :%s", offerType.name, offerUUID));
            return offerUUID;
        } else if (response.getReplaced() == 1) {
            logger.info(String.format("%s replaced", offerType.name));
        } else if (response.getDeleted() == 1) {
            logger.info(String.format("%s deleted", offerType.name));
        }
        return null;
    }

    public <T> List<T> castStringToCustomObject(Class<T> clazz, List<LinkedHashMap> hashMaps) {
        List<T> offerList = new ArrayList<>();
        for (LinkedHashMap currentHmap : hashMaps) {
            JsonObject jsOffer = Config.gson.toJsonTree(currentHmap).getAsJsonObject();
            if (jsOffer.has("billing")) {
                if (jsOffer.getAsJsonObject("billing").has("business_segment")) {
                    try {
                        jsOffer.getAsJsonObject("billing").get("business_segment").getAsString();
                    } catch (Exception e) {
                        String business_seg = jsOffer.getAsJsonObject("billing").getAsJsonObject("business_segment").get("bus_seg_descr").getAsString();
                        jsOffer.getAsJsonObject("billing").addProperty("business_segment", business_seg);
                    }
                }
            }
            T offer = Config.gson.fromJson(jsOffer.toString(), (Type) clazz);
            offerList.add(offer);
        }
        return offerList;
    }

    public <T> List<T> getOffersFromDashBoard(Config.Mode mode,
                                              Class<T> clazz,
                                              List<SolrParam> params) throws HttpResponseStatusException {

        OfferEnum offerEnum = OfferEnum.fromClass(clazz);

        String url = String.join("", mode.urlDashboard, offerEnum.getUrl(), "?");

        StringJoiner paramJoiner = new StringJoiner("&");
        for (SolrParam currentParam : params) {
            paramJoiner.add(String.format("%s={%s}",
                    currentParam.getKey(),
                    currentParam.getKey()));
        }

        url = String.join("", url, paramJoiner.toString());

        String[] paramsValues = params.stream().map(SolrParam::getValue).toArray(String[]::new);


        logger.info(String.format("GET: %s", url));

        List<LinkedHashMap> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                true,
                LinkedHashMap.class,
                paramsValues);

        List<T> offerList = castStringToCustomObject(clazz, docs);

        return offerList;
    }

    public <T> List<T> getOffersFromDashBoard(Config.Mode mode,
                                              Country.CountryEnum country,
                                              Class<T> clazz,
                                              String fqParam) throws HttpResponseStatusException {

        SolrParam startParam = new SolrParam("start", "0");
        SolrParam rowsParam = new SolrParam("rows", "150");
        SolrParam fqSolrParam = new SolrParam("fq", fqParam);

        return getOffersFromDashBoard(mode, clazz, Arrays.asList(startParam, rowsParam, fqSolrParam));
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        OfferService offerService = new OfferService(new CRestTemplate());

        /***************** GET CAMPAIGN *****************/
//		String fqParam = "{\"name\":\"Coupon 3€\",\"campaign\":{\"name\":\"[Test] Ocicat - Test frequency capping 17/01/20\"},\"adgroup\":{\"name\":\"[Test] Ocicat - Adgroup 08/01/2020 17:28\"}}";
//		offerService.getOffersFromDashBoard(Config.Mode.PROD, Country.CountryEnum.FRANCE,Ad.class,fqParam);
        /***********************************************/

        /******************* GET AD *******************/
        String fqParam = "{\"external_id\":\"BL_9962_5556\"}";
        offerService.getOffersFromDashBoard(Config.Mode.PROD, Country.CountryEnum.FRANCE, Ad.class, fqParam);
        /***********************************************/

        /******************* GET AD *******************/
//		SolrParam startParam = new SolrParam("start","0");
//		SolrParam rowsParam = new SolrParam("rows","150");
//		SolrParam wcParam = new SolrParam("wc","{\"external_id\":\"BL_4106_6126|BL_0834_4167\"}");
//
//		offerService.getOffersFromDashBoard(Config.Mode.PROD,Ad.class,Arrays.asList(startParam,
//				rowsParam,
//				wcParam));
        /***********************************************/
    }
}
