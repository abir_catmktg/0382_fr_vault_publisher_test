package com.catalina.vault_publisher.data.offers;

import com.catalina.vault_publisher.CPGPublisher;
import com.catalina.vault_publisher.data.AdGroupLimit;
import com.catalina.vault_publisher.data.AdGroupRules;
import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.capping.CappingService;
import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.experience.AdGroupExperience;
import com.catalina.vault_publisher.data.measurement.AdGroupMeasurement;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AdGroupService {

    private static final Logger logger = LogManager.getLogger(AdGroupService.class);
    private CRestTemplate cRestTemplate;
    private CappingService cappingService;
    private OfferService offerService;

    public AdGroupService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.cappingService = new CappingService();
        this.offerService = new OfferService(cRestTemplate);
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        AdGroupService adGroupService = new AdGroupService(new CRestTemplate());


        adGroupService.searchAdGroup(Mode.PROD,
                CountryEnum.FRANCE,
                "24902e52-63ac-495d-956e-fcc473b468ba",
                "ECOBON");


    }

    public List<Adgroup> searchAdGroup(Mode mode, CountryEnum country, String campaignId, String name) throws HttpResponseStatusException {
        String fqParam = String.format("{\"campaign\":{\"id\":\"%s\"},\"name\":\"%s\"}", campaignId, name);
        List<Adgroup> docs = offerService.getOffersFromDashBoard(mode, country, Adgroup.class, fqParam);
        return docs;
    }

    public Adgroup createMinimalAdGroup(String campaignId,
                                        String campaignName,
                                        String adGroupName,
                                        Adgroup.DeliveryMethod deliveryMethod,
                                        Adgroup.AdRotation adRotation,
                                        Adgroup.RoadBloacking roadBlocking,
                                        String user) {
        Adgroup adgroup = new Adgroup(adGroupName,
                Constants.STATUS_DRAFT,
                user,
                adRotation.getValue(),
                new Budget(false),
                new ParentOffer(campaignId, campaignName),
                deliveryMethod.getValue(),
                new AdGroupExperience(),
                new ArrayList<>(),
                new AdGroupMeasurement(),
                roadBlocking.getValue(),
                new AdGroupRules(new AdGroupLimit()));
        return adgroup;
    }

    public Adgroup setupAdGroupManuf(String campaignId, String campaignName, String adGroupName, String deliveryMethod) {

        List<FrequencyCap> capsList = new ArrayList<>();
        if ("ECOLINK".equalsIgnoreCase(adGroupName)) {
            capsList.add(cappingService.setupFrequencyCapsForEcolink());
        }

        Adgroup adgroup = createMinimalAdGroup(campaignId,
                campaignName,
                adGroupName,
                Adgroup.DeliveryMethod.fromValue(deliveryMethod),
                Adgroup.AdRotation.DEFAULT,
                Adgroup.RoadBloacking.SINGLE,
                CPGPublisher.createdBy != null ? CPGPublisher.createdBy : Constants.BULK_MANUF);

        adgroup.setFrequency_caps(capsList);

        return adgroup;
    }
}
