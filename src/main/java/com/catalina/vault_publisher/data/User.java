package com.catalina.vault_publisher.data;

public class User {
    public String displayName;
    public String id;
    public String email;

    public User() {
    }

    public User(String displayName, String id, String email) {
        this.displayName = displayName;
        this.id = id;
        this.email = email;
    }
}
