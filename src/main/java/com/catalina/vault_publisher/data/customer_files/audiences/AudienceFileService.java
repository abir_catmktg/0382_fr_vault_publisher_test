package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.data.customer_files.CustomFileService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

public class AudienceFileService {

    private static final Logger logger = LogManager.getLogger(AudienceFileService.class);
    private CRestTemplate cRestTemplate;
    private AudienceService audienceService;

    public AudienceFileService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.audienceService = new AudienceService(cRestTemplate);
    }

    public static void main(String[] args) {
        new AudienceFileService(new CRestTemplate()).formatFileToAudience(Country.CountryEnum.FRANCE, 23, "C:/Users/fperessi/Desktop/cid_20191114_142700.txt");
    }

    public AudienceFile setAudienceFile(Config.Mode mode, String audienceId, String fileName, User user, String countryCode) throws HttpResponseStatusException {
        Audience audience = audienceService.getAudienceFromId(mode, audienceId);
        AudienceFile audienceFile = new AudienceFile(UUID.randomUUID().toString(),
                CustomFileService.FileType.audience.getName(),
                audience,
                audience.getName() + " - " + fileName,
                mode.modeStr,
                Constants.VAULT_SYSTEM_ID,
                user,
                mode.urlDashboard + "/targeting/status",
                fileName,
                null,
                countryCode);
        return audienceFile;
    }

    public AudienceFile setAudienceFile(Config.Mode mode, Audience audience, String fileName, User user, String countryCode) {
        AudienceFile audienceFile = new AudienceFile(UUID.randomUUID().toString(),
                CustomFileService.FileType.audience.getName(),
                audience,
                audience.getName() + " - " + fileName,
                mode.modeStr,
                Constants.VAULT_SYSTEM_ID,
                user,
                mode.urlDashboard + "/targeting/status",
                fileName,
                null,
                countryCode);
        return audienceFile;
    }

    public void firstPostAudienceFile(Config.Mode mode, AudienceFile audienceFile) throws HttpResponseStatusException {
        if (audienceFile.getAudience().getSpecifications() == null) {
            audienceFile.getAudience().setSpecifications(new ArrayList<>());
        }
        String audienceFileStr = Config.toJsString(audienceFile);
        String url = mode.dmpUrl + "/upload";
        cRestTemplate.executeRequestWithJsonHeader(url, audienceFileStr, String.class, HttpMethod.POST, HttpStatus.ACCEPTED);
    }

    public void secondPostAudienceFile(Config.Mode mode, AudienceFile audienceFile, String sftpFilePath) throws HttpResponseStatusException {
        audienceFile.setPath(sftpFilePath);
        String audienceFileStr = Config.toJsString(audienceFile);
        String url = mode.dmpUrl + "/batch/requests";
        cRestTemplate.executeRequestWithJsonHeader(url, audienceFileStr, String.class, HttpMethod.POST, HttpStatus.OK);
    }

    public void formatFileToAudience(Country.CountryEnum country, int networkId, String filePath) {
        String network = String.format("%04d", networkId);
        try {
            BufferedReader file = new BufferedReader(new FileReader(filePath));
            StringBuffer inputBuffer = new StringBuffer();
            String line;
            while ((line = file.readLine()) != null) {
                line = String.join("-", country.getCode(), network, line);
                inputBuffer.append(line);
                inputBuffer.append('\n');
            }
            file.close();

            // write the new string with the replaced line OVER the same file
            FileOutputStream fileOut = new FileOutputStream(filePath);
            fileOut.write(inputBuffer.toString().getBytes());
            fileOut.close();
        } catch (IOException e) {
            throw new RuntimeException(String.format("error formatting to audience the file: %s", filePath));
        }
    }

}
