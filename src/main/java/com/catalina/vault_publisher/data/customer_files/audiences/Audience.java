package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class Audience {

    private String id;
    private String name;
    private User user;
    private String status;
    private String date_modified;
    private String country_code;
    private List<String> tags;
    private List<Specifications> specifications;

    public Audience() {
    }

    public Audience(String id, String name, User user, String status, String country_code, List<String> tags) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.status = status;
        this.date_modified = DateUtils.getCurrentDateMilliSecondPrecision();
        this.country_code = country_code;
        this.tags = tags;
        this.specifications = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public String getStatus() {
        return status;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getCountry_code() {
        return country_code;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<Specifications> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(List<Specifications> specifications) {
        this.specifications = specifications;
    }
}
