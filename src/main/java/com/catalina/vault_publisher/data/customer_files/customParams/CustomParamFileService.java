package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.data.customer_files.CustomFileService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.UUID;

public class CustomParamFileService {

    private static final Logger logger = LogManager.getLogger(CustomParamFileService.class);
    private CRestTemplate cRestTemplate;

    public CustomParamFileService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public CustomParamFile setCustomParamFile(Mode mode, CustomParam customParam, String fileName, User user, String countryCode) {
        CustomParamFile customParamFile = new CustomParamFile(UUID.randomUUID().toString(),
                CustomFileService.FileType.customParam.getName(),
                customParam,
                customParam.getName() + " - " + fileName,
                mode.modeStr,
                Constants.VAULT_SYSTEM_ID,
                user,
                mode.urlDashboard + "/targeting/status",
                fileName,
                null,
                countryCode);
        return customParamFile;
    }

    public void firstPostCustomFile(Mode mode, CustomParamFile customParamFile) throws HttpResponseStatusException {
        if (customParamFile.getEntity().getFiles() == null) {
            customParamFile.getEntity().setFiles(new ArrayList<>());
        }
        String customParamFileStr = Config.toJsString(customParamFile);
        String url = mode.dmpUrl + "/upload";
        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", customParamFileStr));
        cRestTemplate.executeRequestWithJsonHeader(url, customParamFileStr, String.class, HttpMethod.POST, HttpStatus.ACCEPTED);
    }

    public void secondPostCustomFile(Mode mode, CustomParamFile customParamFile, String sftpFilePath) throws HttpResponseStatusException {
        customParamFile.setPath(sftpFilePath);
        String customParamFileStr = Config.toJsString(customParamFile);
        String url = mode.dmpUrl + "/batch/requests";
        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", customParamFileStr));
        cRestTemplate.executeRequestWithJsonHeader(url, customParamFileStr, String.class, HttpMethod.POST, HttpStatus.OK);

    }

}
