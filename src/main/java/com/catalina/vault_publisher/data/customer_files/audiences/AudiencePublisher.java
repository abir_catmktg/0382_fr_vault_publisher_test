package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class AudiencePublisher {

    private static final Logger logger = LogManager.getLogger(AudiencePublisher.class);
    Mode m_mode;
    CountryEnum m_country;
    private CRestTemplate cRestTemplate;
    private Audience m_Audience = null;
    private AudienceService audiencesService;
    private AudienceFile m_AudienceFile = null;
    private AudienceFileService audienceFileService;


    public AudiencePublisher(CRestTemplate cRestTemplate) {
        this(Mode.SQA, CountryEnum.FRANCE, cRestTemplate);
    }

    public AudiencePublisher(Mode mode, CRestTemplate cRestTemplate) {
        this(mode, CountryEnum.FRANCE, cRestTemplate);
    }

    public AudiencePublisher(Mode mode, CountryEnum country, CRestTemplate cRestTemplate) {
        this.m_mode = mode;
        this.m_country = country;
        this.cRestTemplate = cRestTemplate;
        this.audiencesService = new AudienceService(this.cRestTemplate);
        this.audienceFileService = new AudienceFileService(this.cRestTemplate);
    }

    public static void main(String[] args) {


        // ######## Wallet File upload example ########
        String audienceName = "Bulk test fabien SFTP 31-10-2019_004";
        List<String> tags = Collections.singletonList("AUDIENCES BATCH UPLOAD FR");
        String currentFilePath = "sftp://FR-DevSFTP:YC3738lYYT841hK@198.204.75.221/Vault_audience_pull/audience5.txt";

        AudiencePublisher myAudience = new AudiencePublisher(Mode.PROD, CountryEnum.FRANCE, new CRestTemplate());
//		Result res = myAudience.createNewAudience(audienceName,
//				Constants.SURICAT_USER,
//				Country.CountryEnum.FRANCE.code,
//				tags,
//				currentFileName,
//				currentFilePath);


        try {
            HttpStatus httpStatus = myAudience.addFilesToExistingAudience("585b0ecb-9a93-4e4f-9fd7-a59cdd0b65b2",
                    Constants.SURICAT_USER,
                    currentFilePath);
        } catch (HttpResponseStatusException e) {
            e.printStackTrace();
        }

        System.out.println("OK");
        // ############################################
    }

    public void formatAudience(int networkId, String filePath) {
        audienceFileService.formatFileToAudience(m_country, networkId, filePath);
    }

    public HttpStatus createNewAudience(String audienceName,
                                        User user,
                                        List<String> tags,
                                        String filePath) throws HttpResponseStatusException {
        this.m_Audience = new Audience(UUID.randomUUID().toString(),
                audienceName,
                user,
                Constants.STATUS_DRAFT,
                m_country.getCode(),
                tags);
        Path path = Paths.get(filePath);
        audiencesService.createAudience(m_mode, m_Audience);
        m_AudienceFile = audienceFileService.setAudienceFile(m_mode, this.m_Audience, path.getFileName().toString(), user, m_country.getCode());
        audienceFileService.firstPostAudienceFile(m_mode, m_AudienceFile);
        audienceFileService.secondPostAudienceFile(m_mode, m_AudienceFile, filePath);
        HttpStatus httpStatus = audiencesService.updateSpecifications(m_mode, m_Audience, m_AudienceFile, user);
        return httpStatus;

    }

    public HttpStatus addFilesToExistingAudience(String audienceId,
                                                 User user,
                                                 String filePath) throws HttpResponseStatusException {
        this.m_Audience = audiencesService.getAudienceFromId(m_mode, audienceId);
        Path path = Paths.get(filePath);
        m_AudienceFile = audienceFileService.setAudienceFile(m_mode,
                this.m_Audience.getId(),
                path.getFileName().toString(),
                user,
                m_Audience.getCountry_code());
        audienceFileService.firstPostAudienceFile(m_mode, m_AudienceFile);
        audienceFileService.secondPostAudienceFile(m_mode, m_AudienceFile, filePath);
        HttpStatus httpStatus = audiencesService.updateSpecifications(m_mode, m_Audience, m_AudienceFile, user);
        return httpStatus;
    }

}
