package com.catalina.vault_publisher.data.customer_files;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.util.UUID;

public class CustomFileService {

    private static final Logger logger = LogManager.getLogger(CustomFileService.class);
    private CRestTemplate cRestTemplate;

    public CustomFileService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public String getIdDMP(Config.Mode mode, FileType fileType, String uuid) {
        String url = mode.dmpUrl + fileType.urlSuffix + "/" + uuid;
        Boolean uuidAvailable = false;
        int nbAttemps = 0;
        while (!uuidAvailable && nbAttemps < 5) {
            try {
                logger.info(String.format("GET: %s", url));
                cRestTemplate.executeRequestWithJsonHeader(url, null, String.class, HttpMethod.GET, HttpStatus.NO_CONTENT);
                uuidAvailable = true;
                logger.info(String.format("%s UUID: %s", fileType.name, uuid));
                return uuid;
            } catch (HttpResponseStatusException e) {
                logger.warn(String.format("%s UUID is already in use: %s", fileType.name, uuid));
                uuid = UUID.randomUUID().toString();
                nbAttemps++;
            }
        }
        throw new RuntimeException(String.format("Could not find any available UUID for this %s", fileType.name));
    }

    public enum FileType {
        audience("audience", "/audiences"), customParam("custom-params", "/parameters");

        private final String name, urlSuffix;

        FileType(String name, String urlSuffix) {
            this.name = name;
            this.urlSuffix = urlSuffix;
        }

        public String getName() {
            return name;
        }

        public String getUrlSuffix() {
            return urlSuffix;
        }
    }

}
