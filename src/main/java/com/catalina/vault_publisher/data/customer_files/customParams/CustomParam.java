package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class CustomParam {
    private String id;
    private String name;
    private User user;
    private String status;
    private String date_modified;
    private String country_code;
    private String _;
    private List<String> tags;
    private List<Files> files;

    public CustomParam() {
    }

    public CustomParam(String id, String name, User user, String status, String country_code, String _, List<String> tags) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.status = status;
        this.date_modified = DateUtils.getCurrentDateMilliSecondPrecision();
        this.country_code = country_code;
        this._ = _;
        this.tags = tags;
        this.files = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<Files> getFiles() {
        return files;
    }

    public void setFiles(List<Files> files) {
        this.files = files;
    }
}
