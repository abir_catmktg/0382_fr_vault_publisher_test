package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.data.customer_files.CustomFileService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.DateUtils;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

public class CustomParamService {

    private static final Logger logger = LogManager.getLogger(CustomParamService.class);
    private CRestTemplate cRestTemplate;
    private CustomFileService customFileService;
    private FilesService filesService;

    public CustomParamService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.customFileService = new CustomFileService(cRestTemplate);
        this.filesService = new FilesService();
    }

    public void createCustomParam(Mode mode, CustomParam customParam) throws HttpResponseStatusException {
        CustomFileService.FileType fileType = CustomFileService.FileType.customParam;
        String uuid = customFileService.getIdDMP(mode, fileType, customParam.getId());
        customParam.setId(uuid);

        String jsCustomParam = Config.toJsString(customParam);
        String url = mode.urlDashboard + fileType.getUrlSuffix();

        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", jsCustomParam));
        String response = cRestTemplate.executePostRequestWithJsonHeader(url, jsCustomParam, String.class);
        //logger.debug(String.format("response:\\n %s",response));
    }

    public HttpStatus updateFiles(Mode mode, CustomParam customParam, CustomParamFile customParamFile, User user) throws HttpResponseStatusException {
        Files files = filesService.setFileFR(mode, customParamFile, user);
        customParam.getFiles().add(files);
        customParam.setDate_modified(DateUtils.getCurrentDateMilliSecondPrecision());

        String body = Config.toJsString(customParam);
        String url = mode.urlDashboard + CustomFileService.FileType.customParam.getUrlSuffix();

        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", body));
        String response = cRestTemplate.executePostRequestWithJsonHeader(url, body, String.class);
        //logger.debug(String.format("response:\\n %s",response));
        return HttpStatus.OK;


    }
}
