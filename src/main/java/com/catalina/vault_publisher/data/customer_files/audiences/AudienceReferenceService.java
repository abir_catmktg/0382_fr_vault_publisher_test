package com.catalina.vault_publisher.data.customer_files.audiences;

public class AudienceReferenceService {

    private static final String FIELD_VALUE_FOR_AUDIENCE_REF = "audience";
    private static final String NAME_PATTERN_FOR_AUDIENCE_REF = "<a target=\\\"_blank\\\" href=#/platform/audiences/edit/%s>%s</a>";
    private static final String URL_PATTERN_FOR_AUDIENCE_REF = "#/platform/audiences/edit/%s";

    public AudienceReferenceService() {
    }

    public AudienceReference createAudienceReference(String id, String name) {
        AudienceReference audienceReference = new AudienceReference(FIELD_VALUE_FOR_AUDIENCE_REF,
                id,
                String.format(NAME_PATTERN_FOR_AUDIENCE_REF, id, name),
                String.format(URL_PATTERN_FOR_AUDIENCE_REF, id));
        return audienceReference;
    }


}
