package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.User;

public class AudienceFile {
    private String id;
    private String _t;
    private Audience audience;
    private String name;
    private String environment;
    private String system_id;
    private User user;
    private String callback_url;
    private String filename;
    private String path;
    private String country_code;

    public AudienceFile() {
    }

    public AudienceFile(String id,
                        String _t,
                        Audience audience,
                        String name,
                        String environment,
                        String system_id,
                        User user,
                        String callback_url,
                        String filename,
                        String path,
                        String country_code) {
        this.id = id;
        this._t = _t;
        this.audience = audience;
        this.name = name;
        this.environment = environment;
        this.system_id = system_id;
        this.user = user;
        this.callback_url = callback_url;
        this.filename = filename;
        this.path = path;
        this.country_code = country_code;
    }

    public String getId() {
        return id;
    }

    public String get_t() {
        return _t;
    }

    public Audience getAudience() {
        return audience;
    }

    public String getName() {
        return name;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getSystem_id() {
        return system_id;
    }

    public User getUser() {
        return user;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public String getFilename() {
        return filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCountry_code() {
        return country_code;
    }
}
