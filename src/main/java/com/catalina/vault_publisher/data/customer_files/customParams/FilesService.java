package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.DateUtils;

public class FilesService {

    private static final String URI_PATTERN_FOR_CUSTOM_PARAM_FILE = "s3://uploads.catalinamarketing.com/audiences-eu/date=%s/upload=%s/%s";

    public FilesService() {
    }

    public Files setFileFR(Config.Mode mode, CustomParamFile customParamFile, User user) {
        return new Files(customParamFile.getId(),
                "",
                DateUtils.getCurrentDateMilliSecondPrecision(),
                DateUtils.getCurrentDateMilliSecondPrecision(),
                user,
                customParamFile.getFilename(),
                String.format(URI_PATTERN_FOR_CUSTOM_PARAM_FILE,
                        DateUtils.getCurrentDate(),
                        customParamFile.getId(),
                        customParamFile.getFilename()),
                customParamFile.getPath(),
                Constants.STATUS_PROCESSING,
                customParamFile.getFilename(),
                mode.modeStr);
    }

}
