package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.data.customer_files.CustomFileService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.DateUtils;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.util.List;

public class AudienceService {

    private static final Logger logger = LogManager.getLogger(AudienceService.class);
    private CRestTemplate cRestTemplate;
    private CustomFileService customFileService;

    public AudienceService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.customFileService = new CustomFileService(cRestTemplate);
    }

    public void createAudience(Config.Mode mode, Audience audience) throws HttpResponseStatusException {
        CustomFileService.FileType fileType = CustomFileService.FileType.audience;
        String uuid = customFileService.getIdDMP(mode, fileType, audience.getId());
        audience.setId(uuid);
        String jsAudience = Config.toJsString(audience);

        String url = mode.urlDashboard + fileType.getUrlSuffix();
        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", jsAudience));
        String response = cRestTemplate.executePostRequestWithJsonHeader(url, jsAudience, String.class);
        //logger.debug(String.format("response:\\n %s",response));
    }

    public Audience getAudienceFromId(Config.Mode mode, String audienceId) throws HttpResponseStatusException {
        String url = mode.urlDashboard + CustomFileService.FileType.audience.getUrlSuffix() + "/" + audienceId;
        logger.info(String.format("GET: %s", url));
        Audience audience = cRestTemplate.executeGetRequestWithJsonHeader(url, Audience.class);
        //logger.debug(String.format("audience : %s",Config.toJsString(audience)));
        return audience;

    }

    public List<Audience> getAudienceFromName(Config.Mode mode, CountryEnum country, String audienceName) throws HttpResponseStatusException {
        String url = mode.urlDashboard + CustomFileService.FileType.audience.getUrlSuffix()
                + "?rows={rows}&start={start}&in={in}&wc={wc}&op={op}";
        String rowsParam = "20";
        String startParam = "0";
        String inParam = "{\"country_code\":\"" + country.getCode() + "\"}";
        String wcParam = "{\"name\":\"" + audienceName + "\",\"filename\":\"" + audienceName + "\"}";
        String opParam = "or";
        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n rows: %s \\n start: %s \\n in: %s \\n wc: %s \\n op: %s",
                rowsParam,
                startParam,
                inParam,
                wcParam,
                opParam));
        List<Audience> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url,
                Audience.class,
                rowsParam,
                startParam,
                inParam,
                wcParam,
                opParam);
        logger.debug(String.format("%s Audience(s) found for the name %s", docs.size(), audienceName));
        return docs;
    }

    public HttpStatus updateSpecifications(Config.Mode mode, Audience audience, AudienceFile audienceFile, User user) throws HttpResponseStatusException {

        Specifications specs = new SpecificationsService().setSpecificationsFR(mode, audienceFile, user);
        audience.getSpecifications().add(specs);
        audience.setDate_modified(DateUtils.getCurrentDateMilliSecondPrecision());

        String body = Config.toJsString(audience);
        String url = mode.urlDashboard + CustomFileService.FileType.audience.getUrlSuffix();

        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", body));
        cRestTemplate.executePostRequestWithJsonHeader(url, body, String.class);
        return HttpStatus.OK;
    }

}
