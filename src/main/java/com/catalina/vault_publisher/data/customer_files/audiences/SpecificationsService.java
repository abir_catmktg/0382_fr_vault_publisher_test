package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.DateUtils;

public class SpecificationsService {

    public SpecificationsService() {
    }

    public Specifications setSpecificationsFR(Config.Mode mode, AudienceFile audienceFile, User user) {
        return new Specifications(audienceFile.getId(),
                "",
                DateUtils.getCurrentDateMilliSecondPrecision(),
                DateUtils.getCurrentDateMilliSecondPrecision(),
                user,
                audienceFile.getFilename(),
                "s3://uploads.catalinamarketing.com/audiences-eu/date=" + DateUtils.getCurrentDate() + "/upload=" + audienceFile.getId() + "/" + audienceFile.getFilename(),
                audienceFile.getPath(),
                Constants.STATUS_PROCESSING,
                "File",
                audienceFile.getFilename(),
                mode.modeStr,
                Country.CountryEnum.FRANCE.getCode());
    }
}
