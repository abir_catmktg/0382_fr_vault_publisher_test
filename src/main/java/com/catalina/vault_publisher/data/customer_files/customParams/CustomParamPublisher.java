package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.User;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class CustomParamPublisher {

    private static final Logger logger = LogManager.getLogger(CustomParamPublisher.class);
    Mode m_mode;
    CountryEnum m_country;
    private CRestTemplate cRestTemplate;
    private CustomParam m_customParam;
    private CustomParamService customParamService;
    private com.catalina.vault_publisher.data.customer_files.customParams.CustomParamFile m_customParamFile;
    private CustomParamFileService customParamFileService;

    public CustomParamPublisher(CRestTemplate cRestTemplate) {
        this(Mode.SQA, CountryEnum.FRANCE, cRestTemplate);
    }

    public CustomParamPublisher(Mode mode, CRestTemplate cRestTemplate) {
        this(mode, CountryEnum.FRANCE, cRestTemplate);
    }

    public CustomParamPublisher(Mode mode, CountryEnum country, CRestTemplate cRestTemplate) {
        this.m_mode = mode;
        this.m_country = country;
        this.cRestTemplate = cRestTemplate;
        this.customParamService = new CustomParamService(cRestTemplate);
        this.customParamFileService = new CustomParamFileService(cRestTemplate);
    }

    public static void main(String[] args) {
        // ######## Wallet File upload example ########
        String customParamName = "TEST CUSTOM PARAM FPE 27-11-13 16-31";
        List<String> tags = Collections.singletonList("CUSTOM PARAM BATCH UPLOAD FR");
        String currentFilePath = "sftp://FR-DevSFTP:YC3738lYYT841hK@198.204.75.221/vault_custom_param_wallet/wallet3.txt";
        String currentFileName = "wallet3.txt";

        CustomParamPublisher myParam = new CustomParamPublisher(Mode.PROD, CountryEnum.FRANCE, new CRestTemplate());

        try {
            HttpStatus httpStatus = myParam.createNewCustomParam(customParamName,
                    Constants.SURICAT_USER,
                    tags,
                    currentFileName,
                    currentFilePath);
        } catch (HttpResponseStatusException e) {
            e.printStackTrace();
        }
        System.out.println("OK");

    }

    public HttpStatus createNewCustomParam(String customParamName, User user, List<String> tags, String fileName, String filePath) throws HttpResponseStatusException {
        m_customParam = new CustomParam(UUID.randomUUID().toString(),
                customParamName,
                user,
                Constants.STATUS_DRAFT,
                m_country.getCode(),
                "static",
                tags);

        customParamService.createCustomParam(m_mode, m_customParam);
        m_customParamFile = customParamFileService.setCustomParamFile(m_mode, this.m_customParam, fileName, user, m_country.getCode());

        customParamFileService.firstPostCustomFile(m_mode, m_customParamFile);
        customParamFileService.secondPostCustomFile(m_mode, m_customParamFile, filePath);
        HttpStatus httpStatus = customParamService.updateFiles(m_mode, m_customParam, m_customParamFile, user);

        return httpStatus;
    }
}
