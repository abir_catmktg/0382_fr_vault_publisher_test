package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.User;

public class Files {
    public String id;
    public String comments;
    public String date_created;
    public String date_modified;
    public User user;
    public String name;
    public String uri;
    public String path;
    public String status;
    public String filename;
    public String environment;


    public Files() {
    }

    public Files(String id,
                 String comments,
                 String date_created,
                 String date_modified,
                 User user,
                 String name,
                 String uri,
                 String path,
                 String status,
                 String filename,
                 String environment) {
        this.id = id;
        this.comments = comments;
        this.date_created = date_created;
        this.date_modified = date_modified;
        this.user = user;
        this.name = name;
        this.uri = uri;
        this.path = path;
        this.status = status;
        this.filename = filename;
        this.environment = environment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(String date_modified) {
        this.date_modified = date_modified;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}
