package com.catalina.vault_publisher.data.customer_files.customParams;

import com.catalina.vault_publisher.data.User;

public class CustomParamFile {
    private String id;
    private String _t;
    private CustomParam entity;
    private String name;
    private String environment;
    private String system_id;
    private User user;
    private String callback_url;
    private String filename;
    private String path;
    private String country_code;

    public CustomParamFile() {
    }

    public CustomParamFile(String id,
                           String _t,
                           CustomParam entity,
                           String name,
                           String environment,
                           String system_id,
                           User user,
                           String callback_url,
                           String filename,
                           String path,
                           String country_code) {
        this.id = id;
        this._t = _t;
        this.entity = entity;
        this.name = name;
        this.environment = environment;
        this.system_id = system_id;
        this.user = user;
        this.callback_url = callback_url;
        this.filename = filename;
        this.path = path;
        this.country_code = country_code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String get_t() {
        return _t;
    }

    public void set_t(String _t) {
        this._t = _t;
    }

    public CustomParam getEntity() {
        return entity;
    }

    public void setEntity(CustomParam entity) {
        this.entity = entity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getSystem_id() {
        return system_id;
    }

    public void setSystem_id(String system_id) {
        this.system_id = system_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}
