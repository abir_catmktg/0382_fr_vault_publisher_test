package com.catalina.vault_publisher.data.customer_files.audiences;

public class AudienceReference {
    private String field;
    private String id;
    private String name;
    private String text;
    private String url;
    private String value;


    public AudienceReference() {
    }

    public AudienceReference(String field, String id, String name, String url) {
        this.field = field;
        this.id = this.value = id;
        this.name = this.text = name;
        this.url = url;
    }

    public String getField() {
        return field;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public String getUrl() {
        return url;
    }

    public String getValue() {
        return value;
    }
}
