package com.catalina.vault_publisher.data.customer_files.audiences;

import com.catalina.vault_publisher.data.User;

public class Specifications {
    public String id;
    public String comments;
    public String date_created;
    public String date_modified;
    public User user;
    public String name;
    public String uri;
    public String path;
    public String status;
    public String type = "File";
    public String filename;
    public String environment;
    public String country_code;

    public Specifications() {
    }

    public Specifications(String id,
                          String comments,
                          String date_created,
                          String date_modified,
                          User user,
                          String name,
                          String uri,
                          String path,
                          String status,
                          String type,
                          String filename,
                          String environment,
                          String country_code) {
        this.id = id;
        this.comments = comments;
        this.date_created = date_created;
        this.date_modified = date_modified;
        this.user = user;
        this.name = name;
        this.uri = uri;
        this.path = path;
        this.status = status;
        this.type = type;
        this.filename = filename;
        this.environment = environment;
        this.country_code = country_code;
    }

    public String getId() {
        return id;
    }

    public String getComments() {
        return comments;
    }

    public String getDate_created() {
        return date_created;
    }

    public String getDate_modified() {
        return date_modified;
    }

    public User getUser() {
        return user;
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public String getPath() {
        return path;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getFilename() {
        return filename;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getCountry_code() {
        return country_code;
    }
}
