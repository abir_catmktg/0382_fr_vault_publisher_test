package com.catalina.vault_publisher.data;

public class AuditHistory {
    private String email;
    private String modified_by;
    private String modified_date;
    private Boolean publish;

    public AuditHistory() {
    }

    public AuditHistory(String email, String modified_by, String modified_date, Boolean publish) {
        this.email = email;
        this.modified_by = modified_by;
        this.modified_date = modified_date;
        this.publish = publish;
    }

    public String getEmail() {
        return email;
    }

    public String getModified_by() {
        return modified_by;
    }

    public String getModified_date() {
        return modified_date;
    }

    public Boolean getPublish() {
        return publish;
    }
}
