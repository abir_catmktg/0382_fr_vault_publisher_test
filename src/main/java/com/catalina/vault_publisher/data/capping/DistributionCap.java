package com.catalina.vault_publisher.data.capping;

public class DistributionCap extends Capping {
    private String scope;
//	private String uuid;

    public DistributionCap() {
    }

    public DistributionCap(String channel_type, int count, String event_type, String level, String time_period, int time_units, String scope) {
        super(channel_type, count, event_type, level, time_period, time_units);
        this.scope = scope;
//		this.uuid = uuid;
    }

    public String getScope() {
        return scope;
    }
/*
	public String getUuid() {
		return uuid;
	}
 */
}
