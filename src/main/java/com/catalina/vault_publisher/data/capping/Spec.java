package com.catalina.vault_publisher.data.capping;

public class Spec {
    private String date;
    private String type;

    public Spec() {
    }

    public Spec(String date, String type) {
        this.date = date;
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }
}
