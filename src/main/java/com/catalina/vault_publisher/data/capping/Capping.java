package com.catalina.vault_publisher.data.capping;

import java.util.Arrays;

public abstract class Capping {
    private String channel_type;
    private int count;
    private String event_type;
    private String level;
    private String time_period;
    private Integer time_units;

    public Capping() {
    }

    public Capping(String channel_type, int count, String event_type, String level, String time_period, Integer time_units) {
        this.channel_type = channel_type;
        this.count = count;
        this.event_type = event_type;
        this.level = level;
        this.time_period = time_period;
        this.time_units = time_units;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getLevel() {
        return level;
    }

    public String getTime_period() {
        return time_period;
    }

    public Integer getTime_units() {
        return time_units;
    }

    public enum EventType {
        CLIP("clip"),
        IMPRESSION("impression");

        private String value;

        EventType(String value) {
            this.value = value;
        }

        public static EventType fromValue(String eventType) {
            return Arrays.stream(EventType.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(eventType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Event Type %s.", eventType)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum TimePeriodFrequencyCap {
        MINUTE("Minute"),
        VISIT("Session"),
        DAY("Day"),
        WEEK("Week"),
        MONTH("Month"),
        LIFETIME("Lifetime");

        private String value;

        TimePeriodFrequencyCap(String value) {
            this.value = value;
        }

        public static TimePeriodFrequencyCap fromValue(String timePeriod) {
            return Arrays.stream(TimePeriodFrequencyCap.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(timePeriod))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Time Period for FrequencyCap %s.", timePeriod)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum TimePeriodDistributionCap {
        HOUR("Hour"),
        DAY("Day"),
        WEEK("Week"),
        LIFETIME("Lifetime");

        private String value;

        TimePeriodDistributionCap(String value) {
            this.value = value;
        }

        public static TimePeriodDistributionCap fromValue(String timePeriod) {
            return Arrays.stream(TimePeriodDistributionCap.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(timePeriod))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Time Period for DistributionCap : %s.", timePeriod)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum ScopeDistributionCap {
        COUNTRY("Global"),
        SITE("Touchpoint");

        private String value;

        ScopeDistributionCap(String value) {
            this.value = value;
        }

        public static ScopeDistributionCap fromValue(String scope) {
            return Arrays.asList(ScopeDistributionCap.values()).stream()
                    .filter(x -> x.getValue().equalsIgnoreCase(scope))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Scope for DistributionCap : %s.", scope)));
        }

        public String getValue() {
            return value;
        }
    }
}
