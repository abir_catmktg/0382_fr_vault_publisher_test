package com.catalina.vault_publisher.data.capping;

import com.catalina.vault_publisher.data.Item;
import com.catalina.vault_publisher.data.offers.AbstractOffer;

import java.util.List;

public class CappingService {

    public CappingService() {
    }

    private FrequencyCap createFrequencyCaps(int count,
                                             Capping.EventType eventType,
                                             Capping.TimePeriodFrequencyCap timePeriod,
                                             FrequencyCap.TimeWindow timeWindow,
                                             FrequencyCap.WeekGroup weekGroup,
                                             Spec spec,
                                             Integer timeUnits,
                                             boolean multiple,
                                             AbstractOffer.Scope level,
                                             List<Item> additional_items,
                                             FrequencyCap.LocationScope locationScope,
                                             AbstractOffer.ChannelType channelType) {
        return new FrequencyCap(additional_items,
                channelType != null ? channelType.getValue() : null,
                count,
                eventType != null ? eventType.getValue() : null,
                level != null ? level.getValue() : null,
                locationScope != null ? locationScope.getValue() : null,
                String.valueOf(multiple),
                spec,
                timePeriod != null ? timePeriod.getValue() : null,
                timeUnits,
                timeWindow != null ? timeWindow.getValue() : null,
                weekGroup != null ? weekGroup.getValue() : null);
    }

    private DistributionCap createDistributionCapping(int count,
                                                      Capping.EventType eventType,
                                                      Capping.TimePeriodDistributionCap timePeriod,
                                                      Integer timeUnits,
                                                      AbstractOffer.Scope level,
                                                      Capping.ScopeDistributionCap scope,
                                                      AbstractOffer.ChannelType channelType) {
        return new DistributionCap(channelType.getValue(),
                count,
                eventType.getValue(),
                level.getValue().toUpperCase(),
                timePeriod.getValue(),
                timeUnits,
                scope.getValue());
    }

    public FrequencyCap createFrequencyCapsLifeTime(int count,
                                                    Capping.EventType eventType,
                                                    boolean multiple,
                                                    AbstractOffer.Scope level,
                                                    List<Item> additionalItems,
                                                    FrequencyCap.LocationScope locationScope,
                                                    AbstractOffer.ChannelType channelType) {
        return createFrequencyCaps(count,
                eventType,
                Capping.TimePeriodFrequencyCap.LIFETIME,
                null,
                null,
                null,
                1,
                multiple,
                level,
                additionalItems,
                locationScope,
                channelType);
    }

    public DistributionCap createDistributionCappingLifeTime(int count,
                                                             Capping.EventType eventType,
                                                             Integer timeUnits,
                                                             AbstractOffer.Scope level,
                                                             Capping.ScopeDistributionCap scope,
                                                             AbstractOffer.ChannelType channelType) {
        return createDistributionCapping(count, eventType, Capping.TimePeriodDistributionCap.LIFETIME, timeUnits, level, scope, channelType);
    }

    public DistributionCap setupDistributionCappingManuf(String count) {
        return createDistributionCapping(Integer.parseInt(count),
                Capping.EventType.IMPRESSION,
                Capping.TimePeriodDistributionCap.LIFETIME,
                1,
                AbstractOffer.Scope.AD,
                Capping.ScopeDistributionCap.COUNTRY,
                AbstractOffer.ChannelType.IN_STORE);
    }

    public FrequencyCap createMiniFrequencyCapping(Integer count) {
        return createFrequencyCapsLifeTime(count,
                Capping.EventType.IMPRESSION,
                false,
                AbstractOffer.Scope.AD,
                null,
                null,
                null);
    }

    public FrequencyCap setupFrequencyCapsForEcolink() {
        return createFrequencyCapsLifeTime(1,
                Capping.EventType.IMPRESSION,
                false,
                AbstractOffer.Scope.AD,
                null,
                null,
                AbstractOffer.ChannelType.IN_STORE);
    }

}
