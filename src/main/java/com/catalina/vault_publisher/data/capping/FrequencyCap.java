package com.catalina.vault_publisher.data.capping;

import com.catalina.vault_publisher.data.Item;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class FrequencyCap {
    private List<Item> additional_items;
    private String channel_type;
    private int count;
    private String event_type;
    private String level;
    private String location_scope;
    private String multiple;
    private Spec spec;
    private String time_period;
    private Integer time_units;
    private String time_window;
    private String week_group;

    public FrequencyCap() {
    }

    public FrequencyCap(List<Item> additional_items,
                        String channel_type,
                        int count,
                        String event_type,
                        String level,
                        String location_scope,
                        String multiple,
                        Spec spec,
                        String time_period,
                        int time_units,
                        String time_window,
                        String week_group) {
        this.additional_items = additional_items;
        this.channel_type = channel_type;
        this.count = count;
        this.event_type = event_type;
        this.level = level;
        this.location_scope = location_scope;
        this.multiple = multiple;
        this.spec = spec;
        this.time_period = time_period;
        this.time_units = time_units;
        this.time_window = time_window;
        this.week_group = week_group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FrequencyCap that = (FrequencyCap) o;
        return count == that.count &&
                time_units == that.time_units &&
                Objects.equals(additional_items, that.additional_items) &&
                Objects.equals(channel_type, that.channel_type) &&
                Objects.equals(event_type, that.event_type) &&
                Objects.equals(level, that.level) &&
                Objects.equals(location_scope, that.location_scope) &&
                Objects.equals(multiple, that.multiple) &&
                Objects.equals(time_period, that.time_period);
    }

    public List<Item> getAdditional_items() {
        return additional_items;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public int getCount() {
        return count;
    }

    public String getEvent_type() {
        return event_type;
    }

    public String getLevel() {
        return level;
    }

    public String getLocation_scope() {
        return location_scope;
    }

    public String getMultiple() {
        return multiple;
    }

    public Spec getSpec() {
        return spec;
    }

    public String getTime_period() {
        return time_period;
    }

    public int getTime_units() {
        return time_units;
    }

    public String getTime_window() {
        return time_window;
    }

    public String getWeek_group() {
        return week_group;
    }

    public enum WeekGroup {
        MONDAY_SUNDAY("1"),
        TUESDAY_MONDAY("2"),
        WEDNESDAY_TUESDAY("3"),
        THURSDAY_WEDNESDAY("4"),
        FRIDAY_THURSDAY("5"),
        SATURDAY_FRIDAY("6"),
        SUNDAY_SATURDAY("7");

        private String value;

        WeekGroup(String value) {
            this.value = value;
        }

        public static WeekGroup fromValue(String weekGroup) {
            return Arrays.stream(WeekGroup.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(weekGroup))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Week Group %s.", weekGroup)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum TimeWindow {
        SLIDING("Sliding"),
        ANCHORED("Fixed");

        private String value;

        TimeWindow(String value) {
            this.value = value;
        }

        public static TimeWindow fromValue(String timeWindow) {
            return Arrays.stream(TimeWindow.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(timeWindow))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Time Window %s.", timeWindow)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum LocationScope {
        NETWORK("Network"),
        TOUCHPOINT("Touchpoint");

        private String value;

        LocationScope(String value) {
            this.value = value;
        }

        public static LocationScope fromValue(String locationScope) {
            return Arrays.stream(LocationScope.values())
                    .filter(e -> e.getValue().equalsIgnoreCase(locationScope))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Location Scope %s.", locationScope)));
        }

        public String getValue() {
            return value;
        }
    }
}
