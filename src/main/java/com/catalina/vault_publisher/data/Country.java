package com.catalina.vault_publisher.data;

import java.util.Arrays;
import java.util.Objects;

public class Country {
    public static final String FRA_COUNTRY_CODE = "FRA";
    public static final String FRA_COUNTRY_NAME = "France";
    public static final String FRA_BILLING_EXTERNAL_SYS = "CATS";
    public static final String DEU_COUNTRY_CODE = "DEU";
    public static final String DEU_COUNTRY_NAME = "Germany";
    public static final String DEU_BILLING_EXTERNAL_SYS = "";
    public static final String ITA_COUNTRY_CODE = "ITA";
    public static final String ITA_COUNTRY_NAME = "Italy";
    public static final String ITA_BILLING_EXTERNAL_SYS = "";
    private String country_code;
    private String country_name;

    public Country() {
    }

    public Country(CountryEnum country) {
        this.country_code = country.code;
        this.country_name = country.name;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Country country = (Country) o;
        return Objects.equals(country_code, country.country_code) &&
                Objects.equals(country_name, country.country_name);
    }

    public enum CountryEnum {
        FRANCE(FRA_COUNTRY_CODE, FRA_COUNTRY_NAME, FRA_BILLING_EXTERNAL_SYS),
        GERMANY(DEU_COUNTRY_CODE, DEU_COUNTRY_NAME, DEU_BILLING_EXTERNAL_SYS),
        ITALY(ITA_COUNTRY_CODE, ITA_COUNTRY_NAME, ITA_BILLING_EXTERNAL_SYS);

        private final String code, name, billingSystem;

        CountryEnum(String code, String name, String billingSystem) {
            this.code = code;
            this.name = name;
            this.billingSystem = billingSystem;
        }

        public static CountryEnum fromValues(String countryCode) {
            return Arrays.stream(CountryEnum.values())
                    .filter(country -> country.getCode().equalsIgnoreCase(countryCode))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported country code : %s.", countryCode)));
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }

        public String getBillingSystem() {
            return billingSystem;
        }

        @Override
        public String toString() {
            return name.toUpperCase();
        }
    }
}
