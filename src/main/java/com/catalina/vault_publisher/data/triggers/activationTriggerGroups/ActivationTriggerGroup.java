package com.catalina.vault_publisher.data.triggers.activationTriggerGroups;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;

import java.util.List;

public class ActivationTriggerGroup {
    private String id;
    private int group_index;
    private String group_name;
    private boolean is_group_required;
    private String type;
    private String placeholder_name;
    private List<ActivationTrigger> triggers;

    public enum TriggerCombination {
        AND(0),
        OR(1);
        public final int value;

        TriggerCombination(int value) {
            this.value = value;
        }
    }

    public ActivationTriggerGroup() {
    }

    public ActivationTriggerGroup(String id,
                                  int group_index,
                                  String group_name,
                                  boolean is_group_required,
                                  String type,
                                  int groupNbr,
                                  List<ActivationTrigger> triggers) {
        this.id = id;
        this.group_index = group_index;
        this.group_name = group_name;
        this.is_group_required = is_group_required;
        this.type = type;
        this.placeholder_name = String.format("Trigger Group #%02d", groupNbr);
        this.triggers = triggers;
    }

    public String toString() {
        return Config.toJsString(this);
    }

    public String getId() {
        return id;
    }

    public int getGroup_index() {
        return group_index;
    }

    public String getGroup_name() {
        return group_name;
    }

    public boolean isIs_group_required() {
        return is_group_required;
    }

    public String getType() {
        return type;
    }

    public String getPlaceholder_name() {
        return placeholder_name;
    }

    public void setPlaceholder_name(String placeholder_name) {
        this.placeholder_name = placeholder_name;
    }

    public List<ActivationTrigger> getTriggers() {
        return triggers;
    }

    public void setTriggers(List<ActivationTrigger> triggers) {
        this.triggers = triggers;
    }
}
