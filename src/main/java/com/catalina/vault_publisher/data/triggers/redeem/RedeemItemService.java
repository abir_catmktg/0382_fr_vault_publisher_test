package com.catalina.vault_publisher.data.triggers.redeem;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.offers.FlatPromotion;
import com.catalina.vault_publisher.data.offers.PromotionService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;

import java.util.List;

public class RedeemItemService {

    private CRestTemplate cRestTemplate;
    private PromotionService promotionService;

    private static final String REDEEM_ITEM_NAME_PATTERN = "%s:%s Advertisement";

    public RedeemItemService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.promotionService = new PromotionService(cRestTemplate);
    }

    public RedeemItem createRedeemItem(Config.Mode mode, String adExternalId) throws HttpResponseStatusException {

        String fqParam = String.format("external_id:%s", adExternalId);

        List<FlatPromotion> flatPromotionList = promotionService.getFlatPromotionsFromCustomFilters(mode, fqParam);

        if (flatPromotionList.isEmpty() || flatPromotionList.size() > 1) {
            throw new RuntimeException(String.format("%s promotion found for external_id: ",
                    flatPromotionList.size(),
                    adExternalId));
        }

        FlatPromotion flatPromotion = flatPromotionList.get(0);

        String completeBl = flatPromotion.getId();
        String name = flatPromotion.getName();

        return new RedeemItem(completeBl,
                String.format(REDEEM_ITEM_NAME_PATTERN, adExternalId, name));

    }

}
