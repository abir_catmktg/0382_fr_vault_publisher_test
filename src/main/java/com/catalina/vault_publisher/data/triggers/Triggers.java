package com.catalina.vault_publisher.data.triggers;

import com.catalina.common.Constraint;
import com.catalina.vault_publisher.data.targeting.Configuration;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;

import java.util.List;

public class Triggers {
    @Constraint(notNull = true)
    public List<ActivationTriggerGroup> activation_trigger_groups;
    @Constraint(notNull = true)
    public List<ActivationTrigger> activation_triggers;
    public Configuration configuration;
    public int minimum_groups_to_trigger;
    public boolean readonly;
    public String type;

    public Triggers() {
    }

    public Triggers(List<ActivationTriggerGroup> activation_trigger_groups,
                    List<ActivationTrigger> activation_triggers,
                    Configuration configuration,
                    int minimum_groups_to_trigger,
                    boolean readonly,
                    String type) {
        this.activation_trigger_groups = activation_trigger_groups;
        this.activation_triggers = activation_triggers;
        this.configuration = configuration;
        this.minimum_groups_to_trigger = minimum_groups_to_trigger;
        this.readonly = readonly;
        this.type = type;
    }
}
