package com.catalina.vault_publisher.data.triggers;

import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;

import java.util.List;

public class TriggerPackage {
    private List<ActivationTrigger> activationTriggers;
    private List<ActivationTriggerGroup> activationTriggerGroups;

    public TriggerPackage(List<ActivationTrigger> activationTriggers, List<ActivationTriggerGroup> activationTriggerGroups) {
        this.activationTriggers = activationTriggers;
        this.activationTriggerGroups = activationTriggerGroups;
    }

    public List<ActivationTrigger> getActivationTriggers() {
        return activationTriggers;
    }

    public List<ActivationTriggerGroup> getActivationTriggerGroups() {
        return activationTriggerGroups;
    }
}
