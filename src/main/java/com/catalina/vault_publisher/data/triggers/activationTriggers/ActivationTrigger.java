package com.catalina.vault_publisher.data.triggers.activationTriggers;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Item;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceReference;
import com.catalina.vault_publisher.data.list.AbstractTriggerList;
import com.catalina.vault_publisher.data.triggers.Value;
import com.catalina.vault_publisher.data.triggers.redeem.RedeemItem;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ActivationTrigger {
    private String anonymous;
    private String campaign_id;
    private List<String> categoryList;
    private List<String> code;
    private String condition;
    private String coupon_type;
    private String custom_loyalty_program;
    private Demographic demographic;
    private List<String> departmentCodesList;
    private String edit;
    private String editMode;
    private String event_action;
    private String event_actions;
    private String event_type;
    private String exclude_lane_types;
    private List<AbstractTriggerList> excludedCategoryList;
    private List<AbstractTriggerList> excludedDepartmentCodesList;
    private String excludedType;
    private List<AbstractTriggerList> excludedUpcList;
    private String group_id;
    private int group_index;
    private String group_name;
    private String id;
    private Boolean include_item_purchase_in_aggregation;
    private Boolean is_group_required;
    private List<String> item_regex;
    private String items;
    private List<AbstractTriggerList> lists;
    private String measure;
    private String modified;
    private Multiplier multiplier;
    private String operation;
    private String product_attribute;
    private String program;
    private String purchase_measure;
    private String qualifying_order_location_scope;
    private String qualifying_trip_amount;
    private List<RedeemItem> redeem_items;
    private String redeem_scope;
    private String reference;
    private List<String> regex;
    private List<Item> retargeting_items;
    private String retargeting_scope;
    private List<String> reward_ids;
    private String scope;
    private List<AudienceReference> segments;
    private Boolean shouldSingleSessionBeAllowed;
    private List<String> source_ids;
    private String start_date;
    private String stop_date;
    private String targeted_lane_types;
    private String tender;
    private String time_unit;
    private String time_unit_duration;
    private String time_window;
    private String type;
    private String type_ids;
    private Value value;
    private Weather weather;


    public ActivationTrigger(PurchaseMeasure purchaseMeasure,
                             ProductAttribute productAttribute,
                             TriggerReference reference,
                             TriggerType type,
                             String eventType,
                             String excludedType,
                             List<AbstractTriggerList> excludeUpcsList,
                             List<AbstractTriggerList> excludedDepartmentList,
                             List<AbstractTriggerList> excludedCategoryList,
                             List<AbstractTriggerList> lists,
                             List<String> regex,
                             List<Item> retargeting_items,
                             String retargeting_scope,
                             String scope,
                             Boolean include_item_purchase_in_aggregation,
                             List<String> item_regex,
                             Boolean shouldSingleSessionBeAllowed,
                             Boolean is_group_required,
                             Value value) {
        this.event_type = eventType;
        this.categoryList = this.departmentCodesList = new ArrayList<>();
        this.demographic = new Demographic();
        this.excludedType = excludedType;
        this.excludedUpcList = excludeUpcsList;
        this.excludedDepartmentCodesList = excludedDepartmentList;
        this.excludedCategoryList = excludedCategoryList;
        this.id = UUID.randomUUID().toString();
        this.include_item_purchase_in_aggregation = include_item_purchase_in_aggregation;
        this.is_group_required = is_group_required;
        this.lists = lists;
        this.multiplier = new Multiplier();
        this.product_attribute = (productAttribute == null) ? null : productAttribute.value;
        this.purchase_measure = (purchaseMeasure == null) ? null : purchaseMeasure.value;
        this.reference = (reference == null) ? null : reference.value;
        this.regex = regex;
        this.retargeting_items = retargeting_items;
        this.retargeting_scope = retargeting_scope;
        this.scope = scope;
        this.item_regex = item_regex;
        this.shouldSingleSessionBeAllowed = shouldSingleSessionBeAllowed;
        this.reward_ids = new ArrayList<>();
        this.scope = "Session";
        this.type = type.value;
        this.value = value;
        this.weather = new Weather();
    }

    public ActivationTrigger() {
    }

    public ActivationTrigger(String anonymous, TriggerType type, Value value) {
        this.anonymous = anonymous;
        this.demographic = new Demographic();
        this.id = UUID.randomUUID().toString();
        this.is_group_required = false;
        this.multiplier = new Multiplier();
        this.type = type.value;
        this.value = value;
        this.weather = new Weather();
    }

    public ActivationTrigger(TriggerType type, String condition, List<AudienceReference> segments) {
        this.condition = condition;
        this.demographic = new Demographic();
        this.id = UUID.randomUUID().toString();
        this.is_group_required = false;
        this.multiplier = new Multiplier();
        this.segments = segments;
        this.type = type.value;
        this.value = new Value(null, null, "");
        this.weather = new Weather();
    }

    public ActivationTrigger(TriggerType type, boolean shouldSingleSessionBeAllowed, List<String> source_ids) {
        this.demographic = new Demographic();
        this.id = UUID.randomUUID().toString();
        this.is_group_required = false;
        this.multiplier = new Multiplier();
        this.type = type.value;
        this.shouldSingleSessionBeAllowed = shouldSingleSessionBeAllowed;
        this.source_ids = source_ids;
        this.value = new Value(null, null, "");
        this.weather = new Weather();
    }

    public ActivationTrigger(String excludedType, String operation, List<RedeemItem> redeem_items, String redeem_scope, Boolean shouldSingleSessionBeAllowed, String type) {
        this.demographic = new Demographic();
        this.excludedType = excludedType;
        this.is_group_required = false;
        this.multiplier = new Multiplier();
        this.operation = operation;
        this.redeem_items = redeem_items;
        this.redeem_scope = redeem_scope;
        this.reward_ids = new ArrayList<>();
        this.scope = "Session";
        this.shouldSingleSessionBeAllowed = shouldSingleSessionBeAllowed;
        this.type = type;
        this.value = new Value(null, null, "");
        this.weather = new Weather();
    }

    public ActivationTrigger(List<String> code, String coupon_type, String operation, Boolean shouldSingleSessionBeAllowed, String type) {
        this.code = code;
        this.coupon_type = coupon_type;
        this.demographic = new Demographic();
        this.multiplier = new Multiplier();
        this.operation = operation;
        this.reward_ids = new ArrayList<>();
        this.shouldSingleSessionBeAllowed = shouldSingleSessionBeAllowed;
        this.type = type;
        this.value = new Value(null, null, "");
        this.weather = new Weather();
    }

    // Usage : create empty audience triggers for manuf ecolinks
    public ActivationTrigger(String group_id) {
        this.demographic = new Demographic();
        this.multiplier = new Multiplier();
        this.regex = new ArrayList<>();
        this.group_id = group_id;
        this.id = UUID.randomUUID().toString();
        this.lists = new ArrayList<>();
        this.reward_ids = new ArrayList<>();
        this.is_group_required = false;
        this.type = "TriggerUserSegment";
        this.weather = new Weather();
    }

    @Override
    public String toString() {
        return Config.toJsString(this);
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGroup_index() {
        return group_index;
    }

    public void setGroup_index(int group_index) {
        this.group_index = group_index;
    }

    public boolean isIs_group_required() {
        return is_group_required;
    }

    public List<AbstractTriggerList> getLists() {
        return lists;
    }

    public void setLists(List<AbstractTriggerList> lists) {
        this.lists = lists;
    }

    public List<String> getRegex() {
        return regex;
    }

    public String getScope() {
        return scope;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getStop_date() {
        return stop_date;
    }

    public String getType() {
        return type;
    }

    public Value getValue() {
        return value;
    }

    public void setCode(List<String> code) {
        this.code = code;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public enum ProductAttribute {
        UPC("Upc"),
        DEPARTMENT_ID("Department");

        public final String value;

        ProductAttribute(String value) {
            this.value = value;
        }

        public static ProductAttribute fromValue(String value) {
            for (ProductAttribute productAttribute : ProductAttribute.values()) {
                if (value.equals(productAttribute.value)) return productAttribute;
            }
            return null;
        }
    }

    public enum PurchaseMeasure {
        UNITS_PURCHASED("Units"),
        TOTAL_SPEND("Spend");

        public final String value;

        PurchaseMeasure(String value) {
            this.value = value;
        }

        public static PurchaseMeasure fromValue(String value) {
            for (PurchaseMeasure purchaseMeasure : PurchaseMeasure.values()) {
                if (value.equals(purchaseMeasure.value)) return purchaseMeasure;
            }
            return null;
        }
    }

    public enum TriggerReference {
        MANUAL("manual"),
        LIST("list"),
        LIST_MANAGER("lmc"),
        LIST_MANAGER_GROUPS("lmc-group");

        public final String value;

        TriggerReference(String value) {
            this.value = value;
        }

        public static TriggerReference fromValue(String value) {
            for (TriggerReference triggerReference : TriggerReference.values()) {
                if (value.equals(triggerReference.value)) return triggerReference;
            }
            return null;
        }
    }

    public enum TriggerType {
        ITEM_PURCHASE("TriggerPurchaseEvent"),
        TOTAL_ORDER("TriggerCheckoutEvent"),
        CATALINA_REDEMPTION("TriggerRedeemEvent"),
        RETARGETING("TriggerRetargetingEvent"),
        POS_REDEMPTION("TriggerPointOfSaleCouponEvent"),
        AUDIENCE_SEGMENT("TriggerUserSegment"),
        USER_IDENTIFIER("TriggerUserIdentifierEvent"),
        CUSTOM_TARGETING("TriggerWalletEvent");

        public final String value;

        TriggerType(String value) {
            this.value = value;
        }

        public static TriggerType fromValue(String value) {
            for (TriggerType triggerType : TriggerType.values()) {
                if (value.equals(triggerType.value)) return triggerType;
            }
            return null;
        }
    }

    public enum Operator {
        SUPERIOR_TO(">="),
        EQUAL("=="),
        INFERIOR_TO("<="),
        BETWEEN("between");

        private final String value;

        Operator(String value) {
            this.value = value;
        }

        public static Operator fromValue(String operator) {
            if (operator == null || operator.isEmpty()) return null;
            return Arrays.stream(Operator.values())
                    .filter(x -> x.value.equalsIgnoreCase(operator))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Operator %s.", operator)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum EventType {
        IMPRESSION("impression"),
        REDEMPTION("redemption");

        private final String value;

        EventType(String value) {
            this.value = value;
        }

        public static EventType fromValue(String eventType) {
            return Arrays.stream(EventType.values())
                    .filter(x -> x.getValue().equalsIgnoreCase(eventType))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported event type: %s", eventType)));
        }

        public String getValue() {
            return value;
        }
    }

    public enum ExcludeType {
        ALL_UPCS("NO_EXCLUSION"),
        EXCLUDING_UPCS("EXCLUDING_UPCS");

        private final String value;

        ExcludeType(String value) {
            this.value = value;
        }

        public static ExcludeType fromValue(String value) {
            for (ExcludeType excludeType : ExcludeType.values()) {
                if (value.equals(excludeType.value)) return excludeType;
            }
            return null;
        }

        public String getValue() {
            return value;
        }
    }

    public enum Operation {
        DID_REDEEM("DID_REDEEM"),
        DID_NOT_REDEEM("DID_NOT_REDEEM");

        public final String value;

        Operation(String value) {
            this.value = value;
        }

        public static Operation fromValue(String value) {
            for (Operation operation : Operation.values()) {
                if (value.equals(operation.value)) return operation;
            }
            return null;
        }
    }

    public enum Condition {
        IN("IN"),
        NOT_IN("NOT_IN");

        public final String value;

        Condition(String value) {
            this.value = value;
        }

        public static Condition fromValue(String value) {
            for (Condition condition : Condition.values()) {
                if (value.equals(condition.value)) return condition;
            }
            return null;
        }
    }

    public enum CouponType {
        GS1_COUPON("GS1_COUPON_TRAN"),
        GENERIC_COUPON("ITEM_PURCHASED_TRAN");

        public final String value;

        CouponType(String value) {
            this.value = value;
        }

        public static CouponType fromValue(String value) {
            for (CouponType couponType : CouponType.values()) {
                if (value.equals(couponType.value)) return couponType;
            }
            return null;
        }

    }

    public enum RetargetingScope {
        CAMPAIGN("CAMPAIGN"),
        ADGROUP("ADGROUP"),
        AD("AD");

        private final String value;

        RetargetingScope(String value) {
            this.value = value;
        }

        public static RetargetingScope fromValue(String retargetingScope) {
            return Arrays.stream(RetargetingScope.values())
                    .filter(x -> x.getValue().equalsIgnoreCase(retargetingScope))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Retargeting Scope %s.", retargetingScope)));
        }

        public String getValue() {
            return value;
        }
    }

    private static class Multiplier {
        JsonObject rows = new JsonObject();
        String type = "catalog";
    }

    private static class Weather {
        String status;
        String type;
        JsonObject value = new JsonObject();
    }

    public static class Demographic {
        String buyer_time_quantity = "90";
        String id = UUID.randomUUID().toString();
        String lapse_time_quantity = "30";
        Lookback lookback = new Lookback();
        JsonObject superset = new JsonObject();
        String type;
        Value value = new Value();
    }

    public static class Lookback {
        String type;
    }

}
