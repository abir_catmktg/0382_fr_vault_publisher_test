package com.catalina.vault_publisher.data.triggers.activationTriggerGroups;

import com.catalina.vault_publisher.data.list.lmc.group.LMCGroupAsTriggerList;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup.TriggerCombination;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ActivationTriggerGroupService {

    public ActivationTriggerGroupService() {
    }

    public List<ActivationTriggerGroup> createTriggerGroups(TriggerCombination combination,
                                                            int startIndex,
                                                            List<ActivationTrigger> activationTriggers,
                                                            String triggerGroupName) {
        List<ActivationTriggerGroup> list = new ArrayList<>();

        if (combination.equals(TriggerCombination.AND)) {
            ActivationTriggerGroup group = new ActivationTriggerGroup(UUID.randomUUID().toString(),
                    startIndex,
                    triggerGroupName != null && !triggerGroupName.isEmpty() ? triggerGroupName : "Group " + startIndex + 1,
                    false,
                    "TriggerGroup",
                    startIndex + 1,
                    new ArrayList<>());

            List<ActivationTrigger> triggerList = new ArrayList<>();

            for (ActivationTrigger currentTrigger : activationTriggers) {
                currentTrigger.setGroup_id(group.getId());
                currentTrigger.setGroup_index(group.getGroup_index());
                currentTrigger.setGroup_name(group.getGroup_name());
                triggerList.add(currentTrigger);
            }
            group.setTriggers(triggerList);
            list.add(group);
            return list;
        }

        for (ActivationTrigger currentTrigger : activationTriggers) {
            ActivationTriggerGroup group = new ActivationTriggerGroup(UUID.randomUUID().toString(),
                    startIndex,
                    "Group " + startIndex + 1,
                    false,
                    "TriggerGroup",
                    startIndex + 1,
                    new ArrayList<>());

            currentTrigger.setGroup_id(group.getId());
            currentTrigger.setGroup_index(group.getGroup_index());
            currentTrigger.setGroup_name(group.getGroup_name());
            group.setTriggers(Stream.of(currentTrigger).collect(Collectors.toList()));
            list.add(group);
            startIndex++;
        }
        return list;
    }

    public List<ActivationTriggerGroup> createTriggerGroupsForLMCGroup(int startIndex,
                                                                       List<ActivationTrigger> activationTriggers) {
        List<ActivationTriggerGroup> list = new ArrayList<>();
        for (ActivationTrigger currentTrigger : activationTriggers) {
            LMCGroupAsTriggerList lmcGroupAsTriggerList = (LMCGroupAsTriggerList) currentTrigger.getLists().get(0);
            String groupName = lmcGroupAsTriggerList.getGroup_name();
            ActivationTriggerGroup atGroup = new ActivationTriggerGroup(UUID.randomUUID().toString(),
                    startIndex,
                    groupName,
                    false,
                    "TriggerGroup",
                    startIndex + 1,
                    new ArrayList<>());

            currentTrigger.setGroup_id(atGroup.getId());
            currentTrigger.setGroup_index(atGroup.getGroup_index());
            currentTrigger.setGroup_name(groupName);
            atGroup.setTriggers(Stream.of(currentTrigger).collect(Collectors.toList()));
            list.add(atGroup);
            startIndex++;

        }
        return list;
    }

}
