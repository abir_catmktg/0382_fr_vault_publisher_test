package com.catalina.vault_publisher.data.triggers;

import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup.TriggerCombination;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroupService;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTriggerService;
import com.catalina.vault_publisher.utils.CRestTemplate;

import java.util.List;

public class TriggerPackageService {

    private ActivationTriggerService activationTriggerService;
    private ActivationTriggerGroupService activationTriggerGroupService;

    public TriggerPackageService(CRestTemplate cRestTemplate) {
        this.activationTriggerService = new ActivationTriggerService(cRestTemplate);
        this.activationTriggerGroupService = new ActivationTriggerGroupService();
    }

    public TriggerPackage createTriggerPackage(TriggerCombination combination,
                                               int startIndex,
                                               List<ActivationTrigger> activationTriggerList) {
        List<ActivationTriggerGroup> activationTriggerGroups =
                activationTriggerGroupService.createTriggerGroups(combination, startIndex, activationTriggerList, null);
        List<ActivationTrigger> activationTriggers =
                activationTriggerService.getUpdatedActivationTriggers(activationTriggerGroups);

        TriggerPackage triggerPackage = new TriggerPackage(activationTriggers, activationTriggerGroups);

        return triggerPackage;
    }
}
