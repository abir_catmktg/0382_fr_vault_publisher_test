package com.catalina.vault_publisher.data.triggers.activationTriggers;

import com.catalina.vault_publisher.data.*;
import com.catalina.vault_publisher.data.customer_files.audiences.Audience;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceReference;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceReferenceService;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceService;
import com.catalina.vault_publisher.data.list.AbstractTriggerList;
import com.catalina.vault_publisher.data.list.lmc.group.LMCGroupAsTriggerList;
import com.catalina.vault_publisher.data.list.lmc.group.LMCGroupAsTriggerListService;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItem;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerList;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerListService;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemService;
import com.catalina.vault_publisher.data.list.vault.VaultListService;
import com.catalina.vault_publisher.data.list.vault.VaultTriggerList;
import com.catalina.vault_publisher.data.offers.Ad;
import com.catalina.vault_publisher.data.offers.OfferService;
import com.catalina.vault_publisher.data.triggers.Value;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger.*;
import com.catalina.vault_publisher.data.triggers.redeem.RedeemItem;
import com.catalina.vault_publisher.data.triggers.redeem.RedeemItemService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class ActivationTriggerService {

	private CRestTemplate cRestTemplate;
	private AudienceService audienceService;
	private AudienceReferenceService audienceReferenceService;
	private LMCItemService lmcItemService;
	private VaultListService vaultListService;
	private OfferService offerService;
	private static final Logger logger = LogManager.getLogger(ActivationTriggerService.class);

	public ActivationTriggerService(CRestTemplate cRestTemplate) {
		this.cRestTemplate = cRestTemplate;
		this.audienceService = new AudienceService(cRestTemplate);
		this.audienceReferenceService = new AudienceReferenceService();
		this.lmcItemService = new LMCItemService(cRestTemplate);
		this.vaultListService = new VaultListService(cRestTemplate);
		this.offerService = new OfferService(cRestTemplate);
	}

	//Total Order / All UPCs
	public ActivationTrigger createActivationTriggerTotalOrderAllUPCs(PurchaseMeasure measure,
	                                                                  Double minQty,
	                                                                  Double maxQty,
	                                                                  Operator operator) {
		ActivationTrigger activationTriggerTotalOrder = createActivationTriggerTotalOrder(measure,
				TriggerReference.MANUAL,
				ExcludeType.ALL_UPCS,
				null,
				null,
				null,
				null,
				null,
				true,
				minQty,
				maxQty,
				operator);
		return activationTriggerTotalOrder;
	}

	//Total Order / Excluding UPCs
	public ActivationTrigger createActivationTriggerTotalOrderExcludingVaultList(Config.Mode mode,
	                                                                             PurchaseMeasure measure,
	                                                                             Double minQty,
	                                                                             Double maxQty,
	                                                                             Operator operator,
	                                                                             List<String> listName) throws HttpResponseStatusException {
		List<AbstractTriggerList> triggerLists = new ArrayList<>();

		for (String currentListName : listName) {
			Result res = vaultListService.getVaultList(mode, currentListName, VaultTriggerList.ListType.PRODUCT);
			if (res.getStatus() != 0) throw new RuntimeException(res.getMessage());

			VaultTriggerList vaultList = (VaultTriggerList) res.getObject();
			triggerLists.add(vaultList);
		}


		ActivationTrigger activationTrigger = createActivationTriggerTotalOrder(measure,
				TriggerReference.LIST,
				ExcludeType.EXCLUDING_UPCS,
				triggerLists,
				null,
				null,
				null,
				new ArrayList<>(),
				true,
				minQty,
				maxQty,
				operator);

		return activationTrigger;
	}

	//Total Order
	private ActivationTrigger createActivationTriggerTotalOrder(PurchaseMeasure measure,
	                                                            TriggerReference reference,
	                                                            ExcludeType excludeType,
	                                                            List<AbstractTriggerList> excludedUPCs,
	                                                            List<AbstractTriggerList> excludedDepartment,
	                                                            List<AbstractTriggerList> excludedCategory,
	                                                            List<AbstractTriggerList> lists,
	                                                            List<String> regex,
	                                                            Boolean shouldSingleSessionBeAllowed,
	                                                            Double minQty,
	                                                            Double maxQty,
	                                                            Operator operator) {
		ActivationTrigger activationTrigger = new ActivationTrigger(measure,
				null,
				reference,
				TriggerType.TOTAL_ORDER,
				null,
				excludeType.getValue(),
				excludedUPCs,
				excludedDepartment,
				excludedCategory,
				lists,
				regex,
				null,
				null,
				null,
				null,
				null,
				shouldSingleSessionBeAllowed,
				null,
				new Value((minQty == null) ? null : String.valueOf(minQty),
						(maxQty == null) ? null : String.valueOf(maxQty),
						operator != null ? operator.getValue() : null));

		return activationTrigger;
	}

	//Item Purchase / Manual List
	public ActivationTrigger createActivationTriggerItemPurchaseManualList(ProductAttribute product,
	                                                                       PurchaseMeasure measure,
	                                                                       Double minQty,
	                                                                       Double maxQty,
	                                                                       Operator operator,
	                                                                       List<String> manualList1,
	                                                                       List<String> manualList2) {
		ActivationTrigger activationTrigger = setActivationTriggerItemPurchase(measure,
				product,
				TriggerReference.MANUAL,
				ExcludeType.ALL_UPCS,
				null,
				null,
				null,
				null,
				manualList1,
				(manualList2 == null || manualList2.isEmpty()) ? false : true,
				manualList2,
				null,
				minQty,
				maxQty,
				operator);
		return activationTrigger;
	}

	//Item Purchase / List stored in VAULT
	public ActivationTrigger createActivationTriggerItemPurchaseVaultList(Config.Mode mode,
	                                                                      ProductAttribute product,
	                                                                      PurchaseMeasure measure,
	                                                                      Double minQty,
	                                                                      Double maxQty,
	                                                                      Operator operator,
	                                                                      List<String> listName) throws HttpResponseStatusException {

		List<AbstractTriggerList> triggerLists = new ArrayList<>();

		for (String currentListName : listName) {
			Result res = vaultListService.getVaultList(mode, currentListName, VaultTriggerList.ListType.PRODUCT);
			if (res.getStatus() != 0) throw new RuntimeException(res.getMessage());

			VaultTriggerList vaultList = (VaultTriggerList) res.getObject();
			triggerLists.add(vaultList);
		}

		ActivationTrigger activationTrigger = setActivationTriggerItemPurchase(measure,
				product,
				TriggerReference.LIST,
				ExcludeType.ALL_UPCS,
				null,
				null,
				null,
				triggerLists,
				new ArrayList<>(),
				null,
				null,
				true,
				minQty,
				maxQty,
				operator);

		return activationTrigger;
	}

	//Item Purchase / List Manager Cloud
	public ActivationTrigger createActivationTriggerItemPurchaseLMC(Config.Mode mode,
	                                                                ProductAttribute product,
	                                                                PurchaseMeasure measure,
	                                                                Double minQty,
	                                                                Double maxQty,
	                                                                Operator operator,
	                                                                List<String> listName) throws HttpResponseStatusException {

		List<AbstractTriggerList> triggerLists = new ArrayList<>();
		for (String currentListName : listName) {

			List<LMCItem> lmcItems = lmcItemService.getListLMC(mode, currentListName);
			LMCItem currentLmcItem;
			if (lmcItems.size() == 1) currentLmcItem = lmcItems.get(0);
			else currentLmcItem = lmcItemService.getMatchingLMCitem(lmcItems, currentListName);

			LMCItemAsTriggerList lmcTriggerList =
					new LMCItemAsTriggerListService().createLMCItemAsTriggerList(currentLmcItem);

			triggerLists.add(lmcTriggerList);
		}

		ActivationTrigger activationTrigger = setActivationTriggerItemPurchase(measure,
				product,
				TriggerReference.LIST_MANAGER,
				null,
				null,
				null,
				null,
				triggerLists,
				new ArrayList<>(),
				null,
				null,
				null,
				minQty,
				maxQty,
				operator);

		return activationTrigger;
	}

	//Item Purchase
	private ActivationTrigger setActivationTriggerItemPurchase(PurchaseMeasure measure,
	                                                           ProductAttribute product,
	                                                           TriggerReference reference,
	                                                           ExcludeType excludedType,
	                                                           List<AbstractTriggerList> excludedUPCs,
	                                                           List<AbstractTriggerList> excludedDepartment,
	                                                           List<AbstractTriggerList> excludedCategory,
	                                                           List<AbstractTriggerList> lists,
	                                                           List<String> manualList1,
	                                                           Boolean includeUpcs,
	                                                           List<String> manualList2,
	                                                           Boolean shouldSingleSessionBeAllowed,
	                                                           Double minQty,
	                                                           Double maxQty,
	                                                           Operator operator) {
		ActivationTrigger triggerItemPurchase = new ActivationTrigger(measure,
				product,
				reference,
				TriggerType.ITEM_PURCHASE,
				null,
				(excludedType == null) ? null : excludedType.getValue(),
				(excludedUPCs == null) ? null : excludedUPCs,
				(excludedDepartment == null) ? null : excludedDepartment,
				(excludedCategory == null) ? null : excludedCategory,
				lists,
				manualList1,
				null,
				null,
				null,
				includeUpcs,
				manualList2,
				shouldSingleSessionBeAllowed,
				false,
				new Value((minQty == null) ? null : String.valueOf(minQty),
						(maxQty == null) ? null : String.valueOf(maxQty),
						operator.getValue()));
		return triggerItemPurchase;
	}

	//Catalina Redemption scope Ad
	public ActivationTrigger createActivationTriggerCatalinaRedemptionForAds(Config.Mode mode,
	                                                                         Operation operation,
	                                                                         List<String> blIds) throws HttpResponseStatusException {
		ActivationTrigger activationTrigger =
				createActivationTriggerCatalinaRedemption(mode, RetargetingScope.AD, operation, blIds);

		return activationTrigger;
	}

	//Catalina Redemption
	private ActivationTrigger createActivationTriggerCatalinaRedemption(Config.Mode mode,
	                                                                    RetargetingScope scope,
	                                                                    Operation operation,
	                                                                    List<String> blIds) throws HttpResponseStatusException {
		List<RedeemItem> redeemItemList = new ArrayList<>();
		for (String currentBlId : blIds) {
			RedeemItem redeemItem = new RedeemItemService(cRestTemplate).createRedeemItem(mode, currentBlId);
			redeemItemList.add(redeemItem);
		}

		return new ActivationTrigger("NO_EXCLUSION",
				operation.value,
				redeemItemList,
				scope.getValue(),
				true,
				TriggerType.CATALINA_REDEMPTION.value);
	}

	//Retargeting Event
	private ActivationTrigger createActivationTriggerRetargeting(RetargetingScope scope,
	                                                             List<Item> retargetingItems,
	                                                             EventType eventType,
	                                                             Double minQty,
	                                                             Double maxQty,
	                                                             Operator operator) {

		ActivationTrigger triggerRetarget = new ActivationTrigger(PurchaseMeasure.UNITS_PURCHASED,
				null,
				null,
				TriggerType.RETARGETING,
				eventType.getValue(),
				ExcludeType.ALL_UPCS.getValue(),
				null,
				null,
				null,
				null,
				null,
				retargetingItems,
				scope.getValue(),
				"Session",
				null,
				null,
				true,
				false,
				new Value((minQty == null) ? null : String.valueOf(minQty),
						(maxQty == null) ? null : String.valueOf(maxQty),
						operator.getValue()));
		return triggerRetarget;

	}

	//Retargeting Ad
	public ActivationTrigger createActivationTriggerRetargetingAd(Config.Mode mode,
	                                                              List<String> adExternalIds) throws HttpResponseStatusException {


		if (adExternalIds == null || adExternalIds.isEmpty()) {
			return createActivationTriggerRetargeting(RetargetingScope.AD,
					new ArrayList<>(),
					EventType.REDEMPTION,
					1.00,
					null,
					Operator.EQUAL);
		}

		StringJoiner paramJoiner = new StringJoiner("|");
		for (String currentExternalId : adExternalIds) paramJoiner.add(currentExternalId);

		SolrParam wcParam = new SolrParam("wc", String.format("{\"external_id\":\"%s\"}", paramJoiner.toString()));

		List<Ad> adList = offerService.getOffersFromDashBoard(mode,
				Ad.class,
				Arrays.asList(wcParam));

		List<Item> targetingItems = adList.stream()
				.map(x -> new Item(x.getExperience().getReward().getId(),
						String.format("%s: %s Advertisement", x.getExternal_id(), x.getName())))
				.collect(Collectors.toList());

		ActivationTrigger activationTriggerRetargeting = createActivationTriggerRetargeting(RetargetingScope.AD,
				targetingItems,
				EventType.REDEMPTION,
				1.00,
				null,
				Operator.EQUAL);

		return activationTriggerRetargeting;
	}

	//POS Redemption
	public ActivationTrigger createActivationTriggerPOSRedemption(CouponType couponType,
	                                                              Operation operation,
	                                                              List<String> genCodeList) {
		ActivationTrigger activationTrigger = new ActivationTrigger(genCodeList,
				couponType.value,
				operation.value,
				true,
				TriggerType.POS_REDEMPTION.value);
		return activationTrigger;
	}

	//User identifier
	public ActivationTrigger createActivationTriggerLoyaltyCard(boolean cardIsPresent) {
		ActivationTrigger activationTrigger = new ActivationTrigger(String.valueOf(!cardIsPresent),
				TriggerType.USER_IDENTIFIER,
				new Value(null, null, ""));
		return activationTrigger;
	}

	//Audience segment
	public ActivationTrigger createActivationTriggerAudience(Config.Mode mode,
	                                                         Country.CountryEnum countryEnum,
	                                                         Condition condition,
	                                                         List<String> audienceNames) throws HttpResponseStatusException {
		List<AudienceReference> audienceReferenceList = new ArrayList<>();
		for (String currentName : audienceNames) {
			List<Audience> audienceList = audienceService.getAudienceFromName(mode, countryEnum, currentName);
			Audience audience;
			if (audienceList.size() == 0) {
				logger.warn(String.format("No audiences found for the name %s", currentName));
				continue;
			}
			audience = audienceList.get(0);
			AudienceReference audienceReference =
					audienceReferenceService.createAudienceReference(audience.getId(), audience.getName());
			audienceReferenceList.add(audienceReference);
		}

		ActivationTrigger activationTrigger = new ActivationTrigger(TriggerType.AUDIENCE_SEGMENT,
				condition.value,
				audienceReferenceList);
		return activationTrigger;
	}

	//Custom Targeting
	public ActivationTrigger createActivationTriggerCustomTargeting(List<String> sourceIds) {
		ActivationTrigger activationTrigger = new ActivationTrigger(TriggerType.CUSTOM_TARGETING,
				true,
				sourceIds);
		return activationTrigger;
	}

	public List<ActivationTrigger> getUpdatedActivationTriggers(List<ActivationTriggerGroup> activationTriggerGroups) {
		List<ActivationTrigger> atList = new ArrayList<>();
		activationTriggerGroups.stream().forEach(x -> atList.addAll(x.getTriggers()));
		return atList;
	}

	//Item Purchase / List Manager Cloud Group
	public List<ActivationTrigger> createActivationTriggerItemPurchaseLMCGroup(Config.Mode mode,
	                                                                           ProductAttribute product,
	                                                                           PurchaseMeasure measure,
	                                                                           Double minQty,
	                                                                           Double maxQty,
	                                                                           Operator operator,
	                                                                           String listName) throws HttpResponseStatusException {

		List<ActivationTrigger> activationTriggerList = new ArrayList<>();

		// GET LMC ITEM
		List<LMCItem> lmcItems = lmcItemService.getListLMC(mode, listName);
		LMCItem currentLmcItem;
		if (lmcItems.size() == 1) currentLmcItem = lmcItems.get(0);
		else currentLmcItem = lmcItemService.getMatchingLMCitem(lmcItems, listName);

		//ITERATE THROUGH EACH GROUPS OF THIS LMC ITEM
		for (int i = 0; i < currentLmcItem.getGroups().size(); i++) {
			LMCGroupAsTriggerList lmcTriggerList =
					new LMCGroupAsTriggerListService()
							.createTriggerListForGroupsFromLmcItem(currentLmcItem, currentLmcItem.getGroups().get(i), i);

			List<AbstractTriggerList> list = Arrays.asList(lmcTriggerList);

			// CREATE AN ACTIVATION TRIGGER
			ActivationTrigger currentActivationTrigger = setActivationTriggerItemPurchase(measure,
					product,
					TriggerReference.LIST_MANAGER_GROUPS,
					null,
					null,
					null,
					null,
					list,
					new ArrayList<>(),
					null,
					null,
					null,
					minQty,
					maxQty,
					operator);

			// ADD THE ACTIVATION TRIGGER TO THE LIST
			activationTriggerList.add(currentActivationTrigger);
		}
		return activationTriggerList;
	}
}
