package com.catalina.vault_publisher.data.triggers;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.data.targeting.ConfigurationService;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroupService;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger.Operator;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTriggerService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TriggerService {

    private CRestTemplate cRestTemplate;
    private ConfigurationService configurationService;
    private ActivationTriggerService activationTriggerService;
    private ActivationTriggerGroupService activationTriggerGroupService;
    private static final Logger logger = LogManager.getLogger(TriggerService.class);

    public TriggerService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.configurationService = new ConfigurationService();
        this.activationTriggerService = new ActivationTriggerService(cRestTemplate);
        this.activationTriggerGroupService = new ActivationTriggerGroupService();
    }

    public Result getTriggersManuf(Config.Mode mode,
                                   String leverType,
                                   Operator operator,
                                   Double value,
                                   String triggerListName,
                                   String triggerGroupName,
                                   List<String> adExternalIdListForCT) throws HttpResponseStatusException {

        logger.debug("lever type " + leverType + "/ triggerGroupName = " + triggerGroupName +
                " / Operator =  " + operator == null ? null : operator +
                " / value = " + value + " / triggerListName = " + triggerListName);
        /****************************************** ECOLINK ******************************************/
        if ("EK".equalsIgnoreCase(leverType) && !triggerGroupName.isEmpty()) {
            ActivationTrigger activationTrigger = null;
            try {
                activationTrigger = activationTriggerService.createActivationTriggerAudience(mode, Country.CountryEnum.FRANCE, ActivationTrigger.Condition.IN,
                        new ArrayList<>());
            } catch (HttpResponseStatusException e) {
                logger.error(e);
            }
            List<ActivationTriggerGroup> triggerGroupList = activationTriggerGroupService.createTriggerGroups(ActivationTriggerGroup.TriggerCombination.AND,
                    0,
                    Arrays.asList(activationTrigger), triggerGroupName);
            List<ActivationTrigger> triggerList = activationTriggerService.getUpdatedActivationTriggers(triggerGroupList);
            return new Result(new TriggerPackage(triggerList, triggerGroupList));
        }
        /*********************************************************************************************/


        /****************************************** ECOBON ******************************************/
        if (!"EK".equalsIgnoreCase(leverType)) {
            if (leverType.equalsIgnoreCase("CT")) {
                ActivationTrigger trigger =
                        activationTriggerService.createActivationTriggerRetargetingAd(mode, adExternalIdListForCT);
                List<ActivationTriggerGroup> triggerGroupList = activationTriggerGroupService
                        .createTriggerGroups(ActivationTriggerGroup.TriggerCombination.AND,
                                0,
                                Arrays.asList(trigger),
                                triggerGroupName);

                List<ActivationTrigger> triggerList = activationTriggerService.getUpdatedActivationTriggers(triggerGroupList);
                return new Result(new TriggerPackage(triggerList, triggerGroupList));
            } else {
                if (value >= 2) { // IMPORT COMPLETE LIST
                    ActivationTrigger trigger = activationTriggerService.createActivationTriggerItemPurchaseLMC(mode,
                            ActivationTrigger.ProductAttribute.UPC,
                            ActivationTrigger.PurchaseMeasure.UNITS_PURCHASED,
                            value,
                            null,
                            operator,
                            Arrays.asList(triggerListName));

                    List<ActivationTriggerGroup> triggerGroupList = activationTriggerGroupService
                            .createTriggerGroups(ActivationTriggerGroup.TriggerCombination.AND,
                                    0,
                                    Arrays.asList(trigger),
                                    triggerGroupName);

                    List<ActivationTrigger> triggerList = activationTriggerService.getUpdatedActivationTriggers(triggerGroupList);
                    return new Result(new TriggerPackage(triggerList, triggerGroupList));


                } else if (Double.compare(value, Double.valueOf(1)) == 0) { // IMPORT BY GROUP
                    List<ActivationTrigger> triggerList =
                            activationTriggerService.createActivationTriggerItemPurchaseLMCGroup(mode,
                                    ActivationTrigger.ProductAttribute.UPC,
                                    ActivationTrigger.PurchaseMeasure.UNITS_PURCHASED,
                                    value,
                                    null,
                                    operator,
                                    triggerListName);

                    List<ActivationTriggerGroup> triggerGroupList = activationTriggerGroupService
                            .createTriggerGroupsForLMCGroup(0, triggerList);

                    List<ActivationTrigger> finalTriggerList = activationTriggerService.getUpdatedActivationTriggers(triggerGroupList);
                    return new Result(new TriggerPackage(finalTriggerList, triggerGroupList));
                }
            }

        }
        /*********************************************************************************************/
        return null;
    }

    public Triggers createTriggers(List<ActivationTrigger> activationTriggers, List<ActivationTriggerGroup> activationTriggerGroups) {
        return new Triggers(activationTriggerGroups,
                activationTriggers,
                configurationService.setupConfigurationTrigger(),
                1,
                false,
                "class");
    }

    public Triggers createTriggersFromPackages(List<TriggerPackage> triggerPackages) {
        if (triggerPackages.size() < 1) return createTriggers(new ArrayList<>(), new ArrayList<>());
        TriggerPackage completePackage = new TriggerPackage(new ArrayList<>(), new ArrayList<>());
        for (TriggerPackage currentPackage : triggerPackages) {
            completePackage.getActivationTriggerGroups().addAll(currentPackage.getActivationTriggerGroups());
            completePackage.getActivationTriggers().addAll(currentPackage.getActivationTriggers());
        }
        return createTriggers(completePackage.getActivationTriggers(), completePackage.getActivationTriggerGroups());
    }

}
