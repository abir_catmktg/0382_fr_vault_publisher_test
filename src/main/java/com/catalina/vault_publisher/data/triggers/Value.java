package com.catalina.vault_publisher.data.triggers;

public class Value {
    private String max;
    private String min;
    private String op;

    public Value() {
    }

    public Value(String min, String op) {
        this.min = min;
        this.op = op;
    }

    public Value(String min, String max, String op) {
        this.min = /*(min==null)?"":*/min;
        this.max = /*(max==null)?"":*/max;
        this.op = /*(op==null)?"":*/op;
    }
}
