package com.catalina.vault_publisher.data;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class Locations {
    private List<LinkedHashMap> exclude;
    private List<LinkedHashMap> include;
    private boolean override;


    public Locations() {
    }

    public Locations(boolean override) {
        this.exclude = new ArrayList<>();
        this.include = new ArrayList<>();
        this.override = override;
    }

    public List<LinkedHashMap> getExclude() {
        return exclude;
    }

    public List<LinkedHashMap> getInclude() {
        return include;
    }

    public boolean isOverride() {
        return override;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        //implement List comparaison using HashSet
        Locations locations = (Locations) o;
        return override == locations.override &&
                Objects.equals(exclude, locations.exclude) &&
                Objects.equals(include, locations.include);
    }

}
