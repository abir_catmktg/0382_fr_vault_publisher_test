package com.catalina.vault_publisher.data;

public class SolrParam {
    private String key;
    private String value;

    public SolrParam() {
    }

    public SolrParam(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
