package com.catalina.vault_publisher.data.measurement;

import com.google.gson.JsonElement;

import java.util.Objects;

public class AdGroupMeasurement {
    private JsonElement touchpoint;
    private JsonElement user;

    public AdGroupMeasurement() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdGroupMeasurement that = (AdGroupMeasurement) o;
        return Objects.equals(touchpoint, that.touchpoint) &&
                Objects.equals(user, that.user);
    }

    public JsonElement getTouchpoint() {
        return touchpoint;
    }

    public void setTouchpoint(JsonElement touchpoint) {
        this.touchpoint = touchpoint;
    }

    public JsonElement getUser() {
        return user;
    }

    public void setUser(JsonElement user) {
        this.user = user;
    }
}
