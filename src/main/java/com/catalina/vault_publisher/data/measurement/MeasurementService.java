package com.catalina.vault_publisher.data.measurement;

import com.catalina.common.util.Pair;
import com.catalina.vault_publisher.data.forecasting.Forecasting;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MeasurementService {

    private static final Logger logger = LogManager.getLogger(MeasurementService.class);

    public MeasurementService() {
    }

    public Pair<Measurement, Forecasting> setMeasurementManufEcolink(String distributionCappingStr, Double _percent) {

        try {
            int distributionCapping = Integer.parseInt(distributionCappingStr);
            Double count = (distributionCapping * _percent) / (1 - _percent);
            Measurement measurement = new Measurement("Manual", "Adgroup", _percent, count);
            Forecasting forecasting = new Forecasting("store", "impression", distributionCapping);

            Pair<Measurement, Forecasting> pair = new Pair<>(measurement, forecasting);
            return pair;
        } catch (NumberFormatException nfe) {
            logger.error(String.format("%s cannot be cast as an Integer: %s", distributionCappingStr, nfe.getMessage()));
            return null;
        }

    }
}
