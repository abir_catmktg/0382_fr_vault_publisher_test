package com.catalina.vault_publisher.data.measurement;

public class Measurement {

    private Double count_of_ids;
    private String dynamic_user_control;
    private Double dynamic_user_control_percentage;
    private String scope;

    public Measurement() {
    }

    public Measurement(String _userControl, String _scope, Double _percent, Double _countOfIds) {
        count_of_ids = _countOfIds;
        dynamic_user_control = _userControl;
        dynamic_user_control_percentage = _percent;
        scope = _scope;
    }

}
