package com.catalina.vault_publisher.data.billing.targetingMethod;

public class TargetingMethodAPI {
    private Integer BusinessUnit;
    private String CMTSText;
    private String CountryCode;
    private String Description;
    private Boolean GlobalFlag;
    private Boolean IDFlag;
    private String ShortDescription;
    private String StartDate;
    private String StopDate;
    private Integer TargetingMethodNumber;
    private Integer TargetingMethodParentNumber;
    private String id;
    private String status;
    private String system_id;

    public TargetingMethodAPI() {
    }

    public Integer getBusinessUnit() {
        return BusinessUnit;
    }

    public String getCMTSText() {
        return CMTSText;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public String getDescription() {
        return Description;
    }

    public Boolean getGlobalFlag() {
        return GlobalFlag;
    }

    public Boolean getIDFlag() {
        return IDFlag;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getStopDate() {
        return StopDate;
    }

    public Integer getTargetingMethodNumber() {
        return TargetingMethodNumber;
    }

    public Integer getTargetingMethodParentNumber() {
        return TargetingMethodParentNumber;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getSystem_id() {
        return system_id;
    }
}
