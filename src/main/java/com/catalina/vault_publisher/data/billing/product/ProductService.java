package com.catalina.vault_publisher.data.billing.product;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ProductService {
    private static final Logger logger = LogManager.getLogger(ProductService.class);
    private CRestTemplate cRestTemplate;

    public ProductService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        new ProductService(new CRestTemplate()).getProduct(Config.Mode.PROD, CountryEnum.FRANCE, "Message");
    }

    public Product getProductCoupon() {
        Product product = new Product("product", "FRA-CATS-515", "Coupon", 515);
        return product;
    }

    public Product getProductMessage() {
        Product product = new Product("product", "FRA-CATS-505", "Message", 505);
        return product;
    }

    public Product getProduct(Config.Mode mode, CountryEnum country, String name) throws HttpResponseStatusException {
        String url = String.format("%s/billing_products?fq={fq}", mode.urlDashboard);
        String fqParam = String.format("{\"Description\":\"%s\",\"CountryCode\":\"%s\"}", name, country.getCode());

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fq: %s", fqParam));
        List<ProductAPI> productAPIS =
                cRestTemplate.executeSolrGetRequestWithJsonHeader(url, ProductAPI.class, fqParam);

        if (productAPIS.size() != 1) throw new RuntimeException(
                String.format("%s billing product(s) found with the name: %s & countryCode: %s",
                        productAPIS.size(),
                        name,
                        country.getCode()));

        ProductAPI productAPI = productAPIS.get(0);

        Product product = new Product("product",
                productAPI.getId(),
                productAPI.getDescription(),
                productAPI.getProdNumber());

        return product;
    }
}
