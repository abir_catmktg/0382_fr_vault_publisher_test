package com.catalina.vault_publisher.data.billing.product;

public class ProductAPI {
    private String AnnouncedStatCode;
    private Integer BusinessUnits;
    private String CountryCode;
    private String Description;
    private Boolean GlobalFlag;
    private Boolean PayFlag;
    private Integer ProdNumber;
    private Integer ProdParentNumber;
    private String ShortDescription;
    private String StartDate;
    private String StopDate;
    private String UnitsMVDFlad;
    private String id;
    private String status;
    private String system_id;

    public ProductAPI() {
    }

    public String getAnnouncedStatCode() {
        return AnnouncedStatCode;
    }

    public Integer getBusinessUnits() {
        return BusinessUnits;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public String getDescription() {
        return Description;
    }

    public Boolean getGlobalFlag() {
        return GlobalFlag;
    }

    public Boolean getPayFlag() {
        return PayFlag;
    }

    public Integer getProdNumber() {
        return ProdNumber;
    }

    public Integer getProdParentNumber() {
        return ProdParentNumber;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public String getStartDate() {
        return StartDate;
    }

    public String getStopDate() {
        return StopDate;
    }

    public String getUnitsMVDFlad() {
        return UnitsMVDFlad;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getSystem_id() {
        return system_id;
    }
}
