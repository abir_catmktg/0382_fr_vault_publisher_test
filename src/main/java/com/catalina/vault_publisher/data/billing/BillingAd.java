package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.billing.product.Product;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethod;

public class BillingAd extends BillingCampaign {
    private Product product;
    private TargetingMethod targeting_method;

    public BillingAd() {
    }

    public BillingAd(Country country, int external_id, String external_system, String id, String name, String status, String text, String account_id, String client_id, String business_segment, Product product, TargetingMethod targeting_method) {
        super(country, external_id, external_system, id, name, status, text, account_id, client_id, business_segment);
        this.product = product;
        this.targeting_method = targeting_method;
    }
}
