package com.catalina.vault_publisher.data.billing.targetingMethod;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static com.catalina.vault_publisher.utils.Constants.TARGETING_METHOD_ECOBON;
import static com.catalina.vault_publisher.utils.Constants.TARGETING_METHOD_ECOLINK;

public class TargetingMethodService {
    private static final Logger logger = LogManager.getLogger(TargetingMethodService.class);
    private CRestTemplate cRestTemplate;

    public TargetingMethodService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public TargetingMethod getTargetingMethod(Mode mode, CountryEnum country, String name) throws HttpResponseStatusException {

        String url = String.format("%s/billing_targeting_methods?fq={fq}", mode.urlDashboard);
        String fqParam = String.format("{\"Description\":\"%s\",\"CountryCode\":\"%s\"}", name, country.getCode());

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fq: %s", fqParam));
        List<TargetingMethodAPI> targetMethodAPIList =
                cRestTemplate.executeSolrGetRequestWithJsonHeader(url, TargetingMethodAPI.class, fqParam);

        if (targetMethodAPIList.size() != 1) throw new RuntimeException(
                String.format("%s targetingMethod(s) found with the name: %s & countryCode: %s",
                        targetMethodAPIList.size(),
                        name,
                        country.getCode()));

        TargetingMethodAPI tmAPI = targetMethodAPIList.get(0);

        TargetingMethod targetingMethod = new TargetingMethod("targeting_method",
                tmAPI.getId(),
                tmAPI.getDescription(),
                tmAPI.getTargetingMethodNumber());

        return targetingMethod;
    }

    public TargetingMethod getTargetingMethodEcolink() {
        return new TargetingMethod("targeting_method", "FRA-CATS-505", TARGETING_METHOD_ECOLINK, 505);
    }

    public TargetingMethod getTargetingMethodEcobon() {
        return new TargetingMethod("targeting_method", "FRA-CATS-500", TARGETING_METHOD_ECOBON, 500);
    }


}
