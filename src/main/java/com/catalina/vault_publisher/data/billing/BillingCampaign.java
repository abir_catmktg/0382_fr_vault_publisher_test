package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Country;

import java.util.Objects;

public class BillingCampaign extends Billing {

    public String business_segment;


    public BillingCampaign() {
    }

    public BillingCampaign(Country country, int external_id, String external_system, String id, String name, String status, String text, String account_id, String client_id, String business_segment) {
        super(country, external_id, external_system, id, name, status, text, account_id, client_id);
        this.business_segment = business_segment;
    }

    public String getBusiness_segment() {
        return business_segment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BillingCampaign that = (BillingCampaign) o;
        return super.equals(o) && Objects.equals(business_segment, that.business_segment);
    }
}
