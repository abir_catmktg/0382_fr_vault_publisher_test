package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Country;

import java.util.Objects;

public abstract class Billing extends Client {

    public String account_id;
    public String client_id;

    public Billing() {
    }

    public Billing(Country country, int external_id, String external_system, String id, String name, String status, String text, String account_id, String client_id) {
        super(country, external_id, external_system, id, name, status, text);
        this.account_id = account_id;
        this.client_id = client_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public String getClient_id() {
        return client_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Billing billing = (Billing) o;
        return super.equals(o) && Objects.equals(account_id, billing.account_id) &&
                Objects.equals(client_id, billing.client_id);
    }
}

