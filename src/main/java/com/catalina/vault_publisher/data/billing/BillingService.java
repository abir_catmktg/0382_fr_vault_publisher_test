package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.billing.product.ProductService;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethod.TargetingMethodEnum;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethodService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;

public class BillingService {

    private static final Logger logger = LogManager.getLogger(BillingService.class);
    private static HashMap<String, BillingAPI> BILLING_MAP = null;
    private CRestTemplate cRestTemplate;
    private ProductService productService;
    private TargetingMethodService targetingMethodService;

    public BillingService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.productService = new ProductService(cRestTemplate);
        this.targetingMethodService = new TargetingMethodService(cRestTemplate);
    }

    public static void main(String[] args) throws Exception {

    }

    public void initBilling() {
        BILLING_MAP = new HashMap<>();
    }

    private BillingAPI getBillingAPI(Mode mode, String billingNbr, String productCode, CountryEnum country) throws HttpResponseStatusException {
        BillingAPI billing;
        String billingId = country.getCode() + "-" + country.getBillingSystem() + "-" + billingNbr + "_" + productCode;

        if (BILLING_MAP.containsKey(billingNbr + "_" + productCode)) {
            return BILLING_MAP.get(billingNbr + "_" + productCode);
        } else {
            String url = mode.urlDashboard + "/billing?fq={fq}&func={func}";
            String fqParam = "{\"id\":\"" + billingId + "\"}";
            String funcParam = "return d(\"id\").match(\"" + country.getCode() + "\")";

            logger.debug(String.format("Getting BillingAPI object for billing %s, and country code %s",
                    billingId,
                    country.getCode()));

            List<BillingAPI> billings = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, BillingAPI.class, fqParam, funcParam);
            // Until we have an answer on the case of multiple billing found, we'll keep the first one [MANUF ONLY]
            if (billings.size() >= 1) {
                billing = billings.get(0);
                if (mode == Mode.SQA) {
                    billing.setName("NAME IS EMPTY IN SQA");
                }

                BILLING_MAP.put(billingNbr + "_" + productCode, billing);
                return billing;
            }
            throw new RuntimeException("getBilling error : no billing found");
        }
    }

    public BillingCampaign getBillingCampaign(Config.Mode mode, CountryEnum country, String billingNbr, String productCd) throws HttpResponseStatusException {
        BillingAPI billingAPI = getBillingAPI(mode, billingNbr, productCd, country);

        BillingCampaign billingCampaign = createBillingCampaignFromBillingAPI(billingAPI);
        return billingCampaign;
    }

    private BillingCampaign createBillingCampaignFromBillingAPI(BillingAPI billingAPI) {
        return new BillingCampaign(billingAPI.getCountry(),
                billingAPI.getExternal_id(),
                billingAPI.getExternal_system(),
                billingAPI.getId(),
                billingAPI.getName(),
                billingAPI.getStatus(),
                billingAPI.getText(),
                billingAPI.getAccount_id(),
                billingAPI.getClient_id(),
                billingAPI.getBusiness_segment().getBus_seg_descr());
    }

    public BillingAd createBillingAD(Config.Mode mode,
                                     CountryEnum country,
                                     String billingNbr,
                                     String productCd,
                                     TargetingMethodEnum targetingMethodEnum) throws HttpResponseStatusException {
        BillingAPI billing = getBillingAPI(mode, billingNbr, productCd, country);
        return new BillingAd(billing.getCountry(),
                billing.getExternal_id(),
                billing.getExternal_system(),
                billing.getId(),
                billing.getName(),
                billing.getStatus(),
                billing.getText(),
                billing.getAccount_id(),
                billing.getClient_id(),
                billing.getBusiness_segment().getBus_seg_descr(),
                productService.getProductCoupon(),
                targetingMethodEnum.targetingMethod);
    }

}
