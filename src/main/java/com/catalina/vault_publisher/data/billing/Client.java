package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Country;

import java.util.Objects;

public class Client {

    private Country country;
    private int external_id;
    private String external_system;
    private String id;
    private String name;
    private String status;
    private String text;

    public Client() {
    }

    public Client(Country country, int external_id, String external_system, String id, String name, String status, String text) {
        this.country = country;
        this.external_id = external_id;
        this.external_system = external_system;
        this.id = id;
        this.name = name;
        this.status = status;
        this.text = text;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getExternal_id() {
        return external_id;
    }

    public void setExternal_id(int external_id) {
        this.external_id = external_id;
    }

    public String getExternal_system() {
        return external_system;
    }

    public void setExternal_system(String external_system) {
        this.external_system = external_system;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;
        return external_id == client.external_id &&
                Objects.equals(country, client.country) &&
                Objects.equals(external_system, client.external_system) &&
                Objects.equals(id, client.id) &&
                Objects.equals(name, client.name) &&
                Objects.equals(status, client.status) &&
                Objects.equals(text, client.text);
    }
}
