package com.catalina.vault_publisher.data.billing;

public class BusinessSegment {
    private String bus_seg_descr;
    private int bus_seg_nbr;

    public BusinessSegment() {
    }

    public BusinessSegment(String bus_seg_descr, int bus_seg_nbr) {
        this.bus_seg_descr = bus_seg_descr;
        this.bus_seg_nbr = bus_seg_nbr;
    }

    public String getBus_seg_descr() {
        return bus_seg_descr;
    }

    public void setBus_seg_descr(String bus_seg_descr) {
        this.bus_seg_descr = bus_seg_descr;
    }

    public int getBus_seg_nbr() {
        return bus_seg_nbr;
    }

    public void setBus_seg_nbr(int bus_seg_nbr) {
        this.bus_seg_nbr = bus_seg_nbr;
    }
}
