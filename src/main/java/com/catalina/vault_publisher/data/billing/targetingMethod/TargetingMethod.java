package com.catalina.vault_publisher.data.billing.targetingMethod;

import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;

import java.util.Arrays;
import java.util.Objects;

public class TargetingMethod {
    private String field;
    private String id;
    private String name;
    private String text;
    private int value;

    public TargetingMethod() {
    }

    public TargetingMethod(String field, String id, String name, int value) {
        this.field = field;
        this.id = id;
        this.name = this.text = name;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetingMethod that = (TargetingMethod) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(text, that.text) &&
                Objects.equals(value, that.value);
    }

    public enum TargetingMethodEnum {
        ECOBON(Constants.TARGETING_METHOD_ECOBON, new TargetingMethodService(new CRestTemplate()).getTargetingMethodEcobon()),
        ECOLINK(Constants.TARGETING_METHOD_ECOLINK, new TargetingMethodService(new CRestTemplate()).getTargetingMethodEcolink());

        public final String name;
        public final TargetingMethod targetingMethod;

        TargetingMethodEnum(String name, TargetingMethod targetingMethod) {
            this.name = name;
            this.targetingMethod = targetingMethod;
        }

        public static TargetingMethodEnum fromValue(String method) {
            return Arrays.stream(TargetingMethod.TargetingMethodEnum.values())
                    .filter(x -> x.name.contains(method))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported Targeting Method name : %s.", method)));
        }
    }
}
