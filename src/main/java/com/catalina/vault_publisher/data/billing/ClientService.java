package com.catalina.vault_publisher.data.billing;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;

public class ClientService {

    private static final Logger logger = LogManager.getLogger(ClientService.class);
    private static HashMap<String, Client> CLIENT_MAP = null;
    private CRestTemplate cResttemplate;

    public ClientService(CRestTemplate cResttemplate) {
        this.cResttemplate = cResttemplate;
    }

    public static void main(String[] args) throws HttpResponseStatusException {
        ClientService service = new ClientService(new CRestTemplate());
        service.initClient();
        service.getClientFromDashBoard(Config.Mode.SQA, "FRA-CATS-374");
    }

    public void initClient() {

        CLIENT_MAP = new HashMap<>();
    }

    public Client getClient(Config.Mode mode, String clientId) throws HttpResponseStatusException {
        return getClientFromDashBoard(mode, clientId);
    }

    public Client getClientFromDashBoard(Config.Mode _mode, String clientId) throws HttpResponseStatusException {
        if (CLIENT_MAP == null) initClient();

        if (CLIENT_MAP.containsKey(clientId)) return CLIENT_MAP.get(clientId);

        String url = _mode.urlDashboard + "/clients?fq={fq}";
        String fqParam = String.format("{\"id\":\"%s\"}", clientId);

        logger.info(String.format("GET: %s", url));
        logger.debug(String.format("params: \\n fq: %s", fqParam));
        List<Client> docs = cResttemplate.executeSolrGetRequestWithJsonHeader(url, Client.class, fqParam);

        if (docs.size() != 1) throw new RuntimeException(String.format("%s client(s) found with the id: %s",
                docs.size(),
                clientId));

        CLIENT_MAP.put(clientId, docs.get(0));
        return docs.get(0);

    }
}
