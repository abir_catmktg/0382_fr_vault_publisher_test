package com.catalina.vault_publisher.data.billing.product;

import java.util.Objects;

public class Product {
    private String field;
    private String id;
    private String name;
    private String text;
    private int value;

    public Product() {
    }

    public Product(String field, String id, String name, int value) {
        this.field = field;
        this.id = id;
        this.name = this.text = name;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product that = (Product) o;
        return Objects.equals(field, that.field) &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(text, that.text) &&
                Objects.equals(value, that.value);
    }
}
