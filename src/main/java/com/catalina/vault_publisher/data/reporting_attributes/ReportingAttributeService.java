package com.catalina.vault_publisher.data.reporting_attributes;

public class ReportingAttributeService {

    public ReportingAttributeService() {
    }

    public ReportingAttribute createReportingAttribute(String value) {
        ReportingAttribute.Key key = new ReportingAttribute.Key("category_catalina_description",
                "Category Catalina Description");

        return new ReportingAttribute(false, value, key);
    }
}
