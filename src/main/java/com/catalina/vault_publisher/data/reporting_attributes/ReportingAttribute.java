package com.catalina.vault_publisher.data.reporting_attributes;

public class ReportingAttribute {

    private boolean custom;
    private String value;
    private Key key;

    public ReportingAttribute() {
    }

    public ReportingAttribute(boolean custom, String value, Key key) {
        this.custom = custom;
        this.value = value;
        this.key = key;
    }

    protected static class Key {
        private String id;
        private String name;
        private String text;

        public Key() {
        }

        public Key(String id, String name) {
            this.id = id;
            this.name = this.text = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getText() {
            return text;
        }
    }
}
