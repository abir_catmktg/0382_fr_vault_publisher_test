package com.catalina.vault_publisher.data.productValidation;

import com.catalina.vault_publisher.data.list.AbstractTriggerList;
import com.google.gson.JsonArray;

import java.util.List;

public class ProductValidation {
    private JsonArray departmentClassCodes;
    private JsonArray departmentCodes;
    private String expiry;
    private boolean is_list_based;
    private boolean is_list_based_checkbox;
    private List<AbstractTriggerList> lists;
    private String min_date;
    private String rolling_days;
    private String selected_type;

    public ProductValidation() {
    }

    public ProductValidation(JsonArray departmentClassCodes, JsonArray departmentCodes, String expiry, boolean is_list_based, boolean is_list_based_checkbox, List<AbstractTriggerList> lists, String min_date, String rolling_days, String selected_type) {
        this.departmentClassCodes = departmentClassCodes;
        this.departmentCodes = departmentCodes;
        this.expiry = expiry;
        this.is_list_based = is_list_based;
        this.is_list_based_checkbox = is_list_based_checkbox;
        this.lists = lists;
        this.min_date = min_date;
        this.rolling_days = rolling_days;
        this.selected_type = selected_type;
    }

    public JsonArray getDepartmentClassCodes() {
        return departmentClassCodes;
    }

    public JsonArray getDepartmentCodes() {
        return departmentCodes;
    }

    public String getExpiry() {
        return expiry;
    }

    public boolean isIs_list_based() {
        return is_list_based;
    }

    public boolean isIs_list_based_checkbox() {
        return is_list_based_checkbox;
    }

    public List<AbstractTriggerList> getLists() {
        return lists;
    }

    public String getMin_date() {
        return min_date;
    }

    public String getRolling_days() {
        return rolling_days;
    }

    public String getSelected_type() {
        return selected_type;
    }
}
