package com.catalina.vault_publisher.data.productValidation;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItem;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerList;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemAsTriggerListService;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductValidationService {

    private CRestTemplate cRestTemplate;
    private LMCItemService lmcItemService;
    private LMCItemAsTriggerListService lmcItemAsTriggerListService;

    public ProductValidationService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.lmcItemService = new LMCItemService(cRestTemplate);
        this.lmcItemAsTriggerListService = new LMCItemAsTriggerListService();
    }

    public ProductValidation createProductValidationManuf(LMCItemAsTriggerList list, String rollingDays) {
        return new ProductValidation(new JsonArray(),
                new JsonArray(),
                "rolling_days",
                true,
                true,
                Collections.singletonList(list),
                null,
                rollingDays,
                "upc");
    }


    public List<ProductValidation> setProductValidationManuf(Config.Mode mode, String upcListName, String triggerListName, String campgnTyp, String leverType) throws HttpResponseStatusException {
        List<ProductValidation> listProductVal = new ArrayList<>();


        //GET LIST VUPC
        List<LMCItem> lmcItems = lmcItemService.getListLMC(mode, upcListName);

        LMCItemAsTriggerList lmcTriggerList;
        if (lmcItems.size() > 1) lmcTriggerList = findListForProductValidation(lmcItems, upcListName);
        else lmcTriggerList = lmcItemAsTriggerListService.createLMCItemAsTriggerList(lmcItems.get(0));

        ProductValidation productValidation = createProductValidationManuf(lmcTriggerList, "28");
        listProductVal.add(productValidation);

        if (triggerListName != null && !triggerListName.isEmpty()) {
            //GET LIST TRIGGER
            List<LMCItem> lmcItemsTrigger = lmcItemService.getListLMC(mode, triggerListName);
            LMCItemAsTriggerList listLMCTrigger = findListForProductValidation(lmcItemsTrigger, triggerListName);

            // EVERY CAMPAIGN & LEVER-TYPE == CP
            if ("CP".equalsIgnoreCase(leverType)) {
                ProductValidation productValidationCP = createProductValidationManuf(listLMCTrigger, "0");
                listProductVal.add(productValidationCP);
            }
            // ALCOOL CAMPAIGN
            if (!"NON ALCOOL".equalsIgnoreCase(campgnTyp)) {
                // EVERY LEVER-TYPE != CP => VUPC 0
                if (!"CP".equalsIgnoreCase(leverType)) {
                    ProductValidation productValidationAlcool = createProductValidationManuf(listLMCTrigger, "0");
                    listProductVal.add(productValidationAlcool);
                }
            }
        }
        return listProductVal;
    }

    public LMCItemAsTriggerList findListForProductValidation(List<LMCItem> lmcItems, String listName) {
        LMCItem lmcList = null;
        int i = 0;
        for (LMCItem currentList : lmcItems) {
            if (currentList.getName().contains(listName)) {
                lmcList = currentList;
                i++;
            }
        }
        if (i != 1) throw new RuntimeException(String.format("%s LMCItem(s) found for listname: %s",
                lmcItems.size(),
                listName));

        LMCItemAsTriggerList lists = lmcItemAsTriggerListService.createLMCItemAsTriggerList(lmcList);
        return lists;
    }

}
