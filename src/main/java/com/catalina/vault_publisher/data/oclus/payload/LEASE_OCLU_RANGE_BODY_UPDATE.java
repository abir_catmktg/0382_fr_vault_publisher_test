package com.catalina.vault_publisher.data.oclus.payload;

import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;

import java.util.List;

public class LEASE_OCLU_RANGE_BODY_UPDATE extends LEASE_OCLU_RANGE_BODY {
    private List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes;
    private String offerCodeType;

    public LEASE_OCLU_RANGE_BODY_UPDATE() {
    }

    public LEASE_OCLU_RANGE_BODY_UPDATE(String dateStop, DateSpecification dateSpecification, List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes, String offerCodeType) {
        super(dateStop, dateSpecification);
        this.offerCodes = offerCodes;
        this.offerCodeType = offerCodeType;
    }

    public List<OCLU_LEASE_RESPONSE_ENTITY> getOfferCodes() {
        return offerCodes;
    }

    public String getOfferCodeType() {
        return offerCodeType;
    }
}
