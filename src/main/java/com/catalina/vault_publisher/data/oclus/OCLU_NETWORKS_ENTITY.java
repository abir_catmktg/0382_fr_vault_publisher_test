package com.catalina.vault_publisher.data.oclus;

import java.util.List;

public class OCLU_NETWORKS_ENTITY {
    private OCLU_NETWORK network;
    private List<OCLU_RESERVATION> reservations;

    public OCLU_NETWORKS_ENTITY() {
    }

    public OCLU_NETWORK getNetwork() {
        return network;
    }

    public List<OCLU_RESERVATION> getReservations() {
        return reservations;
    }
}
