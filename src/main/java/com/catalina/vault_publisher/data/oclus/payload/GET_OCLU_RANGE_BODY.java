package com.catalina.vault_publisher.data.oclus.payload;

import com.catalina.vault_publisher.data.experience.Funding;
import com.catalina.vault_publisher.data.targeting.Targeting;

public class GET_OCLU_RANGE_BODY {
    private String promotionId;
    private Funding funding;
    private Targeting targeting;
    private String status;

    public GET_OCLU_RANGE_BODY() {
    }

    public GET_OCLU_RANGE_BODY(String promotionId, Funding funding, Targeting targeting, String status) {
        this.promotionId = promotionId;
        this.funding = funding;
        this.targeting = targeting;
        this.status = status;
    }

    public String getPromotionId() {
        return promotionId;
    }

    public Funding getFunding() {
        return funding;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public String getStatus() {
        return status;
    }
}
