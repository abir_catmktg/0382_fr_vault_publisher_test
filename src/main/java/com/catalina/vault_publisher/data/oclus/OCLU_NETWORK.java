package com.catalina.vault_publisher.data.oclus;

public class OCLU_NETWORK {
    public String id;
    public String name;
    public String country_code;

    public OCLU_NETWORK() {
    }

    public OCLU_NETWORK(String id, String name, String country_code) {
        this.id = id;
        this.name = name;
        this.country_code = country_code;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry_code() {
        return country_code;
    }
}
