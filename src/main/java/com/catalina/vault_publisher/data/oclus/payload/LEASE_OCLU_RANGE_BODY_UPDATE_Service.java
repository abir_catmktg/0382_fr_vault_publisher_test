package com.catalina.vault_publisher.data.oclus.payload;

import com.catalina.vault_publisher.data.experience.reward.ValidityDate;
import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;
import com.catalina.vault_publisher.data.offers.Promotion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class LEASE_OCLU_RANGE_BODY_UPDATE_Service {

    private static final Logger logger = LogManager.getLogger(LEASE_OCLU_RANGE_BODY_UPDATE_Service.class);

    public LEASE_OCLU_RANGE_BODY_UPDATE_Service() {
    }

    public LEASE_OCLU_RANGE_BODY_UPDATE createLeaseUpdateBody(Promotion promotion, List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes) {
        try {
            ValidityDate.DateType dateType =
                    ValidityDate.DateType.fromValue(promotion.getExperience().getReward().getExpiration_date().getType());
            String value = promotion.getExperience().getReward().getExpiration_date().getValue();
            DateSpecification dateSpecification = new DateSpecification(dateType.getType(), value);
            return new LEASE_OCLU_RANGE_BODY_UPDATE(promotion.getStop_date(), dateSpecification, offerCodes, "GENERATED");
        } catch (NullPointerException npe) {
            logger.error(String.format("null pointer while parsing promotion %s:\\n %s", promotion.getId(), npe.getMessage()));
        }
        return null;
    }
}
