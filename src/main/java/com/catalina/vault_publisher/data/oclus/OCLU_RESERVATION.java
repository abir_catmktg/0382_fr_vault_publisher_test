package com.catalina.vault_publisher.data.oclus;

import java.util.List;

public class OCLU_RESERVATION {
    private OCLU_RANGE range;
    private List<String> leases;

    public OCLU_RESERVATION() {
    }

    public OCLU_RANGE getRange() {
        return range;
    }

    public List<String> getLeases() {
        return leases;
    }
}
