package com.catalina.vault_publisher.data.oclus;

public class OCLU_MINI_RANGE {
    private String id;
    private OCLU_NETWORK network;

    public OCLU_MINI_RANGE() {
    }

    public OCLU_MINI_RANGE(String id, OCLU_NETWORK network) {
        this.id = id;
        this.network = network;
    }

    public String getId() {
        return id;
    }

    public OCLU_NETWORK getNetwork() {
        return network;
    }
}
