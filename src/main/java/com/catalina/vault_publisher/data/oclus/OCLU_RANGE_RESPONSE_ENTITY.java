package com.catalina.vault_publisher.data.oclus;

import java.util.List;

public class OCLU_RANGE_RESPONSE_ENTITY {
    private List<String> errors;
    private List<String> warnings;
    private List<OCLU_NETWORKS_ENTITY> networks;

    public OCLU_RANGE_RESPONSE_ENTITY() {
    }

    public List<String> getErrors() {
        return errors;
    }

    public List<String> getWarnings() {
        return warnings;
    }

    public List<OCLU_NETWORKS_ENTITY> getNetworks() {
        return networks;
    }
}
