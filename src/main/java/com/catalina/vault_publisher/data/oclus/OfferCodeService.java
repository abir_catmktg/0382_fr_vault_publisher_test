package com.catalina.vault_publisher.data.oclus;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.oclus.payload.GET_OCLU_RANGE_BODY_Service;
import com.catalina.vault_publisher.data.oclus.payload.LEASE_OCLU_RANGE_BODY_Service;
import com.catalina.vault_publisher.data.oclus.payload.LEASE_OCLU_RANGE_BODY_UPDATE;
import com.catalina.vault_publisher.data.oclus.payload.LEASE_OCLU_RANGE_BODY_UPDATE_Service;
import com.catalina.vault_publisher.data.offers.Promotion;
import com.catalina.vault_publisher.data.offers.PromotionService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class OfferCodeService {

    private static final Logger logger = LogManager.getLogger(OfferCodeService.class);
    private CRestTemplate cRestTemplate;
    private PromotionService promotionServices;
    private GET_OCLU_RANGE_BODY_Service GETOCLURANGEBODYService;
    private LEASE_OCLU_RANGE_BODY_Service LEASEOCLURANGEBODYService;
    private LEASE_OCLU_RANGE_BODY_UPDATE_Service LEASEOCLURANGEBODYUPDATEService;

    public OfferCodeService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
        this.promotionServices = new PromotionService(cRestTemplate);
        this.GETOCLURANGEBODYService = new GET_OCLU_RANGE_BODY_Service();
        this.LEASEOCLURANGEBODYService = new LEASE_OCLU_RANGE_BODY_Service();
        this.LEASEOCLURANGEBODYUPDATEService = new LEASE_OCLU_RANGE_BODY_UPDATE_Service();
    }

    public static void main(String[] args) {
        try {
            new OfferCodeService(new CRestTemplate())
                    .releaseOfferCodesFromPromotionsId(Config.Mode.PROD, Arrays.asList("FRA-BLIP-BL_7290_0797",
                            "FRA-BLIP-BL_4490_9263",
                            "FRA-BLIP-BL_1737_0308",
                            "FRA-BLIP-BL_7472_7372"));
        } catch (HttpResponseStatusException e) {
            e.printStackTrace();
        }
    }

    private List<OCLU_RANGE> getOclusRangesByNetwork(Config.Mode _mode, Promotion promotion) throws HttpResponseStatusException {
        String url = _mode.dmpUrl + "/offercodes/promotions/" + promotion.getId();

        String payload = Config.toJsString(GETOCLURANGEBODYService.createPayLoadToGetRangeFromPromotionId(promotion));

        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", payload));
        OCLU_RANGE_RESPONSE_ENTITY response = cRestTemplate.executePostRequestWithJsonHeader(url, payload, OCLU_RANGE_RESPONSE_ENTITY.class);
        List<OCLU_RANGE> rangeList = response.getNetworks().stream()
                .filter(ntwkEntity -> ntwkEntity.getReservations().size() > 0)
                .map(ntwkEntity -> ntwkEntity.getReservations().get(0).getRange())
                .collect(Collectors.toList());

        logger.debug(String.format("%s range(s) found for promotion: %s", rangeList.size(), promotion.getId()));

        return rangeList;

    }

    private OCLU_LEASE_RESPONSE_ENTITY leaseRangeId(Config.Mode _mode, Promotion promotion, String rangeId) throws HttpResponseStatusException {
        String url = _mode.dmpUrl + "/offercodes/promotions/" + promotion.getId() + "/leases/" + rangeId;
        String body = Config.toJsString(LEASEOCLURANGEBODYService.createRangeExpirationFromPromotion(promotion));
        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", body));
        OCLU_LEASE_RESPONSE_ENTITY response = cRestTemplate.executePostRequestWithJsonHeader(url, body, OCLU_LEASE_RESPONSE_ENTITY.class);
        return response;
    }

    private void leaseUpdate(Config.Mode m_mode, Promotion promotion, List<OCLU_LEASE_RESPONSE_ENTITY> responses) throws HttpResponseStatusException {
        String url = m_mode.dmpUrl + "/offercodes/promotions/" + promotion.getId() + "/leases/update";
        LEASE_OCLU_RANGE_BODY_UPDATE payloadObj = LEASEOCLURANGEBODYUPDATEService.createLeaseUpdateBody(promotion, responses);
        String payload = Config.toJsString(payloadObj);

        logger.info(String.format("POST: %s", url));
        logger.debug(String.format("body:\\n %s", payload));
        cRestTemplate.executeRequestWithJsonHeader(url, payload, String.class, HttpMethod.POST, HttpStatus.OK);
    }

    public void release(Config.Mode _mode, String promoId, String... rangeIds) throws HttpResponseStatusException {
        for (String currentRange : rangeIds) {
            String url = _mode.dmpUrl + "/offercodes/promotions/" + promoId + "/leases/" + currentRange;
            logger.info(String.format("DELETE: %s", url));
            cRestTemplate.executeRequestWithJsonHeader(url, null, String.class, HttpMethod.DELETE, HttpStatus.OK);
        }
    }

    public List<OCLU_LEASE_RESPONSE_ENTITY> leaseOfferCodesForPromotion(Config.Mode mode, Promotion promotion) throws HttpResponseStatusException {
        List<OCLU_LEASE_RESPONSE_ENTITY> leaseResponseEntityList = new ArrayList<>();
        List<OCLU_RANGE> rangesList = getOclusRangesByNetwork(mode, promotion);
        for (OCLU_RANGE currentRange : rangesList) {
            //If no offerCodes available anymore
            if (currentRange.getMin() == currentRange.getMax()) {
                throw new RuntimeException(String.format("No available offerCodes for network: %s type: %s",
                        currentRange.getNetwork().getName(),
                        currentRange.getType()));
            }
            OCLU_LEASE_RESPONSE_ENTITY leaseResponseEntity = leaseRangeId(mode, promotion, currentRange.getId());
            leaseResponseEntityList.add(leaseResponseEntity);
        }
        leaseUpdate(mode, promotion, leaseResponseEntityList);
        for (OCLU_LEASE_RESPONSE_ENTITY currentEntity : leaseResponseEntityList) currentEntity.setStatus("ACTIVE");
        return leaseResponseEntityList;
    }

    public void releaseOfferCodesFromPromotionsId(Config.Mode mode, List<String> promoIds) throws HttpResponseStatusException {
        for (String currentPromotionId : promoIds) {
            Promotion promotion = promotionServices.getPromotion(mode, currentPromotionId);

            List<String> rangeIdList = promotion.getExperience().getReward().getOffer_codes().stream()
                    .map(x -> x.getRange().getId())
                    .collect(Collectors.toList());
            String[] array = new String[rangeIdList.size()];
            release(mode, currentPromotionId, rangeIdList.toArray(array));
        }
    }

    public void releaseOfferCodesFromPromotionsList(Config.Mode mode, List<Promotion> promotionList) throws HttpResponseStatusException {
        for (Promotion currentPromotion : promotionList) {

            List<String> rangeIdList = currentPromotion.getExperience().getReward().getOffer_codes().stream()
                    .map(x -> x.getRange().getId())
                    .collect(Collectors.toList());
            String[] array = new String[rangeIdList.size()];
            release(mode, currentPromotion.getId(), rangeIdList.toArray(array));
        }
    }

    public void leaseOrReleaseOfferCodesForCampaign(Config.Mode mode, OfferCodeAction offerCodeAction, List<String> campaignIds) throws HttpResponseStatusException {
        List<String> promotionIdList = promotionServices.getFlatPromotionsIdFromCampaignId(mode, campaignIds);
        List<Promotion> promotionList = new ArrayList<>();
        for (String currentPromo : promotionIdList)
            promotionList.add(promotionServices.getPromotion(mode, currentPromo));

        if (offerCodeAction == OfferCodeAction.LEASE) {
            for (Promotion currentPromo : promotionList) leaseOfferCodesForPromotion(mode, currentPromo);
        }
        if (offerCodeAction == OfferCodeAction.RELEASE) {
            releaseOfferCodesFromPromotionsList(mode, promotionList);
        }
    }

    public enum OfferCodeAction {
        LEASE(0, "lease"),
        RELEASE(1, "release");

        private int code;
        private String value;

        OfferCodeAction(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public static OfferCodeAction fromValues(String offerCodeAction) {
            return Arrays.asList(OfferCodeAction.values()).stream()
                    .filter(x -> x.getValue().equalsIgnoreCase(offerCodeAction))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("Unsupported OfferCodeEnum Action %s.", offerCodeAction)));
        }

        public int getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }

    }
}
