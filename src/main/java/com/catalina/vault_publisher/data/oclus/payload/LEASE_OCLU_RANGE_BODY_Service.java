package com.catalina.vault_publisher.data.oclus.payload;

import com.catalina.vault_publisher.data.experience.reward.ValidityDate;
import com.catalina.vault_publisher.data.offers.Promotion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LEASE_OCLU_RANGE_BODY_Service {

    private static final Logger logger = LogManager.getLogger(LEASE_OCLU_RANGE_BODY_Service.class);

    public LEASE_OCLU_RANGE_BODY_Service() {
    }

    private LEASE_OCLU_RANGE_BODY createBodyExpiration(ValidityDate.DateType dateType, String dateStop, String value) {
        DateSpecification dateSpecification = new DateSpecification(dateType.getType(), value);
        return new LEASE_OCLU_RANGE_BODY(dateStop, dateSpecification);
    }

    public LEASE_OCLU_RANGE_BODY createRangeExpirationFromPromotion(Promotion promotion) {
        try {
            ValidityDate.DateType dateType =
                    ValidityDate.DateType.fromValue(promotion.getExperience().getReward().getExpiration_date().getType());
            String stopDate = promotion.getStop_date();
            String value = promotion.getExperience().getReward().getExpiration_date().getValue();
            return createBodyExpiration(dateType, stopDate, value);
        } catch (NullPointerException npe) {
            logger.error(String.format("null pointer while parsing promotion %s:\\n %s", promotion.getId(), npe.getMessage()));
        }
        return null;
    }
}
