package com.catalina.vault_publisher.data.oclus;

import com.catalina.vault_publisher.data.touchpoint.Touchpoints;

public class OCLU_RANGE extends OCLU_MINI_RANGE {
    private String type;
    private int min;
    private int max;
    private Touchpoints touchpoints;
    private String status;
    private int expiration_rolling_days;
    private String description;

    public OCLU_RANGE() {
    }

    public String getType() {
        return type;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public Touchpoints getTouchpoints() {
        return touchpoints;
    }

    public String getStatus() {
        return status;
    }

    public int getExpiration_rolling_days() {
        return expiration_rolling_days;
    }

    public String getDescription() {
        return description;
    }
}
