package com.catalina.vault_publisher.data.oclus.payload;

public class DateSpecification {
    private String type;
    private String value;

    public DateSpecification() {
    }

    public DateSpecification(String type, String value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }
}
