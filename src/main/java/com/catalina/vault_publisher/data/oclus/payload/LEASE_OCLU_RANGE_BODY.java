package com.catalina.vault_publisher.data.oclus.payload;

public class LEASE_OCLU_RANGE_BODY {
    private String stopDate;
    private DateSpecification dateSpecification;

    public LEASE_OCLU_RANGE_BODY() {
    }

    public LEASE_OCLU_RANGE_BODY(String stopDate, DateSpecification dateSpecification) {
        this.stopDate = stopDate;
        this.dateSpecification = dateSpecification;
    }

    public String getStopDate() {
        return stopDate;
    }

    public DateSpecification getDateSpecification() {
        return dateSpecification;
    }
}
