package com.catalina.vault_publisher.data.oclus.payload;

import com.catalina.vault_publisher.data.offers.Promotion;
import com.catalina.vault_publisher.utils.Constants;

public class GET_OCLU_RANGE_BODY_Service {

    public GET_OCLU_RANGE_BODY createPayLoadToGetRangeFromPromotionId(Promotion promotion) {
        return new GET_OCLU_RANGE_BODY(promotion.getId(),
                promotion.getExperience().getReward().getFunding(),
                promotion.getTargeting(),
                Constants.STATUS_DRAFT);
    }


}
