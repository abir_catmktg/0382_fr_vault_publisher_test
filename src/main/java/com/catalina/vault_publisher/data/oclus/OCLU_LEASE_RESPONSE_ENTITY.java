package com.catalina.vault_publisher.data.oclus;

import com.catalina.vault_publisher.data.targeting.Targeting;

public class OCLU_LEASE_RESPONSE_ENTITY {
    private String type;
    private String id;
    private String value;
    private Targeting targeting;
    private String date_expires;
    private String date_issued;
    private String date_deleted;
    private String status;
    private String promotion_id;
    private OCLU_MINI_RANGE range;

    public OCLU_LEASE_RESPONSE_ENTITY() {
    }

    public OCLU_LEASE_RESPONSE_ENTITY(String type,
                                      String id,
                                      String value,
                                      Targeting targeting,
                                      String date_expires,
                                      String date_issued,
                                      String date_deleted,
                                      String status,
                                      String promotion_id,
                                      OCLU_MINI_RANGE range) {
        this.type = type;
        this.id = id;
        this.value = value;
        this.targeting = targeting;
        this.date_expires = date_expires;
        this.date_issued = date_issued;
        this.date_deleted = date_deleted;
        this.status = status;
        this.promotion_id = promotion_id;
        this.range = range;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public Targeting getTargeting() {
        return targeting;
    }

    public String getDate_expires() {
        return date_expires;
    }

    public String getDate_issued() {
        return date_issued;
    }

    public String getDate_deleted() {
        return date_deleted;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromotion_id() {
        return promotion_id;
    }

    public OCLU_MINI_RANGE getRange() {
        return range;
    }
}
