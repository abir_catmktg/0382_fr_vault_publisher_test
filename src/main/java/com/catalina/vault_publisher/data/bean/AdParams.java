package com.catalina.vault_publisher.data.bean;


import java.time.LocalDate;
import java.time.LocalTime;

public class AdParams {
    public String id;
    public LocalDate startDate;
    public LocalTime startTime;
    public LocalDate stopDate;
    public LocalTime stopTime;
    public String status;
    public Integer capping;

    public AdParams() {
    }
}
