package com.catalina.vault_publisher.data;

import java.util.Objects;

public class AdGroupRules {
    AdGroupLimit adGroupLimit;

    public AdGroupRules() {
    }

    public AdGroupRules(AdGroupLimit adGroupLimit) {
        this.adGroupLimit = adGroupLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdGroupRules adGroupRules = (AdGroupRules) o;
        return Objects.equals(adGroupLimit, adGroupRules.adGroupLimit);
    }

    public AdGroupLimit getAdGroupLimit() {
        return adGroupLimit;
    }

    public void setAdGroupLimit(AdGroupLimit adGroupLimit) {
        this.adGroupLimit = adGroupLimit;
    }
}
