package com.catalina.vault_publisher.data;

import com.catalina.vault_publisher.utils.DateUtils;

public class History {
    private String created_by;
    private String created_date;
    private String modified_by;
    private String modified_date;

    public History() {
    }

    public History(String created_by) {
        this.created_by = this.modified_by = created_by;
        this.created_date = this.modified_date = DateUtils.getCurrentDateMilliSecondPrecision();
    }

    public History(String created_by, String created_date) {
        this.created_by = this.modified_by = created_by;
        this.created_date = this.modified_date = created_date;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreated_date() {
        return created_date;
    }

    public String getModified_by() {
        return modified_by;
    }

    public String getModified_date() {
        return modified_date;
    }
}
