package com.catalina.vault_publisher.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

public class FileUtils {
    public static String loadResourceIntoString(String resourcePath) throws IOException {
        InputStream is = FileUtils.class.getResourceAsStream(resourcePath);
        StringWriter writer = new StringWriter();
        IOUtils.copy(is, writer, Charset.forName("UTF-8"));
        String tmpStr = writer.toString();
        return tmpStr;
    }
}
