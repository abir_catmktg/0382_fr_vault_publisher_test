package com.catalina.vault_publisher.utils;

import java.util.Map;

public class MapUtils {

    public static double getDoubleValue(Map map, String key) {
        Number x = (Number) map.get(key);
        return x.doubleValue();
    }
}
