package com.catalina.vault_publisher.utils;

import com.catalina.vault_publisher.data.Config;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CRestTemplate {

    private static RestTemplate restTemplate;
    private static RestTemplate encodeRestTemplate;

    static {
        Gson gson = new Gson();
        restTemplate = new RestTemplate(Arrays.asList(new ByteArrayHttpMessageConverter(),
                new StringHttpMessageConverter(Charset.forName("UTF-8")),
                new ResourceHttpMessageConverter(),
                new AllEncompassingFormHttpMessageConverter(),
                new GsonHttpMessageConverter(gson)));

        encodeRestTemplate = new RestTemplate(
                new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));

    }

    private static Map<String, String> extractQueryParamsFromUrl(String url, Object... params) {
        if (params.length == 0) return null;
        int paramsStart = url.indexOf('?');
        String paramsStr = url.substring(paramsStart + 1);
        String[] paramArray = paramsStr.split(Pattern.quote("&"));
        Map<String, String> paramMap = new HashMap<>();
        //String[] entry;
        for (int i = 0; i < params.length; i++) {
            String[] entry = paramArray[i].split("=");
            paramMap.put(entry[0], params[i].toString());
        }
        return paramMap;
    }

    public <T> List<T> executeSolrGetRequestWithJsonHeader(String url,
                                                           Class<T> clazz,
                                                           Object... params) throws HttpResponseStatusException {
        return executeSolrGetRequestWithJsonHeader(url, false, clazz, params);
    }

    public <T> List<T> executeSolrGetRequestWithJsonHeader(String url,
                                                           boolean encode,
                                                           Class<T> clazz,
                                                           Object... params) throws HttpResponseStatusException {
        return executeSolrRequestWithJsonHeader(url, encode, null, clazz, HttpMethod.GET, HttpStatus.OK, params);
    }

    public <T> List<T> executeSolrPostRequestWithJsonHeader(String url,
                                                            Object body,
                                                            Class<T> clazz,
                                                            Object... params) throws HttpResponseStatusException {
        return executeSolrRequestWithJsonHeader(url, body, clazz, HttpMethod.POST, HttpStatus.OK, params);
    }

    public <T> List<T> executeSolrRequestWithJsonHeader(String url,
                                                        Object body,
                                                        Class<T> clazz,
                                                        HttpMethod httpMethod,
                                                        HttpStatus expectedResponseStatus,
                                                        Object... params) throws HttpResponseStatusException {
        return executeSolrRequestWithJsonHeader(url, false, body, clazz, httpMethod, expectedResponseStatus, params);
    }

    public <T> List<T> executeSolrRequestWithJsonHeader(String url,
                                                        boolean encode,
                                                        Object body,
                                                        Class<T> clazz,
                                                        HttpMethod httpMethod,
                                                        HttpStatus expectedResponseStatus,
                                                        Object... params) throws HttpResponseStatusException {
        if (clazz == JsonObject.class) {
            JsonObject response = executeRequestWithJsonHeader(url, encode, body, JsonObject.class, null, httpMethod, expectedResponseStatus, params);
            JsonArray docs = response.getAsJsonObject("response").getAsJsonArray("docs");
            List<JsonObject> docList = StreamSupport.stream(docs.spliterator(), false)
                    .map(JsonElement::getAsJsonObject)
                    .collect(Collectors.toList());
            return (List<T>) docList;
        } else {
            //todo to test
            SolrResponseWrapper<T> solrResponse = (SolrResponseWrapper<T>) executeRequestWithJsonHeader(url, encode, body, null, TypeToken.getParameterized(SolrResponseWrapper.class, clazz), httpMethod, expectedResponseStatus, params);
            return solrResponse.readDocs();
        }
    }

    public <T> T executeGetRequestWithJsonHeader(String url,
                                                 Class<T> clazz,
                                                 Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, null, clazz, null, HttpMethod.GET, HttpStatus.OK, params);
    }

    public <T> T executeGetRequestWithJsonHeader(String url,
                                                 TypeToken<T> type,
                                                 Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, null, null, type, HttpMethod.GET, HttpStatus.OK, params);
    }

    public <T> T executePostRequestWithJsonHeader(String url,
                                                  Object body,
                                                  Class<T> clazz,
                                                  Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, body, clazz, null, HttpMethod.POST, HttpStatus.OK, params);
    }

    public <T> T executePostRequestWithFormDataHeader(String url,
                                                      Object body,
                                                      Class<T> clazz,
                                                      Object... params) throws HttpResponseStatusException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        return executeRequestWithCustomHeader(url, body, clazz, null, HttpMethod.POST, headers, HttpStatus.OK, params);
    }

    public <T> T executeRequestWithJsonHeader(String url,
                                              Object body,
                                              Class<T> clazz,
                                              HttpMethod httpMethod,
                                              HttpStatus expectedResponseStatus,
                                              Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, body, clazz, null, httpMethod, expectedResponseStatus, params);
    }

    public <T> T executePostRequestWithJsonHeader(String url,
                                                  Object body,
                                                  TypeToken<T> type,
                                                  Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, body, null, type, HttpMethod.POST, HttpStatus.OK, params);
    }

    public <T> T executeRequestWithJsonHeader(String url,
                                              Object body,
                                              Class<T> clazz,
                                              TypeToken<T> type,
                                              HttpMethod httpMethod,
                                              HttpStatus expectedResponseStatus,
                                              Object... params) throws HttpResponseStatusException {
        return executeRequestWithJsonHeader(url, false, body, clazz, type, httpMethod, expectedResponseStatus, params);
    }

    public <T> T executeRequestWithJsonHeader(String url,
                                              boolean encode,
                                              Object body,
                                              Class<T> clazz,
                                              TypeToken<T> type,
                                              HttpMethod httpMethod,
                                              HttpStatus expectedResponseStatus,
                                              Object... params) throws HttpResponseStatusException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return executeRequestWithCustomHeader(url, encode, body, clazz, type, httpMethod, headers, expectedResponseStatus, params);
    }

    public <T> T executeRequestWithCustomHeader(String url,
                                                Object body,
                                                Class<T> clazz,
                                                TypeToken<T> type,
                                                HttpMethod httpMethod,
                                                HttpHeaders headers,
                                                HttpStatus expectedResponseStatus,
                                                Object... params) throws HttpResponseStatusException {
        return executeRequestWithCustomHeader(url, false, body, clazz, type, httpMethod, headers, expectedResponseStatus, params);
    }

    public <T> T executeRequestWithCustomHeader(String url,
                                                boolean encode,
                                                Object body,
                                                Class<T> clazz,
                                                TypeToken<T> type,
                                                HttpMethod httpMethod,
                                                HttpHeaders headers,
                                                HttpStatus expectedResponseStatus,
                                                Object... params) throws HttpResponseStatusException {
        if (clazz != null && type != null) {
            throw new IllegalArgumentException("Please either specify clazz or type, not both");
        }

        ResponseEntity responseEntity = executeRequestWithCustomHeaderAsResponseEntity(url, encode, body, clazz, type, httpMethod, headers, expectedResponseStatus, params);
        if (responseEntity == null) return null;

        if (responseEntity.getStatusCode() != expectedResponseStatus)
            throw new HttpResponseStatusException("Unexpected response status [expected=" + expectedResponseStatus.value()
                    + "], [actual=" + responseEntity.getStatusCode().value() + "]", responseEntity.getStatusCode());

        T response;
        if (clazz == JsonObject.class || clazz == JsonArray.class) {
            JsonElement jsElement = Config.parser.parse((String) responseEntity.getBody());
            response = (T) (clazz == JsonObject.class ? jsElement.getAsJsonObject() : jsElement.getAsJsonArray());
        } else {
            response = (T) responseEntity.getBody();
        }
        return response;
    }

    private <T> ResponseEntity executeRequestWithCustomHeaderAsResponseEntity(String url,
                                                                              boolean encode,
                                                                              Object body,
                                                                              Class<T> clazz,
                                                                              TypeToken<T> type,
                                                                              HttpMethod httpMethod,
                                                                              HttpHeaders headers,
                                                                              HttpStatus expectedResponseStatus,
                                                                              Object... params) throws HttpResponseStatusException {

        if (encode)
            return encodeNExecuteRequestWithCustomHeaderAsResponseEntity(url, body, clazz, type, httpMethod, headers, expectedResponseStatus, params);
        else {
            HttpEntity entity = body == null ? new HttpEntity(headers) : new HttpEntity<>(body, headers);
            ResponseEntity responseEntity;
            try {
                if (clazz == JsonObject.class || clazz == JsonArray.class) {
                    responseEntity = restTemplate.exchange(url, httpMethod, entity, String.class, params);
                } else if (type != null) {
                    RequestCallback requestCallback = restTemplate.httpEntityCallback(entity, type.getType());
                    ResponseExtractor<ResponseEntity<T>> responseExtractor = restTemplate.responseEntityExtractor(type.getType());
                    responseEntity = restTemplate.execute(url, httpMethod, requestCallback, responseExtractor, params);
                } else {
                    responseEntity = restTemplate.exchange(url, httpMethod, entity, clazz, params);
                }
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() != expectedResponseStatus) {
                    throw new HttpResponseStatusException("Unexpected response status [expected=" + expectedResponseStatus.value()
                            + "], [actual=" + e.getStatusCode().value() + "]", e.getStatusCode(), e);
                }
                return null;
            }
            return responseEntity;
        }
    }

    private <T> ResponseEntity encodeNExecuteRequestWithCustomHeaderAsResponseEntity(String url,
                                                                                     Object body,
                                                                                     Class<T> clazz,
                                                                                     TypeToken<T> type,
                                                                                     HttpMethod httpMethod,
                                                                                     HttpHeaders headers,
                                                                                     HttpStatus expectedResponseStatus,
                                                                                     Object... params) throws HttpResponseStatusException {


        HttpEntity entity = body == null ? new HttpEntity(headers) : new HttpEntity<>(body, headers);
        ResponseEntity responseEntity;

        Map<String, String> paramsMap = extractQueryParamsFromUrl(url, params);
        String urlBase = Arrays.asList(url.split(Pattern.quote("?"))).get(0);
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(urlBase);
        try {

            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                builder.queryParam(entry.getKey(), URLEncoder.encode(entry.getValue(), StandardCharsets.UTF_8.toString()));
            }

            URI uri = builder.build(true).toUri();

            if (clazz == JsonObject.class || clazz == JsonArray.class) {
                responseEntity = encodeRestTemplate.exchange(uri, HttpMethod.GET, entity, String.class);
            } else if (type != null) {
                RequestCallback requestCallback = encodeRestTemplate.httpEntityCallback(entity, type.getType());
                ResponseExtractor<ResponseEntity<T>> responseExtractor = encodeRestTemplate.responseEntityExtractor(type.getType());
                responseEntity = encodeRestTemplate.execute(uri, httpMethod, requestCallback, responseExtractor);
            } else {
                responseEntity = encodeRestTemplate.exchange(uri, httpMethod, entity, clazz);
            }

        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e.getMessage(), e);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() != expectedResponseStatus) {
                throw new HttpResponseStatusException("Unexpected response status [expected=" + expectedResponseStatus.value()
                        + "], [actual=" + e.getStatusCode().value() + "]", e.getStatusCode(), e);
            }
            return null;
        }
        return responseEntity;

    }

    public static class SolrResponseWrapper<T> {
        public SolrResponse<T> response;

        public SolrResponse<T> getResponse() {
            return response;
        }

        public void setResponse(SolrResponse<T> response) {
            this.response = response;
        }

        private List<T> readDocs() {
            if (response == null) return null;
            return response.docs;
        }
    }

    public static class SolrResponse<T> {
        int numFound;
        int start;
        int rows;
        List<T> docs;

        public int getNumFound() {
            return numFound;
        }

        public void setNumFound(int numFound) {
            this.numFound = numFound;
        }

        public int getStart() {
            return start;
        }

        public void setStart(int start) {
            this.start = start;
        }

        public int getRows() {
            return rows;
        }

        public void setRows(int rows) {
            this.rows = rows;
        }

        public List<T> getDocs() {
            return docs;
        }

        public void setDocs(List<T> docs) {
            this.docs = docs;
        }
    }


}
