package com.catalina.vault_publisher.utils;


import org.springframework.http.HttpStatus;

public class HttpResponseStatusException extends Exception {
    public final HttpStatus httpStatus;

    public HttpResponseStatusException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpResponseStatusException(String message, HttpStatus httpStatus, Throwable cause) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }
}
