package com.catalina.vault_publisher.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ClazzUtils {

    public static <T> List<T> castLinkedHashMapsToClazz(List<LinkedHashMap> listHM, Class<T> clazz) {

        List<T> resList = listHM.stream()
                .filter(x -> clazz.isAssignableFrom(x.getClass()))
                .map(clazz::cast)
                .collect(Collectors.toList());
        return resList;
    }

    public static <T> List<LinkedHashMap> castClazzToLinkedHashMaps(List<T> listClazz) {

        List<LinkedHashMap> resList = listClazz.stream()
                .map(x -> (LinkedHashMap) x)
                /*.map((linkedHashMap) ->
                        mapper.convertValue(linkedHashMap, clazz))*/
                .collect(Collectors.toList());
        return resList;
    }

    public static void main(String[] args) {

    }
}
