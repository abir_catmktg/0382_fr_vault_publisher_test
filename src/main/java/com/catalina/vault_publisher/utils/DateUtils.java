package com.catalina.vault_publisher.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static String getCurrentDateSecondPrecision() {
        return currentDateWithFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static String getCurrentDateMilliSecondPrecision() {
        return currentDateWithFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    }

    public static String getCurrentDate() {
        return currentDateWithFormat("yyyy-MM-dd");
    }

    public static String currentDateWithFormat(String dateFormat) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        String modifiedDate = sdf.format(date);
        return modifiedDate;
    }
}
