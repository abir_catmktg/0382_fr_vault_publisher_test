package com.catalina.vault_publisher.utils;

import java.util.List;
import java.util.StringJoiner;

public class StringUtils {

    public StringUtils() {
    }

    public static String concatStringCollection(List<String> stringList, String separator) {
        StringJoiner joiner = new StringJoiner(separator);
        for (String currentStr : stringList) {
            joiner.add(currentStr);
        }
        return joiner.toString();
    }
}
