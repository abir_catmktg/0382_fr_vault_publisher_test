package com.catalina.vault_publisher.utils;

import com.catalina.vault_publisher.data.User;

public interface Constants {


    User SURICAT_USER = new User("Bulk Upload", "suricat@catalina.com", "suricat@catalina.com");
    String VAULT_SYSTEM_ID = "VAULT";

    String TARGETING_METHOD_ECOBON = "Vault ECOBON";
    String TARGETING_METHOD_ECOLINK = "Vault ECOLINK";

    String MANUF_BUSINESS_SEGMENT = "MANUFACTURING";
    String MANUF_TYPE = "Manufacturer";

    String RETAIL_BUSINESS_SEGMENT = "RETAIL";
    String RETAIL_TYPE = "Retailer";

    Double MEASUREMENT_PERCENT_ECOLINK = 0.10;

    String EXTERNAL_SYSTEM = "BLIP";


    String BULK_MANUF = "Bulk Manuf";
    String COLIBRI = "Colibri";
    String VAULT_PUBLISHER = "Vault Publisher FR";

    String TAG_FILE_UPLOAD = "SFTP Batch Request";

    String STATUS_PAUSED = "PAUSED";
    String STATUS_DRAFT = "DRAFT";
    String STATUS_PROCESSING = "PROCESSING";
    String STATUS_PUBLISHED = "PUBLISHED";

    String OFFER_CODE_TYPE_GENERATED = "GENERATED";

    String LIST_VAULT_HEADER = "list";
}
