package com.catalina.vault_publisher.highco.data;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;

import java.util.LinkedHashMap;
import java.util.List;

public class CampaignService {

    private CRestTemplate cRestTemplate;

    public CampaignService(CRestTemplate cRestTemplate) {
        this.cRestTemplate = cRestTemplate;
    }

    public String search(Config.Mode _mode, String _name) throws Exception {

        String url = _mode.urlDashboard + "/campaigns?";

        String startParam = "0";
        String rowsParam = "1";
        String inParam = "{\"country_code\":\"FRA,\"}";
        String opParam = "or";
        String flParam = "id|name";
        String fqParam = "{\"name\":\"" + _name + "\"}";

        try {
            List<LinkedHashMap> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, LinkedHashMap.class, startParam, rowsParam, inParam, opParam, flParam, fqParam);
            if (docs.size() == 1) {
                String campaignUUID = (String) docs.get(0).get("id");
                return campaignUUID;
            }
        } catch (HttpResponseStatusException e) {
            System.err.println(e.httpStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
