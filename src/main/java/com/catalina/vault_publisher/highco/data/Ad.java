package com.catalina.vault_publisher.highco.data;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

public class Ad {

    private static HashMap<String, Client> CLIENT_MAP = null;
    private static HashMap<String, Billing> BILLING_MAP = null;
    String m_jsonStr = null;
    String m_name = null;
    String m_external_id = null;
    Mode m_mode = null;
    String m_frequencyCap = null;
    String m_distributionCap = null;
    String m_barCode = null;
    String m_date_start = null;// yyyy-MM-dd "2018-07-07"
    String m_date_stop = null; // yyyy-MM-dd "2018-07-13"
    //	String m_campaignName, m_adGroupName;
    String m_campaignId, m_adGroupId;
    String m_account_id = null;
    String m_billing_nbr = null;    //From input file
    String m_billing_id = null;
    String m_billing_name = null;
    String m_client_id = null;
    String m_client_ext_id = null;
    String m_client_name = null;
    List<String> m_list = new ArrayList<String>();
    private CRestTemplate cRestTemplate;

    public Ad(String _name, String _external_id, String _billingNbr, Mode _mode, CRestTemplate cRestTemplate) {
        m_name = _name;
        m_external_id = _external_id;
        m_billing_nbr = _billingNbr;
        m_mode = _mode;
        this.cRestTemplate = cRestTemplate;
    }

    public Ad(String _jsonStr, Mode _mode, CRestTemplate cRestTemplate) {
        m_jsonStr = _jsonStr;
        m_mode = _mode;
        this.cRestTemplate = cRestTemplate;
    }

    public static void init() {
        CLIENT_MAP = new HashMap<String, Client>();
        BILLING_MAP = new HashMap<String, Billing>();
    }

    public void set(String _frequencyCap, String _distributionCap, String _barCode
            , String _dateStart, String _dateStop) {
        m_frequencyCap = _frequencyCap;
        m_distributionCap = _distributionCap;
        m_barCode = _barCode;
        m_date_start = _dateStart;
        m_date_stop = _dateStop;
    }

    public void getClient(String _client_id) {
/*		if(CLIENT_MAP.containsKey(_client_id))
		{
			Client client = CLIENT_MAP.get(_client_id);
			m_client_name = client.name;
			m_client_ext_id = client.external_id;
			System.out.println("GET Client Name : ("+_client_id+")"+ m_client_name);
		}
		else
		{
*/
        String url = m_mode.urlDashboard + "/clients?fq={fq}";
        String fqParam = "{\"id\":\"" + _client_id + "\"}";
        //TODO REFACTOR TO USE CLIENT OBJECT
        try {
            List<LinkedHashMap> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, LinkedHashMap.class, fqParam);
            if (docs.size() == 1) {
                m_client_name = (String) docs.get(0).get("name");
                m_client_ext_id = (String) docs.get(0).get("external_id");
            } else {
                System.err.println("More or less than one client found for : " + _client_id);
            }
        } catch (HttpResponseStatusException e) {
            System.err.println(e.httpStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getBilling(String _billingNbr, String _productCd) {
        System.out.println("START GET BILLING IN AD");
/*		if (BILLING_MAP.containsKey(_billingNbr)) {
			Billing billing = BILLING_MAP.get(_billingNbr);
			m_client_id = billing.client_id;
			m_billing_id = billing.id;
			m_billing_name = billing.name;
			m_account_id = billing.account_id;
			getClient(m_client_id);
		} else {
*/
        //TODO REFACTOR TO USE BILLING OBJECT
        String url = m_mode.urlDashboard + "/billing?fq={fq}&func={funq}";
        String fqParam = "{\"id\":\"FRA-CATS-" + _billingNbr + "_" + _productCd + "\"}";
        String funcParam = "return d(\"id\").match(\"FRA\")";

        try {
            List<LinkedHashMap> docs = cRestTemplate.executeSolrGetRequestWithJsonHeader(url, LinkedHashMap.class, fqParam, funcParam);
            if (docs.size() == 1) {
                m_client_id = (String) docs.get(0).get("client_id");
                m_billing_id = (String) docs.get(0).get("id");
                m_billing_name = (String) docs.get(0).get("name");
                m_account_id = (String) docs.get(0).get("account_id");
            } else {
                System.err.println("More or less than one billing found");
            }

        } catch (HttpResponseStatusException e) {
            System.err.println(e.httpStatus);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("END GET BILLING IN AD");
        }
    }

    //updateJson
    public void update(String _campaignId, String _campaignName, String _adGroupId, String _adGroupName) {
//		m_campaignName = _campaignName;
//		m_adGroupName = _adGroupName;

        String _jsonAdFilePath = null;
        if (m_mode == Mode.SQA) {
            _jsonAdFilePath = "/json/SQA-Highco-Ad.json";
        } else if (m_mode == Mode.PROD) {
            _jsonAdFilePath = "/json/PROD-Highco-Ad.json";
        }
        String tmpStr = "";
        try {
            InputStream is = getClass().getResourceAsStream(_jsonAdFilePath);
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, Charset.forName("UTF-8"));
            tmpStr = writer.toString();

            // Remove frequency caps from JSON when value is 0
            if ("0".equalsIgnoreCase(m_frequencyCap)) {
                JSONObject json = new JSONObject(tmpStr);
                int arrayLength = json.getJSONObject("experience").getJSONArray("frequency_caps").length();
                for (int i = arrayLength - 1; i >= 0; i--) {
                    json.getJSONObject("experience").getJSONArray("frequency_caps").remove(i);
                }
                tmpStr = json.toString();
            }

            // Remove distribution caps from JSON when value is 0
            if ("0".equalsIgnoreCase(m_distributionCap)) {
                JSONObject json = new JSONObject(tmpStr);
                int arrayLength = json.getJSONObject("budget").getJSONArray("caps").length();
                for (int i = arrayLength - 1; i >= 0; i--) {
                    json.getJSONObject("budget").getJSONArray("caps").remove(i);
                }
                tmpStr = json.toString();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String modifiedDate = sdf.format(date);

        System.out.println("--------------- AD BEGIN -----------------");
        tmpStr = tmpStr.replaceFirst("VAR_CREATIVE_SET_ID", UUID.randomUUID().toString())
                .replaceAll("VAR_NAME", m_name)
                .replaceAll("VAR_DATE_MODIFIED", modifiedDate)
                .replaceAll("VAR_CLIENT_ID", m_client_id)
                .replaceAll("VAR_CLIENT_EXT_ID", m_client_ext_id)
                .replaceAll("VAR_CLIENT_NAME", m_client_name)
                .replaceAll("VAR_ACCOUNT_ID", m_account_id)
                .replaceAll("\"VAR_BILLING_NBR\"", m_billing_nbr)
                .replaceAll("VAR_BILLING_ID", m_billing_id)
                .replaceAll("VAR_BILLING_NAME", m_billing_name)
                .replaceAll("VAR_DISTRIBUTION_CAP", m_distributionCap)
                .replaceAll("VAR_FREQUENCY_CAP", m_frequencyCap)
                .replaceAll("VAR_BARCODE", m_barCode)
                .replaceAll("VAR_RETAILER_AD_ID", m_external_id);

        tmpStr = tmpStr.replaceAll("VAR_DATE_START", m_date_start).replaceAll("VAR_DATE_STOP", m_date_stop);
        m_jsonStr = tmpStr;
        // System.out.println(m_jsonStr);

        m_campaignId = _campaignId;
        m_adGroupId = _adGroupId;

        if (m_jsonStr != null) {
            if (_campaignId != null) {
                m_jsonStr = m_jsonStr.replaceAll("VAR_CAMPAIGN_ID", _campaignId).replaceAll("VAR_CAMPAIGN_NAME", _campaignName);
            }
            if (_adGroupId != null) {
                m_jsonStr = m_jsonStr.replaceAll("VAR_ADGROUP_ID", _adGroupId).replaceAll("VAR_ADGROUP_NAME", _adGroupName);
            }
        }
        System.out.println("--------------- AD END -----------------");
    }

    public Result post(boolean onlyUpdate) {
        String url = m_mode.urlDashboard + "/ads";

        // getBL
        String bl_id = null;
        if (!onlyUpdate) {
            try {
                bl_id = getBL();
                String bl_external_id = "FRA-BLIP-" + bl_id;
                m_jsonStr = m_jsonStr.replaceAll("VAR_BL_ID", bl_id).replaceAll("VAR_BL_EXTERNAL_ID", bl_external_id);
            } catch (Exception e) {
                e.printStackTrace();
                return new Result(7, "error during A::post - check existing BH_xxxx_xxxx unavailable");
            }
        }
        // POST to WS
        try {
            LinkedHashMap response = cRestTemplate.executePostRequestWithJsonHeader(url, m_jsonStr, LinkedHashMap.class);
            if (response.get("error").equals(0) && response.get("inserted").equals(1)) {
                List keys = (List) response.get("generated_keys");
                String adUUID = (String) keys.get(0);
                System.out.println("Ad inserted :" + adUUID);
                return new Result(bl_id);
            }
            return new Result(5, "adPost - error : " + response.toString());
        } catch (HttpResponseStatusException e) {
            System.err.println(e.httpStatus);
            return new Result(5, "adPost - error : " + e.httpStatus);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(5, "adPost - error : " + e.getMessage());
        }

    }

    protected String generateBL() {
        String prefix = "BH";//prefix for HighCo
        String postfix = String.format("%04d", (int) (Math.floor(Math.random() * 9999) + 1));
        String rearfix = String.format("%04d", (int) (Math.floor(Math.random() * 9999) + 1));
        String external_id = prefix + '_' + rearfix + '_' + postfix;
        return external_id;
    }

    protected String getBL() throws Exception {
        String external_id = generateBL();

        boolean bBlExists = blExists(external_id);
        while (bBlExists) {
            external_id = generateBL();
            bBlExists = blExists(external_id);
        }
        return external_id;
    }

    private boolean blExists(String _bl) throws Exception {
        String external_id = "FRA-BLIP-" + _bl;
        String urlApi = m_mode.dmpUrl + "/promotions/" + external_id;
        try {
            cRestTemplate.executeRequestWithJsonHeader(urlApi, null, String.class, HttpMethod.GET, HttpStatus.NOT_FOUND);
            return false;
        } catch (HttpResponseStatusException e) {
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void addImageInfo(String filename, int size, String id, String url) {
        JSONObject object = new JSONObject(m_jsonStr);
        JSONArray creativesArray = object.getJSONArray("creatives");
        System.out.println("addImageInfo : " + creativesArray.length());

        //remove m_mode
        String _jsonCreativeFilePath = "/json/Highco-creativevar-staticimage.json";

        String tmpStr = "";
        try {
            InputStream is = getClass().getResourceAsStream(_jsonCreativeFilePath);
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, Charset.forName("UTF-8"));
            tmpStr = writer.toString();
//			System.out.println(tmpStr);

//			File imgFile = new File(getClass().getResource(_jsonCreativeFilePath).toURI());
//			System.out.println(imgFile.getAbsolutePath()+" : "+imgFile.exists());
//			tmpStr = new String(Files.readAllBytes(imgFile.toPath()),"UTF-8");
//		} catch (URISyntaxException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        tmpStr = tmpStr.replaceAll("VAR_CREATIVE_NAME", filename)
                .replaceAll("VAR_IMAGE_ID", id)
                .replaceAll("VAR_IMAGE_URL", url)
                .replace("VAR_UUID", UUID.randomUUID().toString())//not all
                .replaceAll("\"size\": -1,", "\"size\": " + size + ",");

        JSONObject creativeObj = new JSONObject(tmpStr);
        creativesArray.put(creativeObj);
        m_jsonStr = object.toString();
    }

    public Result cancel() {
//		  "history":{"published_date": "2019-01-25T10:58:19.076Z"}
//		 "status": "PAUSED",
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        String publishedDate = sdf.format(date);

        JSONObject adObj = new JSONObject(m_jsonStr);
        JSONObject historyObj = adObj.getJSONObject("history");
//		historyObj.remove("published_date");
        historyObj.put("published_date", publishedDate);

//		adObj.remove("status");
        adObj.put("status", "PAUSED");

        m_jsonStr = adObj.toString();

        return post(true);
    }

    class Client {
        String name;
        String external_id;

        public Client(String _name, String _ext_id) {
            name = _name;
            external_id = _ext_id;
        }
    }

    class Billing {
        String client_id;
        String id;
        String name;
        String account_id;

        public Billing(String _id, String _name, String _client_id, String _account_id) {
            id = _id;
            name = _name;
            client_id = _client_id;
            account_id = _account_id;
        }
    }
}

