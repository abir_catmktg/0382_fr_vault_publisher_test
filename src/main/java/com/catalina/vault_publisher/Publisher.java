package com.catalina.vault_publisher;

import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.data.offers.AbstractOffer;
import com.catalina.vault_publisher.data.offers.AdService;
import com.catalina.vault_publisher.data.offers.OfferService;
import com.catalina.vault_publisher.highco.data.Ad;
import com.catalina.vault_publisher.highco.data.Campaign;
import com.catalina.vault_publisher.highco.data.CampaignService;
import com.catalina.vault_publisher.image_upload.CustomImageService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.json.JSONObject;

import java.io.File;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Publisher {

    private static boolean DO_NOT_SAVE = false;
    Mode m_mode = Mode.SQA;
    Country.CountryEnum m_country = Country.CountryEnum.FRANCE;
    private Campaign m_campaign = null;
    private Ad m_tmpAd = null;

    private CRestTemplate cRestTemplate;

    private AdService adService;

    private CampaignService campaignService;
    private OfferService offerService;

    public Publisher(CRestTemplate cRestTemplate) {
        this(Mode.SQA, Country.CountryEnum.FRANCE, false, cRestTemplate);
    }

    public Publisher(Mode mode, CRestTemplate cRestTemplate) {
        this(mode, Country.CountryEnum.FRANCE, false, cRestTemplate);
    }

    public Publisher(Mode mode, Country.CountryEnum country, boolean do_not_save, CRestTemplate cRestTemplate) {
        m_mode = mode;
        m_country = country;
        DO_NOT_SAVE = do_not_save;
        this.cRestTemplate = cRestTemplate;
        this.adService = new AdService(cRestTemplate);
        this.campaignService = new CampaignService(cRestTemplate);
        this.offerService = new OfferService(cRestTemplate);
    }

    public static void main(String[] args) throws Exception {
        String filePath = "C:/Users/fperessi/Desktop/shared_to_vm/0375_CRF_Proxi_Trade_Offers_HighCo/8120150002807720_image_20190408_160207.png";

        Publisher myPublisher = new Publisher(Mode.PROD, new CRestTemplate());//true -> do_not_save! (Vault Dashboard API is not used)
//		System.out.println(myPublisher.getResource());
        //helper to create Date
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String zeDate = sdf.format(date);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String zeDateForAG = sdf2.format(date);

        //Set Campaign example
        //Result res = myPublisher.setCampaign("[CRF Proxi - HighCo] Campaign Sample FPE2 "+zeDate,"","");//[CRF Proxi - HighCo] 2019 Q1

        Result res = myPublisher.setCampaign("[TEST] fpe hIGHCO - BILLING 09-05", "58625", "1");//[CRF Proxi - HighCo] 2019 Q1
        System.out.println("Campaign : " + res.toString());
        if (res.getStatus() == 0) {
            res = myPublisher.createAdgroup("[CRF Proxi - HighCo] AdGroup Sample FPE4 ");
            System.out.println("Adgroup : " + res.toString());
            if (res.getStatus() == 0) {
                //TODO : where CRF ID has to be placed?
//				JSONObject jsonImage = myPublisher.uploadImage(filePath);
                res = myPublisher.createAd("58625", "1", "[TEST] Ad Sample 8120150002807721 " + zeDateForAG, "8120150002807721", "2019-04-19", "2019-04-19", "2072015999919", 1, 46000, null);//-1 here frequencyPerUser and distributionPerAd
                System.out.println("BL : " + res.toString());
                //res = myPublisher.createAd("[CRF Proxi - HighCo] Ad Sample 98765432109 "+zeDateForAG, "98765432109", "2019-02-24", "2019-02-28", "987654321012", 0, 0,jsonImage);//-1 here frequencyPerUser and distributionPerAd
                //System.out.println("BL : "+res.toString());
            }
        }
        //will return "Result(-1,"not implemented")

        //Test for search campaign
//		String campaignId = Campaign.search(Mode.SQA, "[CRF Proxi - HighCo] Campaign Sample 2019-01-13");
//		System.out.println("Campaign Id : "+campaignId);

/*
		String postUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/upload";
		RestfulClient restfulClient = new RestfulClient();

		File file = new File(filePath);
		System.err.println("File exists? "+file.exists());
	    JSONObject jsonImage = restfulClient.postEntity(postUrl, "TestJerome.jpg", filePath);
		System.out.println(jsonImage);
*/

        //Cancel
//		Result resCancel = myPublisher.cancelAd("[CRF Proxi - HighCo] Ad Sample 2019-01-16 11:11:37");
//		System.out.println(resCancel.toString());
    }

    public Result setCampaign(String name, String billingNbr, String productCd) {
        //getCampaign from Vault
        try {
            String campaignId = campaignService.search(m_mode, name);
            if (campaignId != null) {
                m_campaign = new Campaign(false, name, campaignId, billingNbr, m_mode, cRestTemplate);
            } else //or create a new one
            {
                m_campaign = new Campaign(true, name, null, billingNbr, m_mode, cRestTemplate);
                m_campaign.getBilling(billingNbr, productCd);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(8, "Issue during searching for Campaign");
        }
        return new Result();
    }

    public String getResource() {
        File file;
        try {
            file = new File(getClass().getResource("/").toURI());
            return file.getAbsolutePath();
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

    }

    public Result createAdgroup(String name) {
        if (m_campaign == null) {
            return new Result(2, "please call setCampaign before");
        } else {
            m_campaign.createAdgroup(name);
            if (m_campaign.isNew()) {
                m_campaign.updateJson();
                if (DO_NOT_SAVE) {
                    return new Result(999, "Not saved on Dashboard");
                } else {
                    boolean bRes = m_campaign.post();
                    if (bRes)
                        return new Result();
                    else
                        return new Result(3, "error during Campaign or AdGroup post");
                }
            } else {
                if (DO_NOT_SAVE) {
                    return new Result(999, "Not saved on Dashboard");
                } else {
                    boolean bRes = m_campaign.postAgroup();
                    if (bRes)
                        return new Result();
                    else
                        return new Result(4, "error during AdGroup post");
                }
            }
        }
    }

    public JSONObject uploadImage(String filePath) {
        //
        //JEB 190114 : https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/upload/custom_creatives
        String postUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/upload/custom_creatives";

        File file = new File(filePath);

        String imgName = file.getName();
        if (DO_NOT_SAVE) {
            return null;
        } else {
            CustomImageService customImageService = new CustomImageService(new CRestTemplate());
            JSONObject jsonImage = customImageService.postEntity(postUrl, imgName, filePath);
            System.out.println(jsonImage);
            return jsonImage;
        }
    }

    public Result createAd(String billingNbr, String productCd, String name, String external_id, String start_date, String stop_date, String ean13BarcodeOn12Digits, int frequencyPerUser,
                           int distributionPerAd, JSONObject jsonImage) {
        if (m_campaign == null || m_campaign.getAdgroup() == null) {
            return new Result(2, "please call setCampaign / createAdgroup before");
        } else {
            m_tmpAd = new Ad(name, external_id, billingNbr, m_mode, cRestTemplate);
            m_tmpAd.set("" + frequencyPerUser, "" + distributionPerAd, ean13BarcodeOn12Digits, start_date, stop_date);
            System.out.println("*****AD***** -> " + billingNbr + " - " + productCd);
            m_tmpAd.getBilling(billingNbr, productCd);
            m_tmpAd.update(m_campaign.getId(), m_campaign.getName(), m_campaign.getAdgroupId(), m_campaign.getAdgroupName());
            if (jsonImage != null && !jsonImage.isNull("fileName") && !jsonImage.isNull("size") && !jsonImage.isNull("id") && !jsonImage.isNull("url")) {
                m_tmpAd.addImageInfo(jsonImage.getString("fileName"), jsonImage.getInt("size"), jsonImage.getString("id"), jsonImage.getString("url"));
            }
            if (DO_NOT_SAVE) {
                return new Result(999, "Not saved on Dashboard");
            } else {
                return m_tmpAd.post(false);
            }
        }
    }

    public Result cancelAd(String ad_name) throws HttpResponseStatusException {

        String fqParam = String.format("{\"name\":\"%s\"}", ad_name);
        List<com.catalina.vault_publisher.data.offers.Ad> adList = offerService.getOffersFromDashBoard(m_mode, m_country, com.catalina.vault_publisher.data.offers.Ad.class, fqParam);

        if (adList.size() != 1)
            throw new RuntimeException(String.format("%s ad(s) found with the name: %s", adList.size(), ad_name));

        com.catalina.vault_publisher.data.offers.Ad ad = adList.get(0);
        ad.setStatus(Constants.STATUS_PAUSED);

        offerService.postOffer(m_mode, AbstractOffer.OfferEnum.AD, ad);
        return new Result(ad.getExternal_id());
    }
}
