package com.catalina.vault_publisher;

import com.catalina.vault_publisher.data.*;
import com.catalina.vault_publisher.data.Config.AdStatus;
import com.catalina.vault_publisher.data.Config.Mode;
import com.catalina.vault_publisher.data.Country.CountryEnum;
import com.catalina.vault_publisher.data.barcode.Barcode;
import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeFormatEnum;
import com.catalina.vault_publisher.data.barcode.Barcode.BarcodeType;
import com.catalina.vault_publisher.data.barcode.BarcodeService;
import com.catalina.vault_publisher.data.bean.AdParams;
import com.catalina.vault_publisher.data.billing.*;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethod.TargetingMethodEnum;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.capping.Capping;
import com.catalina.vault_publisher.data.capping.Capping.EventType;
import com.catalina.vault_publisher.data.capping.CappingService;
import com.catalina.vault_publisher.data.capping.DistributionCap;
import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.capping.FrequencyCap.LocationScope;
import com.catalina.vault_publisher.data.classification.Classification;
import com.catalina.vault_publisher.data.classification.Classification.ClassificationName;
import com.catalina.vault_publisher.data.classification.ClassificationService;
import com.catalina.vault_publisher.data.customer_files.audiences.Audience;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceFile;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceFileService;
import com.catalina.vault_publisher.data.customer_files.audiences.AudienceService;
import com.catalina.vault_publisher.data.experience.Experience;
import com.catalina.vault_publisher.data.experience.Experience.ExperienceEnum;
import com.catalina.vault_publisher.data.experience.ExperienceService;
import com.catalina.vault_publisher.data.experience.Funding;
import com.catalina.vault_publisher.data.experience.reward.Reward;
import com.catalina.vault_publisher.data.experience.reward.RewardService;
import com.catalina.vault_publisher.data.experience.reward.ValidityDate;
import com.catalina.vault_publisher.data.experience.reward.ValidityDateService;
import com.catalina.vault_publisher.data.graphics.TemplatePackage;
import com.catalina.vault_publisher.data.graphics.TemplatePackageService;
import com.catalina.vault_publisher.data.graphics.creativeVars.variables.meta.MetaVariableService;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM.AssetType;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAM.PrinterLevel;
import com.catalina.vault_publisher.data.graphics.creatives.dam.ImageDAMService;
import com.catalina.vault_publisher.data.graphics.templateAPI.TemplateAPIService;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItemService;
import com.catalina.vault_publisher.data.list.vault.MinimalVaultListService;
import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;
import com.catalina.vault_publisher.data.oclus.OfferCodeService;
import com.catalina.vault_publisher.data.oclus.OfferCodeService.OfferCodeAction;
import com.catalina.vault_publisher.data.offers.*;
import com.catalina.vault_publisher.data.offers.AbstractOffer.ChannelType;
import com.catalina.vault_publisher.data.offers.AbstractOffer.OfferEnum;
import com.catalina.vault_publisher.data.offers.AbstractOffer.Scope;
import com.catalina.vault_publisher.data.ratio.Ratio;
import com.catalina.vault_publisher.data.ratio.RatioService;
import com.catalina.vault_publisher.data.reporting_attributes.ReportingAttribute;
import com.catalina.vault_publisher.data.reporting_attributes.ReportingAttributeService;
import com.catalina.vault_publisher.data.targeting.schedule.Schedule;
import com.catalina.vault_publisher.data.targeting.schedule.ScheduleObject;
import com.catalina.vault_publisher.data.targeting.schedule.ScheduleObject.DayEnum;
import com.catalina.vault_publisher.data.targeting.schedule.ScheduleObjectService;
import com.catalina.vault_publisher.data.targeting.schedule.ScheduleService;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.Touchpoints;
import com.catalina.vault_publisher.data.trailer.Trailer;
import com.catalina.vault_publisher.data.triggers.TriggerPackage;
import com.catalina.vault_publisher.data.triggers.TriggerPackageService;
import com.catalina.vault_publisher.data.triggers.TriggerService;
import com.catalina.vault_publisher.data.triggers.Triggers;
import com.catalina.vault_publisher.data.triggers.activationTriggerGroups.ActivationTriggerGroup.TriggerCombination;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTrigger.*;
import com.catalina.vault_publisher.data.triggers.activationTriggers.ActivationTriggerService;
import com.catalina.vault_publisher.image_upload.CustomImageService;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.jansi.AnsiConsole;
import org.springframework.http.HttpStatus;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

public class CPGPublisher {
    private static boolean DO_NOT_SAVE = false;
    public static String createdBy;
    Mode m_mode = Mode.SQA;
    CountryEnum m_country = CountryEnum.FRANCE;
    private Campaign m_campaign = null;
    private Adgroup m_adgroup = null;
    private Ad m_ad = null;
    private Promotion m_promotion = null;
    private static final Logger logger = LogManager.getLogger(CPGPublisher.class);

    private CRestTemplate cRestTemplate;

    private CampaignService campaignService;
    private AdGroupService adGroupService;
    private AdService adService;
    private OfferService offerService;
    private OfferCodeService offerCodeService;
    private BillingService billingService;
    private ClientService clientService;
    private TemplatePackageService templatePackageService;
    private MetaVariableService metaVariableService;
    private TriggerService triggerService;
    private ScheduleObjectService scheduleObjectService;
    private ScheduleService scheduleService;
    private ActivationTriggerService activationTriggerService;
    private TriggerPackageService triggerPackageService;
    private ImageDAMService imageDAMService;
    private CappingService cappingService;
    private BarcodeService barcodeService;
    private ValidityDateService validityDateService;
    private RewardService rewardService;
    private ExperienceService experienceService;
    private PromotionService promotionService;
    private RatioService ratioService;
    private ClassificationService classificationService;
    private ReportingAttributeService reportingAttributeService;
    private AudienceService audiencesService;
    private AudienceFileService audienceFileService;

    public CPGPublisher(CRestTemplate cRestTemplate) {
        this(Mode.SQA, CountryEnum.FRANCE, false, cRestTemplate);
    }

    public CPGPublisher(Mode mode, CRestTemplate cRestTemplate) {
        this(mode, CountryEnum.FRANCE, false, cRestTemplate);
    }

    public CPGPublisher(Mode mode, CountryEnum country, CRestTemplate cRestTemplate) {
        this(mode, country, false, cRestTemplate);
    }

    public CPGPublisher(Mode mode, CountryEnum country, boolean do_not_save, CRestTemplate cRestTemplate) {
        System.setProperty("jansi.passthrough", "true");
        AnsiConsole.systemInstall();
        m_mode = mode;
        m_country = country;
        DO_NOT_SAVE = do_not_save;

        this.cRestTemplate = cRestTemplate;
        this.campaignService = new CampaignService(cRestTemplate);
        this.adGroupService = new AdGroupService(cRestTemplate);
        this.adService = new AdService(cRestTemplate);
        this.offerService = new OfferService((cRestTemplate));
        this.offerCodeService = new OfferCodeService(cRestTemplate);
        this.billingService = new BillingService(cRestTemplate);
        this.clientService = new ClientService(cRestTemplate);
        this.imageDAMService = new ImageDAMService(cRestTemplate);
        this.templatePackageService = new TemplatePackageService(cRestTemplate);
        this.metaVariableService = new MetaVariableService(cRestTemplate);
        this.triggerPackageService = new TriggerPackageService(cRestTemplate);
        this.triggerService = new TriggerService(cRestTemplate);
        this.activationTriggerService = new ActivationTriggerService(cRestTemplate);
        this.scheduleService = new ScheduleService();
        this.scheduleObjectService = new ScheduleObjectService();
        this.cappingService = new CappingService();
        this.barcodeService = new BarcodeService(cRestTemplate);
        this.validityDateService = new ValidityDateService();
        this.rewardService = new RewardService();
        this.experienceService = new ExperienceService();
        this.promotionService = new PromotionService(cRestTemplate);
        this.ratioService = new RatioService();
        this.classificationService = new ClassificationService(cRestTemplate);
        this.reportingAttributeService = new ReportingAttributeService();
        this.audiencesService = new AudienceService(cRestTemplate);
        this.audienceFileService = new AudienceFileService(cRestTemplate);

        this.billingService.initBilling();
        this.clientService.initClient();
        this.imageDAMService.resetCache();
        new LMCItemService(cRestTemplate).resetCache();
        new TemplateAPIService(cRestTemplate).initTemplate();
        this.metaVariableService.resetMetaMap();
        new MinimalVaultListService(cRestTemplate).initMinimalVaultList();
    }

    public BillingCampaign getBillingCampaignManuf(String billingNbr) throws HttpResponseStatusException {
        return billingService.getBillingCampaign(m_mode,
                m_country,
                billingNbr,
                "1");
    }

    public BillingCampaign getBillingCampaignRetail(String billingNbr, String productCd) throws HttpResponseStatusException {
        return billingService.getBillingCampaign(m_mode, m_country, billingNbr, productCd);
    }

    public BillingAd getBillingAdManuf(String billingNbr, String adgrouName) throws HttpResponseStatusException {
        TargetingMethodEnum targetingMethodEnum = TargetingMethodEnum.fromValue(adgrouName);
        return billingService.createBillingAD(m_mode, m_country, billingNbr, "1", targetingMethodEnum);
    }

    public BillingAd getBillingAdRetail(String billingNbr,
                                        String productCd,
                                        TargetingMethodEnum targetingMethodEnum) throws HttpResponseStatusException {
        return billingService.createBillingAD(m_mode, m_country, billingNbr, productCd, targetingMethodEnum);
    }

    public Client getClient(String clientId) throws HttpResponseStatusException {
        return clientService.getClient(m_mode, clientId);
    }

    public Touchpoints createTouchpoints(List<InputVariable> include, List<InputVariable> exclude) throws HttpResponseStatusException {
        return offerService.setTouchpointsForAnOffer(m_mode, m_country, include, exclude);
    }

    public TemplatePackage createTemplatePackages(List<String> templateIdList, List<InputVariable> inputVariableList) throws HttpResponseStatusException {
        return this.templatePackageService.createTemplatePackage(m_mode, templateIdList, inputVariableList);
    }

    public ActivationTrigger createActivationTriggerTotalOrderAllUPCs(PurchaseMeasure purchaseMeasure,
                                                                      Double minQty,
                                                                      Double maxQty,
                                                                      Operator operator) {
        return activationTriggerService.createActivationTriggerTotalOrderAllUPCs(purchaseMeasure,
                minQty,
                maxQty,
                operator);
    }

    public ActivationTrigger createActivationTriggerTotalOrderExcludingVaultList(PurchaseMeasure purchaseMeasure,
                                                                                 Double minQty,
                                                                                 Double maxQty,
                                                                                 Operator operator,
                                                                                 List<String> listName) throws HttpResponseStatusException {

        return activationTriggerService.createActivationTriggerTotalOrderExcludingVaultList(m_mode,
                purchaseMeasure,
                minQty,
                maxQty,
                operator,
                listName);
    }

    public ActivationTrigger createActivationTriggerItemPurchaseManualList(ProductAttribute productAttribute,
                                                                           PurchaseMeasure purchaseMeasure,
                                                                           Double minQty,
                                                                           Double maxQty,
                                                                           Operator operator,
                                                                           List<String> manualList1,
                                                                           List<String> manualList2) {
        return activationTriggerService.createActivationTriggerItemPurchaseManualList(productAttribute,
                purchaseMeasure,
                minQty,
                maxQty,
                operator,
                manualList1,
                manualList2);
    }

    public ActivationTrigger createActivationTriggerItemPurchaseVaultList(ProductAttribute productAttribute,
                                                                          PurchaseMeasure purchaseMeasure,
                                                                          Double minQty,
                                                                          Double maxQty,
                                                                          Operator operator,
                                                                          List<String> listName) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerItemPurchaseVaultList(m_mode,
                productAttribute,
                purchaseMeasure,
                minQty,
                maxQty,
                operator,
                listName);
    }

    public ActivationTrigger createActivationTriggerItemPurchaseLMC(ProductAttribute productAttribute,
                                                                    PurchaseMeasure purchaseMeasure,
                                                                    Double minQty,
                                                                    Double maxQty,
                                                                    Operator operator,
                                                                    List<String> listName) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerItemPurchaseLMC(m_mode,
                productAttribute,
                purchaseMeasure,
                minQty,
                maxQty,
                operator,
                listName);
    }

    public List<ActivationTrigger> createActivationTriggerItemPurchaseLMCGroup(ProductAttribute productAttribute,
                                                                               PurchaseMeasure purchaseMeasure,
                                                                               Double minQty,
                                                                               Double maxQty,
                                                                               Operator operator,
                                                                               String listName) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerItemPurchaseLMCGroup(m_mode,
                productAttribute,
                purchaseMeasure,
                minQty,
                maxQty,
                operator,
                listName);
    }

    public ActivationTrigger createActivationTriggerCatalinaRedemptionForAds(Operation operation, List<String> blList) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerCatalinaRedemptionForAds(m_mode, operation, blList);
    }

    public ActivationTrigger createActivationTriggerRetargetingAd(List<String> blList) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerRetargetingAd(m_mode, blList);
    }

    public ActivationTrigger createActivationTriggerPOSRedemption(CouponType couponType,
                                                                  Operation operation,
                                                                  List<String> genCodeList) {
        return activationTriggerService.createActivationTriggerPOSRedemption(couponType, operation, genCodeList);
    }

    public ActivationTrigger createActivationTriggerLoyaltyCard(boolean cardShouldBePresent) {
        return activationTriggerService.createActivationTriggerLoyaltyCard(cardShouldBePresent);
    }

    public ActivationTrigger createActivationTriggerAudience(Condition condition,
                                                             List<String> audienceNames) throws HttpResponseStatusException {
        return activationTriggerService.createActivationTriggerAudience(m_mode, m_country, condition, audienceNames);
    }

    public ActivationTrigger createActivationTriggerCustomTargeting(List<String> sourceIds) {
        return activationTriggerService.createActivationTriggerCustomTargeting(sourceIds);
    }

    public TriggerPackage createTriggerPackage(TriggerCombination triggerCombination,
                                               int startIndex,
                                               List<ActivationTrigger> activationTriggerList) {
        return triggerPackageService.createTriggerPackage(triggerCombination, startIndex, activationTriggerList);
    }

    public Triggers createTriggersFromTriggersPackage(List<TriggerPackage> triggerPackages) {
        return triggerService.createTriggersFromPackages(triggerPackages);
    }

    public ImageDAM createImageFromDAM(String assetId, AssetType assetType, PrinterLevel printerLevel) throws HttpResponseStatusException {
        return imageDAMService.createDAMImage(m_mode, m_country, assetId, printerLevel, assetType);
    }

    public ScheduleObject createScheduleObject(DayEnum dayEnum, String startTime, String stopTime) {
        return scheduleObjectService.createScheduleObject(dayEnum, startTime, stopTime);
    }

    public Schedule createSchedule(List<ScheduleObject> include, List<ScheduleObject> exclude) {
        return scheduleService.createSchedule(include, exclude);
    }

    public FrequencyCap createFrequencyCapsLifetime(int count,
                                                    EventType eventType,
                                                    boolean multiple,
                                                    Scope level,
                                                    List<Item> additionalItems,
                                                    LocationScope locationScope,
                                                    ChannelType channelType) {
        return cappingService.createFrequencyCapsLifeTime(count,
                eventType, multiple, level, additionalItems, locationScope, channelType);
    }

    public FrequencyCap createMiniFrequencyCap(Integer count) {
        return cappingService.createMiniFrequencyCapping(count);
    }

    public DistributionCap createDistributionCappingLifeTime(int count,
                                                             EventType eventType,
                                                             Integer timeUnits,
                                                             Scope level,
                                                             Capping.ScopeDistributionCap scope,
                                                             ChannelType channelType) {
        return cappingService.createDistributionCappingLifeTime(count, eventType, timeUnits, level, scope, channelType);
    }

    public Classification createClassification(ClassificationName classification) throws HttpResponseStatusException {
        switch (classification) {
            case GENERIC_COUPON_AD:
                return classificationService.createGenericCouponAdClassification();
            case BUY_N_SAVE_X_PERCENT:
                return classificationService.createSaveXPercentClassification();
            case SPEND_X_SAVE_Y:
                return classificationService.createSpendXSaveYClassification();
        }
        return classificationService.getClassification(m_mode, classification);
    }

    public Barcode createStaticBarcode(BarcodeType barcodeType, String barcodeStr) {
        return barcodeService.createStaticBarcode(barcodeType, barcodeStr);
    }

    public ValidityDate createRollingValidityDate(int value,
                                                  ValidityDate.RollingPeriod period,
                                                  ValidityDate.DateEvent event,
                                                  ValidityDate.DateFormat format) {
        return validityDateService.createRollingValidityDate(value, period, event, format);
    }

    public ValidityDate createFixedValidityDate(String date,
                                                ValidityDate.DateFormat format) {
        return validityDateService.createFixedValidityDate(date, format);
    }

    public Reward createMinimalReward(Barcode barcode,
                                      ValidityDate effectiveDate,
                                      ValidityDate expirationDate,
                                      String adExternalId, //BL-
                                      Funding funding,
                                      String promotionId,  //FRA-BLIP-BL_
                                      List<Trailer> legal,
                                      Schedule schedule) {
        return rewardService.createMinimalReward(barcode, effectiveDate, expirationDate, adExternalId, funding, promotionId, legal, schedule);
    }

    public Experience createMinimalExperience(List<FrequencyCap> frequencyCaps,
                                              ExperienceEnum experienceEnum,
                                              Reward reward,
                                              Triggers triggers) {
        return experienceService.createMinimalExperience(frequencyCaps, experienceEnum, reward, triggers);
    }

    public Ratio createRatio(Ratio.RatioScope scope, List<Double> values) {
        return ratioService.createRatio(scope, values);
    }

    public ReportingAttribute createReportingAttribute(String value) {
        //TO BE SET IN AD.METADATA
        return reportingAttributeService.createReportingAttribute(value);
    }

    public List<String> compareCappingBetweenPromotionNAdManuf(int timePeriodInHour) throws HttpResponseStatusException {
        return promotionService.compareCappingBetweenPromotionNAdManuf(m_mode, m_country, timePeriodInHour);
    }

    public void formatAudience(int networkId, String filePath) {
        audienceFileService.formatFileToAudience(m_country, networkId, filePath);
    }

    public HttpStatus createNewAudience(String audienceName,
                                        User user,
                                        List<String> tags,
                                        String filePath) throws HttpResponseStatusException {
        Audience audience = new Audience(UUID.randomUUID().toString(),
                audienceName,
                user,
                Constants.STATUS_DRAFT,
                m_country.getCode(),
                tags);
        Path path = Paths.get(filePath);
        audiencesService.createAudience(m_mode, audience);
        AudienceFile audienceFile = audienceFileService.setAudienceFile(m_mode,
                audience,
                path.getFileName().toString(),
                user,
                m_country.getCode());
        audienceFileService.firstPostAudienceFile(m_mode, audienceFile);
        audienceFileService.secondPostAudienceFile(m_mode, audienceFile, filePath);
        HttpStatus httpStatus = audiencesService.updateSpecifications(m_mode, audience, audienceFile, user);
        return httpStatus;

    }

    public HttpStatus addFilesToExistingAudience(String audienceId,
                                                 User user,
                                                 String filePath) throws HttpResponseStatusException {
        Audience audience = audiencesService.getAudienceFromId(m_mode, audienceId);
        Path path = Paths.get(filePath);
        AudienceFile audienceFile = audienceFileService.setAudienceFile(m_mode,
                audience.getId(),
                path.getFileName().toString(),
                user,
                audience.getCountry_code());
        audienceFileService.firstPostAudienceFile(m_mode, audienceFile);
        audienceFileService.secondPostAudienceFile(m_mode, audienceFile, filePath);
        HttpStatus httpStatus = audiencesService.updateSpecifications(m_mode, audience, audienceFile, user);
        return httpStatus;
    }

    public Campaign createMinimalCampaign(String user, String name) {
        return campaignService.setupMinimalCampaign(m_country, user, name);
    }

    public Result createCampaign(String name, String billing_nbr, String control_grp) throws HttpResponseStatusException {

        String campaignId = null;
        List<Campaign> campaignList = campaignService.searchForCampaign(m_mode, m_country, name);

        if (!campaignList.isEmpty()) {     //CAMPAIGN EXISTS
            if (campaignList.size() != 1)
                throw new RuntimeException(String.format("%s campaigns found with the name: %s", campaignList.size(), name));

            //IF ONLY ONE CAMPAIGN -> GET ID
            campaignId = campaignList.get(0).getId();
            logger.info(String.format("a campaign named %s, already exists : %s", name, campaignId));
        } else {                           // CREATE CAMPAIGN
            m_campaign = campaignService.setupCampaignManuf(m_mode, name, billing_nbr, control_grp);
            if (DO_NOT_SAVE) {
                logger.info("campaign not saved on the dashboard");
                return new Result(999, "Not saved on Dashboard");
            }
            campaignId = offerService.postOffer(m_mode, AbstractOffer.OfferEnum.CAMPAIGN, m_campaign);
        }
        return new Result(campaignId);

    }

    public <T> boolean doesOfferExists(OfferEnum offerEnum, String queryFilter) throws HttpResponseStatusException {
        boolean exists = false;
        List<AbstractOffer> offerList = getOffersFromDashBoard(offerEnum, queryFilter);
        if (!offerList.isEmpty()) exists = true;
        return exists;
    }

    public <T> List<T> getOffersFromDashBoard(OfferEnum offerEnum, String queryFilter) throws HttpResponseStatusException {
        Class clazz = offerEnum.getClazzOfObject().getClass();
        return offerService.getOffersFromDashBoard(m_mode, m_country, clazz, queryFilter);
    }

    public <T> List<T> getOffersFromDashBoard(OfferEnum offerEnum, List<SolrParam> queryParameters) throws HttpResponseStatusException {
        Class clazz = offerEnum.getClazzOfObject().getClass();
        return offerService.getOffersFromDashBoard(m_mode, clazz, queryParameters);
    }


    public <T> String postOffer(AbstractOffer.OfferEnum offerEnum, T body) throws HttpResponseStatusException {
        String offerId = offerService.postOffer(m_mode, offerEnum, body);
        return offerId;
    }

    public Adgroup createMinimalAdGroup(String campaignId,
                                        String campaignName,
                                        String adGroupName,
                                        Adgroup.DeliveryMethod deliveryMethod,
                                        Adgroup.AdRotation adRotation,
                                        Adgroup.RoadBloacking roadBlocking,
                                        String createdBy) {
        return adGroupService.createMinimalAdGroup(campaignId,
                campaignName,
                adGroupName,
                deliveryMethod,
                adRotation,
                roadBlocking,
                createdBy);
    }

    public Result createAdgroup(String campaignId, String campaignName, String name, String delivery_method) throws HttpResponseStatusException {
        String adGroupId = null;

        List<Adgroup> adgroupList = adGroupService.searchAdGroup(m_mode, m_country, campaignId, name);

        if (!adgroupList.isEmpty()) {   //ADGROUP EXISTS
            if (adgroupList.size() != 1) {
                throw new RuntimeException(String.format("%s adgroups found with the name %s in the campaign: %s",
                        adgroupList.size(),
                        name,
                        campaignId));
            }
            //IF ONLY ONE ADGROUP -> GET ID
            adGroupId = adgroupList.get(0).getId();
            logger.info(String.format("an adgroup named %s, already exists : %s", name, adGroupId));
        } else {                         //CREATE ADGROUP
            m_adgroup = adGroupService.setupAdGroupManuf(campaignId, campaignName, name, delivery_method);
            if (DO_NOT_SAVE) return new Result(999, "Not saved on Dashboard");
            adGroupId = offerService.postOffer(m_mode, AbstractOffer.OfferEnum.ADGROUP, m_adgroup);
        }
        return new Result(adGroupId);
    }

    public Result createAd(String campaignId,
                           String campaignName,
                           String campgn_typ,
                           String billing_nbr,
                           String adgrouId,
                           String adgrouName,
                           String name,
                           String lever_type,
                           String dateStart,
                           String dateStop,
                           String color_trailer,
                           String bw_trailer,
                           String target_mthd,
                           String vupc_listname,
                           List<String> adExternalIdListForCT,
                           String color_template,
                           String deal_nbr,
                           String save_line,
                           String color_img_nbr,
                           String bw_product_img_nbr,
                           String qualifier_line2,
                           String qualifier_line3,
                           String qualifier_line4,
                           String count,
                           String clearing_hs,
                           String veto_crf,
                           String veto_su,
                           String veto_mpx,
                           String veto_itm,
                           String veto_smatch,
                           String audience_nm,
                           String target_desc,
                           String trig_grp_nm,
                           String operator,
                           String trigger_qty,
                           String trigger_list_name,
                           String _reportingAttribute,
                           boolean doLeaseOfferCodes) throws HttpResponseStatusException {

        String str_ad = null;
        String promotionId = null;
        String adExtId = null;

        List<Ad> adList = adService.findAd(m_mode, m_country, campaignId, adgrouId, name);

        if (!adList.isEmpty()) {

            if (adList.size() != 1) {     //MORE THAN ONE AD FOUND
                throw new RuntimeException(String.format("%s ads found with the name: %s, adgroup_id: %s, campaign_id: %s", adList.size(), name, adgrouId, campaignId));
            }

            logger.warn(String.format("This Ad already exist: %s \\n(same campaign, same adgroup, same name)", adExtId));
            adExtId = adList.get(0).getExternal_id();
            return new Result(-1, "Ad already exists", adExtId);
        }


        adExtId = adService.getBL(m_mode, m_country);
        promotionId = m_country.getCode() + "-" + Constants.EXTERNAL_SYSTEM + "-" + adExtId;
        System.out.println(promotionId);


        m_ad = new Ad(campaignId,
                campaignName,
                adgrouId,
                adgrouName,
                name,
                adExtId,
                m_country,
                dateStart,
                dateStop,
                CPGPublisher.createdBy != null ? CPGPublisher.createdBy : Constants.BULK_MANUF,
                target_desc);

        Result resAdUpdate = new AdService(cRestTemplate).updateAdManuf(m_ad,
                m_mode,
                billing_nbr,
                campgn_typ,
                adgrouName,
                target_mthd,
                adExtId,
                promotionId,
                lever_type,
                save_line,
                count,
                color_img_nbr,
                bw_product_img_nbr,
                vupc_listname,
                adExternalIdListForCT,
                clearing_hs,
                color_template,
                deal_nbr,
                qualifier_line2,
                qualifier_line3,
                qualifier_line4,
                color_trailer,
                bw_trailer,
                operator,
                trigger_qty,
                trigger_list_name,
                trig_grp_nm,
                _reportingAttribute);

        if (resAdUpdate.getStatus() != 0) {
            return resAdUpdate;
        }

        //GET CAMPAIGN TOUCHPOINTS
        List<TouchPoint> campaignTouchpoint = adService.getCampaignTouchpoints(m_mode, m_country, m_campaign, campaignId);

        //HANDLE VETO MANUF
        adService.updateAdVetoManuf(m_ad, campaignTouchpoint, veto_itm, veto_su, veto_smatch, veto_mpx, veto_crf);

        m_promotion = promotionService.createMinimalPromotion(m_ad, campaignTouchpoint);

        if (DO_NOT_SAVE) return new Result(999, "Not saved on Dashboard");

        offerService.postPromotion(m_mode, m_promotion);

        //LEASE OFFER CODES
        if (doLeaseOfferCodes) {
            List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes = offerCodeService.leaseOfferCodesForPromotion(m_mode, m_promotion);
            m_ad.getExperience().getReward().setOffer_code_type(Constants.OFFER_CODE_TYPE_GENERATED);
            m_ad.getExperience().getReward().setOffer_codes(offerCodes);
        }

        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            logger.error(String.format("Thread.sleep interruption: %s", e.getMessage()));
        }

        str_ad = Config.toJsString(m_ad);
        str_ad = adService.finalize(str_ad);
        offerService.postOffer(m_mode, AbstractOffer.OfferEnum.AD, str_ad);
        return new Result(m_ad.getExternal_id());
    }

    public Promotion createMinimalPromotion(Ad ad, List<TouchPoint> include) {
        return promotionService.createMinimalPromotion(ad, include);
    }

    public void postPromotion(Promotion promotion) throws HttpResponseStatusException {
        offerService.postPromotion(m_mode, promotion);
    }

    public void updateAd(String externalId, AdParams adParams) throws HttpResponseStatusException {

        List<Ad> adList = adService.searchAdFromBL(m_mode, m_country, externalId, HttpStatus.OK);

        if (adList.size() != 1) throw new RuntimeException(String.format("%s ad(s) found for the external_id: %s",
                adList.size(),
                externalId));

        Ad tmpAd = adList.get(0);

        if (adParams.startDate != null) {
            tmpAd.setStart_date(adParams.startDate.toString());
        }

        if (adParams.startTime != null) {
            tmpAd.setStart_time(adParams.startTime.toString());
        }

        if (adParams.stopDate != null) {
            tmpAd.setStop_date(adParams.stopDate.toString());
        }

        if (adParams.stopTime != null) {
            tmpAd.setStop_time(adParams.stopTime.toString());
        }

        if (adParams.status != null) {
            Config.AdStatus adStatus = Config.AdStatus.getStatus(adParams.status);
            if (adStatus == AdStatus.PAUSED) tmpAd.setStatus("PAUSED");
            else if (adStatus == AdStatus.PUBLISHED) tmpAd.setStatus("PUBLISHED");
            tmpAd.setPublish(true);
        }

        if (adParams.capping != null) {
            Budget updatedBudget = updateCapping(tmpAd.getBudget(), adParams.capping);
            tmpAd.setBudget(updatedBudget);
        }

        offerService.postOffer(m_mode, AbstractOffer.OfferEnum.AD, tmpAd);
    }

    public Result postAd(Ad ad, boolean doLeaseOfferCodes) throws HttpResponseStatusException {
        String campaignId = ad.getCampaign().getId();
        String adGroupId = ad.getAdgroup().getId();
        String adName = ad.getName();

        //STEP 1 CONTROL IF AD EXISTS
        String queryParam1 = String.format("{\"campaign\": {\"id\": \"%s\"},\"adgroup\":{\"id\": \"%s\"},\"name\": \"%s\"}",
                campaignId,
                adGroupId,
                adName);
        boolean offerExists = doesOfferExists(OfferEnum.AD, queryParam1);
        if (offerExists) {
            logger.warn("This Ad already exist ! (same campaign, same adgroup, same name)");
            return new Result(-1, "Ad already exists");
        }

        //STEP 2 GET TOUCHPOINT FROM THE WANTED LEVEL (CAMPAIGN,ADGROUP,AD)
        List<TouchPoint> touchPointList = adService.getCampaignTouchpoints(m_mode, m_country, m_campaign, campaignId);

        //STEP 3 CREATE MINIMAL PROMOTION && POST IT
        Promotion promotion = createMinimalPromotion(ad, touchPointList);
        postPromotion(promotion);

        //STEP 4 LEASE OFFER_CODES
        if (doLeaseOfferCodes) {
            Map<String, List<OCLU_LEASE_RESPONSE_ENTITY>> ocluMap = leaseOfferCodesForPromotions(Collections.singletonList(promotion));
            ad.getExperience().getReward().setOffer_code_type(Constants.OFFER_CODE_TYPE_GENERATED);
            ad.getExperience().getReward().setOffer_codes(ocluMap.get(promotion.getId()));
        }

        //STEP 5 SLEEP 15 SECONDS
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            logger.error(String.format("Thread.sleep Interruption: %s", e.getMessage()));
        }

        //STEP 6 POST AD
        postOffer(AbstractOffer.OfferEnum.AD, ad);

        return new Result(ad.getExternal_id());
    }

    private Budget updateCapping(Budget budget, Integer capping) {

        //count -1 -> delete the current capping if present
        if (capping == -1) budget = new Budget(new ArrayList<>(), false);

            //add or update current capping
        else {
            //Capping exists -> update current capping
            if (budget.getCaps() != null && !budget.getCaps().isEmpty()) {
                //TODO test if it is a valid capping
                for (DistributionCap currentCap : budget.getCaps()) {
                    currentCap.setCount(capping);
                }
            }
            //Capping do not exists -> create new capping
            else {
                DistributionCap distributionCap = cappingService.createDistributionCappingLifeTime(capping,
                        EventType.IMPRESSION,
                        1,
                        Scope.AD,
                        Capping.ScopeDistributionCap.COUNTRY,
                        ChannelType.IN_STORE);
                budget.setCaps(Arrays.asList(distributionCap));
            }
        }
        return budget;
    }

    public Result duplicateAd(String externalId, LocalDate startDate, LocalDate stopDate, Integer capping) throws HttpResponseStatusException {

        List<Ad> adList = adService.searchAdFromBL(m_mode, m_country, externalId, HttpStatus.OK);

        if (adList.size() != 1) throw new RuntimeException(String.format("%s ad(s) found for the external_id: %s",
                adList.size(),
                externalId));

        Ad ad = adList.get(0);

        String referencePromotionId = ad.getExperience().getReward().getId();

        // editValues update ids && name
        adService.editValues(m_mode, ad);

        String campaignId = ad.getCampaign().getId();
        String externalIdDuplicatedAd = ad.getExternal_id();

        //Update start and stop date for the duplicated ad
        if (startDate != null) ad.setStart_date(startDate.toString());
        if (stopDate != null) ad.setStop_date(stopDate.toString());

        if (capping != null) {
            Budget updatedBudget = updateCapping(ad.getBudget(), capping);
            ad.setBudget(updatedBudget);
        }

        //set created by
        if (ad.getHistory() != null) ad.getHistory().setCreated_by(createdBy);


        //GET TOUCHPOINTS
        Promotion referencePromotion = promotionService.getPromotion(m_mode, referencePromotionId);
        List<TouchPoint> touchpoints = referencePromotion.getTargeting().getTouchpoints().getInclude();
        m_promotion = promotionService.createMinimalPromotion(ad, touchpoints);
        offerService.postPromotion(m_mode, m_promotion);

        //Add OfferCodes
        if (ad.getExperience() != null && ad.getExperience().getReward() != null && ad.getExperience().getReward().getBarcode() != null) {
            String barcodeFormatType = ad.getExperience().getReward().getBarcode().getFormat().getType();
            if (barcodeFormatType.equals(BarcodeFormatEnum.DYNAMIC_BARCODE.getFormat())) {
                List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes = offerCodeService.leaseOfferCodesForPromotion(m_mode, m_promotion);
                ad.getExperience().getReward().setOffer_code_type(Constants.OFFER_CODE_TYPE_GENERATED);
                ad.getExperience().getReward().setOffer_codes(offerCodes);
            }
        }


        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            logger.error(String.format("Thread.sleep interruption: %s", e.getMessage()));
        }
        String adId = offerService.postOffer(m_mode, AbstractOffer.OfferEnum.AD, ad);
        return new Result(0, externalIdDuplicatedAd, adId);
    }

    public Result uploadCustomImage(String base64Img) {
        return new CustomImageService(cRestTemplate).postImageB64(m_mode, base64Img);
    }

    public void releaseOfferCodesForPromotions(List<String> offerIds) throws HttpResponseStatusException {
        offerCodeService.releaseOfferCodesFromPromotionsId(m_mode, offerIds);
    }

    public void leaseOfferCodesFromPromotionIds(List<String> offerIds) throws HttpResponseStatusException {
        List<Promotion> promotionList = new ArrayList<>();
        for (String currentPromotionId : offerIds) {
            Promotion promotion = promotionService.getPromotion(m_mode, currentPromotionId);
            promotionList.add(promotion);
        }
        leaseOfferCodesForPromotions(promotionList);
    }

    public Map<String, List<OCLU_LEASE_RESPONSE_ENTITY>> leaseOfferCodesForPromotions(List<Promotion> promotionList) throws HttpResponseStatusException {
        Map<String, List<OCLU_LEASE_RESPONSE_ENTITY>> ocluMap = new HashMap();
        for (Promotion currentPromotion : promotionList) {
            List<OCLU_LEASE_RESPONSE_ENTITY> offerCodes =
                    offerCodeService.leaseOfferCodesForPromotion(m_mode, currentPromotion);
            ocluMap.put(currentPromotion.getId(), offerCodes);
        }
        return ocluMap;
    }

    public void leaseOrReleaseOfferCodesForCampaign(OfferCodeAction offerCodeAction, List<String> campaignIds) throws HttpResponseStatusException {
        offerCodeService.leaseOrReleaseOfferCodesForCampaign(m_mode, offerCodeAction, campaignIds);
    }

    public OfferCodeService getOfferCodeServiceInstance(CRestTemplate cRestTemplate) {
        return new OfferCodeService(cRestTemplate);
    }

    public Result createMinimalAd(String campaignId,
                                  String campaignName,
                                  String adgroupId,
                                  String adgroupName,
                                  String name,
                                  String dateStart,
                                  String dateStop) {
        return adService.createMinimalAd(m_mode,
                campaignId,
                campaignName,
                adgroupId,
                adgroupName,
                name,
                dateStart,
                dateStop,
                m_country);
    }

    public String getPromotionIdFromMinimalAd(Ad ad) {
        String promotionId = ad.getCountry_code() + "-" + Constants.EXTERNAL_SYSTEM + "-" + ad.getExternal_id();
        return promotionId;
    }

    public String getPromotionId(String adExternalId) {
        String promotionId = m_country.getCode() + "-" + Constants.EXTERNAL_SYSTEM + "-" + adExternalId;
        return promotionId;
    }

    public static void testCaseEcolinkColgate() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] Colgate Total C4 2019 - FPE", "61741", "mags_off_mut_IA419COL01_VAULT");

        System.out.println("Campaign : " + res.getData());
        String campaignId = res.getData();

        if (res.getStatus() == 0) {
            res = myPublisher.createAdgroup(campaignId, "[TEST] Colgate Total C4 2019 - FPE", "ECOLINK", "ACCELERATED");
            System.out.println("AdGroup : " + res.getData());
            String adGroupId = res.getData();


            try {
                res = myPublisher.createAd(campaignId,
                        "[TEST] Colgate Total C4 2019 - FPE",
                        "NON ALCOOL",
                        "61741",
                        adGroupId,
                        "ECOLINK",
                        "[TEST] ad FPE EK from cleanup 3",
                        "EK",
                        "2019-10-05",
                        "2019-10-13",
                        "<|>",
                        "COLGATE PALMOLIVE SAS AU CAPITAL<br/>DE 6 911 180 EUROS. RCS 478991649<br/>NANTERRE F-92700 COLOMBES",
                        "VAULT ECOLINK",
                        "VUPC Colgate Gamme blancheur  PREMIUM 61741",
                        null,
                        "FRA Manuf- Coupon EXPRESSION from C3 2019",
                        "121277",
                        "1,00",
                        "234411",
                        "234592",
                        "",
                        "2 DENTIFRICES<br/>COLGATE®",
                        "AU CHOIX DANS LA GAMME<br/>MAX WHITE",
                        "22675",
                        "SCANCOUPON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "",
                        "",
                        "15% H B V1 Colgate Gamme bl PREM",
                        "",
                        "",
                        "",
                        "Dentifrices Générélistes",
                        false);
            } catch (HttpResponseStatusException e) {
                logger.error(String.format("Htt: %s", e.getMessage()));
            }

            String adBL = res.getData();
            System.out.println("Ad BL => " + adBL);
        }

    }

    public static void testCaseEcobonRozana() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019", "64421", "mags_off_mut_MD519ROZ01_VAULT");

        System.out.println("Campaign : " + res.getData());
        String campaignId = res.getData();

        if (res.getStatus() == 0) {
            res = myPublisher.createAdgroup(campaignId, "[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019", "ECOBON", "ACCELERATED");
            System.out.println("AdGroup : " + res.getData());
            String adGroupId = res.getData();


            try {
                res = myPublisher.createAd(campaignId,
                        "[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019",
                        "NON ALCOOL",
                        "64421",
                        adGroupId,
                        "ECOBON",
                        "[TEST] 004ES OU Ach nat de Rozana 6X1L x1 MBU via VP 2",
                        "OU",
                        "2019-11-20",
                        "2019-12-31",
                        "<|>",
                        "RCS : FR40 428 645 402 00036",
                        "VAULT ECOBON",
                        "VUPC ROZANA 6x1L 64421",
                        null,
                        "FRA Manuf- Coupon ESSENTIEL from C3 2019",
                        "122921",
                        "0,40",
                        "234400",
                        "245035",
                        "DE REDUCTION A VALOIR SUR",
                        "2 PACKS EAU MINERALE ROZANA",
                        "NATURELLEMENT GAZEUSE 6X1L",
                        "",
                        "SCANCOUPON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "",
                        "Acheteurs d'1 produit parmi:",
                        "Rozana GF Pack",
                        "==",
                        "1",
                        "CR2 Acheteurs naturels de Rozana 6X1L -64421",
                        "",
                        false);
            } catch (HttpResponseStatusException e) {
                e.printStackTrace();
            }

            String adBL = res.getData();
            System.out.println("Ad BL => " + adBL);
        }

    }

    public static void testCaseEcolinkRozana() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019", "64421", "mags_off_mut_MD519ROZ01_VAULT");

        System.out.println("Campaign : " + res.getData());
        String campaignId = res.getData();

        if (res.getStatus() == 0) {
            res = myPublisher.createAdgroup(campaignId, "[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019", "ECOLINK", "ACCELERATED");
            System.out.println("AdGroup : " + res.getData());
            String adGroupId = res.getData();


            try {
                res = myPublisher.createAd(campaignId,
                        "[TEST] Rozana GF C5 2019 TEST Suricat 21-11-2019",
                        "NON ALCOOL",
                        "64421",
                        adGroupId,
                        "ECOLINK",
                        "[TEST] 001ES EK 20% TTG + TG ROZANA PACK 1L V1 test",
                        "EK",
                        "2019-11-20",
                        "2019-12-31",
                        "<|>",
                        "RCS : FR40 428 645 402 00036",
                        "VAULT ECOLINK",
                        "VUPC ROZANA 6x1L 64421",
                        null,
                        "FRA Manuf- Coupon ESSENTIEL from C3 2019",
                        "122918",
                        "1,00",
                        "245765",
                        "245035",
                        "DE REDUCTION A VALOIR SUR",
                        "3 PACKS EAU MINERALE ROZANA",
                        "NATURELLEMENT GAZEUSE 6X1L",
                        "40020",
                        "SCANCOUPON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "OUI",
                        "",
                        "Profil de foyers porteurs de la carte de fidélité",
                        "20% TTG + TG ROZANA PACK 1L V1",
                        "",
                        "",
                        "",
                        "",
                        false);
            } catch (HttpResponseStatusException e) {
                e.printStackTrace();
            }

            String adBL = res.getData();
            System.out.println("Ad BL => " + adBL);
        }

    }

    public static void testCaseEcolinkRozanaCampaign() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] Rozana GF C5 2019 TEST Suricat 2", "64421", "mags_off_mut_MD519ROZ01_VAULT");

        System.out.println("Campaign : " + res.getData());
    }

    public static void testCaseEcobonColgate() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, CountryEnum.FRANCE, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] colgate TEST FPE", "61741", "mags_off_mut_IA419COL01_VAULT");

        System.out.println("Campaign : " + res.getData());
        String campaignId = res.getData();

        if (res.getStatus() == 0) {

            res = myPublisher.createAdgroup(campaignId, "[TEST] colgate TEST FPE", "ECOBON", "ACCELERATED");
            System.out.println("AdGroup : " + res.getData());
            String adGroupId = res.getData();


            try {
                res = myPublisher.createAd(campaignId,
                        "[TEST] colgate TEST FPE",
                        "NON ALCOOL",
                        "61741",
                        adGroupId,
                        "ECOBON",
                        "[TEST] ad FPE OU from develop 11",
                        "OU",
                        "2020-10-05",
                        "2020-10-12",
                        "<|>",
                        "COLGATE PALMOLIVE SAS AU CAPITAL<br/>DE 6 911 180 EUROS. RCS 478991649<br/>NANTERRE F-92700 COLOMBES",
                        "Vault ECOBON",
                        "VUPC Colgate Total nouvelle formule 61741",
                        null,
                        "FRA Manuf- Coupon EXPRESSION from C3 2019",
                        "121285",
                        "1,00",
                        "234400",
                        "234592",
                        "DE RÉDUCTION À VALOIR SUR",
                        "3 DENTIFRICES<br/>COLGATE® TOTAL",
                        "AU CHOIX DANS LA GAMME<br/>MAX WHITE",
                        "",
                        "SCANCOUPON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "NON",
                        "",
                        "",
                        "VUPC Colgate Total",
                        ">=",
                        "1",
                        "CR9 Acheteurs naturels de Colgate Total nouvelle formule -61741",
                        "Dentifrices Généralistes",
                        false);
            } catch (HttpResponseStatusException e) {
                e.printStackTrace();
            }

            String adBL = res.getData();
            System.out.println("Ad BL => " + adBL);
        }
    }

    public static void testCaseCampaignColgate() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Result res = myPublisher.createCampaign("[TEST] Colgate Total C4 2019 - FPE1", "61741", "mags_off_mut_IA419COL01_VAULT");
    }

    public static void testCaseAdGroupAdHoc() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());
        Adgroup adGroup = myPublisher.adGroupService.createMinimalAdGroup("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                "[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19",
                "Ad Group 1",
                Adgroup.DeliveryMethod.ACCELERATED,
                Adgroup.AdRotation.DEFAULT,
                Adgroup.RoadBloacking.SINGLE,
                Constants.VAULT_PUBLISHER);
        adGroup.setDelivery_method("NORMAL");
        String adGroupId = myPublisher.postOffer(OfferEnum.ADGROUP, adGroup);
        System.out.println("Ad Group : " + adGroupId);
    }

    public static void testDuplicateAdEcobonColgate() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, CountryEnum.FRANCE, new CRestTemplate());

        LocalDate startDate = LocalDate.of(2020, 12, 20);
        LocalDate stopDate = LocalDate.of(2020, 12, 20);

        Result res = myPublisher.duplicateAd("BL_9962_5556", startDate, stopDate, null);

        System.out.println(res.toString());
    }

    public static void testDuplicateHandMadeAd() throws HttpResponseStatusException {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, CountryEnum.FRANCE, new CRestTemplate());

        LocalDate startDate = LocalDate.of(2020, 12, 20);
        LocalDate stopDate = LocalDate.of(2020, 12, 20);

        Result res = myPublisher.duplicateAd("BL_6030_7194", startDate, stopDate, null);

        System.out.println(res.toString());
    }

    public static void testCaseAdAdHoc() {
        CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());

        Result res = myPublisher.createMinimalAd("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                "[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19",
                "d04ea7da-6246-4140-b7f9-f977e8936700",
                "Ad Group 1",
                "[TEST] SU_BK_Q1_19 - Dynamiser le trafic en semaine lundi -> jeudi - FPE 07-11-19",
                "2020-02-01",
                "2020-02-13");
        if (res.getStatus() != 0) {
            System.err.println(res.getMessage());
        }
        Ad ad = (Ad) res.getObject();
        try {
            res = myPublisher.postAd(ad, false);
        } catch (HttpResponseStatusException e) {
            e.printStackTrace();
        }
        System.out.println("Ad : " + res.getData());
    }

    public static void main(String[] args) throws Exception {


//		testCaseEcolinkColgate();
        testCaseEcobonColgate();
//		testDuplicateAdEcobonColgate();
//		testDuplicateHandMadeAd();
//		testCaseCampaignColgate();

/****		TEST DUPLICATE AD
 CPGPublisher myPublisher = new CPGPublisher(Mode.PROD, new CRestTemplate());

 myPublisher.duplicateAd(Mode.PROD,
 "BL_3428_0159",
 LocalDate.of(2020, Month.APRIL,1),
 LocalDate.of(2020, Month.APRIL,2),
 10);
 */
//		testCaseCampaignTraficSu();
//      testCaseAdGroupAdHoc();
//		testCaseAdAdHoc();
    }
}
