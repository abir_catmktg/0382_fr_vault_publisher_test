package com.catalina.vault_publisher;

import com.catalina.vault_publisher.data.Config;
import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.Result;
import com.catalina.vault_publisher.data.SolrParam;
import com.catalina.vault_publisher.data.bean.AdParams;
import com.catalina.vault_publisher.data.billing.Billing;
import com.catalina.vault_publisher.data.billing.BillingCampaign;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethod;
import com.catalina.vault_publisher.data.billing.targetingMethod.TargetingMethodService;
import com.catalina.vault_publisher.data.capping.FrequencyCap;
import com.catalina.vault_publisher.data.classification.Classification;
import com.catalina.vault_publisher.data.classification.ClassificationService;
import com.catalina.vault_publisher.data.list.lmc.item.LMCItem;
import com.catalina.vault_publisher.data.oclus.OCLU_LEASE_RESPONSE_ENTITY;
import com.catalina.vault_publisher.data.oclus.OCLU_MINI_RANGE;
import com.catalina.vault_publisher.data.oclus.OCLU_NETWORK;
import com.catalina.vault_publisher.data.oclus.OfferCodeService;
import com.catalina.vault_publisher.data.offers.AbstractOffer.OfferEnum;
import com.catalina.vault_publisher.data.offers.*;
import com.catalina.vault_publisher.data.targeting.Targeting;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.chain.Chain;
import com.catalina.vault_publisher.data.touchpoint.siteId.SiteId;
import com.catalina.vault_publisher.utils.CRestTemplate;
import com.catalina.vault_publisher.utils.Constants;
import com.catalina.vault_publisher.utils.DateUtils;
import com.catalina.vault_publisher.utils.HttpResponseStatusException;
import com.google.gson.*;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.Normalizer;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Unit test for simple App.
 */
@RunWith(MockitoJUnitRunner.class)
public class PublisherTest {

    @Mock
    CRestTemplate cRestTemplate;

    @Spy
    @InjectMocks
    CPGPublisher cpgPublisher = new CPGPublisher(Config.Mode.PROD, cRestTemplate);

    @Mock
    AdService adService;

    @Mock
    CampaignService campaignService;

    @Mock
    AdGroupService adGroupService;

    @Mock
    OfferService offerService;

    @Mock
    ClassificationService classificationService;

    @Mock
    Promotion promotion;

    @Mock
    OfferCodeService offerCodeService;

    @Ignore
    @Test
    public void should_create_campain() throws HttpResponseStatusException {
        //Given
        String listUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/lists?wc={wc}&fq={fq}&fl={fl}";
        String listWcParam = "{\"name\":\"mags_off_mut_SM419HEP01_VAULT\"}";
        String listFqParam = "{\"type\":\"touchpoint\"}";
        String listFlParam = "id,name";

        String networkUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/networks?fl={fl}&fq={fq}";
        String networkFqParam = "{\"country\":{\"country_code\":\"FRA\"}}";
        String networkFlParam = "id,name,external_id";

        String billingUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/billing?fq={fq}&func={func}";
        String billingFqParam = "{\"external_id\":62421}";
        String billingFuncParam = "return d(\"id\").match(\"FRA\")";

        String clientUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/clients?fq={fq}";
        String clientFqParam = "{\"id\":\"FRA-CATS-374\"}";

        String campainName = "Hépar C4 2019";
        String billingNbr = "62421";
        String controlGrp = "mags_off_mut_SM419HEP01_VAULT";
        Config.Mode m_mode = Config.Mode.SQA;

        List<Billing> billings = getBillings();
        List<Client> clients = getClients();

        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("id", "e51edebe-a67a-459f-bb4a-198912d4ca21");
        linkedHashMap.put("name", "mags_off_mut_SM419HEP01_VAULT.txt");

        CampaignService campaignService = new CampaignService(cRestTemplate);

        //When
        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(listUrl, LinkedHashMap.class, listWcParam, listFqParam, listFlParam))
                .thenReturn(Arrays.asList(linkedHashMap));

        when(cRestTemplate.executeGetRequestWithJsonHeader(networkUrl, String.class, networkFlParam, networkFqParam))
                .thenReturn(getJsonFileContent("Networks.json"));

        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(billingUrl, Billing.class, billingFqParam, billingFuncParam))
                .thenReturn(billings);

        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(clientUrl, Client.class, clientFqParam))
                .thenReturn(clients);


        //Then
        Campaign m_campaign = campaignService.setupCampaignManuf(m_mode, campainName, billingNbr, controlGrp);
        String str_campaign = Config.toJsString(m_campaign);

        String jsonCampaign = getJsonFileContent("Campaign.json");

        JsonObject strJson = new Gson().fromJson(str_campaign, JsonObject.class);
        JsonObject campaignJson = new Gson().fromJson(jsonCampaign, JsonObject.class);


        //assertEquals(strJson.get("account"), campaignJson.get("account"));
        //assertEquals(strJson.get("billing"), campaignJson.get("billing"));
        //assertEquals(strJson.get("billing"),campaignJson.get("billing"));

        assertEquals(jsonCampaign, str_campaign);
        //assertEquals(strJson, campaignJson);
    }


    @Ignore
    @Test
    public void should_create_ecolink_ad_group() {

        //Given
        String campaignId = "677504d8-7013-4043-adb7-fe526d6866c6";
        String campaignName = "Hépar C4 2019";
        String name = "ECOLINK";
        String deliveryMethod = "ACCELERATED";
        List<FrequencyCap> capsList = new ArrayList<>();
        AdGroupService adGroupService = new AdGroupService(cRestTemplate);

        //Then
        Adgroup m_adgroup = adGroupService.setupAdGroupManuf(campaignId, campaignName, name, deliveryMethod);
        String str_adGroup = Config.toJsString(m_adgroup);

        String jsonAdGroup = getJsonFileContent("AdGroupEcolink.json");

        JsonObject adGroupJsonFromCode = new Gson().fromJson(str_adGroup, JsonObject.class);
        JsonObject adGroupJsonFromFile = new Gson().fromJson(jsonAdGroup, JsonObject.class);

        //assertEquals(adGroupJsonFromCode.get("campaign"), adGroupJsonFromFile.get("campaign"));
        //assertEquals(adGroupJsonFromCode.get("frequency_caps"), adGroupJsonFromFile.get("frequency_caps"));
        //assertEquals(adGroupJsonFromCode.get("targeting"), adGroupJsonFromFile.get("targeting"));

        assertEquals(str_adGroup, jsonAdGroup);
        assertEquals(adGroupJsonFromCode, adGroupJsonFromFile);

    }

    @Ignore
    @Test
    public void should_create_ecobon_ad_group() {

        //Given
        String campaignId = "677504d8-7013-4043-adb7-fe526d6866c6";
        String campaignName = "Hépar C4 2019";
        String name = "ECOBON";
        String deliveryMethod = "ACCELERATED";
        AdGroupService adGroupService = new AdGroupService(cRestTemplate);

        //Then
        Adgroup m_adgroup = adGroupService.setupAdGroupManuf(campaignId, campaignName, name, deliveryMethod);
        String str_adGroup = Config.toJsString(m_adgroup);

        String jsonAdGroup = getJsonFileContent("AdGroupEcobon.json");

        JsonObject adGroupJsonFromCode = new Gson().fromJson(str_adGroup, JsonObject.class);
        JsonObject adGroupJsonFromFile = new Gson().fromJson(jsonAdGroup, JsonObject.class);

        //assertEquals(adGroupJsonFromCode.get("campaign"), adGroupJsonFromFile.get("campaign"));
        //assertEquals(adGroupJsonFromCode.get("frequency_caps"), adGroupJsonFromFile.get("frequency_caps"));
        //assertEquals(adGroupJsonFromCode.get("targeting"), adGroupJsonFromFile.get("targeting"));

        assertEquals(str_adGroup, jsonAdGroup);
        assertEquals(adGroupJsonFromCode, adGroupJsonFromFile);

    }

    @Ignore
    @Test
    public void should_create_ad() throws HttpResponseStatusException {

        //Given

        //Ad params
        String campaignId = "2892f1cf-b9da-450c-8a27-5260ca625421";
        String campaignName = "[TEST] FPE Hepar C4 2019";
        String billingNbr = "62421";
        String campgn_typ = "NON ALCOOL";
        String adgrouId = "64f67e9a-7784-4e25-94c0-748e38857eca";
        String adgrouName = "ECOBON";
        String name = "010ES CP Ach AM x1 et + EPN PF 50cl";
        String lever_type = "CP";
        String dateStart = "2019-08-05";
        String dateStop = "2019-10-13";
        String color_trailer = "** Pour un montant minimum d’achat de 7 euros";
        String bw_trailer = "NW M&D<br>92130 - Issy les Moulineaux<br>RCS Nanterre -479 463 044.<br>*HEPAR peut etre laxative.<br>Boire 1L d'HEPAR par jour <br>dans le cadre d'une alimentationvariee et <br>equilibree et d'un mode de vie  sain.<br>Offre non cumulable avec d'autres promotions.";
        String target_mthd = "VAULT ECOBON";
        String vupc_listname = "VUPC Hepar 8x33cl 62421";
        String color_template = "FRA Manuf- Coupon ESSENTIEL from C3 2019";
        String deal_nbr = "0008136478";
        String save_line = "0,60";
        String color_img_nbr = "233492";
        String bw_static_img_nbr = "";
        String bw_product_img_nbr = "235569";
        String qualifier_line2 = "SUR VOS PROCHAINS ACHATS** DONT";
        String qualifier_line3 = "1 PACK HEPAR";
        String qualifier_line4 = "LA JOIE D'AVOIR UN BON TRANSIT*";
        String count = "";
        String clearing_hs = "SOGEC";
        String veto_crf = "NON";
        String veto_su = "NON";
        String veto_mpx = "NON";
        String veto_itm = "NON";
        String veto_smatch = "NON";
        String audience_nm = "";
        String target_desc = "Paniers de produits dont 1 UC parmi:";
        String trig_grp_nm = "courmayeur 50cl";
        String operator = ">=";
        String trigger_qty = "1";
        String trigger_list_name = "CR5 Acheteurs d'autres marques EPN PF 62421";
        String reportingAttribute = "EPN Petits Formats";
        boolean do_lease = false;
        Config.Mode m_mode = Config.Mode.SQA;

        //APIs URL to mock
        String billingUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/billing?fq={fq}&func={func}";
        String billingFqParam = "{\"external_id\":62421}";
        String billingFuncParam = "return d(\"id\").match(\"FRA\")";

        String clientUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/clients?fq={fq}";
        String clientFqParam = "{\"id\":\"FRA-CATS-374\"}";

        String promotionUrl = " https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/promotions";
        String offerCodePromotionUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/offercodes/promotions/FRA-BLIP-BL_0690_2584";

        String listFromBillingUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/lists/search";
        String listFromBillingBody = "{\"user\":\"suricat\",\"list_type\":\"product\",\"filters\":[\"name:*CR5*Acheteurs*d'autres*marques*EPN*PF*62421*\"],\"start\":0}";

        String lmcUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/lists/search";
        String lmcBody = "{\"user\":\"suricat\",\"list_type\":\"product\",\"filters\":[\"name:*VUPC*Hepar*8x33cl*62421*\"],\"start\":0}";

        String barCodeUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/barcode/discount/0,60 €/codes/FRA";
        String adUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/ads";

        String leaseUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/offercodes/promotions/FRA-BLIP-BL_0690_2584/leases/";
        String leaseBody = "{\"stopDate\": \"" + "2019-06-30" + "\", \"dateSpecification\": {\"type\":\"rolling\",\"value\":\"NOW+12WEEK\"}}";

        String leaseUpdateUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/offercodes/promotions/FRA-BLIP-BL_0690_2584/leases/update";

        String templateUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/templates?fq={fq}&fl={fl}";
        String templateFqParamColor = "{\"adsize\":{\"placement\":{\"id\":\"f5721e38-3793-4d3e-be8d-ed91c8313e88\"}}}";
        String templateFqParamBW = "{\"adsize\":{\"placement\":{\"id\":\"e24fa236-0db0-41a1-8098-f436f8a671ae\"}}}";
        String templateFlParam = "adsize,id,name,variables,preview";


        String damProductShotUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/dam/FRA/PRODUCT_SHOT/235569";
        String damStaticImageUrl = "https://sqa.dashboard.personalization.catalinamarketing.com/route-eu/dmp/rest/creatives/images/dam/FRA/STATIC_IMAGE/233492";


        List<Billing> billings = getBillings();
        List<Client> clients = getClients();


        //When
        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(billingUrl, Billing.class, billingFqParam, billingFuncParam))
                .thenReturn(billings);

        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(clientUrl, Client.class, clientFqParam))
                .thenReturn(clients);

        when(cRestTemplate.executePostRequestWithJsonHeader(listFromBillingUrl, listFromBillingBody, LMCItem[].class))
                .thenReturn(getLmcItemsFromFile("ListFromBilling.json"));

        when(cRestTemplate.executePostRequestWithJsonHeader(lmcUrl, lmcBody, LMCItem[].class))
                .thenReturn(getLmcItemsFromFile("LMC.json"));


        when(cRestTemplate.executeGetRequestWithJsonHeader(barCodeUrl, String.class))
                .thenReturn(getJsonFileContent("Barcode.json"));


        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(templateUrl, JsonObject.class, templateFqParamColor, templateFlParam))
                .thenReturn(getTemplatesColorContentAsJsonObject());

        when(cRestTemplate.executeSolrGetRequestWithJsonHeader(templateUrl, JsonObject.class, templateFqParamBW, templateFlParam))
                .thenReturn(getTemplatesBWContentAsJsonObject());

        when(cRestTemplate.executePostRequestWithJsonHeader(promotionUrl, getJsonFileContent("PromotionBody.json"), String.class))
                .thenReturn("ok");

        when(cRestTemplate.executeRequestWithJsonHeader(offerCodePromotionUrl, null, String.class, HttpMethod.GET, HttpStatus.NOT_FOUND))
                .thenReturn("{\"message\":\"PROMOTION_NOT_FOUND\",\"details\":null,\"error\":\"NOT_ACCEPTABLE\"}");

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUrl + "ce3e4703-12b7-4cac-8aa6-8ff2742c80f2", leaseBody, String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn(getJsonFileContent("OfferCode.json"));

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUrl + "2753ff5f-c15d-4588-8685-00dccc9537ab", leaseBody, String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn(getJsonFileContent("OfferCode.json"));

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUrl + "1229c07a-f808-43d8-9a89-f9ca92585d1a", leaseBody, String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn(getJsonFileContent("OfferCode.json"));

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUrl + "8af1d68f-9ce6-4240-8403-79c643700c41", leaseBody, String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn(getJsonFileContent("OfferCode.json"));

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUrl + "d0b0d39b-b91c-498f-99c3-f8a8c286318a", leaseBody, String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn(getJsonFileContent("OfferCode.json"));

        when(cRestTemplate.executeRequestWithJsonHeader(leaseUpdateUrl, getJsonFileContent("LeasesUpdateBody.json"), String.class, HttpMethod.POST, HttpStatus.OK))
                .thenReturn("ok");

        when(cRestTemplate.executePostRequestWithJsonHeader(adUrl, getJsonFileContent("AdsBody.json"), LinkedHashMap.class))
                .thenReturn(null);

        when(cRestTemplate.executeGetRequestWithJsonHeader(damProductShotUrl, String.class))
                .thenReturn(getJsonFileContent("DamProductShotImage.json"));

        when(cRestTemplate.executeGetRequestWithJsonHeader(damStaticImageUrl, String.class))
                .thenReturn(getJsonFileContent("DamStaticImage.json"));

        //Then
        String adId = null;
        String adExtId = null;
        try {
            adExtId = new AdService(cRestTemplate).getBL(m_mode, Country.CountryEnum.FRANCE);
            adId = "FRA-BLIP-" + adExtId;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        Ad m_ad = new Ad(campaignId,
                campaignName,
                adgrouId,
                adgrouName,
                name,
                adExtId,
                Country.CountryEnum.FRANCE,
                dateStart,
                dateStop,
                Constants.BULK_MANUF,
                target_desc);

        Result resAdUpdate = new AdService(cRestTemplate).updateAdManuf(m_ad,
                m_mode,
                billingNbr,
                campgn_typ,
                adgrouName,
                target_mthd,
                adExtId,
                adId,
                lever_type,
                save_line,
                count,
                color_img_nbr,
                bw_product_img_nbr,
                vupc_listname,
                null,
                clearing_hs,
                color_template,
                deal_nbr,
                qualifier_line2,
                qualifier_line3,
                qualifier_line4,
                color_trailer,
                bw_trailer,
                operator,
                trigger_qty,
                trigger_list_name,
                trig_grp_nm,
                reportingAttribute);

        String str_ad = Config.toJsString(m_ad);
        //System.err.println(str_ad);
        String strJsonAdFile = getJsonFileContent("HeparC4-CP-Ad.json");

        JsonObject adJson = new Gson().fromJson(str_ad, JsonObject.class);
        JsonObject adJsonFromFile = new Gson().fromJson(strJsonAdFile, JsonObject.class);

        assertFalse("ad_creative_set_id is missing", adJson.get("ad_creative_set_id").isJsonNull());
        assertEquals("AdGroup is different of ad file", adJsonFromFile.get("adgroup"), adJson.get("adgroup"));
        assertEquals("AdGroupId is different from ad file", adJsonFromFile.get("ad_group_id"), adJson.get("ad_group_id"));
        //assertEquals("Billing is different from ad file", adJsonFromFile.get("billing"), adJson.get("billing"));
        assertEquals("Budget is different from ad file", adJsonFromFile.get("budget"), adJson.get("budget"));
        assertEquals("Campaign is different from ad file", adJsonFromFile.get("campaign"), adJson.get("campaign"));
        assertEquals("campaign_id is different from ad file", adJsonFromFile.get("campaign_id"), adJson.get("campaign_id"));
        assertEquals("channels is different from ad file", adJsonFromFile.get("channels"), adJson.get("channels"));

        //check classification
        assertEquals("classification id is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("id"), adJson.get("classification").getAsJsonObject().get("id"));
        assertEquals("classification name is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("name"), adJson.get("classification").getAsJsonObject().get("name"));
        assertEquals("classification reward is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("reward"), adJson.get("classification").getAsJsonObject().get("reward"));
        assertEquals("classification risk is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("risk"), adJson.get("classification").getAsJsonObject().get("risk"));
        assertEquals("classification scope is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("scope"), adJson.get("classification").getAsJsonObject().get("scope"));
        assertEquals("classification type is different from ad file", adJsonFromFile.get("classification").getAsJsonObject().get("type"), adJson.get("classification").getAsJsonObject().get("type"));

        //assertEquals("client is different from ad file",adJsonFromFile.get("client"), adJson.get("client"));
        assertEquals("country is different from ad file", adJsonFromFile.get("country"), adJson.get("country"));
        assertEquals("country_code is different from ad file", adJsonFromFile.get("country_code"), adJson.get("country_code"));
        assertEquals("creative_variables size is different from ad file", adJsonFromFile.get("creative_variables").getAsJsonArray().size(), adJson.get("creative_variables").getAsJsonArray().size());
        assertEquals("creatives size is different from ad file", adJsonFromFile.get("creatives").getAsJsonArray().size(), adJson.get("creatives").getAsJsonArray().size());
        assertEquals("do_not_sync is different from ad file", adJsonFromFile.get("do_not_sync"), adJson.get("do_not_sync"));
        assertEquals("durable_mode is different from ad file", adJsonFromFile.get("durable_mode"), adJson.get("durable_mode"));

        //check experience
        assertEquals("experience cap_per_session is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("cap_per_session"), adJson.get("experience").getAsJsonObject().get("cap_per_session"));
        assertEquals("experience end_of_order is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("end_of_order"), adJson.get("experience").getAsJsonObject().get("end_of_order"));
        assertEquals("experience frequency_caps is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("frequency_caps"), adJson.get("experience").getAsJsonObject().get("frequency_caps"));
        assertEquals("experience frequency_statics is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("frequency_statics"), adJson.get("experience").getAsJsonObject().get("frequency_statics"));
        assertEquals("experience id is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("id"), adJson.get("experience").getAsJsonObject().get("id"));
        assertEquals("experience loyalty is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("loyalty"), adJson.get("experience").getAsJsonObject().get("loyalty"));
        assertEquals("experience name is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("name"), adJson.get("experience").getAsJsonObject().get("name"));
        assertEquals("experience pins is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("pins"), adJson.get("experience").getAsJsonObject().get("pins"));
        assertEquals("experience element size is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("format").getAsJsonObject().get("elements").getAsJsonArray().size(),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("format").getAsJsonObject().get("elements").getAsJsonArray().size());
        assertEquals("experience element type is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("format").getAsJsonObject().get("elements").getAsJsonArray().get(0).getAsJsonObject().get("type"),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("format").getAsJsonObject().get("elements").getAsJsonArray().get(0).getAsJsonObject().get("type"));
        assertFalse("experience element preview_url is missing", adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("format").getAsJsonObject().get("preview_url").isJsonNull());
        assertEquals("experience barcode type is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("type"),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("barcode").getAsJsonObject().get("type"));
        assertFalse("experience activation trigger group is missing", adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("conditions").getAsJsonObject().get("activation_trigger_groups").isJsonNull());
        assertFalse("experience activation triggers is missing", adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("conditions").getAsJsonObject().get("activation_triggers").isJsonNull());
        assertEquals("experience expiration_date type is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("expiration_date"),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("expiration_date"));
        assertFalse("experience reward external_id triggers is missing", adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("external_id").isJsonNull());
        assertEquals("experience reward funding is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("funding"),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("funding"));
        assertFalse("experience reward id is missing", adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("id").isJsonNull());

        checkExperienceLegal(adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("legal").getAsJsonArray(),
                adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("legal").getAsJsonArray());

        assertEquals("experience reward offer_code_type is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("offer_code_type"),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("offer_code_type"));
        assertEquals("experience reward offer_codes size is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("offer_codes").getAsJsonArray().size(),
                adJson.get("experience").getAsJsonObject().get("reward").getAsJsonObject().get("offer_codes").getAsJsonArray().size());
        assertEquals("experience triggers activation_trigger_groups size is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_trigger_groups").getAsJsonArray().size(),
                adJson.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_trigger_groups").getAsJsonArray().size());
        assertEquals("experience triggers activation_trigger_groups size is different from experience triggers activation_triggers size", adJson.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_triggers").getAsJsonArray().size(),
                adJson.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_trigger_groups").getAsJsonArray().size());
        assertEquals("experience triggers activation_triggers size is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_triggers").getAsJsonArray().size(),
                adJson.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("activation_triggers").getAsJsonArray().size());
        assertEquals("experience triggers configuration is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("configuration"),
                adJson.get("experience").getAsJsonObject().get("triggers").getAsJsonObject().get("configuration"));
        assertEquals("experience type is different from ad file", adJsonFromFile.get("experience").getAsJsonObject().get("type"),
                adJson.get("experience").getAsJsonObject().get("type"));

        assertFalse("external_id is missing", adJson.get("external_id").isJsonNull());
        assertFalse("forecasting is missing", adJson.get("forecasting").isJsonNull());
        assertFalse("id is missing", adJson.get("id").isJsonNull());
        assertEquals("measurement is different of ad file", adJsonFromFile.get("measurement"), adJson.get("measurement"));
        assertEquals("metadata.value is different from ad file", adJsonFromFile.get("metadata").getAsJsonObject().get("value"),
                adJson.get("metadata").getAsJsonObject().get("value"));
        assertEquals("name is different of ad file", adJsonFromFile.get("name"), adJson.get("name"));

        //check products
        JsonObject adProductInventory = adJson.get("products").getAsJsonObject().get("inventory").getAsJsonObject();
        JsonObject adFileProductInventory = adJsonFromFile.get("products").getAsJsonObject().get("inventory").getAsJsonObject();

        assertEquals("products inventory id is different from ad file", adFileProductInventory.get("id"),
                adProductInventory.get("id"));
        assertEquals("products inventory lists size should be 1", 1, adProductInventory.get("lists"));
        assertEquals("products inventory rolling_days is different from ad file", adFileProductInventory.get("rolling_days"),
                adProductInventory.get("rolling_days"));

        JsonObject adProductInventoryListItem = adProductInventory.get("lists").getAsJsonArray().get(0).getAsJsonObject();
        JsonObject adFileProductInventoryListItem = adFileProductInventory.get("lists").getAsJsonArray().get(0).getAsJsonObject();

        assertEquals("products inventory lists _ is different from ad file", adFileProductInventoryListItem.get("_"),
                adProductInventoryListItem.get("_"));
        assertEquals("products inventory lists catalog is different from ad file", adFileProductInventoryListItem.get("catalog"),
                adProductInventoryListItem.get("catalog"));
        assertEquals("products inventory lists count is different from ad file", adFileProductInventoryListItem.get("count"),
                adProductInventoryListItem.get("count"));
        assertEquals("products inventory lists external_id is different from ad file", adFileProductInventoryListItem.get("external_id"),
                adProductInventoryListItem.get("external_id"));
        assertEquals("products inventory lists groups size is different from ad file", adFileProductInventoryListItem.get("groups").getAsJsonArray().size(),
                adProductInventoryListItem.get("groups").getAsJsonArray().size());
        assertEquals("products inventory lists name is different from ad file", adFileProductInventoryListItem.get("name"),
                adProductInventoryListItem.get("name"));
        assertEquals("products legal restrictions nbic_use is different from ad file", adJsonFromFile.get("products").getAsJsonObject().get("legal_restrictions").getAsJsonObject().get("nbic_use"),
                adJson.get("products").getAsJsonObject().get("legal_restrictions").getAsJsonObject().get("nbic_use"));

        assertEquals("start_date is different of ad file", adJsonFromFile.get("start_date"), adJson.get("start_date"));
        assertFalse("status is missing", adJson.get("status").isJsonNull());
        assertEquals("stop_date is different of ad file", adJsonFromFile.get("stop_date"), adJson.get("stop_date"));
        assertEquals("system_id is different of ad file", adJsonFromFile.get("system_id"), adJson.get("system_id"));
        assertEquals("targeting is different of ad file", adJsonFromFile.get("targeting"), adJson.get("targeting"));
        assertEquals("templates is different of ad file", adJsonFromFile.get("templates"), adJson.get("templates"));
    }

    @Ignore
    @Test
    public void should_update_ad() throws HttpResponseStatusException {
        //Given
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        AdParams adParams = new AdParams();
        adParams.id = "BL_8583_7174";
        adParams.startDate = LocalDate.of(2018, 4, 20);
        adParams.stopDate = LocalDate.of(2018, 5, 20);
        //TODO REFACTOR
        //  Result resultSearchAd = getSearchAdResult();

        //When
        // when(adService.searchAdFromBL(mode,country,adParams.id, HttpStatus.OK)).thenReturn(resultSearchAd);
        when(offerService.postOffer(any(), any(), any())).thenReturn(String.valueOf(""));

        //Then
        //TODO REFACTOR
        // Result result = cpgPublisher.updateAd(adParams.id, adParams);
        //   assertEquals(0, result.getStatus());
    }

    @Ignore
    @Test
    public void should_return_error_status_on_update_ad_when_ad_not_found() throws HttpResponseStatusException {
        //Given
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        AdParams adParams = new AdParams();
        adParams.id = "BL_8583_7174";

        //When
        //TODO REFACTOR
        //when(adService.searchAdFromBL(mode,country,adParams.id, HttpStatus.OK)).thenReturn(new Result(10, "no ad found"));

        //Then
        //TODO REFACTOR
//        Result result = cpgPublisher.updateAd(adParams.id, adParams);
//        assertEquals(true, result.getStatus() != 0);
    }

    @Ignore
    @Test
    public void should_duplicate_ad() throws HttpResponseStatusException {
        //Given
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        String externalId = "BL_1000_1000";
        LocalDate startDate = LocalDate.of(2018, 4, 20);
        LocalDate stopDate = LocalDate.of(2018, 5, 20);
        Integer capping = null;

        //When
        //TODO REFACTOR
        when(adService.searchAdFromBL(mode, country, externalId, HttpStatus.OK)).thenReturn(getSearchAdResult());
        when(offerService.postPromotion(any(), any())).thenReturn("");
        when(cpgPublisher.getOfferCodeServiceInstance(cRestTemplate)).thenReturn(offerCodeService);
        when(offerCodeService.leaseOfferCodesForPromotion(any(), any())).thenReturn(getOfferCodesManuf("BL_1001_1001"));
        when(offerService.postOffer(any(), any(), any())).thenReturn("BL_1001_1001");

        //Then
        Result resultDuplicateAd = cpgPublisher.duplicateAd(externalId, startDate, stopDate, capping);

        assertEquals(0, resultDuplicateAd.getStatus());
        assertEquals("BL_1001_1001", resultDuplicateAd.getData());
        assertEquals("BL_1000_1000", resultDuplicateAd.getMessage());
    }

    @Ignore
    @Test
    public void should_return_error_on_duplicate_ad_when_ad_not_found() throws HttpResponseStatusException {
        //Given
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        String externalId = "BL_1000_1000";
        LocalDate startDate = LocalDate.of(2018, 4, 20);
        LocalDate stopDate = LocalDate.of(2018, 5, 20);
        Integer capping = null;

        //When
        //TODO REFACTOR
//        when(adService.searchAdFromBL(mode,country,externalId, HttpStatus.OK)).thenReturn(new Result(10, "no ad found"));

        //Then
        Result resultDuplicateAd = cpgPublisher.duplicateAd(externalId, startDate, stopDate, capping);
        assertEquals(true, resultDuplicateAd.getStatus() != 0);
    }

    private List<Ad> getSearchAdResult() {

        Ad ad = new Ad();
        ad.setStart_date("2014-04-28");
        ad.setStop_date("2014-05-28");
        ad.setStatus("DRAFT");

        return Collections.singletonList(ad);
    }

    private Result getEditValuesResult() {
        JsonObject editValueJsonResult = new JsonObject();
        editValueJsonResult.addProperty("external_id", "BL_1000_1000");
        editValueJsonResult.addProperty("stop_date", "2018-04-20");
        editValueJsonResult.addProperty("start_date", "2018-04-15");
        editValueJsonResult.addProperty("date_modified", "2018-04-17");
        editValueJsonResult.addProperty("ad_creative_set_id", "creative_id");
        editValueJsonResult.addProperty("name", "name");
        editValueJsonResult.addProperty("status", "PUBLISHED");
        editValueJsonResult.add("adgroup", new JsonObject());

        JsonObject billing = new JsonObject();
        billing.addProperty("id", "id_billing");
        billing.addProperty("name", "name_billing");
        billing.addProperty("business_segment", "segment_billing");
        editValueJsonResult.add("billing", billing);

        editValueJsonResult.add("classification", new JsonObject());
        editValueJsonResult.add("country", new JsonObject());

        JsonObject campaign = new JsonObject();
        campaign.addProperty("id", "aaaa-bbbb-cccc-dddd");
        editValueJsonResult.add("campaign", campaign);

        JsonObject experience = new JsonObject();
        JsonObject reward = new JsonObject();
        reward.addProperty("id", "idReward");
        experience.add("reward", reward);
        editValueJsonResult.add("experience", experience);

        return new Result(editValueJsonResult);
    }

    private List<TouchPoint> getMockedTouchPoints() {
        TouchPoint firstTouchPoint = new SiteId("FRA", "1000", "ITM 1");
        TouchPoint secondTouchPoint = new SiteId("FRA", "2000", "ITM 2");

        return Arrays.asList(firstTouchPoint, secondTouchPoint);
    }

    private OCLU_LEASE_RESPONSE_ENTITY generateMockOcluLeaseResponseEntity(String uuid,
                                                                           String promotionId,
                                                                           String ocluValue,
                                                                           String networkId,
                                                                           String networkName,
                                                                           Country.CountryEnum country) {

        //REMOVE DIACRITICS + UPPERCASE
        String networkNameToUpperCase = Normalizer.normalize(networkName, Normalizer.Form.NFD);
        networkNameToUpperCase = networkNameToUpperCase.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        networkNameToUpperCase = networkNameToUpperCase.toUpperCase();

        List chainValues;
        if (networkName.equalsIgnoreCase("Carrefour Proxi")) {
            chainValues = Arrays.asList("CARREFOUR PROXI EXPRESS",
                    "CARREFOUR PROXI QUARTIER",
                    "CARREFOUR PROXI CONVENIENT",
                    "CARREFOUR PROXI MONTAGNE",
                    "CARREFOUR PROXI CONTACT",
                    "CARREFOUR PROXI CONTACT MARCHE");
        } else if (networkName.equalsIgnoreCase("Système U")) {
            chainValues = Arrays.asList("SYSTEME U SUD", "SYSTEME U NORD-OUEST", "SYSTEME U EST", "SYSTEME U OUEST");
        } else if (networkName.equalsIgnoreCase("Intermarche")) {
            chainValues = Arrays.asList("INTERMARCHE REGION NORD",
                    "INTERMARCHE REGION CENTRE EST",
                    "INTERMARCHE REGION CENTREOUEST",
                    "INTERMARCHE REGION SUD EST",
                    "INTERMARCHE REGION PARISIENNE",
                    "INTERMARCHE REGION OUEST",
                    "INTERMARCHE REGION EST",
                    "INTERMARCHE REGION SUD OUEST");
        } else {
            chainValues = Collections.singletonList(networkNameToUpperCase);
        }

        Chain chain = new Chain();

        Targeting targeting = new Targeting(Collections.singletonList(chain), new ArrayList<>());

        OCLU_NETWORK network = new OCLU_NETWORK(networkId, networkName, country.getCode());
        OCLU_MINI_RANGE oclu_mini_range = new OCLU_MINI_RANGE(uuid, network);

        OCLU_LEASE_RESPONSE_ENTITY offerCode = new OCLU_LEASE_RESPONSE_ENTITY("GENERATED",
                String.format("%s:%s", uuid.toUpperCase(), ocluValue),
                ocluValue,
                targeting,
                DateUtils.getCurrentDateMilliSecondPrecision(),
                DateUtils.getCurrentDateMilliSecondPrecision(),
                null,
                "DRAFT",
                promotionId,
                oclu_mini_range);

        return offerCode;
    }


    private List<OCLU_LEASE_RESPONSE_ENTITY> getOfferCodesManuf(String BL) {
        String promotionId = String.format("%s-BLIP-%s", Country.CountryEnum.FRANCE.getCode(), BL);
        OCLU_LEASE_RESPONSE_ENTITY firstOfferCode = generateMockOcluLeaseResponseEntity("aaaa-bbbb-cccc-dddd",
                promotionId,
                "1000",
                "20",
                "Supermarchés Match",
                Country.CountryEnum.FRANCE);

        OCLU_LEASE_RESPONSE_ENTITY secondOfferCode = generateMockOcluLeaseResponseEntity("bbbb-cccc-dddd-eeee",
                promotionId,
                "2000",
                "6",
                "Intermarche",
                Country.CountryEnum.FRANCE);

        return Arrays.asList(firstOfferCode, secondOfferCode);
    }

    @Ignore
    @Test
    public void testCancelAd() throws HttpResponseStatusException {
        // mocking without annotations
//        cRestTemplate = mock(CRestTemplate.class);
//        cRestTemplateSpy = spy(CRestTemplate.class);

        Publisher publisher = new Publisher(cRestTemplate);

        //______________________________________Do Throw Example____________________________________________________
//    	doThrow(HttpResponseStatusException.class).when(cRestTemplate).executeSolrRequestWithJsonHeader(anyString(),
//                any(), any(), any(HttpMethod.class), any(HttpStatus.class), anyString());
        //__________________________________________________________________________________________________________


        //_______________________________Do Answer Example ____________________________________
//    	doAnswer(invokation -> {
//    	    System.out.println("test");
//    	    return null;
//        }).when(cRestTemplate).executeSolrRequestWithJsonHeader(anyString(),
//            any(), any(), any(HttpMethod.class), any(HttpStatus.class), anyString());
        //______________________________________________________________________________________

        //________________________Do Return/Chaining Example________________________________________________
//    	doReturn(null)
//                .doReturn(null)
//                .doThrow(new HttpResponseStatusException("error msg", HttpStatus.NOT_FOUND))
//                .when(cRestTemplate).executeGetRequestWithJsonHeader(any(), any(), any());
//    	Result res = publisher.cancelAd(null);
//        assertNotEquals( 0, res.getStatus() );
        //________________________________________________________________________________________________


        //_______________________Verify + Argument Captor Example_________________________________________
        Result res = publisher.cancelAd(null);
        ArgumentCaptor<String> urlCapture = ArgumentCaptor.forClass(String.class);
        verify(cRestTemplate, times(1)).executeSolrRequestWithJsonHeader(urlCapture.capture(),
                any(), any(), any(HttpMethod.class), any(HttpStatus.class), anyString());
        List<String> allCapturedValues = urlCapture.getAllValues();
        assertEquals("https://sqa.dashboard.personalization.catalinamarketing.com/ads?fq={fq}", allCapturedValues.get(0));
//        assertEquals("value2", allCapturedValues.get(1));
        //_________________________________________________________________________________________________
    }

    //AD Hoc campaign test


    @Test
    public void test_campaignExists() throws HttpResponseStatusException {

        //Given
        String fqParam = "{\"id\":\"e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b\",\"name\":\"Trafic semaine : Système U - Local 2019\"}";

        //When
        when(offerService.getOffersFromDashBoard(any(), any(), any(), any()))
                .thenReturn(Collections.singletonList(new Campaign("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                        "Trafic semaine : Système U - Local 2019")));

        assertTrue(cpgPublisher.doesOfferExists(OfferEnum.CAMPAIGN, fqParam));
    }

    @Test
    public void test_postCampaign() throws HttpResponseStatusException {
        when(offerService.postOffer(any(), any(), any())).thenReturn("38899b5a-413e-4656-980e-41edeb927d5e");

        Campaign campaign = new Campaign("38899b5a-413e-4656-980e-41edeb927d5e", "[Test] Campaign Ad Hoc");
        String campaignId = cpgPublisher.postOffer(OfferEnum.CAMPAIGN, campaign);

        assertEquals(campaignId, "38899b5a-413e-4656-980e-41edeb927d5e");
    }

    @Ignore
    @Test
    public void test_adGroup_Exists() throws HttpResponseStatusException {
        //Given
        String fqParam =
                "{campaign:{\"id\":\"1111111-2222222-33333-4444-5555555\",\"name\":\"[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19\"},\"name\":\"Ad Group 1\"}";

        //When
        when(offerService.getOffersFromDashBoard(any(), any(), any(), any()))
                .thenReturn(Collections.singletonList(new Adgroup("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                        "Ad Group 1")));

        //Then
        assertTrue(cpgPublisher.doesOfferExists(OfferEnum.ADGROUP, fqParam));
    }

    @Test
    public void test_postAdGroup() throws HttpResponseStatusException {
        //Given
        Adgroup adGroup = new Adgroup("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b", "New Ad Group");

        when(offerService.postOffer(any(), any(), any())).thenReturn("2725b250-8c4e-4035-aef5-f5dbc7bb272f");

        String adgroupId = cpgPublisher.postOffer(OfferEnum.ADGROUP, adGroup);
        assertEquals("2725b250-8c4e-4035-aef5-f5dbc7bb272f", adgroupId);
    }

    @Test
    public void test_targetingMethod_Ecobon() throws HttpResponseStatusException {
        //Given
        CRestTemplate cRestTemplate = new CRestTemplate();
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        TargetingMethodService methodService = new TargetingMethodService(cRestTemplate);
        String name = "Vault ECOBON";

        TargetingMethod referenceMethod = TargetingMethod.TargetingMethodEnum.ECOBON.targetingMethod;
        TargetingMethod APIMethod = methodService.getTargetingMethod(mode, country, name);

        assertTrue(referenceMethod.hashCode() == APIMethod.hashCode());
        assertTrue(referenceMethod.equals(APIMethod));
    }

    @Test
    public void test_targetingMethod_Ecolink() throws HttpResponseStatusException {
        //Given
        CRestTemplate cRestTemplate = new CRestTemplate();
        Config.Mode mode = Config.Mode.PROD;
        Country.CountryEnum country = Country.CountryEnum.FRANCE;
        TargetingMethodService methodService = new TargetingMethodService(cRestTemplate);
        String name = "Vault ECOLINK";

        TargetingMethod referenceMethod = TargetingMethod.TargetingMethodEnum.ECOLINK.targetingMethod;
        TargetingMethod APIMethod = methodService.getTargetingMethod(mode, country, name);

        assertTrue(referenceMethod.hashCode() == APIMethod.hashCode());
        assertTrue(referenceMethod.equals(APIMethod));
    }

    @Test
    public void test_classification_Coupon() throws HttpResponseStatusException {
        ClassificationService classificationService = new ClassificationService(new CRestTemplate());
        Classification classificationReference = classificationService.createGenericCouponAdClassification();

        Classification classification = classificationService.getClassification(cpgPublisher.m_mode,
                Classification.ClassificationName.GENERIC_COUPON_AD);

        assertTrue(classificationReference.hashCode() == (classification.hashCode()));
        assertTrue(classificationReference.equals(classification));
    }

    @Test
    public void test_classification_SAVE_X_PERCENT() throws HttpResponseStatusException {
        ClassificationService classificationService = new ClassificationService(new CRestTemplate());
        Classification classificationReference = classificationService.createSaveXPercentClassification();

        Classification classification = classificationService.getClassification(cpgPublisher.m_mode,
                Classification.ClassificationName.BUY_N_SAVE_X_PERCENT);

        assertTrue(classificationReference.hashCode() == (classification.hashCode()));
        assertTrue(classificationReference.equals(classification));
    }

    @Test
    public void test_classification_SPEND_X_SAVE_Y() throws HttpResponseStatusException {
        ClassificationService classificationService = new ClassificationService(new CRestTemplate());
        Classification classificationReference = classificationService.createSpendXSaveYClassification();

        Classification classification = classificationService.getClassification(cpgPublisher.m_mode,
                Classification.ClassificationName.SPEND_X_SAVE_Y);

        assertTrue(classificationReference.hashCode() == (classification.hashCode()));
        assertTrue(classificationReference.equals(classification));
    }

    @Test
    public void test_recent_promotion_parsing() throws HttpResponseStatusException {
        PromotionService promotionService = new PromotionService(new CRestTemplate());
        promotionService.getLastPublishedNManuallyMadePromotion(Config.Mode.PROD, Country.CountryEnum.FRANCE, 3);
    }

    @Test
    public void test_recent_ad_parsing() throws HttpResponseStatusException {
        CRestTemplate cRestTemplate = new CRestTemplate();
        PromotionService promotionService = new PromotionService(cRestTemplate);
        OfferService offerService = new OfferService(cRestTemplate);

        Promotion promotion = promotionService.getLastPublishedNManuallyMadePromotion(Config.Mode.PROD, Country.CountryEnum.FRANCE, 3);

        String adExternalId = promotion.getExternalId();
        SolrParam param = new SolrParam("fq", String.format("{\"external_id\":\"%s\"}", adExternalId));

        offerService.getOffersFromDashBoard(Config.Mode.PROD, Ad.class, Collections.singletonList(param));
    }


    /**
     * Fake Cancel
     */
  /*  public void testCreateAdWithoutCampaign()
    {
    	Publisher publisher = new Publisher();
    	Result res = publisher.createAd("", "","", "", "", -1, -1,null);
        assertTrue( res.getStatus() == 2 );
    }

   */
    public String getJsonFileContent(String filename) {

        String jsonNetworksFilePath = "/json/" + filename;

        String jsonContent = "";
        try {
            InputStream is = getClass().getResourceAsStream(jsonNetworksFilePath);
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, Charset.forName("UTF-8"));
            jsonContent = writer.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonContent;
    }

    public List<Billing> getBillings() {
        LinkedHashMap map = new LinkedHashMap<>();
        map.put("bus_seg_descr", "MANUFACTURING");
        map.put("bus_seg_nbr", 1);
        BillingCampaign billing = new BillingCampaign(getCountry(),
                62421,
                "CATS",
                "FRA-CATS-62421_1",
                "Nestlé Waters Hépar C4 2019  : In store - Nestlé Waters Hépar C4 2019",
                "OPEN",
                "FRA-CATS-62421_1 - Nestlé Waters Hépar C4 2019  : In store - Nestlé Waters Hépar C4 2019",
                "FRA-CATS-38010",
                "FRA-CATS-374",
                "MANUFACTURING");
        return Arrays.asList(billing);
    }

    public List<Client> getClients() {
        Client client = new Client(getCountry(),
                374,
                "CATS",
                "FRA-CATS-374",
                "Nestle - Waters",
                "ACTIVE",
                "FRA-CATS-374 - Nestle - Waters");
        return Arrays.asList(client);
    }

    public Country getCountry() {
        Country country = new Country();
        country.setCountry_code("FRA");
        country.setCountry_name("France");
        return country;
    }

    public List<JsonObject> getTemplatesColorContentAsJsonObject() {
        String templateStr = getJsonFileContent("TemplatesColor.json");
        JsonElement jsonElement = Config.parser.parse(templateStr);
        JsonObject templateJsonObject = jsonElement.getAsJsonObject();
        JsonObject response = templateJsonObject.get("response").getAsJsonObject();
        JsonArray docs = response.get("docs").getAsJsonArray();

        List<JsonObject> docList = StreamSupport.stream(docs.spliterator(), false)
                .map(JsonElement::getAsJsonObject)
                .collect(Collectors.toList());

        return docList;
    }

    public List<JsonObject> getTemplatesBWContentAsJsonObject() {
        String templateStr = getJsonFileContent("TemplatesBW.json");
        JsonElement jsonElement = Config.parser.parse(templateStr);
        JsonObject templateJsonObject = jsonElement.getAsJsonObject();
        JsonObject response = templateJsonObject.get("response").getAsJsonObject();
        JsonArray docs = response.get("docs").getAsJsonArray();

        List<JsonObject> docList = StreamSupport.stream(docs.spliterator(), false)
                .map(JsonElement::getAsJsonObject)
                .collect(Collectors.toList());

        return docList;
    }

    public LMCItem[] getLmcItemsFromFile(String filename) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        String templateStr = getJsonFileContent(filename);
        LMCItem[] items = gson.fromJson(templateStr, LMCItem[].class);

        return items;

    }

    private void checkExperienceLegal(JsonArray adExperienceLegal, JsonArray adFileExperienceLegal) {
        JsonObject adLegalColorItem = null, adLegalBlackWhiteItem = null, adFileLegalColorItem = null, adFileLegalBlackWhiteItem = null;

        for (JsonElement legalItem : adExperienceLegal) {
            String color = legalItem.getAsJsonObject().get("printer_levels").getAsJsonArray().get(0).getAsString();

            if (color.equals("COLOR")) adLegalColorItem = legalItem.getAsJsonObject();
            else if (color.equals("BACK/WHITE")) adLegalBlackWhiteItem = legalItem.getAsJsonObject();
        }

        for (JsonElement legalItem : adFileExperienceLegal) {
            String color = legalItem.getAsJsonObject().get("printer_levels").getAsJsonArray().get(0).getAsString();

            if (color.equals("COLOR")) adFileLegalColorItem = legalItem.getAsJsonObject();
            else if (color.equals("BACK/WHITE")) adFileLegalBlackWhiteItem = legalItem.getAsJsonObject();
        }

        //assertEquals("experience.legal.checked_bw for Color is different from file", adFileLegalColorItem.get("checked_bw"), adLegalColorItem.get("checked_bw"));
        //assertEquals("experience.legal.checked_color for Color is different from file", adFileLegalColorItem.get("checked_color"), adLegalColorItem.get("checked_color"));
        assertEquals("experience.legal.printers_level for Color is different from file", adFileLegalColorItem.get("printers_level"), adLegalColorItem.get("printers_level"));
        //assertEquals("experience.legal.checked_bw for Black/White is different from file", adFileLegalBlackWhiteItem.get("checked_bw"), adLegalBlackWhiteItem.get("checked_color"));
        //assertEquals("experience.legal.checked_color for Black/White is different from file", adFileLegalBlackWhiteItem.get("checked_color"), adLegalBlackWhiteItem.get("checked_color"));
        assertEquals("experience.legal.printers_level for Black/White is different from file", adFileLegalBlackWhiteItem.get("printers_level"), adLegalBlackWhiteItem.get("printers_level"));

    }

}
