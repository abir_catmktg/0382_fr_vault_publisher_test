package com.catalina.vault_publisher;

import com.catalina.vault_publisher.data.Country;
import com.catalina.vault_publisher.data.billing.BillingCampaign;
import com.catalina.vault_publisher.data.billing.BillingService;
import com.catalina.vault_publisher.data.billing.Client;
import com.catalina.vault_publisher.data.billing.ClientService;
import com.catalina.vault_publisher.data.offers.Campaign;
import com.catalina.vault_publisher.data.offers.CampaignService;
import com.catalina.vault_publisher.data.touchpoint.TouchPoint;
import com.catalina.vault_publisher.data.touchpoint.TouchpointsService;
import com.catalina.vault_publisher.utils.Constants;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class CampaignServiceTest {

    @InjectMocks
    @Spy
    CampaignService campaignService;

    @Mock
    BillingService billingService;

    @Mock
    ClientService clientService;

    @Mock
    TouchpointsService tpService;


    @Test
    public void test1_setupCampaignAdHoc() {
        //Given
        Country country = new Country();
        country.setCountry_code("FRA");
        country.setCountry_name("France");
        BillingCampaign bd = new BillingCampaign(country, 52201, "CATS", "FRA-CATS-52201_104", "Trafic semaine : Système U - Local 2019", "OPEN", "FRA-CATS-52201_104 - Trafic semaine : Système U - Local 2019", "FRA-CATS-14", "FRA-CATS-459", "RETAIL");
        Client client = new Client(country, 459, "CATS", "FRA-CATS-459", "Systeme U #25622", "ACTIVE", "FRA-CATS-459 - Systeme U #25622");
        Campaign campaignTest = new Campaign("[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19", bd, client, null, true, Country.FRA_COUNTRY_CODE, new ArrayList<TouchPoint>(), new ArrayList<TouchPoint>(), "DISPLAY", Constants.VAULT_PUBLISHER);

        //When
        when(campaignService.getNewTouchpointsService(any())).thenReturn(tpService);
        when(campaignService.getNewBillingService(any())).thenReturn(billingService);
        when(campaignService.getNewClientService(any())).thenReturn(clientService);
        //TODO : Update Test with new Billing et Client
//        when(billingService.getBillingRetail(any(),any(),any(),any())).thenReturn(new Result(bd));
//        when(clientService.getClient(any(),any())).thenReturn(new Result(client));
        //when(tpService.setupIncludedTouchpointsRetailSysU(any())).thenReturn(new ArrayList<>());

        //Then
//        Campaign campaign = campaignService.setupCampaignAdHoc(Config.Mode.PROD,"[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19", "52201", "104");
//        assertEquals(campaign,campaignTest);
    }

    @Test
    public void test2_setupCampaignAdHoc() {
        //Given
        Campaign campaignTest = new Campaign(null, null, null, null, true, Country.FRA_COUNTRY_CODE, new ArrayList<TouchPoint>(), new ArrayList<TouchPoint>(), "DISPLAY", Constants.VAULT_PUBLISHER);

        //When
        when(campaignService.getNewTouchpointsService(any())).thenReturn(tpService);
        // when(tpService.setupIncludedTouchpointsRetailSysU(any())).thenReturn(new ArrayList<>());

        //Then
//        Campaign campaign = campaignService.setupCampaignAdHoc(Config.Mode.SQA,null, null, null);

        verify(campaignService, never()).getNewClientService(any());
        verify(campaignService, never()).getNewBillingService(any());
//        assertEquals(campaign, campaignTest);
    }


}
