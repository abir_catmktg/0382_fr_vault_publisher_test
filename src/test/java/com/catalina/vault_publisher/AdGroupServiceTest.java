package com.catalina.vault_publisher;


import com.catalina.vault_publisher.data.AdGroupLimit;
import com.catalina.vault_publisher.data.AdGroupRules;
import com.catalina.vault_publisher.data.budget.Budget;
import com.catalina.vault_publisher.data.offers.AbstractOffer;
import com.catalina.vault_publisher.data.offers.AdGroupService;
import com.catalina.vault_publisher.data.offers.Adgroup;
import com.catalina.vault_publisher.utils.Constants;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@Ignore
@RunWith(MockitoJUnitRunner.class)
public class AdGroupServiceTest {

    @InjectMocks
    @Spy
    AdGroupService adGroupService;


    @Test
    public void test_setupAdGroupAdHoc() {
        //Given
        Adgroup adGroupTest = new Adgroup("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                "[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19",
                "Ad Group 1",
                AbstractOffer.AD_ROTATION_DEFAULT,
                "ACCELERATED",
                null,
                AbstractOffer.ROADBLOCKING_METHOD_SINGLE,
                Constants.COLIBRI);
        adGroupTest.setRules(new AdGroupRules(new AdGroupLimit()));
        adGroupTest.setBudget(new Budget(false));

        //When

        //Then
        Adgroup adGroupResult = adGroupService.createMinimalAdGroup("e2133cd3-130d-42dd-8e23-fc5ffd9ddd8b",
                "[TEST] Colibri Book Système-U Trafic Semaine - FPE 07-11-19",
                "Ad Group 1",
                Adgroup.DeliveryMethod.ACCELERATED,
                Adgroup.AdRotation.DEFAULT,
                Adgroup.RoadBloacking.SINGLE,
                Constants.VAULT_PUBLISHER);
        assertEquals(adGroupResult, adGroupTest);
    }


}
